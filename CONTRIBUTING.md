# How to contribute to iota2 ?

Thank you for taking the time to contribute to itoa2! This document will guide you
through the workflow and best practices you need to know to send your
contribution.

There are many ways to contribute to iota2:

* [Reporting a bug](#reporting-bugs)
* [Making a feature request](#feature-requests-and-discussions)
* [Improving documentation](#documentation-improvements)
* [Contributing code (Python, conda, etc.)](#code-contribution)

Our main workflow uses a Gitlab instance (Framagit, hosted by Framasoft) for source control, issues and task tracking :

[`https://framagit.org/iota2-project/iota2`](https://framagit.org/iota2-project/iota2)

Remember to subscibe to email notifications since issues is where we discuss some features, improvements and high level project planning.
You are welcome to ask questions there as a beginner or future iota2 contributor!

## Reporting bugs

If you have found a bug, you can first [search the existing issues](https://framagit.org/iota2-project/iota2/-/issues?label_name%5B%5D=Bug)
to see if it has already been reported.

If it's a new bug, please [open a new issue on framagit](https://framagit.org/iota2-project/iota2/-/issues/new?issue%5Bmilestone_id%5D=).
Try to be as precise as possible, about version, errors and the issue encountered.
Remember to add as much information as possible!
iota2 provides logs and a self generated archive which can be added to the issue.

## Feature requests and discussions

Feature requests are welcome! Generally you are welcome to simply [open an issue](https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb/issues)
and discuss your idea there. 

## Documentation improvements

The main iota2 documentation is
the [online documentation](https://docs.iota2.net/).  The source is
hosted in the main iota2 repository in the `doc/source` directory. Therefore, to
contribute documentation use the same workflow as for code contributions (see
below).

See also the readme in `iota2/doc/` directory for help on building the Sphinx source.

## Code contribution

The iota2 workflow is based on [Merge Requests](https://framagit.org/iota2-project/iota2/-/merge_requests).
Clone the repository, create a feature branch, commit your changes, push the
feature branch to a fork (or the main repository if you prefer),
then send a merge request.

Feature branches will be tested on the iota2 tests (unit test and integration tests).
Tests must be launched before removing the 'draft' label to the MR. To launch the tests, ensure you have an internet connection then:

	cd your_iota2_clone/iota2/Tests
    python launch_tests.py -tests_suite all

### Commit message

On your feature branch, write a good [commit message](https://xkcd.com/1296/):
short and descriptive. If fixing an issue or bug, put the issue number in the
commit message so that Gitlab can [cross-link it](https://docs.gitlab.com/ce/user/project/issues/crosslinking_issues.html).
You can prefix your commit message with an indicating flag (DOC, BUG, TEST, etc.).

Standard prefixes for iota2 commit messages:

    BUG: Fix for runtime crash or incorrect result
	BUG Fix: a bug mentioned in issue was solved by this commit
    DOC: Documentation change
    ENH: Enhancement of a function
	NEW FEATURE: New functionality
    PERF: Performance improvement
    STYLE: No logic impact (indentation, comments)
    TEST: Tests change
	WIP: Work In Progress not ready for merge

For example, here are some good commit messages:

    BUG FIX: #1701 Warn users if parameter string is unset
    DOC: Fix typo in home page
    BUG: variable not initialized if file format is different than sqlite

### Merge request

Your contribution is ready to be added to the main iota2 repository? Send a Merge
Request against the `develop` branch on Framagit using the merge request
template. The merge request will then be discussed by the community and the core
iota2 team.

* Merge requests can not be merged until all discussions have been resolved (this is enforced by Gitlab)
* The merger is responsible for checking that the branch is up-to-date with develop
* Merge requests are merged only by the Merge Master (MM) or by any person who can push on `develop` and who has received the explicit agreement from the MM for the relevant MR.


### Contribution license agreement

iota2 requires that contributors sign out a [Contributor License
Agreement](https://en.wikipedia.org/wiki/Contributor_License_Agreement). The
purpose of this CLA is to ensure that the project has the necessary ownership or
grants of rights over all contributions to allow them to distribute under the
chosen license (Affero General Public License 3.0).

To accept your contribution, we need you to complete, sign and email to *iota2 [at]
 cesbio [dot] cnes [dot] fr* an [Individual Contributor Licensing
Agreement](https://framagit.org/iota2-project/iota2/-/blob/develop/assets/icla-en.odt) (ICLA) form and a
[Corporate Contributor Licensing
Agreement](https://framagit.org/iota2-project/iota2/-/blob/develop/assets/ccla-en.odt) (CCLA) form if you are
contributing on behalf of your company or another entity which retains copyright
for your contribution.


## Gitlab guidelines

In order to organize the issues in our gitlab instance, we use labels:


Regarding labels, we use the following set:

* ~ANITI: for all request related to the ANITI project
* ~Bug: for an unexpected behabior or crash during runtime
* ~CodeStyle: for major change looking at pep
* ~Documentation: for all lacks or errors in documentation
* ~Doing: the issue is currently in work
* ~FAQ: a recurrent question which should be added to the FAQ
* ~Install: an issue concerning installation or compilation
* ~Investigate: the issue need researchs or thinking to be solved, or for new package to be added
* ~Maestria: for all request related to the MAESTRIA project
* ~PARCELLE: for all request related to the PARCELLE project
* ~Question: a question about a feature, a behavior, asking for clarification
* ~Refactoring: code which should be rewrite
* ~"To Do": almost all issue, the work should be done
* ~feature: request to a new feature to be added to iota2
* ~incident: issue during chain processing (segfault,...)
* ~infrastructure: issue about system, internal lib used,...
* ~perfopt: issue to optimise performances
