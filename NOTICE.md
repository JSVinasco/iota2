# iota2 conda package

Project sources: https://framagit.org/iota2-project/iota2

The conda iota2 package compiles several dependencies from source. In this file you will find the list of the different projects involved.

iota2 is free software under the [GNU Affero General Public License v3.0](https://framagit.org/iota2-project/iota2/-/blob/develop/LICENSE)

## OTB

Project sources: https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb

OTB is under the license [Apache License 2.0](https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb/-/blob/release-8.0/LICENSE)

OTB is compiled on the release-8.0 branch, consult the corresponding [NOTICE](https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb/-/blob/release-8.0/NOTICE) file for more details.

The following remote modules are also compiled:

* [iota2 features extraction](https://framagit.org/iota2-project/iota2.git)
* [Temporal GapFilling](https://gitlab.orfeo-toolbox.org/jinglada/temporalgapfilling.git)
* [Point Match Coregistration Model](https://framagit.org/iota2-project/otbPointMatchCoregistrationModel.git)
* [Multitemp Filtering](https://framagit.org/iota2-project/otb-for-biomass.git)
* [SLIC](https://framagit.org/iota2-project/slic.git)
* [Auto-Context](https://framagit.org/iota2-project/auto-context.git)

OTB requires some dependencies to be compiled and conda packages are produced for them:
* libitk
* shark
* muparserx
* tinyxml

## Grass

Project sources: https://github.com/OSGeo/grass

Grass is under the license [GNU GPL license](https://grass.osgeo.org/about/license/)

Grass is compiled from source on branch [7.8](https://github.com/OSGeo/grass/tree/releasebranch_7_8) without modifications 


## config

Project sources: https://github.com/vsajip/py-cfg-lib

config is under [BSD 3-Clause License](https://github.com/vsajip/py-cfg-lib/blob/master/LICENSE)

config is compiled on version from pypi 0.4.2  with a patch to be compatible with python 3
