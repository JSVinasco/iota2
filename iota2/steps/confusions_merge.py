#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Step for merging all confusion matrix found in a folder."""
import logging
import os

from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.validation import confusion_fusion
from iota2.vector_tools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class ConfusionsMerge(iota2_step.Step):
    """Merge confusion matrix of each tile to one."""

    resources_block_name = "confusionMatrixFusion"

    def __init__(self, cfg, cfg_resources_file, working_directory=None):
        """Initialize the step."""
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        self.working_directory = working_directory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.data_field = self.i2_const.re_encoding_label_name
        self.ground_truth = os.path.join(self.output_path,
                                         self.i2_const.re_encoding_label_file)
        self.runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')

        ground_truth = rcf.read_config_file(self.cfg).getParam(
            'chain', 'ground_truth')
        user_data_field = rcf.read_config_file(self.cfg).getParam(
            'chain', 'data_field')
        user_labels_to_i2_labels = get_re_encoding_labels_dic(
            ground_truth, user_data_field)

        user_annual_labels = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'annual_crop')
        annual_crop = [
            user_labels_to_i2_labels[int(user_annual_label)]
            for user_annual_label in user_annual_labels
            if int(user_annual_label) in user_labels_to_i2_labels
        ]

        all_castable = []
        for user_label, _ in user_labels_to_i2_labels.items():
            try:
                _ = int(user_label)
                all_castable.append(True)
            except ValueError:
                all_castable.append(False)
        re_encode_labels = all(all_castable)
        if re_encode_labels:
            user_labels_to_i2_labels = None
            self.ground_truth = rcf.read_config_file(self.cfg).getParam(
                'chain', 'ground_truth')
            self.data_field = rcf.read_config_file(self.cfg).getParam(
                'chain', 'data_field')
        self.comparison_mode = rcf.read_config_file(self.cfg).getParam(
            "arg_classification", "boundary_comparison_mode")
        if self.comparison_mode:
            path_in_out = [
                os.path.join(self.output_path, "final", "standard", "TMP"),
                os.path.join(self.output_path, "final", "boundary", "TMP")
            ]
        else:
            path_in_out = [os.path.join(self.output_path, "final", "TMP")]
        task = self.i2_task(task_name="merge_confusions",
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters={
                                "f":
                                confusion_fusion.confusion_fusion_folder,
                                "input_vector":
                                self.ground_truth,
                                "data_field":
                                self.data_field,
                                "csv_out_list":
                                path_in_out,
                                "txt_out_list":
                                path_in_out,
                                "csv_path_list":
                                path_in_out,
                                "runs":
                                self.runs,
                                "crop_mix":
                                rcf.read_config_file(self.cfg).getParam(
                                    'arg_train', 'crop_mix'),
                                "annual_crop":
                                annual_crop,
                                "annual_crop_label_replacement":
                                (rcf.read_config_file(self.cfg).getParam(
                                    'arg_train',
                                    'a_crop_label_replacement'))[0],
                            },
                            task_resources=self.get_resources())
        self.add_task_to_i2_processing_graph(
            task,
            task_group="confusion_merge",
            task_sub_group="confusion_merge",
            task_dep_dico={
                "tile_tasks_seed": [
                    f"{tile}_{seed}" for seed in range(self.runs)
                    for tile in self.tiles
                ]
            })

    @classmethod
    def step_description(cls):
        """Print a short description of the step's purpose."""
        description = ("Merge all confusions")
        return description
