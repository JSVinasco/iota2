#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
from typing import Optional

from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.sampling.super_pixels_selection import merge_ref_super_pix
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class SuperPixPos(iota2_step.Step):
    resources_block_name = "superPixPos"

    def __init__(
        self, cfg: str, cfg_resources_file: str, workingDirectory: Optional[str] = None
    ) -> None:
        """set up the step"""
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.working_directory = workingDirectory
        self.execution_dir = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )
        self.superPix_field = "superpix"
        self.superPix_belong_field = "is_super_pix"
        self.region_field = rcf.read_config_file(self.cfg).getParam(
            "chain", "region_field"
        )
        self.data_field = self.i2_const.re_encoding_label_name
        self.ram = 1024.0 * get_ram(self.get_resources()["ram"])
        self.nb_runs = rcf.read_config_file(self.cfg).getParam("arg_train", "runs")
        self.sampling_labels_from_raster = {}
        if rcf.read_config_file(self.cfg).getParam(
            "arg_train", "crop_mix"
        ) and rcf.read_config_file(self.cfg).getParam(
            "arg_train", "samples_classif_mix"
        ):
            source_dir = rcf.read_config_file(self.cfg).getParam(
                "arg_train", "annual_classes_extraction_source"
            )

            annual_labels = rcf.read_config_file(self.cfg).getParam(
                "arg_train", "annual_crop"
            )
            classification_raster = os.path.join(
                source_dir, "final", "Classif_Seed_0.tif"
            )
            validity_raster = os.path.join(source_dir, "final", "PixelsValidity.tif")
            val_thresh = rcf.read_config_file(self.cfg).getParam(
                "arg_train", "validity_threshold"
            )
            region_mask_dir = os.path.join(self.execution_dir, "shapeRegion")
            self.sampling_labels_from_raster = {
                "annual_labels": annual_labels,
                "classification_raster": classification_raster,
                "validity_raster": validity_raster,
                "region_mask_directory": region_mask_dir,
                "val_threshold": val_thresh,
            }
        for model_name, model_meta in self.spatial_models_distribution.items():
            for seed in range(self.nb_runs):
                for tile in model_meta["tiles"]:
                    target_model = f"model_{model_name}_seed_{seed}"
                    task = self.i2_task(
                        task_name=f"SP_{target_model}_{tile}",
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters={
                            "f": merge_ref_super_pix,
                            "data": {
                                "SLIC": os.path.join(
                                    self.execution_dir,
                                    "features",
                                    tile,
                                    "tmp",
                                    f"SLIC_{tile}.tif",
                                ),
                                "selection_samples": os.path.join(
                                    self.execution_dir,
                                    "samplesSelection",
                                    f"{tile}_samples_region_{model_name}_seed_{seed}_selection.sqlite",
                                ),
                            },
                            "dataref_field_name": self.data_field,
                            "sp_field_name": self.superPix_field,
                            "sp_belong_field_name": self.superPix_belong_field,
                            "region_field_name": self.region_field,
                            "sampling_labels_from_raster": self.sampling_labels_from_raster,
                            "working_directory": self.working_directory,
                            "ram": self.ram,
                        },
                        task_resources=self.get_resources(),
                    )
                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="tile_tasks_model",
                        task_sub_group=f"{tile}_{model_name}_{seed}",
                        task_dep_dico={"region_tasks": [target_model]},
                    )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Add superPixels positions to reference data"
        return description
