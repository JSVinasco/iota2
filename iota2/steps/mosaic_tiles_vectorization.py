#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os

import iota2.common.i2_constants as i2_const
from iota2.configuration_files import read_config_file as rcf
from iota2.simplification import merge_tile_rasters as mtr
from iota2.steps import iota2_step
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.Iota2Constants()


class MosaicTilesForVectorization(iota2_step.Step):
    resources_block_name = "mosaictiles"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(MosaicTilesForVectorization, self).__init__(
            cfg, cfg_resources_file, self.resources_block_name
        )
        self.clipfile = rcf.read_config_file(self.cfg).getParam(
            "simplification", "clipfile"
        )
        self.clipfield = rcf.read_config_file(self.cfg).getParam(
            "simplification", "clipfield"
        )
        self.gridsize = rcf.read_config_file(self.cfg).getParam(
            "simplification", "gridsize"
        )
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )
        self.grid = os.path.join(self.output_path, "simplification", "grid.shp")
        self.tmp_dir = os.path.join(self.output_path, "simplification", "tmp")
        self.outfilegrid = os.path.join(self.output_path, "simplification", "grid.shp")
        self.outserial = os.path.join(self.output_path, "simplification", "tiles")
        self.outmos = os.path.join(self.output_path, "simplification", "mosaic")

        if self.clipfile is None:
            self.clipfile = os.path.join(self.output_path, "clip.shp")
        if self.clipfield is None:
            self.clipfield = I2_CONST.i2_vecto_clip_field
        # TODO : manage the case, no "clipfile" are provided
        regions = vf.get_fid_spatial_filter(self.clipfile, self.grid, self.clipfield)
        for region in regions:
            task = self.i2_task(
                task_name=f"mosaic_{region}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": mtr.merge_tile_raster,
                    "path": self.tmp_dir,
                    "clipfile": self.clipfile,
                    "fieldclip": self.clipfield,
                    "valueclip": region,
                    "tiles": self.outfilegrid,
                    "tilesfolder": self.outserial,
                    "tile_id": "FID",
                    "tile_name_prefix": "tile_",
                    "out": self.outmos,
                    "working_dir": workingDirectory,
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="regions",
                task_sub_group=f"region_{region}",
                task_dep_dico={
                    "tile_grid": [
                        f"tile_grid_{grid_tile}"
                        for grid_tile in range(self.gridsize**2)
                    ]
                },
            )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Mosaic raster tiles for serialisation strategy)"
        return description
