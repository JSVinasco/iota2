#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import logging

from iota2.configuration_files import read_config_file as rcf
from iota2.segmentation import prepare_segmentation_obia as pso
from iota2.steps import iota2_step

LOGGER = logging.getLogger("distributed.worker")


class LearningZonalStatistics(iota2_step.Step):
    resources_block_name = "learning_zonal_statistics"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.working_directory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.nb_runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')

        self.stats_used = rcf.read_config_file(self.cfg).getParam(
            "obia", "stats_used")
        self.buffer_size = rcf.read_config_file(self.cfg).getParam(
            "obia", "buffer_size")
        self.region_field = rcf.read_config_file(self.cfg).getParam(
            "chain", "region_field")
        self.spatial_res = rcf.read_config_file(self.cfg).getParam(
            "chain", "spatial_resolution")[0]
        for seed in range(self.nb_runs):
            for tile in self.tiles:
                target_model = f"zonal_stats_learn_{tile}_seed_{seed}"
                task = self.i2_task(
                    task_name=f"{target_model}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f":
                        pso.compute_zonal_stats_for_learning_samples_by_tiles,
                        "tile":
                        tile,
                        "working_dir":
                        self.working_directory,
                        "iota2_directory":
                        self.output_path,
                        "seed":
                        seed,
                        "sensors_parameters":
                        rcf.iota2_parameters(rcf.read_config_file(
                            self.cfg)).get_sensors_parameters(tile),
                        "spatial_res":
                        self.spatial_res,
                        "region_field":
                        self.region_field,
                        "stat_used":
                        self.stats_used,
                        "buffer_size":
                        self.buffer_size,
                        "seg_field":
                        self.i2_const.i2_segmentation_field_name
                    },
                    task_resources=self.get_resources())

                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="region_tasks",
                    task_sub_group=target_model,
                    task_dep_dico={"tile_tasks_model": [f"{tile}_{seed}"]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Compute zonal statistics for learning polygons")
        return description
