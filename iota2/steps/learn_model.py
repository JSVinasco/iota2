#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
from itertools import product
from typing import Dict

import iota2.common.i2_constants as i2_const
from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.learning.launch_learning import (learn_autocontext_model,
                                            learn_otb_model,
                                            learn_scikitlearn_model,
                                            learn_torch_model)
from iota2.learning.train_sklearn import cast_config_cv_parameters
from iota2.steps import iota2_step

I2_CONST = i2_const.Iota2Constants()
LOGGER = logging.getLogger("distributed.worker")


class LearnModel(iota2_step.Step):
    resources_block_name = "training"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        self.workingDirectory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.data_field = self.i2_const.re_encoding_label_name

        self.region_field = rcf.read_config_file(self.cfg).getParam(
            'chain', 'region_field')
        self.ground_truth = os.path.join(self.output_path,
                                         self.i2_const.re_encoding_label_file)
        self.runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        self.enable_autoContext = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'enable_autocontext')
        self.autoContext_iterations = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'autocontext_iterations')
        self.superpix_data_field = "superpix"
        self.tiles = rcf.read_config_file(self.cfg).getParam(
            'chain', 'list_tile').split(" ")
        self.available_ram = 1024.0 * get_ram(self.get_resources()["ram"])
        self.use_scikitlearn = rcf.read_config_file(self.cfg).getParam(
            'scikit_models_parameters', 'model_type') is not None

        self.classifier = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'classifier')
        self.classifier_otb_options = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'otb_classifier_options')

        self.apply_standardization = rcf.read_config_file(self.cfg).getParam(
            "scikit_models_parameters", "standardization")
        self.cross_valid_params = cast_config_cv_parameters(
            rcf.read_config_file(self.cfg).getParam(
                "scikit_models_parameters", "cross_validation_parameters"))
        self.cross_val_grouped = rcf.read_config_file(self.cfg).getParam(
            "scikit_models_parameters", "cross_validation_grouped")
        self.folds_number = rcf.read_config_file(self.cfg).getParam(
            "scikit_models_parameters", "cross_validation_folds")
        self.sample_management = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'sample_management')
        self.sk_model_parameters = dict(
            rcf.read_config_file(
                self.cfg).getSection("scikit_models_parameters"))

        self.sensors_parameters = ""

        self.sk_model_name = self.sk_model_parameters['model_type']
        del self.sk_model_parameters['model_type']
        del self.sk_model_parameters['standardization']
        del self.sk_model_parameters['cross_validation_grouped']
        del self.sk_model_parameters['cross_validation_folds']
        del self.sk_model_parameters['cross_validation_parameters']

        self.features_from_raw_dates = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "features_from_raw_dates")
        self.neural_network = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "deep_learning_parameters")
        self.deep_learning_enabled = "dl_name" in self.neural_network

        self.suffix_list = ["usually"]
        if rcf.read_config_file(self.cfg).getParam(
                'arg_train', 'dempster_shafer_sar_opt_fusion') is True:
            self.suffix_list.append("SAR")

        self.db_ext = "sqlite"
        if self.deep_learning_enabled:
            self.db_ext = f"{I2_CONST.i2_database_ext}"
        for suffix in self.suffix_list:
            for model_name, model_meta in self.spatial_models_distribution.items(
            ):
                for seed in range(self.runs):
                    target_model = f"model_{model_name}_seed_{seed}_{suffix}"
                    task_name = f"learning_model_{model_name}_seed_{seed}"
                    vector_file = os.path.join(
                        self.output_path, "learningSamples",
                        f"Samples_region_{model_name}_seed{seed}_learn.{self.db_ext}"
                    )
                    output_model = os.path.join(
                        self.output_path, "model",
                        f"model_{model_name}_seed_{seed}.txt")
                    if suffix == "SAR":
                        task_name += f"_{suffix}"
                        vector_file = vector_file.replace(
                            f".{self.db_ext}", f"_SAR.{self.db_ext}")
                        output_model = output_model.replace(".txt", "_SAR.txt")
                    if self.features_from_raw_dates or self.deep_learning_enabled:
                        batch_size_list = self.neural_network[
                            "hyperparameters_solver"]["batch_size"]
                        learning_rate_list = self.neural_network[
                            "hyperparameters_solver"]["learning_rate"]
                        hyperparameters_couples = list(
                            product(batch_size_list, learning_rate_list))
                        for hyper_num, (batch_size,
                                        learning_rate) in enumerate(
                                            hyperparameters_couples):
                            target_model_hyperparam = f"{target_model}_hyp_{hyper_num}"
                            task_name_hyperparam = f"{task_name}_hyp_{hyper_num}"
                            output_model_hyper = f"{output_model.replace('.txt','')}_hyp_{hyper_num}.txt"
                            task_params = self.get_learning_pytorch_parameters(
                                vector_file, output_model_hyper, batch_size,
                                learning_rate)
                            task = self.i2_task(
                                task_name=task_name_hyperparam,
                                log_dir=self.log_step_dir,
                                execution_mode=self.execution_mode,
                                task_parameters=task_params,
                                task_resources=self.get_resources())
                            dep_key = "region_tasks"
                            dep_values = [target_model]
                            if self.sample_management:
                                dep_key = "seed_tasks"
                                dep_values = [seed]
                            self.add_task_to_i2_processing_graph(
                                task,
                                task_group="region_tasks",
                                task_sub_group=f"{target_model_hyperparam}",
                                task_dep_dico={dep_key: dep_values})
                    else:
                        task_params = self.get_learning_i2_task_parameters(
                            vector_file, output_model, model_name, seed)
                        task = self.i2_task(
                            task_name=task_name,
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters=task_params,
                            task_resources=self.get_resources())
                        dep_key = "region_tasks" if self.enable_autoContext is False else "tile_tasks_model"
                        dep_values = [
                            target_model
                        ] if self.enable_autoContext is False else [
                            f"{tile}_{model_name}_seed_{seed}_{suffix}"
                            for tile in model_meta["tiles"]
                        ]
                        if self.sample_management:
                            dep_key = "seed_tasks"
                            dep_values = [seed]

                        self.add_task_to_i2_processing_graph(
                            task,
                            task_group="region_tasks",
                            task_sub_group=f"{target_model}",
                            task_dep_dico={dep_key: dep_values})

    def get_learning_pytorch_parameters(self, vector_file: str,
                                        output_model: str, batch_size: int,
                                        learning_rate: float) -> Dict:
        """
            """
        task_params = {
            'f':
            learn_torch_model,
            "samples_file":
            vector_file,
            "output_model":
            output_model,
            "data_field":
            self.data_field,
            "torch_model_params":
            self.neural_network,
            "encoder_convert_file":
            output_model.replace(".txt", "_encoder.pickle"),
            "model_parameters_file":
            output_model.replace(".txt", "_parameters.pickle"),
            "learning_rate":
            learning_rate,
            "batch_size":
            batch_size,
            "working_dir":
            self.workingDirectory
        }
        return task_params

    def get_learning_i2_task_parameters(self, vector_file: str,
                                        output_model: str, model_name: str,
                                        seed: int) -> Dict:
        """
        """
        task_params = {}
        if self.use_scikitlearn is True and self.enable_autoContext is True:
            raise ValueError(
                "scikit-learn and autoContext modes are not compatibles")
        if self.use_scikitlearn is True and self.enable_autoContext is False:
            task_params = {
                'f': learn_scikitlearn_model,
                "samples_file": vector_file,
                "output_model": output_model,
                "data_field": self.data_field,
                "sk_model_name": self.sk_model_name,
                "apply_standardization": self.apply_standardization,
                "cross_valid_params": self.cross_valid_params,
                "cross_val_grouped": self.cross_val_grouped,
                "folds_number": self.folds_number,
                "available_ram": self.available_ram,
                "sk_model_params": self.sk_model_parameters
            }
        elif self.enable_autoContext is True:
            model_tiles = self.spatial_models_distribution[model_name]["tiles"]
            suffix_learning_samples = f".{self.db_ext}"
            if f"_SAR.{self.db_ext}" in vector_file:
                suffix_learning_samples = f"_SAR.{self.db_ext}"
            list_learning_samples = [
                os.path.join(
                    self.output_path, "learningSamples",
                    f'{tile}_region_{model_name}_seed{seed}_Samples_learn{suffix_learning_samples}'
                ) for tile in model_tiles
            ]
            list_super_pixel_samples = [
                os.path.join(
                    self.output_path, "learningSamples",
                    f'{tile}_region_{model_name}_seed{seed}_Samples_SP.{self.db_ext}'
                ) for tile in model_tiles
            ]
            list_slic = [
                os.path.join(self.output_path, "features", tile, "tmp",
                             f'SLIC_{tile}.tif') for tile in model_tiles
            ]
            task_params = {
                "f": learn_autocontext_model,
                "model_name": model_name,
                "output_path": self.output_path,
                "superpix_data_field": self.superpix_data_field,
                "seed": seed,
                "list_learning_samples": list_learning_samples,
                "list_superPixel_samples": list_super_pixel_samples,
                "list_slic": list_slic,
                "data_field": self.data_field,
                "iterations": self.autoContext_iterations,
                "ram": self.available_ram,
                "working_directory": self.workingDirectory
            }
        else:
            task_params = {
                "f": learn_otb_model,
                "samples_file": vector_file,
                "output_model": output_model,
                "data_field": self.data_field,
                "classifier": self.classifier,
                "classifier_options": self.classifier_otb_options,
                "i2_running_dir": self.output_path,
                "model_name": model_name,
                "seed": seed,
                "region_field": self.region_field,
                "ground_truth": self.ground_truth
            }
        return task_params

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Learn model")
        return description
