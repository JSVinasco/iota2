# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os

from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import data_augmentation
from iota2.steps import iota2_step
from iota2.vector_tools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class GenSyntheticSamples(iota2_step.Step):
    resources_block_name = "samplesAugmentation"

    def __init__(
        self, cfg, cfg_resources_file, transfert_samples, workingDirectory=None
    ):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)
        # step variables
        self.working_directory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )
        self.ground_truth = os.path.join(
            self.output_path, self.i2_const.re_encoding_label_file
        )
        self.data_field = (
            rcf.read_config_file(self.cfg).getParam("chain", "data_field").lower()
        )

        self.sample_augmentation = dict(
            rcf.read_config_file(self.cfg).getParam("arg_train", "sample_augmentation")
        )

        self.sample_augmentation["target_models"] = list(
            self.sample_augmentation["target_models"]
        )

        self.nb_runs = rcf.read_config_file(self.cfg).getParam("arg_train", "runs")
        random_seed = rcf.read_config_file(self.cfg).getParam(
            "arg_train", "random_seed"
        )

        self.suffix_list = ["usually"]
        if (
            rcf.read_config_file(self.cfg).getParam(
                "arg_train", "dempster_shafer_sar_opt_fusion"
            )
            is True
        ):
            self.suffix_list.append("SAR")

        user_ground_truth = rcf.read_config_file(self.cfg).getParam(
            "chain", "ground_truth"
        )
        user_data_field = rcf.read_config_file(self.cfg).getParam("chain", "data_field")

        user_labels_to_i2_labels = get_re_encoding_labels_dic(
            user_ground_truth, user_data_field
        )

        for suffix in self.suffix_list:
            for model_name, _ in self.spatial_models_distribution.items():
                for seed in range(self.nb_runs):
                    if suffix == "SAR":
                        samples_file = os.path.join(
                            self.output_path,
                            "learningSamples",
                            f"Samples_region_{model_name}_seed{seed}_learn_SAR.sqlite",
                        )
                    else:
                        samples_file = os.path.join(
                            self.output_path,
                            "learningSamples",
                            f"Samples_region_{model_name}_seed{seed}_learn.sqlite",
                        )
                    target_model = f"model_{model_name}_seed_{seed}_{suffix}"
                    task = self.i2_task(
                        task_name=f"data_augmentation_{target_model}",
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters={
                            "f": data_augmentation.data_augmentation_synthetic,
                            "samples": samples_file,
                            "ground_truth": self.ground_truth,
                            "data_field": self.data_field.lower(),
                            "strategies": self.sample_augmentation,
                            "labels_table": user_labels_to_i2_labels,
                            "working_directory": self.working_directory,
                            "random_seed": random_seed,
                        },
                        task_resources=self.get_resources(),
                    )
                    dep_key = "seed_tasks" if transfert_samples else "region_tasks"
                    dep_values = [seed] if transfert_samples else [target_model]
                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="region_tasks",
                        task_sub_group=f"{target_model}",
                        task_dep_dico={dep_key: dep_values},
                    )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Generate synthetic samples"
        return description
