#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import logging
import os

from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.validation import boundary_tile_fusion as btf

LOGGER = logging.getLogger("distributed.worker")


class GenerateBoundaryReports(iota2_step.Step):
    """
    Step for writing analysis matrix
    """
    resources_block_name = "boundary_validation"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        # self.workingDirectory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.user_data_field = rcf.read_config_file(self.cfg).getParam(
            'chain', 'data_field')
        self.runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        self.nomenclature = rcf.read_config_file(self.cfg).getParam(
            'chain', 'nomenclature_path')
        dep = []
        for seed in range(self.runs):
            for tile in self.tiles:
                dep.append(f"final_report_{tile}_{seed}")

        task = self.i2_task(task_name="produce_boundary_metrics",
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters={
                                "f": btf.merge_metrics_matrices,
                                "iota2_directory": self.output_path,
                                "data_field": self.user_data_field,
                                "seeds": self.runs,
                                "nomenclature_path": self.nomenclature
                            },
                            task_resources=self.get_resources())
        self.add_task_to_i2_processing_graph(
            task,
            task_group="final_report",
            task_sub_group="final_report",
            task_dep_dico={"final_report": dep})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Compute boundary validation metrics")
        return description
