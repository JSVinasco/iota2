#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Step for report generation."""
import logging
import os

from iota2.configuration_files import read_config_file as rcf
from iota2.steps import iota2_step
from iota2.validation import gen_results as GR
from iota2.vector_tools.vector_functions import get_reverse_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class ReportGeneration(iota2_step.Step):
    """Create the report generation tasks."""

    resources_block_name = "reportGen"

    def __init__(self, cfg, cfg_resources_file, working_directory=None):
        """Initialize the class."""
        # heritage init
        super().__init__(cfg, cfg_resources_file, self.resources_block_name)

        # step variables
        self.working_directory = working_directory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        nomenclature = rcf.read_config_file(self.cfg).getParam(
            'chain', 'nomenclature_path')
        reference_data = rcf.read_config_file(self.cfg).getParam(
            "chain", "ground_truth")
        reference_data_field = rcf.read_config_file(self.cfg).getParam(
            "chain", "data_field")
        i2_labels_to_user_labels = get_reverse_encoding_labels_dic(
            reference_data, reference_data_field)

        # Update the names only if the comparison mode is enabled
        if rcf.read_config_file(self.cfg).getParam("arg_classification",
                                                   "boundary_comparison_mode"):
            path_in = [
                os.path.join(self.output_path, "final", "standard", "TMP"),
                os.path.join(self.output_path, "final", "boundary", "TMP")
            ]
            path_out = [
                os.path.join(self.output_path, "final", "standard"),
                os.path.join(self.output_path, "final", "boundary")
            ]
        else:
            path_in = [os.path.join(self.output_path, "final", "TMP")]
            path_out = [os.path.join(self.output_path, "final")]
        task = self.i2_task(task_name="final_report",
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters={
                                "f": GR.find_entry_for_gen_results,
                                "in_folder_list": path_in,
                                "iota2_directory": self.output_path,
                                "path_nomenclature": nomenclature,
                                "output_txt_path_list": path_out,
                                "output_png_path_list": path_out,
                                "labels_conversion_table":
                                i2_labels_to_user_labels
                            },
                            task_resources=self.get_resources())
        self.add_task_to_i2_processing_graph(
            task,
            task_group="final_report",
            task_sub_group="final_report",
            task_dep_dico={"confusion_merge": ["confusion_merge"]})

    @classmethod
    def step_description(cls):
        """Print a short description of the step's purpose."""
        description = ("Generate final report")
        return description
