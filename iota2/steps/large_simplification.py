#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os

import iota2.common.i2_constants as i2_const
from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_ram
from iota2.simplification import vector_generalize as vas
from iota2.steps import iota2_step
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.Iota2Constants()


class LargeSimplification(iota2_step.Step):
    resources_block_name = "simplification"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(LargeSimplification, self).__init__(
            cfg, cfg_resources_file, self.resources_block_name
        )
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )
        self.grid = os.path.join(self.output_path, "simplification", "grid.shp")
        self.clip_file = rcf.read_config_file(self.cfg).getParam(
            "simplification", "clipfile"
        )
        self.clip_field = rcf.read_config_file(self.cfg).getParam(
            "simplification", "clipfield"
        )
        self.epsg = int(
            rcf.read_config_file(self.cfg).getParam("chain", "proj").split(":")[-1]
        )
        self.out_mos = os.path.join(self.output_path, "simplification", "mosaic")
        self.douglas = rcf.read_config_file(self.cfg).getParam(
            "simplification", "douglas"
        )

        tmpdir = os.path.join(self.output_path, "simplification", "tmp")
        if self.clip_file is None:
            self.clip_file = os.path.join(self.output_path, "clip.shp")
        if self.clip_field is None:
            self.clip_field = I2_CONST.i2_vecto_clip_field

        if os.path.exists(self.grid):
            list_fid = vf.get_fid_spatial_filter(
                self.clip_file, self.grid, self.clip_field
            )
        else:
            list_fid = vf.list_value_fields(self.clip_file, self.clip_field)

        for fid in list_fid:
            task = self.i2_task(
                task_name=f"large_simpl_region_{fid}",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": vas.generalize_vector,
                    "path": tmpdir,
                    "vector": f"{self.out_mos}/tile_{self.clip_field}_{fid}.sqlite",
                    "paramgene": self.douglas,
                    "method": "douglas",
                    "out": f"{self.out_mos}/tile_{self.clip_field}_{fid}_douglas.sqlite",
                    "epsg": str(self.epsg),
                    "working_dir": workingDirectory,
                    "clipfile": self.clip_file,
                    "clipfield": self.clip_field,
                    "clipvalue": fid,
                    "bufferclip": 20000,
                },
                task_resources=self.get_resources(),
            )
            self.add_task_to_i2_processing_graph(
                task,
                task_group="simplification",
                task_sub_group=f"simplification_{fid}",
                task_dep_dico={"vectorization": [f"vectorization_{fid}"]},
            )

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = "Douglas-Peucker simplification (Serialisation strategy)"
        return description
