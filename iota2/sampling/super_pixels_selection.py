# !/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import collections
import logging
import math
import operator
import os
import random
import shutil
from typing import Dict, Iterable, Iterator, List, Optional, Tuple

import numpy as np
from osgeo import gdal, ogr

from iota2.common.file_utils import file_search_and, read_raster
from iota2.common.otb_app_bank import (
    CreateBandMathApplication,
    CreateRasterizationApplication,
    CreateSampleExtractionApplication,
    CreateSuperimposeApplication,
)
from iota2.vector_tools.add_field import add_field
from iota2.vector_tools.vector_functions import (
    get_all_fields_in_shape,
    get_field_element,
)

LOGGER = logging.getLogger("distributed.worker")
LOGGER.addHandler(logging.NullHandler())

Param = Dict[str, str]
YCOORDS = List[float]
XCOORDS = List[float]


def input_parameters(execution_dir: str) -> List[Param]:
    """Feed merge_ref_super_pix.

    Parameters
    ----------
    execution_dir : str
        path to iota² running directory
    """
    tile_position = 0
    model_position = 3
    seed_position = 5

    sample_selection_directory = os.path.join(execution_dir, "samplesSelection")
    sample_sel_content = list(
        set(
            file_search_and(
                sample_selection_directory,
                False,
                "_samples_region_",
                "selection.sqlite",
            )
        )
    )
    selection_models = [
        (
            c_file.split("_")[model_position],
            int(c_file.split("_")[seed_position]),
            c_file.split("_")[tile_position],
            c_file,
        )
        for c_file in sample_sel_content
    ]
    selected_ref = sorted(selection_models, key=operator.itemgetter(0, 1, 2))

    parameters = []
    for _, _, tile_name, ref in selected_ref:
        slic_path = file_search_and(
            os.path.join(execution_dir, "features", tile_name, "tmp"),
            True,
            "SLIC",
            ".tif",
        )[0]
        ref_path = os.path.join(sample_selection_directory, f"{ref}.sqlite")
        parameters.append({"selection_samples": ref_path, "SLIC": slic_path})

    return parameters


def choosable_annual_pixels(
    classification_raster: str,
    validity_raster: str,
    region_mask: str,
    validity_threshold: str,
):
    """Mask pixels in order to choose ones according to clouds, region...

    Parameters
    ----------
    classification_raster : string
        path to classification raster
    validity_raster : string
        path to vilidity raster
    region_mask : string
        path to region mask raster
    validity_threshold : int
        cloud threashold to pick up samples

    Return
    ------
    tuple (mask, (Ox, Oy, spx, spy))
        mask : numpy.array
            numpy array where non 0 are choosable pixels
        Ox : float
            x origin
        Oy : float
            y origin
        spx : float
            x spacing
        spy : float
            y spacing
    """
    roi_classif, _ = CreateSuperimposeApplication(
        {"inr": region_mask, "inm": classification_raster, "interpolator": "nn"}
    )
    roi_classif.Execute()
    roi_validity, _ = CreateSuperimposeApplication(
        {"inr": region_mask, "inm": validity_raster, "interpolator": "nn"}
    )
    roi_validity.Execute()
    mask_dummy = CreateBandMathApplication({"il": [region_mask], "exp": "im1b1"})
    mask_dummy.Execute()
    valid = CreateBandMathApplication(
        {
            "il": [roi_validity, roi_classif],
            "exp": f"im1b1>{validity_threshold}?im2b1:0",
        }
    )
    valid.Execute()
    choosable = CreateBandMathApplication(
        {"il": [valid, mask_dummy], "exp": "im1b1*(im2b1>=1?1:0)"}
    )
    choosable.Execute()
    oy, ox = choosable.GetImageOrigin("out")
    spx, spy = choosable.GetImageSpacing("out")
    return choosable.GetImageAsNumpyArray("out"), (ox, oy, spx, spy)


def move_annual_samples_from_array(
    samples_position,
    target_label,
    data_field,
    samples_number,
    array,
    ox,
    oy,
    spx,
    spy,
    tile_origin_field_value,
    seed_field_value,
    region_field_value,
):
    """ """
    x_coords, y_coords = np.where(array == target_label)
    # ~ y_coords, x_coords = np.where(array==target_label)
    samples_number = samples_number if len(y_coords) > samples_number else len(y_coords)

    geo_coordinates = []
    for y_coord, x_coord in zip(y_coords, x_coords):
        x_geo = (ox + spx) - spx * (x_coord + 1)
        y_geo = (oy + spy) - spy * (y_coord + 1)

        geo_coordinates.append((x_geo, y_geo))

    random_coords = random.sample(geo_coordinates, samples_number)

    # seek and destroy samples
    driver = ogr.GetDriverByName("SQLite")
    data_source = driver.Open(samples_position, 1)
    layer = data_source.GetLayer()
    for feature in layer:
        if feature.GetField(data_field) == target_label:
            layer.DeleteFeature(feature.GetFID())
    # add new samples according to random_coords
    for x_geo, y_geo in random_coords:
        feature = ogr.Feature(layer.GetLayerDefn())
        feature.SetField(data_field, int(target_label))
        feature.SetField(tile_origin_field_value[0], tile_origin_field_value[1])
        feature.SetField(seed_field_value[0], seed_field_value[1])
        feature.SetField(region_field_value[0], region_field_value[1])

        point = ogr.CreateGeometryFromWkt(f"POINT({y_geo} {x_geo})")
        feature.SetGeometry(point)
        layer.CreateFeature(feature)
        feature = None
    data_source = None


def move_annual_samples_position(
    samples_position,
    data_field,
    annual_labels,
    classification_raster,
    validity_raster,
    region_mask,
    validity_threshold,
    tile_origin_field_value,
    seed_field_value,
    region_field_value,
):
    """Move samples position of labels of interest according to rasters.

    Parameters
    ----------
    samples_position : string
        path to a vector file containing points, representing samples' positions
    data_field : string
        data field in vector
    annual_labels : list
        list of annual labels
    classification_raster : string
        path to classification raster
    validity_raster : string
        path to vilidity raster
    region_mask : string
        path to region mask raster
    validity_threshold : int
        cloud threshold to pick up samples
    """
    annual_labels = map(int, annual_labels)
    class_repartition = get_field_element(
        samples_position,
        driver_name="SQLite",
        field=data_field,
        mode="all",
        elem_type="int",
    )
    class_repartition = collections.Counter(class_repartition)

    mask_array, (ox, oy, spx, spy) = choosable_annual_pixels(
        classification_raster, validity_raster, region_mask, validity_threshold
    )
    for annual_label in annual_labels:
        if annual_label in class_repartition:
            samples_number = class_repartition[annual_label]
            move_annual_samples_from_array(
                samples_position,
                annual_label,
                data_field,
                samples_number,
                mask_array,
                ox,
                oy,
                spx,
                spy,
                tile_origin_field_value,
                seed_field_value,
                region_field_value,
            )
        else:
            continue


def sp_geo_coordinates_with_value(
    coordinates: Tuple[YCOORDS, XCOORDS],
    sp_array: np.ndarray,
    x_origin: float,
    y_origin: float,
    x_size: float,
    y_size: float,
) -> Iterator[Tuple[float, float, float]]:
    """Convert tabular coordinates to geographical.

    Parameters
    ----------
    coordinates: tuple
        tuple of coordinates (lists) to convert
    sp_array: np.array
        numpy array to extract value
    x_origin: float
        x origin
    y_origin: float
        y origin
    x_size: float
        x size
    y_size: float
        y size

    Return
    ------
    tuple(y_geo_coordinates, x_geo_coordinates, array_value)
    """
    offset_y = y_size / 2.0
    offset_x = x_size / 2.0

    for y_coord, x_coord in zip(coordinates[0], coordinates[1]):
        y_geo = y_origin + offset_y + y_size * y_coord
        x_geo = x_origin + offset_x + x_size * x_coord
        value = float(sp_array[y_coord][x_coord])
        yield (y_geo, x_geo, value)


def super_pixels_coordinates(
    input_vector: str,
    segmented_raster: str,
    working_directory: Optional[str] = None,
    logger: logging.Logger = LOGGER,
) -> Iterator[Tuple[float, float, float]]:
    """Return superpixels coordinates which intersect input vector data-set.

    Parameters
    ----------
    input_vector : str
        data-set path
    segmented_raster : str
        raster path
    working_directory : str
        working directory path

    Return
    ------
    tuple
        tuple containing two list, y coordinates and x coordinates
    """
    logger.info(
        (
            "getting superpixels positions from intersection "
            f"of {segmented_raster} and {input_vector}"
        )
    )

    if working_directory is None:
        tmp_dir = os.path.split(input_vector)[0]
    else:
        tmp_dir = working_directory

    input_vector_name = "{os.path.split(os.path.splitext(input_vector)[0])[-1]}.tif"
    vector_raster_path = os.path.join(tmp_dir, input_vector_name)

    ref_raster = CreateRasterizationApplication(
        {
            "in": input_vector,
            "out": vector_raster_path,
            "im": segmented_raster,
            "mode.binary.foreground": "1",
            "background": "0",
        }
    )
    ref_raster.Execute()

    seg_ds = gdal.Open(segmented_raster, 0)
    slic_array = seg_ds.GetRasterBand(1).ReadAsArray()

    mask_array = np.int32(ref_raster.GetImageAsNumpyArray("out"))

    intersection_array = np.multiply(mask_array, slic_array)
    super_pix_id = np.unique(intersection_array)
    # remove label 0
    if 0 in super_pix_id:
        super_pix_id = super_pix_id[super_pix_id != 0]

    y_coordinates, x_coordinates = np.where(np.isin(slic_array, super_pix_id))

    _, _, __, transform = read_raster(segmented_raster)
    x_min, spacing_x, _, y_max, _, spacing_y = transform

    coords = sp_geo_coordinates_with_value(
        coordinates=(y_coordinates, x_coordinates),
        sp_array=slic_array,
        x_origin=x_min,
        y_origin=y_max,
        x_size=spacing_x,
        y_size=spacing_y,
    )
    return coords


def add_sp_to_ref(
    reference: str,
    sp_field: str,
    sp_belong_field: str,
    region_field: str,
    coordinates: Iterable[Tuple[float, float, float]],
    logger: logging.Logger = LOGGER,
) -> None:
    """Add superPixel position to reference samples.

    reference : str
        reference vector data (containing points)
    sp_field : str
        field to descriminate superpixels labels
    sp_belong_field : str
        field to descriminate superpixels from reference labels
    region_field : str
        region field
    coordinates : tuple iterator
        (y geo coordinates, x geo coordinates, superpixel value)
    logger : logging.Logger
        root logger
    """
    logger.info(f"adding superPixel position to {reference}")
    learn_flag = "learn"
    seed_position = 5
    region_position = 3
    tile_position = 0

    driver = "SQLite"
    if sp_field not in get_all_fields_in_shape(reference, "SQLite"):
        add_field(
            reference, sp_field, value_field=0, value_type="int64", driver_name=driver
        )

    if sp_belong_field not in get_all_fields_in_shape(reference, "SQLite"):
        add_field(
            reference,
            sp_belong_field,
            value_field=0,
            value_type=int,
            driver_name=driver,
        )

    seed_number = os.path.basename(os.path.split(reference)[-1]).split("_")[
        seed_position
    ]
    region_name = os.path.basename(os.path.split(reference)[-1]).split("_")[
        region_position
    ]
    tile_origin = os.path.basename(os.path.split(reference)[-1]).split("_")[
        tile_position
    ]

    driver = ogr.GetDriverByName(driver)
    data_source = driver.Open(reference, 1)
    layer = data_source.GetLayer()

    feture_defn = layer.GetLayerDefn()
    for y_coord, x_coord, value in coordinates:
        new_feature = ogr.Feature(feture_defn)
        wkt = f"POINT({x_coord} {y_coord})"
        point = ogr.CreateGeometryFromWkt(wkt)
        new_feature.SetGeometry(point)
        new_feature.SetField(sp_field, int(value))
        new_feature.SetField(f"seed_{seed_number}", learn_flag)
        new_feature.SetField(region_field, region_name)
        new_feature.SetField("tile_o", tile_origin)
        new_feature.SetField(sp_belong_field, 1)
        layer.CreateFeature(new_feature)
    data_source = layer = None


def geo_to_tab(x_coord, y_coord, x_origin, y_origin, x_size, y_size):
    """Convert geo coords to table coord."""
    y_tab = (y_coord - y_origin) / y_size
    x_tab = (x_coord - x_origin) / x_size
    return math.floor(x_tab), math.floor(y_tab)


def add_sp_labels(
    reference: str,
    slic_file: str,
    dataref_field_name: str,
    sp_field_name: str,
    ram: int,
) -> None:
    """Add new column to a database and add raster's value in it.

    Parameters
    ----------
    reference : str
        database path
    slic_file : str
        raster path
    dataref_field_name : str
        database labels field name
    sp_field_name : str
        database field to add raster's value
    ram : int
        available ram
    """
    extraction = CreateSampleExtractionApplication(
        {
            "in": slic_file,
            "vec": reference,
            "outfield": "list",
            "field": dataref_field_name,
            "ram": ram,
            "outfield.list.names": [sp_field_name],
        }
    )
    extraction.ExecuteAndWriteOutput()


def merge_ref_super_pix(
    data: Param,
    dataref_field_name: str,
    sp_field_name: str,
    sp_belong_field_name: str,
    region_field_name: str,
    sampling_labels_from_raster: dict = {},
    working_directory: Optional[str] = None,
    ram: int = 128,
    logger=LOGGER,
):
    """Add superPixels points to reference data.

    Parameters
    ----------
    data : dict
        {"selection_samples": "/path/to/pointsDataBase.sqlite",
         "SLIC": "/path/to/segmentedRaster.tif"}
    dataref_field_name : str
        database labels field name
    sp_field_name : str
        field added in database representing segment labels
    sp_belong_field_name : str
        field added in database discriminating original samples from new samples
    region_field_name : str
        region field in database
    working_directory : str
        path to store temporary files
    ram : int
        available ram
    """
    reference = data["selection_samples"]
    slic = data["SLIC"]

    if working_directory:
        shutil.copy(reference, working_directory)
        _, reference_name = os.path.split(reference)
        reference = os.path.join(working_directory, reference_name)

    if sampling_labels_from_raster:
        model_name = os.path.basename(reference).split("_")[3].split("f")[0]
        tile = os.path.basename(reference).split("_")[0]
        seed = os.path.basename(reference).split("_")[5]
        region_mask = file_search_and(
            sampling_labels_from_raster["region_mask_directory"],
            True,
            f"region_{model_name.split('f')[0]}_{tile}.tif",
        )[0]

        tile_origin_field_value = ("tile_o", tile)
        seed_field_value = (f"seed_{seed}", "learn")
        region_field_value = (region_field_name, model_name)
        move_annual_samples_position(
            reference,
            dataref_field_name,
            sampling_labels_from_raster["annual_labels"],
            sampling_labels_from_raster["classification_raster"],
            sampling_labels_from_raster["validity_raster"],
            region_mask,
            sampling_labels_from_raster["val_threshold"],
            tile_origin_field_value,
            seed_field_value,
            region_field_value,
        )

    add_sp_labels(reference, slic, dataref_field_name.lower(), sp_field_name, ram)

    sp_coords_val = super_pixels_coordinates(reference, slic, logger=logger)

    add_sp_to_ref(
        reference,
        sp_field=sp_field_name,
        sp_belong_field=sp_belong_field_name,
        region_field=region_field_name,
        coordinates=sp_coords_val,
        logger=logger,
    )
    if working_directory:
        shutil.move(reference, data["selection_samples"])
