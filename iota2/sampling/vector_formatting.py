#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""The vector formatting module."""
# import argparse
import logging
import math
import os
import shutil
from typing import List, Optional

# import geopandas as gp
import numpy as np
import xmltodict
from osgeo import ogr

from iota2.common import file_utils as fut
from iota2.common import otb_app_bank as otb
from iota2.common.file_utils import get_raster_resolution
from iota2.common.utils import run
from iota2.sampling import split_in_subsets as subset
from iota2.vector_tools import spatial_operations as intersect
from iota2.vector_tools import vector_functions as vf
from iota2.vector_tools.add_field import add_field
from iota2.vector_tools.compute_boundary_regions import compute_distance_map
from iota2.vector_tools.vector_functions import get_geom_column_name, is_readable

LOGGER = logging.getLogger("distributed.worker")


def check_validation_db_integrity(databases: List[str], logger=LOGGER):
    """Ensure databases integrity."""
    valid_db = [elem for elem in databases if "val.sqlite" in elem]
    driver = ogr.GetDriverByName("SQLite")
    for database in valid_db:
        db_ds = driver.Open(database, 0)
        layer = db_ds.GetLayer()
        if layer is None or layer.GetFeatureCount() == 0:
            # validation on learning polygons
            logger.warning(
                f"{database} is equivalent to "
                f"{database.replace('val.sqlite', 'learn.sqlite')}"
            )
            shutil.copy(database.replace("val.sqlite", "learn.sqlite"), database)


def get_total_number_of_samples(stats_file: str) -> int:
    """Return the total number of samples for xml file.

    Parse statistics file from OTB and return the total number of samples
    to avoid create empty validation files

    Parameters
    ----------
    stats_file:
        the file to be parsed
    """
    with open(stats_file, "rb") as read_file:
        dico = xmltodict.parse(read_file)
        samples_per_class_list = dico["GeneralStatistics"]["Statistic"][0][
            "StatisticMap"
        ]
        values = [int(d["@value"]) for d in samples_per_class_list]
    return np.sum(values)


def split_vector_by_region(
    in_vect: str,
    output_dir: str,
    region_field: str,
    sample_selection_dir: str,  # convert to optional
    sampling_validation: bool,
    runs: int = 1,
    driver: str = "ESRI shapefile",
    proj_in: str = "EPSG:2154",
    proj_out: str = "EPSG:2154",
    mode: str = "usually",
    targeted_chunk: Optional[int] = None,
    logger=LOGGER,
) -> List[str]:
    """Create new files by regions in input vector.

    Return the list of produced vector files.

    Parameters
    ----------
    in_vect:
        input vector path
    output_dir:
        path to output directory
    region_field:
        field in in_vect describing regions
    sample_selection_dir:
        folder where the sample selection files are stored
    sampling_validation:
        enable the validation sampling
    runs:
        the number of seeds
    driver:
        ogr driver
    proj_in:
        input projection
    proj_out:
        output projection
    mode:
        define if we split SAR sensor to the other
    targeted_chunk:
        indicate if the chunk mode is on and then keep the chunk information
    """
    output_paths = []

    # const
    tile_pos = 0
    learn_flag = "learn"
    val_flag = "validation"
    table_name = "output"

    vec_name = os.path.split(in_vect)[-1]
    tile = vec_name.split("_")[tile_pos]
    extent = os.path.splitext(vec_name)[-1]
    targeted_chunk = "" if targeted_chunk is None else f"{targeted_chunk}_"
    regions = vf.get_field_element(
        in_vect,
        driver_name=driver,
        field=region_field,
        mode="unique",
        elem_type="str",
        logger=logger,
    )
    if not regions:
        return []
    table = vec_name.split(".")[0]
    if driver != "ESRI shapefile":
        table = "output"
    # split vector
    for seed in range(runs):
        fields_to_keep = ",".join(
            [
                elem
                for elem in vf.get_all_fields_in_shape(in_vect, "SQLite")
                if "seed_" not in elem
            ]
        )
        for region in regions:
            out_vec_name_learn = (
                f"{tile}_region_{region}_seed{seed}_"
                f"{targeted_chunk}Samples_learn_tmp"
            )
            out_vec_name_val = (
                f"{tile}_region_{region}_seed{seed}_" f"{targeted_chunk}Samples_val_tmp"
            )
            if mode != "usually":
                if targeted_chunk:
                    out_vec_name_learn = "_".join(
                        [
                            tile,
                            "region",
                            region,
                            "seed" + str(seed),
                            str(targeted_chunk),
                            "Samples",
                            "SAR",
                            "learn_tmp",
                        ]
                    )
                    out_vec_name_learn = "_".join(
                        [
                            tile,
                            "region",
                            region,
                            "seed" + str(seed),
                            str(targeted_chunk),
                            "Samples",
                            "SAR",
                            "val_tmp",
                        ]
                    )
                else:
                    out_vec_name_learn = "_".join(
                        [
                            tile,
                            "region",
                            region,
                            "seed" + str(seed),
                            "Samples",
                            "SAR",
                            "learn_tmp",
                        ]
                    )
                    out_vec_name_val = "_".join(
                        [
                            tile,
                            "region",
                            region,
                            "seed" + str(seed),
                            "Samples",
                            "SAR",
                            "val_tmp",
                        ]
                    )
            output_vec_learn = os.path.join(output_dir, out_vec_name_learn + extent)
            seed_clause_learn = f"seed_{seed}='{learn_flag}'"
            region_clause = f"{region_field}='{region}'"
            # split vectors by runs and learning sets
            sql_cmd_learn = (
                f"select * FROM {table} WHERE {seed_clause_learn}"
                f" AND {region_clause}"
            )
            cmd = (
                f"ogr2ogr -t_srs {proj_out} -s_srs {proj_in} -nln {table}"
                f' -f "{driver}" -sql "{sql_cmd_learn}" {output_vec_learn} '
                f"{in_vect}"
            )
            run(cmd, logger=logger)

            # drop useless column
            sql_clause = f"select GEOMETRY,{fields_to_keep} from {table_name}"
            output_vec_learn_out = output_vec_learn.replace("_tmp", "")

            cmd = (
                f"ogr2ogr -s_srs {proj_in} -t_srs {proj_out} -dialect "
                f"'SQLite' -f 'SQLite' -nln {table_name} -sql '{sql_clause}' "
                f"{output_vec_learn_out} {output_vec_learn}"
            )
            run(cmd, logger=logger)
            if is_readable(output_vec_learn_out):
                output_paths.append(output_vec_learn_out)
                os.remove(output_vec_learn)
            else:
                os.remove(output_vec_learn)
                os.remove(output_vec_learn_out)
            # split vectir by runs and validation sets
            logger.info(f"Sampling Validation: {sampling_validation}")
            if sampling_validation:
                total_samples = get_total_number_of_samples(
                    os.path.join(
                        sample_selection_dir,
                        f"{tile}_region_{region}_seed_{seed}_stats.xml",
                    )
                )
                if total_samples > 0:
                    # ensure that stats.xml is not empty

                    output_vec_val = os.path.join(output_dir, out_vec_name_val + extent)
                    seed_clause_val = f"seed_{seed}='{val_flag}'"
                    sql_cmd_val = (
                        f"select * FROM {table} "
                        f"WHERE {seed_clause_val}"
                        f" AND {region_clause}"
                    )
                    cmd = (
                        f"ogr2ogr -t_srs {proj_out} "
                        f"-s_srs {proj_in} -nln {table}"
                        f' -f "{driver}" -sql "{sql_cmd_val}" '
                        f"{output_vec_val} "
                        f"{in_vect}"
                    )
                    run(cmd, logger=logger)

                    # remove useless column in validation file
                    output_vec_val_out = output_vec_val.replace("_tmp", "")
                    cmd = (
                        f"ogr2ogr -s_srs {proj_in} -t_srs {proj_out} -dialect "
                        f"'SQLite' -f 'SQLite' -nln {table_name} "
                        f"-sql '{sql_clause}' "
                        f"{output_vec_val_out} {output_vec_val}"
                    )
                    run(cmd, logger=logger)
                    output_paths.append(output_vec_val_out)
                    os.remove(output_vec_val)
    return output_paths


def create_tile_region_masks(
    tile_region: str,
    region_field: str,
    tile_name: str,
    output_directory: str,
    origin_name: str,
    img_ref: str,
    i2_running_dir: str,
    classification_mode: str,
    tile_boundaries_file: str,
    boundary_fusion_enabled: bool = False,
    logger=LOGGER,
) -> None:
    """Produce mask of regions for classification purpose.

    Parameters
    ----------
    tile_region:
        path to a SQLite file containing polygons. Each feature is a region
    region_field:
        region's field
    tile_name:
        current tile name
    output_directory:
        directory to save masks
    origin_name:
        region's field vector file name
    img_ref:
        path to a tile reference image
    i2_running_dir:
        iota2 output directory
    classification_mode:
        "separate" or "fusion"
    tile_boundaries_file:
         folder to envelope path
    boundary_fusion_enabled:
         enable the boundary fusion mode
    """
    all_regions_tmp = vf.get_field_element(
        tile_region,
        driver_name="SQLite",
        field=region_field.lower(),
        mode="unique",
        elem_type="str",
    )
    # transform sub region'name into complete region
    # (region '1f1' become region '1')
    all_regions = []
    for region in all_regions_tmp:
        reg = region.split("f")[0]
        all_regions.append(reg)
    tile_boundaries_file_raster = tile_boundaries_file.replace(".shp", ".tif")

    x_res, y_res = get_raster_resolution(img_ref)
    run(
        f"gdal_rasterize -ot Byte -tr {x_res} {abs(y_res)} -burn 1 -at"
        f" {tile_boundaries_file} {tile_boundaries_file_raster}",
        logger=logger,
    )

    # in order to be aligned with the ref image
    super_imp, _ = otb.CreateSuperimposeApplication(
        {
            "inr": img_ref,
            "inm": tile_boundaries_file_raster,
            "out": tile_boundaries_file_raster,
            "interpolator": "nn",
        }
    )
    super_imp.ExecuteAndWriteOutput()

    region = None
    driver = ogr.GetDriverByName("ESRI Shapefile")
    for region in all_regions:
        output_name = f"{origin_name}_region_{region}_{tile_name}_tmp.shp"
        output_path = os.path.join(output_directory, output_name)
        db_name = (os.path.splitext(os.path.basename(tile_region))[0]).lower()
        cmd = (
            f"ogr2ogr -f 'ESRI Shapefile' -sql \"SELECT * FROM {db_name}"
            f" WHERE {region_field}='{region}'\" {output_path} {tile_region}"
        )
        run(cmd, logger=logger)

        output_path_buff = output_path.replace("_tmp.shp", "_buff.shp")
        vf.erode_or_dilate_shapefile(
            infile=output_path, outfile=output_path_buff, buffdist=2 * x_res
        )

        tile_region_raster = output_path_buff.replace("_buff.shp", ".tif")
        tile_region_app = otb.CreateRasterizationApplication(
            {
                "in": output_path_buff,
                "out": tile_region_raster,
                "im": img_ref,
                "mode": "binary",
                "pixType": "uint8",
                "background": "0",
                "mode.binary.foreground": "1",
            }
        )
        tile_region_app.ExecuteAndWriteOutput()

        clip = otb.CreateBandMathApplication(
            {
                "il": [tile_region_raster, tile_boundaries_file_raster],
                "exp": "im1b1*im2b1",
                "out": tile_region_raster,
                "pixType": "uint8",
            }
        )
        clip.ExecuteAndWriteOutput()

        tile_region_db = tile_region_raster.replace(".tif", ".shp")

        # generate classification mask
        classification_mask_dir = os.path.join(i2_running_dir, "classif", "MASK")
        classification_mask_name = f"MASK_region_{region}_{tile_name}.tif"
        classification_mask_file = os.path.join(
            classification_mask_dir, classification_mask_name
        )
        fut.ensure_dir(classification_mask_dir)
        if classification_mode == "separate":
            classif_mask_app = otb.CreateRasterizationApplication(
                {
                    "in": output_path_buff,
                    "out": classification_mask_file,
                    "im": img_ref,
                    "mode": "attribute",
                    "mode.attribute.field": region_field,
                }
            )
            classif_mask_app.ExecuteAndWriteOutput()
            clip = otb.CreateBandMathApplication(
                {
                    "il": [classification_mask_file, tile_boundaries_file_raster],
                    "exp": "im1b1*im2b1",
                    "out": classification_mask_file,
                }
            )
            clip.ExecuteAndWriteOutput()
        else:
            shutil.copy(tile_region_raster, classification_mask_file)

        tile_region_db_name = os.path.basename(tile_region_db).split(".")[0]
        run(
            f"gdal_polygonize.py {tile_region_raster}"
            f" {tile_region_db} {tile_region_db_name}",
            logger=logger,
        )
        add_field(tile_region_db, region_field, region, str)

        driver.DeleteDataSource(output_path)
        driver.DeleteDataSource(output_path_buff)
    if not boundary_fusion_enabled:
        # if boundary fusion enabled this file is used as mask
        # at the end of the workflow
        os.remove(tile_boundaries_file_raster)


def keep_fields(
    vec_in: str,
    vec_out: str,
    fields: Optional[List[str]] = None,
    proj_in: int = 2154,
    proj_out: int = 2154,
    logger=LOGGER,
) -> None:
    """Extract fields of an input SQLite file.

    Parameters
    ----------
    vec_in:
        input SQLite vector File
    vec_out:
        output SQLite vector File
    fields:
        list of fields to keep
    proj_in:
        input projection
    proj_out:
        output projection
    """
    fields = [] if fields is None else fields
    table_in = (os.path.splitext(os.path.split(vec_in)[-1])[0]).lower()
    table_out = (os.path.splitext(os.path.split(vec_out)[-1])[0]).lower()

    _, ext = os.path.splitext(vec_in)
    driver_vec_in = "ESRI Shapefile"
    if "sqlite" in ext:
        driver_vec_in = "SQLite"
    geom_column_name = get_geom_column_name(vec_in, driver=driver_vec_in)
    sql_clause = f"select {geom_column_name}," f"{','.join(fields)} from {table_in}"
    cmd = (
        f"ogr2ogr -s_srs EPSG:{proj_in} -t_srs EPSG:{proj_out} -dialect "
        f"'SQLite' -f 'SQLite' -nln {table_out} -sql '{sql_clause}' "
        f"{vec_out} {vec_in}"
    )
    run(cmd, logger=logger)


def split_by_sets(
    vector: str,
    seeds: int,
    split_directory: str,
    proj_in: int,
    proj_out: int,
    tile_name: str,
    split_ground_truth: bool = True,
    logger=LOGGER,
) -> List[str]:
    """Create new vector file by learning / validation sets.

    Parameters
    ----------
    vector:
        path to a shape file containg ground truth
    seeds:
        number of run
    split_directory:
        output directory
    proj_in:
        input projection
    proj_out:
        output projection
    tile_name:
        tile's name
    splitGroundTruth:
        flat to split ground truth
    """
    out_vectors = []

    valid_flag = "validation"
    learn_flag = "learn"
    tile_origin_field_name = "tile_o"

    vector_layer_name = (os.path.splitext(os.path.split(vector)[-1])[0]).lower()

    # predict fields to keep
    fields_to_rm = ["seed_" + str(seed) for seed in range(seeds)]
    fields_to_rm.append(tile_origin_field_name)
    all_fields = vf.get_all_fields_in_shape(vector)

    fields = [field_name for field_name in all_fields if field_name not in fields_to_rm]

    # start split
    for seed in range(seeds):
        valid_clause = f"seed_{seed}='{valid_flag}'"
        learn_clause = f"seed_{seed}='{learn_flag}'"

        sql_cmd_valid = f"select * FROM {vector_layer_name}" f" WHERE {valid_clause}"
        output_vec_valid_name = "_".join([tile_name, "seed_" + str(seed), "val"])
        output_vec_valid_name_tmp = "_".join(
            [tile_name, "seed_" + str(seed), "val", "tmp"]
        )
        output_vec_valid_tmp = os.path.join(
            split_directory, output_vec_valid_name_tmp + ".sqlite"
        )
        output_vec_valid = os.path.join(
            split_directory, output_vec_valid_name + ".sqlite"
        )
        cmd_valid = (
            f"ogr2ogr -t_srs EPSG:{proj_out} -s_srs EPSG:{proj_in} "
            f'-nln {output_vec_valid_name_tmp} -f "SQLite" -sql '
            f'"{sql_cmd_valid}" {output_vec_valid_tmp} {vector}'
        )

        sql_cmd_learn = f"select * FROM {vector_layer_name}" f" WHERE {learn_clause}"
        output_vec_learn_name = "_".join([tile_name, "seed_" + str(seed), "learn"])
        output_vec_learn_name_tmp = "_".join(
            [tile_name, "seed_" + str(seed), "learn", "tmp"]
        )
        output_vec_learn_tmp = os.path.join(
            split_directory, output_vec_learn_name_tmp + ".sqlite"
        )
        output_vec_learn = os.path.join(
            split_directory, output_vec_learn_name + ".sqlite"
        )
        cmd_learn = (
            f"ogr2ogr -t_srs EPSG:{proj_out} -s_srs EPSG:{proj_in} "
            f"-nln {output_vec_learn_name_tmp}"
            f' -f "SQLite" -sql "{sql_cmd_learn}"'
            f" {output_vec_learn_tmp} {vector}"
        )
        if split_ground_truth:
            run(cmd_valid, logger=logger)
            # remove useless fields
            keep_fields(
                output_vec_valid_tmp,
                output_vec_valid,
                fields=fields,
                proj_in=proj_in,
                proj_out=proj_out,
                logger=logger,
            )
            os.remove(output_vec_valid_tmp)
        run(cmd_learn, logger=logger)
        # remove useless fields
        keep_fields(
            output_vec_learn_tmp,
            output_vec_learn,
            fields=fields,
            proj_in=proj_in,
            proj_out=proj_out,
            logger=logger,
        )
        os.remove(output_vec_learn_tmp)

        if split_ground_truth is False:
            shutil.copy(output_vec_learn, output_vec_valid)

        out_vectors.append(output_vec_valid)
        out_vectors.append(output_vec_learn)
    return out_vectors


def built_where_sql_exp(sample_id_to_extract: List[int], clause: str) -> str:
    """Construct the where clause for sql request.

    Parameters
    ----------
    sample_id_to_extract:
        list of ID to extract
    clause:
        "in" or "not in" clause values
    """
    if clause not in ["in", "not in"]:
        raise Exception("clause must be 'in' or 'not in'")
    sql_limit = 1000.0
    sample_id_to_extract = list(map(str, sample_id_to_extract))
    sample_id_to_extract = fut.split_list(
        sample_id_to_extract,
        nbSplit=int(math.ceil(float(len(sample_id_to_extract)) / sql_limit)),
    )
    list_fid = [f"fid {clause} ({','.join(chunk)})" for chunk in sample_id_to_extract]
    sql_exp = " AND ".join(list_fid) if "not" in clause else " OR ".join(list_fid)
    return sql_exp


def extract_maj_vote_samples(
    vec_in: str,
    vec_out: str,
    ratio_to_keep: float,
    data_field: str,
    region_field: str,
    driver_name: str = "ESRI Shapefile",
    logger=LOGGER,
):
    """Extract samples according a ratio.

    dedicated to extract samples by class according to a ratio.
    Samples are remove from vec_in and place in vec_out

    Parameters
    ----------
    vec_in : string
        path to a shapeFile (.shp)
    vec_out : string
        path to a sqlite (.sqlite)
    ratio_to_keep [float]
        percentage of samples to extract ratio_to_keep = 0.1
        mean extract 10% of each class in each regions
    data_field:
        field containing class labels
    region_field:
        field containing regions labels
    driver_name : string
        OGR driver
    """
    class_avail = vf.get_field_element(
        vec_in,
        driver_name=driver_name,
        field=data_field,
        mode="unique",
        elem_type="int",
    )
    region_avail = vf.get_field_element(
        vec_in,
        driver_name=driver_name,
        field=region_field,
        mode="unique",
        elem_type="str",
    )

    driver = ogr.GetDriverByName(driver_name)
    source = driver.Open(vec_in, 1)
    layer = source.GetLayer(0)

    sample_id_to_extract, _ = subset.get_random_poly(
        layer, data_field, class_avail, ratio_to_keep, region_field, region_avail
    )

    # Create new file with targeted FID
    fid_samples_in = built_where_sql_exp(sample_id_to_extract, clause="in")
    cmd = f"ogr2ogr -where '{fid_samples_in}' -f 'SQLite' {vec_out} {vec_in}"
    run(cmd, logger=logger)

    # remove in vec_in targeted FID
    vec_in_rm = vec_in.replace(".shp", "_tmp.shp")
    fid_samples_not_in = built_where_sql_exp(sample_id_to_extract, clause="not in")
    cmd = f"ogr2ogr -where '{fid_samples_not_in}' {vec_in_rm} {vec_in}"
    run(cmd, logger=logger)

    vf.remove_shape(vec_in.replace(".shp", ""), [".prj", ".shp", ".dbf", ".shx"])

    cmd = f"ogr2ogr {vec_in} {vec_in_rm}"
    run(cmd, logger=logger)

    vf.remove_shape(vec_in_rm.replace(".shp", ""), [".prj", ".shp", ".dbf", ".shx"])


def vector_formatting(
    tile_name: str,
    output_path: str,
    ground_truth_vec: str,
    data_field: str,
    cloud_threshold: float,
    ratio: float,
    random_seed: int,
    enable_split_ground_truth: bool,
    fusion_merge_all_validation: bool,
    runs: int,
    epsg: int,
    region_field: str,
    classif_mode: str,
    original_label_column: str,
    exterior_buffer_size: int,
    interior_buffer_size: int,
    epsilon: float,
    spatial_res,
    boundary_fusion_enabled: bool,
    merge_final_classifications: bool = False,
    merge_final_classifications_ratio: Optional[float] = None,
    region_vec: Optional[str] = None,
    working_directory: Optional[str] = None,
    logger=LOGGER,
) -> None:
    """Extract samples by class according to a ratio or a fold number.

    Parameters
    ----------
    tile_name:
        tile name
    output_path:
        iota2 output path
    ground_truth_vec:
        path to the ground truth database
    data_field:
        field into the database containing class labels
    cloud_threshold:
        cloud threshold to pick up polygons in database
    ratio:
        ratio between learning and validation samples-set
    random_seed:
        initialize the random seed
    enable_split_ground_truth:
        flag to split input database in learning and validation
        samples-set
    fusion_merge_all_validation:
        flag to merge all classifications
    runs:
        number of random learning/validation samples-set
    epsg:
        epsg code
    region_field:
        region field in region database
    classif_mode:
        sub-division of too huge region ?
    original_label_column:
        the user label column to be keep in samples files
    merge_final_classifications:
        inform if finals classifications will be merged
    merge_final_classifications_ratio:
        ratio of samples to extract by tile and by region
        in order to compute confusion matrix on classification fusion
    region_vec:
        region database
    working_directory :
        path to a working directory
    logger:
        root logger
    """
    # const
    tile_field = "tile_o"
    formatting_directory = os.path.join(output_path, "formattingVectors")
    if working_directory:
        formatting_directory = working_directory
    output_name = tile_name + ".shp"

    output = os.path.join(formatting_directory, output_name)
    features_directory = os.path.join(output_path, "features")
    cloud_vec = os.path.join(
        features_directory, tile_name, f"CloudThreshold_{cloud_threshold}.shp"
    )
    tile_env_vec = os.path.join(output_path, "envelope", f"{tile_name}.shp")

    region_field = region_field.lower()
    split_directory = os.path.join(output_path, "dataAppVal")
    final_directory = os.path.join(output_path, "final")

    if not region_vec:
        region_vec = os.path.join(output_path, "MyRegion.shp")
    if merge_final_classifications:
        wd_maj_vote = os.path.join(final_directory, "merge_final_classifications")
        if working_directory:
            wd_maj_vote = working_directory

    output_driver = "SQLite"
    if os.path.splitext(os.path.basename(output))[-1] == ".shp":
        output_driver = "ESRI Shapefile"

    work_dir = os.path.join(output_path, "formattingVectors")
    if working_directory:
        work_dir = working_directory

    work_dir = os.path.join(work_dir, tile_name)
    try:
        os.mkdir(work_dir)
    except OSError:
        logger.warning(f"{work_dir} already exists")

    # log
    logger.info(f"formatting vector for tile : {tile_name}")
    logger.debug(
        f"output : {output}\n"
        f"groundTruth : {ground_truth_vec}\n"
        f"cloud : {cloud_vec}\n"
        f"region : {region_vec}\n"
        f"tile envelope : {tile_env_vec}\n"
        f"data field : {data_field}\n"
        f"region field : {region_field}\n"
        f"ratio : {ratio}\n"
        f"seeds : {runs}\n"
        f"epsg : {epsg}\n"
        f"workingDirectory : {work_dir}"
    )

    img_ref = fut.file_search_and(
        os.path.join(features_directory, tile_name, "tmp"), True, ".tif"
    )[0]

    logger.info("launch intersection between tile's envelope and regions")
    tile_region = os.path.join(work_dir, "tileRegion_" + tile_name + ".sqlite")
    region_tile_intersection = intersect.intersectSqlites(
        tile_env_vec, region_vec, tile_region, epsg, [region_field]
    )
    if not region_tile_intersection:
        error_msg = (
            f"there is no intersections between the tile '{tile_name}' "
            f"and the region shape '{region_vec}'"
        )
        logger.critical(error_msg)
        raise Exception(error_msg)

    region_vector_name = os.path.splitext(os.path.basename(region_vec))[0]
    # Create standard mask for sampling and classification masks
    create_tile_region_masks(
        tile_region,
        region_field,
        tile_name,
        os.path.join(output_path, "shapeRegion"),
        region_vector_name,
        img_ref,
        i2_running_dir=output_path,
        classification_mode=classif_mode,
        tile_boundaries_file=tile_env_vec,
        boundary_fusion_enabled=boundary_fusion_enabled,
        logger=logger,
    )

    # Then compute classification masks for boundary
    if boundary_fusion_enabled:
        # input(img_ref)
        common_mask = fut.file_search_and(
            os.path.join(features_directory, tile_name, "tmp"), True, "Mask", ".tif"
        )[0]
        compute_distance_map(
            region_file=region_vec,
            common_mask=common_mask,
            exterior_buffer_size=exterior_buffer_size,
            interior_buffer_size=interior_buffer_size,
            epsilon=epsilon,
            spatial_res=spatial_res,
            output_path=os.path.join(output_path, "features", tile_name, "tmp"),
            masks_region_path=os.path.join(output_path, "classif", "MASK"),
            tile=tile_name,
            region_field=region_field,
            working_directory=working_directory,
        )

    logger.info("launch intersection between tile's envelopeRegion and groundTruth")
    tile_region_ground_truth = os.path.join(
        work_dir, "tileRegionGroundTruth_" + tile_name + ".sqlite"
    )
    fields_to_keep = [data_field, region_field.lower(), original_label_column]

    if (
        intersect.intersectSqlites(
            tile_region,
            ground_truth_vec,
            tile_region_ground_truth,
            epsg,
            fields_to_keep,
            data_base2_FID_field_name="originfid",
        )
        is False
    ):
        warning_msg = (
            f"there si no intersections between the tile "
            f"'{tile_name}' and the ground truth '{ground_truth_vec}'"
        )
        logger.warning(warning_msg)
        return None
    logger.info("remove un-usable samples")
    intersect.intersectSqlites(
        tile_region_ground_truth,
        cloud_vec,
        output,
        epsg,
        fields_to_keep + ["originfid"],
    )

    os.remove(tile_region)
    os.remove(tile_region_ground_truth)

    if merge_final_classifications and fusion_merge_all_validation is False:
        maj_vote_sample_tile_name = f"{tile_name}_majvote.sqlite"
        maj_vote_sample_tile = os.path.join(wd_maj_vote, maj_vote_sample_tile_name)
        extract_maj_vote_samples(
            output,
            maj_vote_sample_tile,
            merge_final_classifications_ratio,
            data_field.lower(),
            region_field.lower(),
            driver_name="ESRI Shapefile",
            logger=logger,
        )

    logger.info(f"split {output} in {runs} subsets with the ratio {ratio}")
    subset.split_in_subsets(
        output,
        data_field.lower(),
        region_field.lower(),
        ratio,
        runs,
        output_driver,
        split_groundtruth=enable_split_ground_truth,
        random_seed=random_seed,
    )

    add_field(output, tile_field, tile_name, value_type=str, driver_name=output_driver)

    split_dir = split_directory
    if working_directory:
        split_dir = work_dir

    # splits by learning and validation sets (use in validations steps)
    output_splits = split_by_sets(
        output,
        runs,
        split_dir,
        epsg,
        epsg,
        tile_name,
        split_ground_truth=enable_split_ground_truth,
        logger=logger,
    )
    check_validation_db_integrity(output_splits, logger=logger)

    if working_directory:
        if output_driver == "SQLite":
            shutil.copy(output, os.path.join(output_path, "formattingVectors"))
            os.remove(output)

        elif output_driver == "ESRI Shapefile":
            vf.cp_shape_file(
                output.replace(".shp", ""),
                os.path.join(output_path, "formattingVectors"),
                [".prj", ".shp", ".dbf", ".shx"],
                True,
            )
            vf.remove_shape(
                output.replace(".shp", ""), [".prj", ".shp", ".dbf", ".shx"]
            )

        for current_split in output_splits:
            shutil.copy(current_split, os.path.join(output_path, "dataAppVal"))
            os.remove(current_split)

        if merge_final_classifications and fusion_merge_all_validation is False:
            shutil.copy(
                maj_vote_sample_tile,
                os.path.join(final_directory, "merge_final_classifications"),
            )
    return None
