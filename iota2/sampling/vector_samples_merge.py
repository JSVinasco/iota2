#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""The vector Samples Merge module"""
import argparse
import logging
import os
import shutil
from collections import OrderedDict
from logging import Logger
from typing import Dict, List, Tuple, Union

from osgeo import ogr, osr

import iota2.common.i2_constants as i2_const
from iota2.common import file_utils as fu
from iota2.vector_tools import vector_functions as vf
from iota2.vector_tools.merge_files import merge_wide_sqlite

I2_CONST = i2_const.Iota2Constants()
LOGGER = logging.getLogger("distributed.worker")


def split_vectors_by_regions(path_list: List[str]) -> List[str]:
    """
    split vectors by regions
    IN:
        path_list [list(str)]: list of path
    OUT:
        list[str] : the list of vector by region
    """
    regions_position = 2
    seed_position = 3

    output = []
    seed_vector_ = fu.sort_by_first_elem(
        [(os.path.split(vec)[-1].split("_")[seed_position], vec) for vec in path_list]
    )
    seed_vector = [seed_vector for seed, seed_vector in seed_vector_]

    for current_seed in seed_vector:
        region_vector = [
            (os.path.split(vec)[-1].split("_")[regions_position], vec)
            for vec in current_seed
        ]
        region_vector_sorted_ = fu.sort_by_first_elem(region_vector)
        region_vector_sorted = [
            r_vectors for region, r_vectors in region_vector_sorted_
        ]
        for seed_vect_region in region_vector_sorted:
            output.append(seed_vect_region)
    return output


def tile_vectors_to_models(iota2_learning_samples_dir: str) -> List[str]:
    """
    use to feed vector_samples_merge function

    Parameters
    ----------
    iota2_learning_samples_dir : string
        path to "learningSamples" iota² directory
    sep_sar_opt : bool
        flag use to inform if SAR data has to be computed separately

    Return
    ------
    list
        list of list of vectors to be merged to form a vector by model
    """
    vectors = fu.file_search_and(
        iota2_learning_samples_dir, True, "Samples_learn.sqlite"
    )
    vectors_sar = fu.file_search_and(
        iota2_learning_samples_dir, True, "Samples_SAR_learn.sqlite"
    )

    vect_to_model = split_vectors_by_regions(vectors) + split_vectors_by_regions(
        vectors_sar
    )
    return vect_to_model


def check_duplicates(sqlite_file: str, logger: Logger = LOGGER) -> None:
    """
    check_duplicates
    Parameters
    ----------
    sqlite_file : string
        the input sqlite file
    Return
    ------
    None
    """
    import sqlite3 as lite

    conn = lite.connect(sqlite_file)
    cursor = conn.cursor()
    sql_clause = (
        "select * from output where ogc_fid in (select min(ogc_fid)"
        " from output group by GEOMETRY having count(*) >= 2);"
    )
    cursor.execute(sql_clause)
    results = cursor.fetchall()

    if results:
        sql_clause = (
            "delete from output where ogc_fid in (select min(ogc_fid)"
            " from output group by GEOMETRY having count(*) >= 2);"
        )
        cursor.execute(sql_clause)
        conn.commit()
        logger.warning(f"{len(results)} were removed in {sqlite_file}")


def clean_repo(output_path: str, logger: Logger = LOGGER):
    """
    remove from the directory learningSamples all unnecessary files
    """
    learning_content = os.listdir(output_path + "/learningSamples")
    for c_content in learning_content:
        c_path = output_path + "/learningSamples/" + c_content
        if os.path.isdir(c_path):
            try:
                shutil.rmtree(c_path)
            except OSError:
                logger.debug(f"{c_path} does not exists")


def is_sar(path: str, sar_pos: int = 5) -> bool:
    """
    Check if the input image is a SAR product
    Parameters
    ----------
    path: string
        the input image
    Return
    bool
    ------
    """
    return os.path.basename(path).split("_")[sar_pos] == "SAR"


def sort_s1_fields(s1_fields: List[Tuple]) -> List[Tuple]:
    """ """
    s1_sorted_fields = []
    # looking for DES vv fields
    des_vv = [field for field in s1_fields if "desvv" in field[0]]
    s1_sorted_fields += sorted(des_vv, key=lambda x: x[-1])
    # looking for DES vh fields
    des_vh = [field for field in s1_fields if "desvh" in field[0]]
    s1_sorted_fields += sorted(des_vh, key=lambda x: x[-1])
    # looking for ASC vv fields
    asc_vv = [field for field in s1_fields if "ascvv" in field[0]]
    s1_sorted_fields += sorted(asc_vv, key=lambda x: x[-1])
    # looking for ASC vh fields
    asc_vh = [field for field in s1_fields if "ascvh" in field[0]]
    s1_sorted_fields += sorted(asc_vh, key=lambda x: x[-1])
    return s1_sorted_fields


def build_raw_db(
    output_dummy: str,
    vector_list: List[str],
    sensors_order: List[str],
    expected_no_features_cols: List[str],
    logger=LOGGER,
):
    """ """
    FieldType = Tuple[str, int, str, int, str, Union[int, str]]
    no_features_fields: List[FieldType] = []
    features_fields: Dict[str, List[FieldType]] = OrderedDict()
    masks: Dict[str, List[FieldType]] = OrderedDict()

    for sensor in sensors_order:
        features_fields[sensor] = []
        masks[sensor] = []

    for vector in vector_list:
        # fields = vf.get_fields(vector, "SQLite")
        driver: ogr.Driver = ogr.GetDriverByName("SQLite")
        ds: ogr.DataSource = driver.Open(vector, 0)
        lyr: ogr.Layer = ds.GetLayer()
        lyr_dfn: ogr.FeatureDefn = lyr.GetLayerDefn()

        for field_num in range(lyr_dfn.GetFieldCount()):
            field_name = lyr_dfn.GetFieldDefn(field_num).GetName()
            try:
                sensors_name, feat, date = field_name.split("_")
                new_field = (field_name, int(date))
                if "mask" in feat.lower():
                    if new_field not in masks[sensors_name]:
                        masks[sensors_name].append(new_field)
                else:
                    if new_field not in features_fields[sensors_name]:
                        features_fields[sensors_name].append(new_field)
            except ValueError:
                new_field = (field_name, "")
                if (
                    new_field not in no_features_fields
                    and field_name in expected_no_features_cols
                ):
                    no_features_fields.append(new_field)

    # features_fields = sorted(features_fields, key=lambda x: x[-1])
    all_fields = no_features_fields
    for sensor_name, fields in features_fields.items():
        if sensor_name == "sentinel1":
            all_fields += sort_s1_fields(fields)
        else:
            all_fields += sorted(fields, key=lambda x: x[-1])
    for sensor_name, fields in masks.items():
        all_fields += sorted(fields, key=lambda x: x[-1])

    all_fields_list = [cfield for cfield, _ in all_fields]

    merge_wide_sqlite(
        vector_list,
        output_dummy,
        fid_col="fid",
        cols_order=all_fields_list,
        nan_values=None,
        specific_nans_rules={"(mask)": I2_CONST.i2_missing_dates_no_data_mask},
        logger=logger,
    )


def vector_samples_merge(
    vector_list: List[str],
    output_path: str,
    user_label_field: str,
    deep_learning_db: bool = False,
    sensors_order: List[str] = [],
    logger: Logger = LOGGER,
) -> None:
    """

    Parameters
    ----------
    vector_list : List[string]
    output_path : string
    Return
    ------

    """
    regions_position = 2
    seed_position = 3

    clean_repo(output_path, logger=logger)

    current_model = os.path.split(vector_list[0])[-1].split("_")[regions_position]
    seed = (
        os.path.split(vector_list[0])[-1].split("_")[seed_position].replace("seed", "")
    )

    shape_out_name = f"Samples_region_{current_model}_seed{seed}_learn"

    if is_sar(vector_list[0]):
        shape_out_name = shape_out_name + "_SAR"

    logger.info(f"Vectors to merge in {shape_out_name}")
    logger.info("\n".join(vector_list))

    vector_list = list(filter(os.path.exists, vector_list))

    if deep_learning_db:
        output_hdf_dir = os.path.join(output_path, "learningSamples")
        output_hdf = os.path.join(
            output_hdf_dir, f"{shape_out_name}.{I2_CONST.i2_database_ext}"
        )
        build_raw_db(
            output_hdf,
            vector_list,
            sensors_order,
            [I2_CONST.re_encoding_label_name, user_label_field, "originfid"],
        )

    else:
        vf.merge_sqlite(
            shape_out_name, os.path.join(output_path, "learningSamples"), vector_list
        )

        check_duplicates(
            os.path.join(
                os.path.join(output_path, "learningSamples"), shape_out_name + ".sqlite"
            ),
            logger=logger,
        )
