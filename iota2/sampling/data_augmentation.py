"""Data augmentation iota2 module."""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import csv
import logging
import os
import shutil
import sqlite3
from collections import Counter
from typing import Dict, List, Optional

import pandas as pd
from osgeo import ogr

from iota2.common import file_utils as fut
from iota2.common.otb_app_bank import CreateSampleAugmentationApplication
from iota2.vector_tools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


def get_user_samples_management(csv_path: str) -> List[List[str]]:
    """Parse CSV file and return rules to fill samples set.

    Parameters
    ----------
    csv_path :
        path to a csv file

    Example
    -------
    >>> cat /MyFile.csv
            1,2,4,2

    Mean:

    +--------+-------------+------------+----------+
    | source | destination | class name | quantity |
    +========+=============+============+==========+
    |   1    |      2      |      4     |     2    |
    +--------+-------------+------------+----------+

    2 samples of class 4 will be added from model 1 to 2
    """
    delimiter = fut.detect_csv_delimiter(csv_path)

    with open(csv_path, "r") as csvfile:
        csv_reader = csv.reader(csvfile, delimiter=delimiter)
        extraction_rules = list(csv_reader)
    return extraction_rules


def get_samples_from_model_name(model_name, samples_set, logger=LOGGER):
    """Use to get the sample file by the model name.

    Parameters
    ----------
    model_name : string
        the model's name
    samples_set: list
        paths to samples
    logger : logging object
        root logger

    Return
    ------
    string
        path to the targeted sample-set.
    """
    model_pos = 2
    sample = [
        path
        for path in samples_set
        if os.path.basename(path).split("_")[model_pos] == model_name
    ]

    if len(sample) > 1:
        logger.error(
            (
                f"Too many files detected in {os.path.split(samples_set[0])[0]} "
                f"for the model {model_name}"
            )
        )
        raise Exception(
            (
                "ERROR in managementSamples.py, too many sample "
                "files detected for a given model name"
            )
        )
    if not sample:
        logger.warning(f"Model {model_name} not found")
        out_samples = None
    else:
        out_samples = sample[0]

    return out_samples


def count_class_in_sqlite(
    source_samples, data_field, class_name, table_name="output", logger=LOGGER
):
    """Count a SQLite file, occurency of a value.

    Parameters
    ----------
    source_samples : string
        Path to a SQLite file
    data_field : string
        Field's name
    class_name : string
        Class name
    table_name : string
        Table's name
    logger : logging object
        root logger

    Return
    ------
    int
        occurrence of the class into the SQLite file
    """
    conn = sqlite3.connect(source_samples)
    cursor = conn.cursor()
    sql = f"select * from {table_name} where {data_field}={class_name}"
    cursor.execute(sql)
    features_number = cursor.fetchall()
    features_number = len(features_number)
    if not features_number:
        logger.warning(
            f"There is no class with the label {class_name} in {source_samples}"
        )
        features_number = 0
    return features_number


def get_projection(vector_file, driver_name="SQLite"):
    """Return the projection of a vector file.

    Parameters
    ----------
    vector_file : string
        Path to a vector file
    driver_name : string
        Ogr driver's name

    Return
    ------
    int
        EPSG's code
    """
    driver = ogr.GetDriverByName(driver_name)
    vector = driver.Open(vector_file)
    layer = vector.GetLayer()
    spatial_ref = layer.GetSpatialRef()
    projection_code = spatial_ref.GetAttrValue("AUTHORITY", 1)
    return projection_code


def get_region_from_sample_name(samples):
    """Get the model's name from samples's path file."""
    region_pos = 2
    return os.path.basename(samples).split("_")[region_pos]


def samples_augmentation_counter(
    class_count, mode, min_number=None, by_class=None, labels_table=None
):
    """Compute how many samples must be add into the sample-set according to user's request.

    Parameters
    ----------
    class_count : dict
        count by class
    mode : string
        minNumber/balance/byClass
    min_number : int
        vector should have at least class samples
    by_class : string
        csv path

    Example
    -------
    >>> class_count = {51: 147, 11: 76, 12: 37, 42: 19}
    >>>
    >>> print samples_augmentation_counter(class_count, mode="min_number", min_number=120)
    >>> {42: 101, 11: 44, 12: 83}
    >>>
    >>> print samples_augmentation_counter(class_count, mode="balance")
    >>> {42: 128, 11: 71, 12: 110}
    >>>
    >>> print samples_augmentation_counter(class_count, mode="byClass",
    >>>                                  min_number=None, by_class="/pathTo/MyFile.csv")
    >>> {42: 11, 51: 33, 12: 1}

    If

    >>> cat /pathTo/MyFile.csv
        51,180
        11,70
        12,38
        42,30

    Return
    ------
    dict
        by class, the number of samples to add in the samples set.
    """
    augmented_class = {}
    if mode.lower() == "minnumber":
        for class_name, count in list(class_count.items()):
            if count < min_number:
                augmented_class[class_name] = min_number - count

    elif mode.lower() == "balance":
        max_class_count = class_count[
            max(class_count, key=lambda key: class_count[key])
        ]
        for class_name, count in list(class_count.items()):
            if count < max_class_count:
                augmented_class[class_name] = max_class_count - count

    elif mode.lower() == "byclass":

        delimiter = fut.detect_csv_delimiter(by_class)

        with open(by_class, "r") as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=delimiter)
            for class_name, class_samples in csv_reader:
                class_name = int(class_name)
                class_samples = int(class_samples)
                if labels_table:
                    class_name = labels_table[class_name]
                if class_samples > class_count[class_name]:
                    augmented_class[class_name] = (
                        class_samples - class_count[class_name]
                    )
    return augmented_class


def do_augmentation(
    samples,
    class_augmentation,
    strategy,
    field,
    excluded_fields=[],
    j_std_factor: Optional[float] = None,
    s_neighbors: Optional[int] = None,
    working_directory: Optional[str] = None,
    random_seed: Optional[int] = None,
    logger=LOGGER,
):
    """Perform data augmentation according to input parameters.

    Parameters
    ----------
    samples : string
        path to the set of samples to augment (OGR geometries must be 'POINT')
    class_augmentation : dict
        number of new samples to compute by class
    strategy : string
        which method to use in order to perform data augmentation (replicate/jitter/smote)
    field : string
        data's field
    excluded_fields : list
        do not consider these fields to perform data augmentation
    j_std_factor : float
        Factor for dividing the standard deviation of each feature
    s_neighbors : int
        Number of nearest neighbors (smote's method)
    random_seed
        Set the random seed
    working_directory : string
        path to a working directory

    Note
    ----
    This function use the OTB's application **SampleAugmentation**,
    more documentation
    `here <http://www.orfeo-toolbox.org/Applications/SampleAugmentation.html>`_
    """
    samples_dir_o, samples_name = os.path.split(samples)
    samples_dir = samples_dir_o
    if working_directory:
        samples_dir = working_directory
        shutil.copy(samples, samples_dir)
    samples = os.path.join(samples_dir, samples_name)

    augmented_files = []
    for class_name, class_samples_augmentation in list(class_augmentation.items()):
        logger.info(
            f"{class_samples_augmentation} samples of class {class_name} will "
            f"be generated by data augmentation ({strategy} method) in {samples}"
        )
        sample_name_augmented = "_".join(
            [
                os.path.splitext(samples_name)[0],
                "aug_class_{}.sqlite".format(class_name),
            ]
        )
        output_sample_augmented = os.path.join(samples_dir, sample_name_augmented)
        parameters = {
            "in": samples,
            "field": field,
            "out": output_sample_augmented,
            "label": class_name,
            "strategy": strategy,
            "samples": class_samples_augmentation,
        }
        if random_seed:
            parameters["seed"] = int(random_seed)
        if excluded_fields:
            parameters["exclude"] = excluded_fields
        if strategy.lower() == "jitter":
            parameters["strategy.jitter.stdfactor"] = j_std_factor
        elif strategy.lower() == "smote":
            parameters["strategy.smote.neighbors"] = s_neighbors

        augmentation_application = CreateSampleAugmentationApplication(parameters)
        augmentation_application.ExecuteAndWriteOutput()
        logger.debug(
            "{} samples of class {} were added in {}".format(
                class_samples_augmentation, class_name, samples
            )
        )
        augmented_files.append(output_sample_augmented)

    output_vector = os.path.join(
        samples_dir, "_".join([os.path.splitext(samples_name)[0], "augmented.sqlite"])
    )

    vf.merge_sqlite(
        "_".join([os.path.splitext(samples_name)[0], "augmented"]),
        samples_dir,
        [samples] + augmented_files,
    )
    logger.info(f"Every data augmentation done in {samples}")
    shutil.move(output_vector, os.path.join(samples_dir_o, samples_name))

    # clean-up
    for augmented_file in augmented_files:
        os.remove(augmented_file)


def get_fields_type(vector_file):
    """Get field's type.

    Parameters
    ----------
    vector_file : string
        path to a shape file
    Return
    ------
    dict
        dictionary with field's name as key and type as value

    Example
    -------
    >>> print get_fields_type("/path/to/MyVector.shp")
        {'CODE': 'integer64', 'id': 'integer64', 'Area': 'integer64'}
    """
    data_source = ogr.Open(vector_file)
    da_layer = data_source.GetLayer(0)
    layer_definition = da_layer.GetLayerDefn()
    field_dict = {}
    for i in range(layer_definition.GetFieldCount()):
        field_name = layer_definition.GetFieldDefn(i).GetName()
        field_type_code = layer_definition.GetFieldDefn(i).GetType()
        field_type = layer_definition.GetFieldDefn(i).GetFieldTypeName(field_type_code)
        field_dict[field_name] = field_type.lower()
    return field_dict


def data_augmentation_synthetic(
    samples,
    ground_truth,
    data_field,
    strategies,
    labels_table: Optional[Dict] = None,
    working_directory: Optional[str] = None,
    random_seed: Optional[int] = None,
    logger=LOGGER,
):
    """Compute how many samples should be add in the sample set and launch data augmentation method.

    Parameters
    ----------
    samples : string
        path to a vector file to augment samples
    ground_truth : string
        path to the original ground truth vector file, in order to list interger / float fields
    data_field : string
        data field's name in samples
    strategies : dict
        dictionary
    random_seed
        Set the random seed
    working_directory : string
        path to a working directory
    """
    if (
        get_region_from_sample_name(samples) in strategies["target_models"]
        or "all" in strategies["target_models"]
    ):
        class_count = Counter(
            vf.get_field_element(
                samples,
                driver_name="SQLite",
                field=data_field,
                mode="all",
                elem_type="int",
            )
        )
        class_augmentation = samples_augmentation_counter(
            class_count,
            mode=strategies["samples.strategy"],
            min_number=strategies.get("samples.strategy.minNumber", None),
            by_class=strategies.get("samples.strategy.byClass", None),
            labels_table=labels_table,
        )

        fields_types = get_fields_type(ground_truth)

        excluded_fields_origin = [
            field_name.lower()
            for field_name, field_type in list(fields_types.items())
            if "int" in field_type or "flaot" in field_type
        ]
        samples_fields = vf.get_all_fields_in_shape(samples, driver="SQLite")
        excluded_fields = list(set(excluded_fields_origin).intersection(samples_fields))
        excluded_fields.append("originfid")

        do_augmentation(
            samples,
            class_augmentation,
            strategy=strategies["strategy"],
            field=data_field,
            excluded_fields=excluded_fields,
            j_std_factor=strategies.get("strategy.jitter.stdfactor", None),
            s_neighbors=strategies.get("strategy.smote.neighbors", None),
            working_directory=working_directory,
            random_seed=random_seed,
            logger=logger,
        )


def do_copy(
    source_samples,
    destination_samples,
    class_name,
    data_field,
    extract_quantity,
    PRIM_KEY="ogc_fid",
    source_samples_table_name="output",
    logger=LOGGER,
):
    """Copy samples to one subset to an other one using a SQLite query.

    Parameters
    ----------
    source_samples : string
        path to the sample-set to extract features
    destination_samples : string
        path to the sample-set to fill-up features
    class_name : string
        class name
    data_field : string
        data's field
    extract_quantity : int
        number of samples to extract
    PRIM_KEY : string
        OGR primary key
    source_samples_table_name : string
        input vector file table's name
    logger : logging object
        root logger
    """
    # open destination sqlite and put it in dataframe
    conn = sqlite3.connect(destination_samples)
    df_destination = pd.read_sql_query(
        f"SELECT * FROM {source_samples_table_name}", conn
    )
    # Same for source
    conx = sqlite3.connect(source_samples)
    df_source = pd.read_sql_query(
        (
            f"SELECT * FROM {source_samples_table_name} WHERE "
            f"{data_field.lower()}={class_name} ORDER BY RANDOM() LIMIT {extract_quantity}"
        ),
        conx,
    )

    df_final = pd.concat([df_destination, df_source], axis="index", ignore_index=True)
    # repack
    if PRIM_KEY in df_final:
        df_final[PRIM_KEY] = list(range(1, len(df_final) + 1))
    # Replace and update the destination sqlite file
    df_final.to_sql(source_samples_table_name, conn, if_exists="replace", index=False)


def data_augmentation_by_copy(
    data_field,
    csv_path,
    samples_set,
    labels_table=None,
    working_directory=None,
    PRIM_KEY="ogc_fid",
    source_samples_table_name="output",
    logger=LOGGER,
):
    """Use to copy samples between models.

    Parameters
    ----------
    data_field : string
        Data's field in vectors
    csv_path : string
        Absolute path to a csv file which describe samples movements
        check :py:meth:`get_user_samples_management` function to know about
        format
    samples_set : list
        Absolute paths to all samples set
    working_directory : string
        Path to a working directory
    PRIM_KEY : string
        OGR primary key
    source_samples_table_name : string
        input vector file table's name
    logger : logging object
        root logger
    """
    if working_directory:
        origin_dir = []
        for ind, sample in enumerate(samples_set):
            shutil.copy(sample, working_directory)
            origin_dir.append(os.path.split(sample)[0])
            samples_set[ind] = os.path.join(
                working_directory, os.path.split(sample)[-1]
            )

    extraction_rules = get_user_samples_management(csv_path)
    print(extraction_rules)
    if labels_table:
        new_extraction_rules = []
        for model_src, model_dst, klass, nb_samples in extraction_rules:
            new_extraction_rules.append(
                [model_src, model_dst, labels_table[int(klass)], nb_samples]
            )
        extraction_rules = new_extraction_rules

    for src_model, dst_model, class_name, extract_quantity in extraction_rules:
        source_samples = get_samples_from_model_name(
            src_model, samples_set, logger=logger
        )
        dst_samples = get_samples_from_model_name(dst_model, samples_set, logger=logger)
        if not source_samples or not dst_samples:
            continue
        if source_samples == dst_samples:
            continue
        if extract_quantity == "-1":
            extract_quantity = count_class_in_sqlite(
                source_samples, data_field, class_name, logger=logger
            )
        if extract_quantity == 0:
            continue

        do_copy(
            source_samples,
            dst_samples,
            class_name,
            data_field,
            extract_quantity,
            PRIM_KEY,
            source_samples_table_name,
        )

    if working_directory:
        for o_dir, sample_aug in zip(origin_dir, samples_set):
            os.remove(os.path.join(o_dir, os.path.split(sample_aug)[-1]))
            shutil.copy(sample_aug, o_dir)
