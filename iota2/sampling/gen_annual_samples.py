#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import argparse
import logging
import os
import random

import numpy as np
import otbApplication as otb
from osgeo import gdal, ogr, osr
from osgeo.gdalconst import *

from iota2.common import file_utils as fu
from iota2.vector_tools import vector_functions as vf
from iota2.vector_tools.add_field import add_field

LOGGER = logging.getLogger("distributed.worker")


def coordParse(s):
    try:
        x, y = list(map(float, s.split(", ")))
        return (x, y)
    except:
        raise argparse.ArgumentTypeError("Coordinates must be x, y")


def raster2array(rasterfn):
    raster = gdal.Open(rasterfn)
    band = raster.GetRasterBand(1)
    return band.ReadAsArray()


def pixCoordinates(x, y, x_origin, y_origin, sizeX, sizeY):
    return ((x + 1) * sizeX + x_origin) - sizeX * 0.5, (
        (y) * sizeY + y_origin
    ) - sizeX * 0.5


def add_origin_fields(
    origin_shape, output_layer, region_field_name, runs, origin_driver="ESRI Shapefile"
):
    """
    usage add field definition from origin_shape to output_layer (except reiong field)

    origin_shape [string] path to a vector file
    output_layer [OGR layer object] output layer
    """

    # TODO ajouter les seeds_ au champs à ne pas rajouter ?
    driver = ogr.GetDriverByName(origin_driver)
    source = driver.Open(origin_shape, 0)
    layer = source.GetLayer()
    layerDefinition = layer.GetLayerDefn()

    output_layers_fields = [
        output_layer.GetLayerDefn().GetFieldDefn(i).GetName()
        for i in range(output_layer.GetLayerDefn().GetFieldCount())
    ]
    output_layers_fields.append(region_field_name)
    for run in range(runs):
        output_layers_fields.append("seed_" + str(run))

    for i in range(layerDefinition.GetFieldCount()):
        fieldName = layerDefinition.GetFieldDefn(i).GetName()
        field_type_code = layerDefinition.GetFieldDefn(i).GetType()
        fieldType = layerDefinition.GetFieldDefn(i).GetFieldTypeName(field_type_code)
        fieldWidth = layerDefinition.GetFieldDefn(i).GetWidth()
        GetPrecision = layerDefinition.GetFieldDefn(i).GetPrecision()

        if fieldName not in output_layers_fields:
            output_layers_fields.append(fieldName)
            output_layer.CreateField(layerDefinition.GetFieldDefn(i))


def genAnnualShapePoints(
    coord,
    gdalDriver,
    workingDirectory,
    rasterResolution,
    classToKeep,
    dataField,
    tile,
    validity_threshold,
    validityRaster,
    classificationRaster,
    masks,
    inlearningShape,
    outlearningShape,
    epsg,
    region_field_name,
    runs,
    annu_repartition,
    logger=LOGGER,
):

    # Const
    region_pos = 2  # in mask name if splited by '_'
    learn_flag = "learn"
    undetermined_flag = "XXXX"

    tile_pos = 0
    currentTile = os.path.splitext(os.path.basename(inlearningShape))[0]
    classifName = currentTile + "_Classif.tif"

    # check HPC mode
    try:
        PathWd = os.environ["TMPDIR"]
    except:
        PathWd = None
    projection = int(epsg)

    vector_regions = []
    add = 0

    for current_seed in range(runs):

        for currentMask in masks:
            currentRegion = os.path.split(currentMask)[-1].split("_")[region_pos]
            vector_region = os.path.join(
                workingDirectory,
                "Annual_"
                + currentTile
                + "_region_"
                + currentRegion
                + "_seed_"
                + str(current_seed)
                + ".sqlite",
            )
            vector_regions.append(vector_region)
            rasterRdy = (
                workingDirectory
                + "/"
                + classifName.replace(
                    ".tif",
                    "_RDY_"
                    + str(currentRegion)
                    + "_seed_"
                    + str(current_seed)
                    + ".tif",
                )
            )

            mapReg = otb.Registry.CreateApplication("ClassificationMapRegularization")
            mapReg.SetParameterString("io.in", classificationRaster)
            mapReg.SetParameterString("ip.undecidedlabel", "0")
            mapReg.Execute()

            useless = otb.Registry.CreateApplication("BandMath")
            useless.SetParameterString("exp", "im1b1")
            useless.SetParameterStringList("il", [validityRaster])
            useless.SetParameterString("ram", "10000")
            useless.Execute()

            uselessMask = otb.Registry.CreateApplication("BandMath")
            uselessMask.SetParameterString("exp", "im1b1")
            uselessMask.SetParameterStringList("il", [currentMask])
            uselessMask.SetParameterString("ram", "10000")
            uselessMask.Execute()

            valid = otb.Registry.CreateApplication("BandMath")
            valid.SetParameterString(
                "exp", "im1b1>" + str(validity_threshold) + "?im2b1:0"
            )
            valid.AddImageToParameterInputImageList(
                "il", useless.GetParameterOutputImage("out")
            )
            valid.AddImageToParameterInputImageList(
                "il", mapReg.GetParameterOutputImage("io.out")
            )
            valid.SetParameterString("ram", "10000")
            valid.Execute()

            rdy = otb.Registry.CreateApplication("BandMath")
            rdy.SetParameterString("exp", "im1b1*(im2b1>=1?1:0)")
            rdy.AddImageToParameterInputImageList(
                "il", valid.GetParameterOutputImage("out")
            )
            rdy.AddImageToParameterInputImageList(
                "il", uselessMask.GetParameterOutputImage("out")
            )
            rdy.SetParameterString(
                "out",
                rasterRdy
                + "?&streaming:type=stripped&streaming:sizemode=nbsplits&streaming:sizevalue=10",
            )
            rdy.SetParameterOutputImagePixelType("out", otb.ImagePixelType_uint8)
            rdy.ExecuteAndWriteOutput()

            rasterArray = raster2array(rasterRdy)
            rasterFile = gdal.Open(rasterRdy)
            x_origin, y_origin = (
                rasterFile.GetGeoTransform()[0],
                rasterFile.GetGeoTransform()[3],
            )
            sizeX, sizeY = (
                rasterFile.GetGeoTransform()[1],
                rasterFile.GetGeoTransform()[5],
            )

            driver = ogr.GetDriverByName(gdalDriver)
            if os.path.exists(vector_region):
                driver.DeleteDataSource(vector_region)

            data_source = driver.CreateDataSource(vector_region)

            srs = osr.SpatialReference()
            srs.ImportFromEPSG(projection)

            layerName = "output"  # layerName
            layerOUT = data_source.CreateLayer(layerName, srs, ogr.wkbPoint)

            add_origin_fields(inlearningShape, layerOUT, region_field_name, runs)

            for currentVal in classToKeep:
                try:
                    nbSamples = annu_repartition[str(currentVal)][currentRegion][
                        current_seed
                    ]
                except:
                    logger.info(
                        "class : {} does not exists in {} at seed {} in region {}".format(
                            currentVal, inlearningShape, current_seed, currentRegion
                        )
                    )
                    continue
                Y, X = np.where(rasterArray == int(currentVal))
                XYcoordinates = []
                for y, x in zip(Y, X):
                    X_c, Y_c = pixCoordinates(x, y, x_origin, y_origin, sizeX, sizeY)
                    XYcoordinates.append((X_c, Y_c))
                if nbSamples > len(XYcoordinates):
                    nbSamples = len(XYcoordinates)
                for Xc, Yc in random.sample(
                    XYcoordinates, nbSamples
                ):  # "0" for nbSamples already manage ?
                    if coord and (Xc, Yc) not in coord:
                        feature = ogr.Feature(layerOUT.GetLayerDefn())
                        feature.SetField(dataField, int(currentVal))
                        wkt = "POINT(%f %f)" % (Xc, Yc)
                        point = ogr.CreateGeometryFromWkt(wkt)
                        feature.SetGeometry(point)
                        layerOUT.CreateFeature(feature)
                        feature.Destroy()
                        add += 1

            data_source.Destroy()
            os.remove(rasterRdy)
            layerOUT = None

            # Add region column and value
            add_field(
                vector_region,
                region_field_name,
                str(currentRegion),
                value_type=str,
                driver_name="SQLite",
            )

            # Add seed columns and value
            for run in range(runs):
                if run == current_seed:
                    add_field(
                        vector_region,
                        "seed_" + str(run),
                        learn_flag,
                        value_type=str,
                        driver_name="SQLite",
                    )
                else:
                    add_field(
                        vector_region,
                        "seed_" + str(run),
                        undetermined_flag,
                        value_type=str,
                        driver_name="SQLite",
                    )

    outlearningShape_name = os.path.splitext(os.path.split(outlearningShape)[-1])[0]
    outlearningShape_dir = os.path.split(outlearningShape)[0]

    vf.merge_sqlite(outlearningShape_name, outlearningShape_dir, vector_regions)

    for vec in vector_regions:
        os.remove(vec)

    if add == 0:
        return False
    else:
        return True
