#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   vector tools
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Merge S2 theia format duplicates dates.

This module is dedicated to detect and merge all
Sentinel-2 acquisitions (THEIA's format) that were taken
on the same day.

If the directory in_s2_dir/T31PGM contains two acquisitions :
'SENTINEL2B_20180421-101701-205_L2A_T31PGM_C_V2-2' and
'SENTINEL2B_20180421-101755-322_L2A_T31PGM_C_V2-2'
then a new directory will be created as
'out_s2_dir/T31PGM/SENTINEL2B_20180421-xxxxxx-xxx_L2A_T31PGM_C_V2-2'

The reflectance we are trying to merge are
B2, B3, B4, B5, B6, B7, B8, B8A, B11, B12, and STACK if exists
only with the the slope correction (FRE)
and masks CLM_R1.tif, EDG_R1.tif, SAT_R1.tif
"""

import argparse
import logging
import sys
from itertools import product
from pathlib import Path
from typing import Dict, List, Tuple

import numpy as np
import rasterio

from iota2.common.file_utils import sort_by_first_elem
from iota2.common.otb_app_bank import (CreateBandMathApplication,
                                       CreateBandMathXApplication)


def date_from_s2_acquisition(dir_name: str) -> str:
    """Exctract date as string in s2 dir name."""
    return dir_name.split("_")[1].split("-")[0]


def detect_duplicated_s2(
        s2_dir: Path) -> Dict[str, List[Tuple[str, List[Path]]]]:
    """Detect same date acquisitions in a iota2 compatible s2_path."""
    tiles = Path.iterdir(s2_dir)
    prod_container = {}
    for tile in tiles:
        tile_name = tile.name
        prod_container[tile_name] = []
        s2_tiled_acquisitions = Path.iterdir(tile)
        for s2_tiled_acquisition in s2_tiled_acquisitions:
            date = date_from_s2_acquisition(s2_tiled_acquisition.name)
            prod_container[tile_name].append((date, s2_tiled_acquisition))
        prod_container[tile_name] = list(
            filter(lambda x: len(x[1]) > 1,
                   sort_by_first_elem(prod_container[tile_name])))
    return prod_container


def merge_refl(s2_paths, output_acquisition_dir, ref_file_name_ind, old_date,
               new_date, no_data_value, available_ram) -> List[Path]:
    """Merge Sentinel-2 reflectance if they are found.

    Otherwise, print a warning message.

    the reflectance we are trying to merge are
    B2, B3, B4, B5, B6, B7, B8, B8A, B11, B12, and STACK if exists
    only with the the slope correction (FRE)

    Parameters
    ----------
    s2_paths
        list of acquisition directory to merge
    output_acquisition_dir
        output acquisition directory
    ref_file_name_ind
        ref index in s2_path to build output names
    old_date
        input date
    new_date
        new date
    no_data_value
        no data value in Sentinel-2 data (usally -10000)
    available_ram
        available ram (otb's pipeline size)
    """
    expected_refl_bands = [
        "B3", "B4", "B5", "B6", "B7", "B8", "B8A", "B11", "B12", "STACK", "B2"
    ]
    expected_corr = ["FRE"]  # actually iota2 only need FRE
    stack_files = []
    for band, corr in product(expected_refl_bands, expected_corr):
        files = []
        for s2_path in s2_paths:
            expected_band = list(s2_path.glob(f"*{corr}_{band}.tif"))
            if not expected_band:
                logging.warning(
                    f"band {band} {corr} not found in {str(s2_path)}")
            if len(expected_band) > 1:
                raise ValueError((f"multiple detection of band {band} {corr} "
                                  f"in {str(s2_path)}"))
            if len(expected_band) == 1:
                files.append(expected_band[0])
                if band == "STACK":
                    stack_files.append(expected_band[0])
        if not files:
            continue

        out_product_file = files[ref_file_name_ind].name.replace(
            old_date, new_date)
        output_file = Path(output_acquisition_dir, out_product_file)
        output_file.parent.mkdir(parents=True, exist_ok=True)
        if band != "STACK":
            exp = ":".join([
                f"im{ind+1}b1!={no_data_value} ? im{ind+1}b1"
                for ind in range(len(files))
            ]) + f": {no_data_value}"
            merge_app = CreateBandMathApplication({
                "il": [str(elem) for elem in files],
                "out":
                str(output_file),
                "pixType":
                "int16",
                "ram":
                str(available_ram),
                "exp":
                exp
            })
            merge_app.ExecuteAndWriteOutput()
        else:
            exp = ":".join([
                f"im{ind+1}!={no_data_value} ? im{ind+1}"
                for ind in range(len(files))
            ]) + f": {no_data_value}"
            merge_app = CreateBandMathXApplication({
                "il": [str(elem) for elem in files],
                "out":
                str(output_file),
                "pixType":
                "int16",
                "ram":
                str(available_ram),
                "exp":
                exp
            })
            merge_app.ExecuteAndWriteOutput()
    if not files and not stack_files:
        raise ValueError((f"band {expected_refl_bands[-1]} "
                          "or STACK.tif are needed "
                          " to merge masks"))
    return files if files else stack_files


def merge_masks(s2_paths, output_acquisition_dir, ref_file_name_ind, old_date,
                new_date, available_ram) -> None:
    """Merge Sentinel-2 masks if they are found.

    Otherwise, print a warning message.
    The masks we are trying to merge are
    CLM_R1.tif, EDG_R1.tif, SAT_R1.tif

    Parameters
    ----------
    s2_paths
        list of acquisition directory to merge
    output_acquisition_dir
        output acquisition directory
    ref_files
        useful to attribute masks value
    ref_file_name_ind
        ref index in s2_path to build output names
    old_date
        input date
    new_date
        new date
    available_ram
        available ram (otb's pipeline size)
    """
    expected_masks = ["CLM_R1", "EDG_R1", "SAT_R1", "BINARY_MASK"]

    for expected_mask in expected_masks:
        masks_files = []
        for s2_path in s2_paths:
            mask_path = s2_path.joinpath("MASKS")
            mask = list(mask_path.glob(f"*{expected_mask}.tif"))
            if not mask:
                logging.warning(
                    f"mask {expected_mask} not found in {str(mask_path)}")
            if len(mask) > 1:
                raise ValueError((f"multiple detection of mask {expected_mask}"
                                  f" in {str(mask_path)}"))
            if len(mask) == 1:
                masks_files.append(mask[0])
        if not masks_files:
            continue
        out_product_file = masks_files[ref_file_name_ind].name.replace(
            old_date, new_date)
        output_file_mask = Path(output_acquisition_dir, "MASKS",
                                out_product_file)
        output_file_mask.parent.mkdir(parents=True, exist_ok=True)

        exp = ":".join([
            f"im{ind+1}b1!=0 ? im{ind+1}b1" for ind in range(len(masks_files))
        ]) + f":im{len(masks_files)}b1"

        merge_app_mask = CreateBandMathApplication({
            "il": [str(elem) for elem in masks_files],
            "out":
            str(output_file_mask),
            "pixType":
            "uint8",
            "ram":
            str(available_ram),
            "exp":
            exp
        })
        merge_app_mask.ExecuteAndWriteOutput()


def merge(s2_paths: List[Path],
          output_dir: Path,
          available_ram: int = 128) -> None:
    """Merge all Sentinel-2 acquisitions that were acquired on the same day.

    If the directory in_s2_dir/T31PGM contains two acquisitions :
    'SENTINEL2B_20180421-101701-205_L2A_T31PGM_C_V2-2' and
    'SENTINEL2B_20180421-101755-322_L2A_T31PGM_C_V2-2'
    then a new directory will be created as
    'out_s2_dir/T31PGM/SENTINEL2B_20180421-xxxxxx-xxx_L2A_T31PGM_C_V2-2'

    The reflectance we are trying to merge are
    B2, B3, B4, B5, B6, B7, B8, B8A, B11, B12, and STACK if exists
    only with the the slope correction (FRE)
    and masks CLM_R1.tif, EDG_R1.tif, SAT_R1.tif

    the s2_paths must be sorted from the highest priority to the lower
    """
    ref_file_name_ind = 0
    no_data_value = -10000

    old_date = s2_paths[ref_file_name_ind].name.split("_")[1]
    new_date = f"{old_date.split('-')[0]}-xxxxxx-xxx"

    out_product_dir = s2_paths[ref_file_name_ind].name.replace(
        old_date, new_date)
    tile_name = s2_paths[ref_file_name_ind].name.split("_")[3]

    out_acquisition_dir = Path(output_dir, tile_name, out_product_dir)
    merge_refl(s2_paths, out_acquisition_dir, ref_file_name_ind, old_date,
               new_date, no_data_value, available_ram)
    merge_masks(s2_paths, out_acquisition_dir, ref_file_name_ind, old_date,
                new_date, available_ram)


def sort_dates(s2_paths: List[Path]) -> List[Path]:
    """Sort date using no data percent, the lowest get the higher priority.

    in masks 'EDG' 1 are nodata and 0 data. The lower mean in 'EDG' mask
    is the date with the more data
    """
    ref_raster = "EDG_R1"
    buff = []
    for s2_path in s2_paths:
        mask_path = s2_path.joinpath("MASKS")
        mask = list(mask_path.glob(f"*{ref_raster}.tif"))
        if not mask:
            raise ValueError(
                f"mask {ref_raster} not found in {str(mask_path)}")
        if len(mask) > 1:
            raise ValueError(
                f"More than one {ref_raster} found in {str(mask_path)}")
        src = rasterio.open(mask[0])
        arr = src.read(1)
        buff.append((np.mean(arr), s2_path))

    s2_date_sorted = sorted(buff, key=lambda x: x[0])
    return [path for _, path in s2_date_sorted]


def merge_s2_acquisitions(in_dir: Path,
                          out_dir: Path,
                          available_ram: int = 128) -> None:
    """Merge all same date Sentinel-2 acquisitions (THEIA's format).

    If the directory in_s2_dir/T31PGM contains two acquisitions :
    'SENTINEL2B_20180421-101701-205_L2A_T31PGM_C_V2-2' and
    'SENTINEL2B_20180421-101755-322_L2A_T31PGM_C_V2-2'
    then a new directory will be created as
    'out_s2_dir/T31PGM/SENTINEL2B_20180421-xxxxxx-xxx_L2A_T31PGM_C_V2-2'

    The reflectance we are trying to merge are
    B2, B3, B4, B5, B6, B7, B8, B8A, B11, B12, and STACK if exists
    only with the the slope correction (FRE)
    and masks CLM_R1.tif, EDG_R1.tif, SAT_R1.tif
    """
    if not out_dir.exists():
        raise ValueError(f"{str(out_dir)} does not exists")

    duplicated = detect_duplicated_s2(in_dir)
    for _, duplicated_dates in duplicated.items():
        for _, dates_path in duplicated_dates:
            dates_path = sort_dates(dates_path)
            merge(dates_path, out_dir, available_ram)


def main():
    """Define the conda entry point."""
    desc = ("This tool is dedicated to detect and merge all Sentinel-2"
            " acquisitions (THEIA's format) that were taken on the same day."
            "\n\nIf thedirectory in_s2_dir/T31PGM contains two acquisitions : "
            "'SENTINEL2B_20180421-101701-205_L2A_T31PGM_C_V2-2' and "
            "'SENTINEL2B_20180421-101755-322_L2A_T31PGM_C_V2-2' then "
            "a new directory will be created as "
            "'out_s2_dir/T31PGM/"
            "SENTINEL2B_20180421-xxxxxx-xxx_L2A_T31PGM_C_V2-2'\n\n"
            "The reflectance we are trying to merge are\n"
            "B2, B3, B4, B5, B6, B7, B8, B8A, B11, B12, and STACK if exists"
            "only with the the slope correction (FRE)\n"
            "and masks CLM_R1.tif, EDG_R1.tif, SAT_R1.tif")
    parser = argparse.ArgumentParser(desc)
    parser.add_argument("-in_s2_dir",
                        help=("absolute path to the directory which"
                              " contains Sentinel-2 acquisitions "
                              " (THEIA's format) stored by tiles"),
                        dest="in_s2_dir",
                        required=True)
    parser.add_argument("-out_s2_dir",
                        help=("absolute path to the directory that"
                              " will contain the merged acquisitions"),
                        dest="out_s2_dir")
    parser.add_argument("-available_ram",
                        help="amount of ram to use in Mo",
                        default=1024,
                        required=False,
                        dest="available_ram")
    args = parser.parse_args()
    merge_s2_acquisitions(Path(args.in_s2_dir), Path(args.out_s2_dir),
                          args.available_ram)


if __name__ == "__main__":
    sys.exit(main())
