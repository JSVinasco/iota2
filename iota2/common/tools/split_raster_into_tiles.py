#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""command line interface to split a large raster covering multiple tiles
and reproject onto these tiles"""

from pathlib import Path

import click

from iota2.common.otb_app_bank import CreateSuperimposeApplication, executeApp


@click.command()
@click.option(
    "-i",
    "--input-raster",
    "raster_data",
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
)
@click.option(
    "-t",
    "--input-tiles",
    "tile_folder",
    type=click.Path(exists=True, file_okay=False, path_type=Path),
)
@click.option(
    "-o",
    "--output-folder",
    "output_folder",
    type=click.Path(file_okay=False, path_type=Path),
)
@click.option(
    "-x",
    "--pixel-type",
    "pixel_type",
    default="uint8",
    show_default=True,
    # FIXME get this list somewhere
    # (currently enclosed in a function)
    # /iota2/common/file_utils.py#L270-L280
    type=click.Choice([
        "complexDouble",
        "complexFloat",
        "double",
        "float",
        "int16",
        "int32",
        "uint16",
        "uint32",
        "uint8",
    ]))
def _reproject(
    raster_data: Path,
    tile_folder: Path,
    output_folder: Path,
    pixel_type: str,
):
    """click decorated version of reproject function"""
    reproject(raster_data, tile_folder, output_folder, pixel_type)


def reproject(
    raster_data: Path,
    tile_folder: Path,
    output_folder: Path,
    pixel_type: str,
):
    """reproject using otb superimpose

    When using exogenous data in external features on multiple tiles, you might want to project your data on the individual tiles.

    Parameters\n
    ----------\n
    raster_data:\n
        raster data to reproject on tiles\n
    tile_folder:\n
        folder containing tiles images, names will be appended in output\n
    output_folder:\n
        output folder that will contain images names {raster_data}_{tile_name}.tif\n
    pixel_type:\n
        one of the following values\n
        complexDouble, complexFloat, double, float, int16, int32, uint16, uint32, uint8, \n

    """
    # creates output directory
    output_folder.mkdir(exist_ok=True)

    # iterate over tiles
    for tile in tile_folder.iterdir():
        # check name validity
        if tile.suffix != ".tif":
            print(f"ignoring {tile.name}")
            continue
        output_image = output_folder / f"{raster_data.stem}_{tile.name}"
        superimpose_application, _ = CreateSuperimposeApplication({
            "inr":
            str(tile),
            "inm":
            str(raster_data),
            "out":
            str(output_image),
            "pixType":
            pixel_type,
        })
        # execute the app and write output (could be done in parallel)
        print(f"writing {output_image.name}")
        superimpose_application.ExecuteAndWriteOutput()


if __name__ == "__main__":
    _reproject()  # pylint: disable=no-value-for-parameter

# example usage

# python iota2/common/tools/split_raster_into_tiles.py -i ./mosaic.tif -t ./tiles/ -o ./reproj -x uint16

# .
# ├── mosaic.tif
# ├── reproj
# │   ├── mosaic_T31TCM.tif
# │   ├── mosaic_T31TCN.tif
# │   ├── mosaic_T31TDL.tif
# │   ├── mosaic_T31TDN.tif
# │   ├── mosaic_T31TEL.tif
# │   └── mosaic_T31TEN.tif
# └── tiles
#     ├── script.sh
#     ├── T31TCM.tif
#     ├── T31TCN.tif
#     ├── T31TDL.tif
#     ├── T31TDN.tif
#     ├── T31TEL.tif
#     └── T31TEN.tif

# tiles/script.sh is ignored
# reproj folder is created and filled with images
