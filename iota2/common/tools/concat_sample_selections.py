#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# pylint: disable=invalid-name
"""command line interface to concatenate sample selections csvs"""

import click

from iota2.validation.results_utils import merge_all_sample_selection
from iota2.vector_tools.vector_functions import get_reverse_encoding_labels_dic


@click.command()
@click.argument('output_path', type=click.Path(exists=True))
@click.argument('nomenclature', type=click.Path(exists=True))
@click.argument('ref_data_path', type=click.Path(exists=True))
@click.option('-f',
              '--field',
              'ref_data_field',
              default="code",
              show_default=True,
              help="original field name in shapefile")
def concat_sample_selection(
    output_path: str,
    nomenclature: str,
    ref_data_path: str,
    ref_data_field: str,
):
    """merge all sample selection files using user label"""
    labels_conversion = get_reverse_encoding_labels_dic(
        ref_data_path, ref_data_field)
    merge_all_sample_selection(output_path, nomenclature, labels_conversion)
    print("wrote csv files to 'learningSamples' folder")


if __name__ == "__main__":
    concat_sample_selection()  # pylint: disable=no-value-for-parameter

# example usage
# python concat_sample_selections.py IOTA2_Outputs/Results_classif/ \
# nomenclature23.txt reference_data.shp
