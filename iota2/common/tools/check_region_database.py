# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

# =========================================================================
#   Program:   vector tools
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import string
import sys
from typing import List

from osgeo import ogr

from iota2.common import service_error
from iota2.common.utils import str2bool
from iota2.vector_tools.delete_duplicate_geometries_sqlite import (
    delete_duplicate_geometries_sqlite,
)
from iota2.vector_tools.vector_functions import (
    check_empty_geom,
    check_valid_geom,
    get_field_type,
    get_fields,
    get_vector_proj,
)


def get_geometries_by_area(
    input_vector: str, area: float, driver_name: str = "ESRI Shapefile"
) -> List:
    """get geometries smaller than the area

    sub geometries of a MULTIPOLYGONS are checked and returned either
    """

    driver = ogr.GetDriverByName(driver_name)
    data_src = driver.Open(input_vector, 0)
    layer_src = data_src.GetLayer()
    output_geoms = []
    for feature in layer_src:
        geom = feature.GetGeometryRef()
        for geom_part in geom:
            geom_area = geom_part.GetArea()
            if geom_area < area:
                output_geoms.append((geom_part.Clone(), geom_area))
    return output_geoms


def vector_name_check(input_vector: str):
    """
    check if first character is a letter
    """
    avail_characters = string.ascii_letters
    first_character = os.path.basename(input_vector)[0]
    return True if first_character in avail_characters else False


def remove_invalid_features(shapefile: str, do_corrections: bool):
    # remove invalid features
    none_geom = 0
    driver = ogr.GetDriverByName("ESRI Shapefile")
    data_source = driver.Open(shapefile, 1)
    if not data_source:
        raise
    layer = data_source.GetLayer()
    count = layer.GetFeatureCount()
    for feature in range(count):
        feat = layer[feature]
        geom = feat.GetGeometryRef()
        if not geom:
            none_geom += 1
            if do_corrections:
                layer.DeleteFeature(feature)
    layer.ResetReading()
    return none_geom


def check_region_database_inplace(
    input_vector: str, field: str, epsg: int, do_corrections: bool
):
    """ """
    area_threshold = 0.1
    input_vector_fields = get_fields(input_vector)
    errors = []

    # check vector's projection
    vector_projection = get_vector_proj(input_vector)
    if vector_projection is None:
        error_msg = f"{input_vector} does not have projection"
        errors.append(service_error.invalidProjection(error_msg))
    elif int(epsg) != int(vector_projection):
        error_msg = f"{input_vector} projection ({vector_projection}) incorrect"
        errors.append(service_error.invalidProjection(error_msg))

    # check vector's name
    name_check = vector_name_check(input_vector)
    if name_check is False:
        error_msg = (
            f"{input_vector} file's name not correct, "
            "it must start with an ascii letter"
        )
        errors.append(service_error.namingConvention(error_msg))

    # check field
    if field not in input_vector_fields:
        errors.append(service_error.missingField(input_vector, field))

    # check field's type
    label_field_type = get_field_type(input_vector, field)
    if label_field_type is not str:
        errors.append(service_error.fieldType(input_vector, field, str))

    # geometries checks
    shape_no_empty_name = "no_empty.shp"
    shape_no_empty_dir = os.path.split(input_vector)[0]
    shape_no_empty = os.path.join(shape_no_empty_dir, shape_no_empty_name)
    shape_no_empty, empty_geom_number = check_empty_geom(
        input_vector, do_corrections=do_corrections, output_file=shape_no_empty
    )
    if empty_geom_number != 0:
        error_msg = (
            f"'{input_vector}' contains {empty_geom_number} empty geometries, "
            "see checkRegionDataBase.py -h to correct the database"
        )
        errors.append(service_error.emptyGeometry(error_msg))

    # remove duplicates features
    shape_no_duplicates_name = "no_duplicates.shp"
    shape_no_duplicates_dir = os.path.split(input_vector)[0]
    shape_no_duplicates = os.path.join(
        shape_no_duplicates_dir, shape_no_duplicates_name
    )

    if not os.path.exists(shape_no_empty):
        shape_no_empty = input_vector

    shape_no_duplicates, duplicated_features = delete_duplicate_geometries_sqlite(
        shape_no_empty, do_corrections, shape_no_duplicates, quiet_mode=True
    )
    if duplicated_features != 0:
        error_msg = (
            f"'{input_vector}' contains {duplicated_features} duplicated features, "
            "see checkRegionDataBase.py -h to correct the database"
        )
        errors.append(service_error.duplicatedFeatures(error_msg))

    # Check valid geometry
    shape_valid_geom_name = "valid_geom.shp"
    shape_valid_geom_dir = os.path.split(input_vector)[0]
    shape_valid_geom = os.path.join(shape_valid_geom_dir, shape_valid_geom_name)

    input_valid_geom_shape = (
        shape_no_duplicates if do_corrections else shape_no_duplicates
    )
    if not os.path.exists(input_valid_geom_shape):
        input_valid_geom_shape = input_vector

    shape_valid_geom, invalid_geom, invalid_geom_corrected = check_valid_geom(
        input_valid_geom_shape, display=False, do_corrections=do_corrections
    )

    # remove features with None geometries
    none_geoms = remove_invalid_features(
        shape_valid_geom, do_corrections=do_corrections
    )
    invalid_geom += none_geoms

    if invalid_geom != 0:
        error_msg = (
            f"'{input_vector}' contains {invalid_geom} invalid geometries, "
            "see checkRegionDataBase.py -h to correct the database"
        )
        errors.append(service_error.invalidGeometry(error_msg))

    nb_too_small_geoms = len(
        get_geometries_by_area(
            input_vector, area=area_threshold, driver_name="ESRI Shapefile"
        )
    )
    if nb_too_small_geoms != 0:
        errors.append(
            service_error.tooSmallRegion(
                input_vector, area_threshold, nb_too_small_geoms
            )
        )
    return errors


def main():
    parent = os.path.abspath(
        os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)
    )
    iota2_scripts_dir = os.path.abspath(os.path.join(parent, os.pardir))

    if iota2_scripts_dir not in sys.path:
        sys.path.append(iota2_scripts_dir)

    description = (
        "check region database\n"
        "\t- remove empty geometries\n"
        "\t- remove duplicate geometries\n"
        "\t- split multi-polygons to polygons\n"
        "\t- check projection\n"
        "\t- check vector's name\n"
        "\t- check if the label field is integer type"
    )
    parser = argparse.ArgumentParser(description)
    parser.add_argument(
        "-in.vector",
        help="absolute path to the vector (mandatory)",
        dest="input_vector",
        required=True,
    )
    parser.add_argument(
        "-out.vector",
        help="output vector",
        dest="output_vector",
        required=False,
        default=None,
    )
    parser.add_argument(
        "-RegionField",
        help="field containing regions (mandatory)",
        dest="region_field",
        required=True,
    )
    parser.add_argument(
        "-epsg",
        help="expected EPSG's code (mandatory)",
        dest="epsg",
        required=True,
        type=int,
    )
    parser.add_argument(
        "-doCorrections",
        help="enable corrections (default = False)",
        dest="do_corrections",
        required=False,
        default=False,
        type=str2bool,
    )
    args = parser.parse_args()

    vector_copy = args.output_vector
    ds = ogr.Open(args.input_vector)
    drv = ds.GetDriver()
    drv.CopyDataSource(ds, vector_copy)

    check_region_database_inplace(
        vector_copy, args.region_field, args.epsg, args.do_corrections
    )


if __name__ == "__main__":
    sys.exit(main())
