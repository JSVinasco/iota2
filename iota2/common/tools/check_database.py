#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

# =========================================================================
#   Program:   vector tools
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import sys
from typing import List

from osgeo import ogr

from iota2.common import service_error
from iota2.common.utils import str2bool
from iota2.vector_tools.delete_duplicate_geometries_sqlite import (
    delete_duplicate_geometries_sqlite,
)
from iota2.vector_tools.multipoly_to_poly import multipoly2poly
from iota2.vector_tools.vector_functions import (
    check_empty_geom,
    check_valid_geom,
    get_field_type,
    get_fields,
    get_vector_proj,
    remove_shape,
)


def get_geometries_by_area(
    input_vector: str, area: float, driver_name: str = "ESRI Shapefile"
) -> List:
    """get geometries smaller than the area

    sub geometries of a MULTIPOLYGONS are checked and returned either
    """

    driver = ogr.GetDriverByName(driver_name)
    data_src = driver.Open(input_vector, 0)
    layer_src = data_src.GetLayer()
    output_geoms = []
    for feature in layer_src:
        geom = feature.GetGeometryRef()
        for geom_part in geom:
            geom_area = geom_part.GetArea()
            if geom_area < area:
                output_geoms.append((geom_part.Clone(), geom_area))
    return output_geoms


def vector_name_check(input_vector: str):
    """
    check if first character is a letter
    """
    import string

    avail_characters = string.ascii_letters
    first_character = os.path.basename(input_vector)[0]
    return True if first_character in avail_characters else False


def remove_invalid_features(shapefile: str, do_corrections: bool):

    # remove invalid features
    none_geom = 0
    driver = ogr.GetDriverByName("ESRI Shapefile")
    data_source = driver.Open(shapefile, 1)
    if not data_source:
        raise Exception
    layer = data_source.GetLayer()
    count = layer.GetFeatureCount()
    for feature in range(count):
        feat = layer[feature]
        geom = feat.GetGeometryRef()
        if not geom:
            none_geom += 1
            if do_corrections:
                layer.DeleteFeature(feature)
    layer.ResetReading()
    return none_geom


def check_region_shape(
    input_vector: str,
    output_vector: str,
    field: str,
    epsg: int,
    do_corrections: bool,
    display: bool = False,
):
    """ """
    area_threshold = 0.1
    input_vector_fields = get_fields(input_vector)
    errors = []

    # check vector's projection
    vector_projection = get_vector_proj(input_vector)
    if vector_projection is None:
        error_msg = f"{input_vector} does not have projection"
        errors.append(service_error.invalidProjection(error_msg))
    elif int(epsg) != int(vector_projection):
        error_msg = f"{input_vector} projection ({vector_projection}) incorrect"
        errors.append(service_error.invalidProjection(error_msg))

    # check vector's name
    name_check = vector_name_check(input_vector)
    if name_check is False:
        error_msg = (
            f"{input_vector} file's name not correct, "
            "it must start with an ascii letter"
        )
        errors.append(service_error.namingConvention(error_msg))

    # check field
    if field not in input_vector_fields:
        errors.append(service_error.missingField(input_vector, field))

    # check field's type
    label_field_type = get_field_type(input_vector, field)
    if label_field_type is not str:
        errors.append(service_error.fieldType(input_vector, field, str))

    # geometries checks
    shape_no_empty_name = "no_empty.shp"
    shape_no_empty_dir = os.path.split(input_vector)[0]
    shape_no_empty = os.path.join(shape_no_empty_dir, shape_no_empty_name)
    shape_no_empty, empty_geom_number = check_empty_geom(
        input_vector, do_corrections=do_corrections, output_file=shape_no_empty
    )
    if empty_geom_number != 0:
        error_msg = f"'{input_vector}' contains {empty_geom_number} empty geometries"
        errors.append(service_error.emptyGeometry(error_msg))

    # remove duplicates features
    shape_no_duplicates_name = "no_duplicates.shp"
    shape_no_duplicates_dir = os.path.split(input_vector)[0]
    shape_no_duplicates = os.path.join(
        shape_no_duplicates_dir, shape_no_duplicates_name
    )

    if not os.path.exists(shape_no_empty):
        shape_no_empty = input_vector

    shape_no_duplicates, duplicated_features = delete_duplicate_geometries_sqlite(
        shape_no_empty, do_corrections, shape_no_duplicates, quiet_mode=True
    )
    if duplicated_features != 0:
        error_msg = (
            f"'{input_vector}' contains {duplicated_features} duplicated features"
        )
        errors.append(service_error.duplicatedFeatures(error_msg))

    # Check valid geometry
    shape_valid_geom_name = "valid_geom.shp"
    shape_valid_geom_dir = os.path.split(input_vector)[0]
    shape_valid_geom = os.path.join(shape_valid_geom_dir, shape_valid_geom_name)
    shape_valid_geom = output_vector if output_vector else shape_valid_geom

    input_valid_geom_shape = (
        shape_no_duplicates if do_corrections else shape_no_duplicates
    )
    if not os.path.exists(input_valid_geom_shape):
        input_valid_geom_shape = input_vector

    shape_valid_geom, invalid_geom, _ = check_valid_geom(
        input_valid_geom_shape, display=False, do_corrections=do_corrections
    )

    # remove features with None geometries
    none_geoms = remove_invalid_features(
        shape_valid_geom, do_corrections=do_corrections
    )
    invalid_geom += none_geoms

    if invalid_geom != 0:
        error_msg = f"'{input_vector}' contains {invalid_geom} invalid geometries"
        errors.append(service_error.invalidGeometry(error_msg))

    nb_too_small_geoms = len(
        get_geometries_by_area(
            input_vector, area=area_threshold, driver_name="ESRI Shapefile"
        )
    )
    if nb_too_small_geoms != 0:
        errors.append(
            service_error.tooSmallRegion(
                input_vector, area_threshold, nb_too_small_geoms
            )
        )
    return errors


def check_ground_truth_inplace(
    input_vector: str, data_field: str, epsg: int, do_corrections: bool, display=False
):
    """ """

    tmp_files = []
    input_vector_fields = get_fields(input_vector)

    errors = []
    # check vector's projection
    vector_projection = get_vector_proj(input_vector)
    if vector_projection is None:
        error_msg = f"{input_vector} does not have projection"
        errors.append(service_error.invalidProjection(error_msg))

    if not int(epsg) == int(vector_projection):
        error_msg = f"{input_vector} projection ({vector_projection}) incorrect"
        errors.append(service_error.invalidProjection(error_msg))

    # check vector's name
    name_check = vector_name_check(input_vector)
    if name_check is False:
        error_msg = "file's name not correct, it must start with an ascii letter"
        errors.append(service_error.namingConvention(error_msg))

    # check field
    if not data_field in input_vector_fields:
        errors.append(service_error.missingField(input_vector, data_field))

    # geometries checks
    shape_no_empty_name = "no_empty.shp"
    shape_no_empty_dir = os.path.split(input_vector)[0]
    shape_no_empty = os.path.join(shape_no_empty_dir, shape_no_empty_name)
    shape_no_empty, empty_geom_number = check_empty_geom(
        input_vector, do_corrections=do_corrections, output_file=shape_no_empty
    )
    if empty_geom_number != 0:
        error_msg = (
            f"'{input_vector}' contains {empty_geom_number} empty geometries,"
            " see check_database.py -h to correct the database"
        )
        if do_corrections:
            error_msg = f"{error_msg} and they were removed"
        errors.append(service_error.emptyGeometry(error_msg))
    tmp_files.append(shape_no_empty)

    # remove duplicates features
    shape_no_duplicates_name = "no_duplicates.shp"
    shape_no_duplicates_dir = os.path.split(input_vector)[0]
    shape_no_duplicates = os.path.join(
        shape_no_duplicates_dir, shape_no_duplicates_name
    )
    if not os.path.exists(shape_no_empty):
        shape_no_empty = input_vector
    shape_no_duplicates, duplicated_features = delete_duplicate_geometries_sqlite(
        shape_no_empty,
        do_corrections=do_corrections,
        output_file=shape_no_duplicates,
        quiet_mode=True,
    )

    if duplicated_features != 0:
        error_msg = (
            f"'{input_vector}' contains {duplicated_features} duplicated features, "
            "see check_database.py -h to correct the database"
        )
        if do_corrections:
            error_msg = f"{error_msg} and they were removed"
        errors.append(service_error.duplicatedFeatures(error_msg))
    tmp_files.append(shape_no_duplicates)

    # remove multipolygons
    shape_no_multi_name = "no_multi.shp"
    shape_no_multi_dir = os.path.split(input_vector)[0]
    shape_no_multi = os.path.join(shape_no_multi_dir, shape_no_multi_name)
    if not os.path.exists(shape_no_duplicates):
        shape_no_duplicates = input_vector
    multipolygons_number = multipoly2poly(
        shape_no_duplicates, shape_no_multi, do_corrections
    )
    if multipolygons_number != 0:
        error_msg = (
            f"'{input_vector}' contains {multipolygons_number} MULTIPOLYGON,"
            " see check_database.py -h to correct the database"
        )
        if do_corrections:
            error_msg = f"{error_msg} and they were removed"
        errors.append(service_error.containsMultipolygon(error_msg))
    tmp_files.append(shape_no_multi)

    # Check valid geometry
    shape_valid_geom_name = "valid_geom.shp"
    shape_valid_geom_dir = os.path.split(input_vector)[0]
    shape_valid_geom = os.path.join(shape_valid_geom_dir, shape_valid_geom_name)
    input_valid_geom_shape = shape_no_multi if do_corrections else shape_no_duplicates
    if not os.path.exists(input_valid_geom_shape):
        input_valid_geom_shape = input_vector
    shape_valid_geom, invalid_geom, _ = check_valid_geom(
        input_valid_geom_shape, display=False, do_corrections=do_corrections
    )

    # remove features with None geometries
    none_geoms = remove_invalid_features(
        shape_valid_geom, do_corrections=do_corrections
    )
    invalid_geom += none_geoms
    if invalid_geom != 0:
        error_msg = (
            f"'{input_vector}' contains {invalid_geom} invalid geometries,"
            " see check_database.py -h to correct the database"
        )
        errors.append(service_error.invalidGeometry(error_msg))
    tmp_files.append(shape_valid_geom)
    for tmp_file in tmp_files:
        if tmp_file is not input_vector and os.path.exists(tmp_file):
            remove_shape(tmp_file.replace(".shp", ""), [".prj", ".shp", ".dbf", ".shx"])
    if display:
        print("\n".join(errors))
    return errors


def check_user_ground_truth(
    input_vector: str, data_field: str, epsg: int, do_corrections: bool, display=True
):
    """ """
    input_vector_fields = get_fields(input_vector)

    errors = []
    # check vector's projection
    vector_projection = get_vector_proj(input_vector)
    if vector_projection is None:
        error_msg = f"{input_vector} does not have projection"
        errors.append(service_error.invalidProjection(error_msg))

    if int(epsg) != int(vector_projection):
        error_msg = f"{input_vector} projection ({vector_projection}) incorrect"
        errors.append(service_error.invalidProjection(error_msg))

    # check vector's name
    name_check = vector_name_check(input_vector)
    if name_check is False:
        error_msg = "file's name not correct, it must start with an ascii letter"
        errors.append(service_error.namingConvention(error_msg))

    # check field
    if data_field not in input_vector_fields:
        errors.append(service_error.missingField(input_vector, data_field))

    # geometries checks
    _, empty_geom_number = check_empty_geom(input_vector, do_corrections=do_corrections)
    if empty_geom_number != 0:
        error_msg = (
            f"'{input_vector}' contains {empty_geom_number} empty geometries, "
            "see check_database.py -h to correct the database"
        )
        if do_corrections:
            error_msg = f"{error_msg} and they were removed"
        errors.append(service_error.emptyGeometry(error_msg))

    # remove duplicates features
    shape_no_duplicates, duplicated_features = delete_duplicate_geometries_sqlite(
        input_vector, do_corrections=do_corrections, quiet_mode=True
    )

    if duplicated_features != 0:
        error_msg = (
            f"'{input_vector}' contains {duplicated_features} duplicated features, "
            "see check_database.py -h to correct the database"
        )
        if do_corrections:
            error_msg = f"{error_msg} and they were removed"
        errors.append(service_error.duplicatedFeatures(error_msg))

    # remove multipolygons
    shape_no_multi_name = "no_multi.shp"
    shape_no_multi_dir = os.path.split(input_vector)[0]
    shape_no_multi = os.path.join(shape_no_multi_dir, shape_no_multi_name)
    multipolygons_number = multipoly2poly(input_vector, shape_no_multi, do_corrections)

    driver = ogr.GetDriverByName("ESRI shapefile")
    driver.DeleteDataSource(input_vector)
    ds = ogr.Open(shape_no_multi)
    driver.CopyDataSource(ds, input_vector)
    driver.DeleteDataSource(shape_no_multi)

    if multipolygons_number != 0:
        error_msg = (
            f"'{input_vector}' contains {multipolygons_number} MULTIPOLYGON, "
            "see check_database.py -h to correct the database"
        )
        if do_corrections:
            error_msg = f"{error_msg} and they were removed"
        errors.append(service_error.containsMultipolygon(error_msg))

    # Check valid geometry
    input_valid_geom_shape = shape_no_multi if do_corrections else shape_no_duplicates
    if not os.path.exists(input_valid_geom_shape):
        input_valid_geom_shape = input_vector
    _, invalid_geom, _ = check_valid_geom(
        input_vector, display=False, do_corrections=do_corrections
    )

    # remove features with None geometries
    none_geoms = remove_invalid_features(input_vector, do_corrections=do_corrections)
    invalid_geom += none_geoms
    if invalid_geom != 0:
        error_msg = (
            f"'{input_vector}' contains {invalid_geom} invalid geometries, "
            "see check_database.py -h to correct the database"
        )
        errors.append(service_error.invalidGeometry(error_msg))

    if display:
        print("\n".join([str(error) for error in errors]))
    return errors


def main():
    parent = os.path.abspath(
        os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)
    )
    iota2_scripts_dir = os.path.abspath(os.path.join(parent, os.pardir))

    if iota2_scripts_dir not in sys.path:
        sys.path.append(iota2_scripts_dir)

    description = (
        "\ncheck ground truth database\n"
        "\t- remove empty geometries\n"
        "\t- remove duplicate geometries\n"
        "\t- split multi-polygons to polygons\n"
        "\t- check projection\n"
        "\t- check vector's name\n"
        "\t- check if the label field is integer type\n"
    )
    parser = argparse.ArgumentParser(description)
    parser.add_argument(
        "-in.vector",
        help="absolute path to the vector (mandatory)",
        dest="input_vector",
        required=True,
    )
    parser.add_argument(
        "-out.vector",
        help="output vector",
        dest="output_vector",
        required=False,
        default=None,
    )
    parser.add_argument(
        "-dataField",
        help="field containing labels (mandatory)",
        dest="data_field",
        required=True,
    )
    parser.add_argument(
        "-epsg",
        help="expected EPSG's code (mandatory)",
        dest="epsg",
        required=True,
        type=int,
    )
    parser.add_argument(
        "-doCorrections",
        help="enable corrections (default = False)",
        dest="do_corrections",
        required=False,
        default=False,
        type=str2bool,
    )
    args = parser.parse_args()

    vector_copy = args.output_vector
    data_src = ogr.Open(args.input_vector)
    drv = data_src.GetDriver()
    drv.CopyDataSource(data_src, vector_copy)

    check_user_ground_truth(
        vector_copy, args.data_field, args.epsg, args.do_corrections
    )


if __name__ == "__main__":
    sys.exit(main())
