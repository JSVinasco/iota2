# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import logging
import os
from typing import List

from iota2.common import file_utils as fu
from iota2.common.utils import run
from iota2.vector_tools import vector_functions as vf

logger = logging.getLogger("distributed.worker")


def split_vector_layer(
    shp_in: str, attribute: str, attribute_type: str, field_vals: List, output_path: str
) -> List[str]:
    """Split a vector layer in function of its attribute.

    Parameters
    ----------
    shp_in
        input shapefile
    attribute
        columns name
    attribute_type
        attribute type which could be "string" or "int"

    Note
    ----
    return a list of shapeFiles
    """
    shp_out_list = []
    name = shp_in.split("/")[-1].split(".")[0]

    if attribute_type == "string":
        for val in field_vals:
            if val != "None":
                shp_out = output_path + "/" + name + "_region_" + str(val) + ".shp"
                if not os.path.isfile(shp_out):
                    cmd = "ogr2ogr "
                    cmd += "-where '" + attribute + ' = "' + val + '"' + "' "
                    cmd += shp_out + " "
                    cmd += shp_in + " "
                    run(cmd)
                shp_out_list.append(shp_out)

    elif attribute_type == "int":
        for val in field_vals:
            shp_out = output_path + "/" + name + "_region_" + str(val) + ".shp"

            if not os.path.isfile(shp_out):
                cmd = "ogr2ogr "
                cmd += "-where '" + attribute + " = " + str(val) + "' "
                cmd += shp_out + " "
                cmd += shp_in + " "
                run(cmd)
            shp_out_list.append(shp_out)
    else:
        raise Exception(
            f"Error for attribute_type {attribute_type}, ! Should be 'string' or 'int'"
        )

    return shp_out_list


def create_regions_by_tiles(
    region_shape, field_Region, env_path, output_path, working_directory, logger_=logger
) -> List[str]:
    """Create a shapeFile into tile's envelope for each regions in region_shape and for each tiles.

    Parameters
    ----------
    region_shape
        the shape which contains all regions
    field_Region
        the field into the region's shape which describes each tile belong to which model
    env_path
        path to the tile's envelope with priority
    output_path
        path to store all resulting shapeFile
    working_directory
        path to working directory (not mandatory)
    """
    path_name = working_directory
    if working_directory is None:
        path_name = output_path

    all_tiles = fu.file_search_and(env_path, True, ".shp")
    region_list = vf.get_field_element(
        region_shape, "ESRI Shapefile", field_Region, "unique"
    )
    shp_region_list = split_vector_layer(
        region_shape, field_Region, "int", region_list, path_name
    )
    all_clip = []
    for shp in shp_region_list:
        for tile in all_tiles:
            logger_.info(f"Extract {shp} in {tile}")
            path_to_clip = vf.clip_vector_data(shp, tile, path_name)
            all_clip.append(path_to_clip)

    if working_directory:
        for clip in all_clip:
            cmd = "cp " + clip.replace(".shp", "*") + " " + output_path
            run(cmd)
    else:
        for shp in shp_region_list:
            path = shp.replace(".shp", "")
            os.remove(path + ".shp")
            os.remove(path + ".shx")
            os.remove(path + ".dbf")
            os.remove(path + ".prj")

    return all_clip
