#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Generate iota² output tree."""
import logging
import os
import shutil
from typing import List

from iota2.common.file_utils import ensure_dir

LOGGER = logging.getLogger("distributed.worker")


def generate_directories(root: str,
                         merge_final_classifications: bool,
                         tile_list: List[str],
                         enable_boundary_fusion: bool = False,
                         comparison_mode: bool = False) -> None:
    """
    Generate iota2 classification workflow output directories.

    Parameters
    ----------
    root:
        iota2 output path
    merge_final_classifications
        flag to generate the directory dedicated to receive the
        fusion of final classifications

    Notes
    -----
    the removal of directories is managed in
     iota.sequence_builders.i2_classification.generate_output_directories
    """
    ensure_dir(root)
    ensure_dir(root + "/samplesSelection")
    ensure_dir(root + "/model")
    ensure_dir(root + "/formattingVectors")
    ensure_dir(root + "/config_model")
    ensure_dir(root + "/envelope")
    ensure_dir(root + "/classif")
    if enable_boundary_fusion:
        ensure_dir(root + "/boundary")
    ensure_dir(root + "/shapeRegion")
    ensure_dir(root + "/final")
    if comparison_mode:
        ensure_dir(root + "/final/standard")
        ensure_dir(root + "/final/standard/TMP")
        ensure_dir(root + "/final/boundary")
        ensure_dir(root + "/final/boundary/TMP")

    ensure_dir(root + "/final/TMP")
    ensure_dir(root + "/features")
    ensure_dir(root + "/dataRegion")
    ensure_dir(root + "/learningSamples")
    ensure_dir(root + "/dataAppVal")
    ensure_dir(root + "/stats")
    ensure_dir(root + "/logs")

    if merge_final_classifications:
        ensure_dir(root + "/final/merge_final_classifications")
    for tile in tile_list:
        ensure_dir(root + "/features/" + tile)
        ensure_dir(root + "/features/" + tile + "/tmp")


def generate_features_maps_directories(root: str,
                                       tile_list: List[str]) -> None:
    """
    Generate output directories for write features maps.

    Parameters
    ----------
    root : str
        iota2 output path
    """
    ensure_dir(root)
    if os.path.exists(root + "/final"):
        shutil.rmtree(root + "/final")
    ensure_dir(root + "/final")
    if os.path.exists(root + "/features"):
        shutil.rmtree(root + "/features")
    ensure_dir(root + "/features")
    for tile in tile_list:
        ensure_dir(root + "/features/" + tile)
        ensure_dir(root + "/features/" + tile + "/tmp")
    if os.path.exists(root + "/customF"):
        shutil.rmtree(root + "/customF")
    ensure_dir(root + "/customF")
    if os.path.exists(root + "/by_tiles"):
        shutil.rmtree(root + "/by_tiles")
    ensure_dir(root + "/by_tiles")


def generate_vectorization_directories(root: str) -> None:
    """
    Generate vectorization needed directories.

    Parameters
    ----------
    root :
        iota2 output path
    """
    ensure_dir(root)
    ensure_dir(os.path.join(root, "vectors"))
    ensure_dir(os.path.join(root, "simplification"))
    ensure_dir(os.path.join(root, "simplification", "tiles"))
    ensure_dir(os.path.join(root, "simplification", "vectors"))
    ensure_dir(os.path.join(root, "simplification", "mosaic"))
    ensure_dir(os.path.join(root, "simplification", "tmp"))


def generate_directories_obia(root: str, merge_final_classifications: bool,
                              tile_list: List[str]) -> None:
    """
    Generate iota2 classification workflow output directories.

    Parameters
    ----------
    root:
        iota2 output path
    merge_final_classifications
        flag to generate the directory dedicated to receive the
        fusion of final classifications

    Notes
    -----
    the removal of directories is managed in
     iota.sequence_builders.i2_classification.generate_output_directories
    """
    ensure_dir(root)
    ensure_dir(root + "/samplesSelection")
    ensure_dir(root + "/model")
    ensure_dir(root + "/formattingVectors")
    ensure_dir(root + "/config_model")
    ensure_dir(root + "/envelope")
    ensure_dir(root + "/classif")
    ensure_dir(root + "/classif/zonal_stats")
    ensure_dir(root + "/classif/classif_tmp")
    ensure_dir(root + "/classif/reduced")
    ensure_dir(root + "/shapeRegion")
    ensure_dir(root + "/final")
    ensure_dir(root + "/final/TMP")
    if merge_final_classifications:
        ensure_dir(root + "/final/merge_final_classifications")
    ensure_dir(root + "/features")
    ensure_dir(root + "/dataRegion")
    ensure_dir(root + "/learningSamples")
    ensure_dir(root + "/learningSamples/zonal_stats")
    ensure_dir(root + "/dataAppVal")
    ensure_dir(root + "/stats")
    ensure_dir(root + "/segmentation")
    ensure_dir(root + "/segmentation/tmp")
    ensure_dir(root + "/segmentation/grid_split")
    ensure_dir(root + "/segmentation/grid_split_learn")
    for tile in tile_list:
        ensure_dir(root + "/features/" + tile)
        ensure_dir(root + "/features/" + tile + "/tmp")
