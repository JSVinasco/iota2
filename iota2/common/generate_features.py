#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Module dedicated to generate the whole iota2 feature pipeline
"""
import argparse
import logging
import os
from collections import Counter
from typing import Dict, List, Optional, Union

LOGGER = logging.getLogger("distributed.worker")

sensors_params = Dict[str, Union[str, List[str], int]]


def build_masks_labels_from_raw_labels(raw_labels: List[str],
                                       sensors_names: List[str]) -> List[str]:
    """
    """
    buff_dates = {}
    for label in raw_labels:
        label_format = label.split("_")
        if label_format[0].lower() in sensors_names:
            sensor_name, feat, date = label_format
            if sensor_name == "sentinel1":
                sensor_name = f"{sensor_name}_{feat[0:3]}"
            if (sensor_name, feat) not in buff_dates:
                buff_dates[(sensor_name, feat)] = [date]
            else:
                buff_dates[(sensor_name, feat)].append(date)
    # check if all items contains the same number of dates
    sensor_dates_counter = {}
    for (sensor, feat_name), dates in buff_dates.items():
        if sensor == "sentinel1":
            sensor = f"{sensor}_{feat_name[0:3]}"
        if sensor not in sensor_dates_counter:
            sensor_dates_counter[sensor] = []
        sensor_dates_counter[sensor].append(len(dates))
    for sensor_name, feat_dates in sensor_dates_counter.items():
        if len(Counter(feat_dates)) != 1:
            raise ValueError("iota2 detected a missmatch during "
                             "the mask label detection")
    masks_labels = []
    sensors_checked = []
    for (sensor, _), dates in buff_dates.items():
        if sensor in sensors_checked:
            continue
        for date in dates:
            if "sentinel1" in sensor:
                masks_labels.append(f"sentinel1_{sensor[10:13]}MASK_{date}")
            else:
                masks_labels.append(f"{sensor}_MASK_{date}")
        sensors_checked.append(sensor)
    return masks_labels


def generate_features(pathWd: str,
                      tile: str,
                      sar_optical_post_fusion: bool,
                      output_path: str,
                      sensors_parameters: sensors_params,
                      force_standard_labels: bool = False,
                      mode: str = "usually",
                      logger: logging.Logger = LOGGER):
    """
    usage : Function use to compute features according to a configuration file

    Parameters
    ----------
    pathWd : str
        path to a working directory
    tile : str
        tile's name
    sar_optical_post_fusion : bool
        flag use to remove SAR data from features
    output_path :
        # DOCME
    sensors_parameters :
        # DOCME
    force_stadard_labels :
        # DOCME
    mode : str
        'usually' / 'SAR' used to get only sar features
    logger :
        # DOCME
        
    Return
    ------
    AllFeatures [OTB Application object] : otb object ready to Execute()
    feat_labels [list] : list of strings, labels for each output band
    dep [list of OTB Applications]
    """
    from iota2.common.otb_app_bank import (CreateConcatenateImagesApplication,
                                           getInputParameterOutput)
    from iota2.sensors.sensors_container import sensors_container

    logger.info(f"prepare features for tile : {tile}")

    sensor_tile_container = sensors_container(tile, pathWd, output_path,
                                              **sensors_parameters)

    dep = []
    interp_apps = []
    masks_apps = []
    raw_apps = []
    if mode == "SAR":
        sensor = sensor_tile_container.get_sensor("Sentinel1")
        (interp_features,
         sensor_features_dep), feat_labels = sensor.get_features(ram=1000,
                                                                 logger=logger)
        interp_features.Execute()
        interp_apps.append(interp_features)

        dep.append(sensor_features_dep)
        mask_features, masks_dep, masks_count = sensor.get_time_series_masks_raw(
            ram=1000, logger=logger)
        mask_features.Execute()
        masks_apps.append(mask_features)
        dep.append(masks_dep)
        raw_features, raw_dep, raw_labels = sensor.get_time_series_raw(
            ram=1000, logger=logger)
        raw_features.Execute()
        raw_apps.append(raw_features)
        dep.append(raw_dep)
        dep.append(raw_apps)

    else:

        # mode == usually
        if sar_optical_post_fusion:
            sensor_tile_container.remove_sensor("Sentinel1")

        sensors_features = sensor_tile_container.get_sensors_features(
            available_ram=1000, logger=logger)

        interp_apps, interp_dep, feat_labels = get_application_list_from_sensors(
            sensors_features, [])
        dep.append(interp_dep)
        dep.append(interp_apps)

        masks_features = sensor_tile_container.get_sensors_time_series_masks_raw(
            available_ram=1000)

        masks_apps = []
        masks_dep = []
        sensors_names = []
        masks_count = 0

        for sname, (mask_feature, mask_dep, nb_mask) in masks_features:
            mask_feature.Execute()
            masks_apps.append(mask_feature)
            masks_dep.append(mask_dep)
            if sname.lower() != "userfeatures":
                sensors_names.append(sname.lower())
                masks_count += nb_mask

        dep.append(masks_dep)
        dep.append(masks_apps)

        raw_features = sensor_tile_container.get_sensors_time_series_raw(
            available_ram=1000)

        raw_apps, raw_dep, raw_labels = get_application_list_from_sensors(
            raw_features, [])
        if len(raw_apps) == 0:
            # userFeat
            raw_apps = interp_apps
            raw_labels = feat_labels
            dep.append(interp_dep)
            dep.append(interp_apps)
        else:
            dep.append(raw_dep)
            dep.append(raw_apps)

    # Prepare concatenation of each pipeline and prepare dictionnary output
    apps_dict = {}
    interp_name = "{}_Features.tif".format(tile)
    masks_name = "{}_binary_masks.tif".format(tile)
    raw_name = "{}_raw_data.tif".format(tile)
    features_dir = os.path.join(output_path, "features", tile, "tmp")
    features_raster = os.path.join(features_dir, interp_name)
    masks_raster = os.path.join(features_dir, masks_name)
    raw_raster = os.path.join(features_dir, raw_name)

    if len(interp_apps) > 1:

        interp_all_features = CreateConcatenateImagesApplication({
            "il":
            interp_apps,
            "out":
            features_raster
        })

        masks_all_features = CreateConcatenateImagesApplication({
            "il":
            masks_apps,
            "out":
            masks_raster
        })

        raw_all_features = CreateConcatenateImagesApplication({
            "il": raw_apps,
            "out": raw_raster
        })

    else:
        interp_all_features = interp_apps[0]
        output_param_name = getInputParameterOutput(interp_all_features)
        interp_all_features.SetParameterString(output_param_name,
                                               features_raster)

        masks_all_features = masks_apps[0]
        output_param_name = getInputParameterOutput(masks_all_features)
        masks_all_features.SetParameterString(output_param_name, masks_raster)

        raw_all_features = raw_apps[0]
        output_param_name = getInputParameterOutput(raw_all_features)
        raw_all_features.SetParameterString(output_param_name, raw_raster)

    dep.append(interp_all_features)
    dep.append(masks_all_features)
    dep.append(raw_all_features)
    apps_dict["interp"] = interp_all_features
    apps_dict["masks"] = masks_all_features
    apps_dict["raw"] = raw_all_features

    masks_labels = build_masks_labels_from_raw_labels(raw_labels,
                                                      sensors_names)
    if len(masks_labels) != masks_count:
        raise ValueError(
            f"inconsistency in masks labels, masks labels = {masks_labels} and masks count = {masks_count}"
        )
    labs_dict = {
        "interp": feat_labels,
        "raw": raw_labels,
        "masks": masks_labels
    }
    # This option allow to impose a standard labeling in vector file
    # so they can be easily merged (usefull for multi annual classification)
    if force_standard_labels:
        feat_labels = [f"value_{i}" for i in range(len(feat_labels))]

    return apps_dict, labs_dict, dep


def get_application_list_from_sensors(sensors_features, ancillary_data):
    """
    Function which get all otb application from an sensor object
    In case of reflectance, ancillary_data is a list of labels
    In case of masks, ancillary_data is a integer counting the number of masks
    """
    apps = []
    dep_app = []
    # feat_labels = []
    for _, ((sensor_features, sensor_features_dep),
            anc_data) in sensors_features:
        # print("sensor feat", sensor_features)
        sensor_features.Execute()
        apps.append(sensor_features)
        dep_app.append(sensor_features_dep)
        ancillary_data += anc_data
    return apps, dep_app, ancillary_data


if __name__ == "__main__":

    from iota2.configuration_files import read_config_file as rcf

    PARSER = argparse.ArgumentParser(
        description="Computes a time series of features")
    PARSER.add_argument("-wd",
                        dest="pathWd",
                        help="path to the working directory",
                        default=None,
                        required=False)
    PARSER.add_argument("-tile",
                        dest="tile",
                        help="tile to be processed",
                        required=True)
    PARSER.add_argument("-conf",
                        dest="pathConf",
                        help="path to the configuration file (mandatory)",
                        required=True)
    ARGS = PARSER.parse_args()

    # load configuration file
    CFG = rcf.read_config_file(ARGS.pathConf)
    PARAMS = rcf.iota2_parameters(CFG)
    SEN_PARAM = PARAMS.get_sensors_parameters(ARGS.tile)

    generate_features(ARGS.pathWd,
                      ARGS.tile,
                      sar_optical_post_fusion=False,
                      output_path=CFG.getParam("chain", "output_path"),
                      sensors_parameters=SEN_PARAM)
