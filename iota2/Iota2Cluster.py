#!/usr/bin/env python3
#-*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Iota2Cluster.py allow user to deploy iota2 on HPC architecture

check 'Iota2Cluster.py -h' for options
"""

import argparse
import logging
import os
import sys
from subprocess import PIPE, Popen
from typing import List, Union

from config import Config
from iota2.common.file_utils import ensure_dir, get_iota2_project_dir
from iota2.common.tools import convert_configuration_files as ccf
from iota2.configuration_files import read_config_file as rcf
from iota2.sequence_builders.i2_sequence_builder_merger import workflow_merger
from iota2.steps import i2_job_helpers

LOGGER = logging.getLogger("distributed.worker")


def get_ram(ram: str) -> float:
    """
        usage return ram in gb
        ram [param] [str]

        out [ram] [str] : ram in gb
    """

    ram = ram.lower().replace(" ", "")
    if "gb" in ram:
        ram_f = float(ram.split("gb")[0])
    elif "mb" in ram:
        ram_f = float(ram.split("mb")[0]) / 1024
    return ram_f


def submit_job_cmd(cfg_file: str, config_ressources: Union[str, None],
                   starting_step: int, ending_step: int, restart: bool,
                   restart_from: Union[str, None], nb_parallel_tasks: int,
                   scheduler_type: str, execution_graph_files: List[str],
                   tmp_directory: str, scheduler_name: str):
    """Build command to launch iota2 on HPC."""
    cfg = rcf.read_config_file(cfg_file)
    log_dir = os.path.join(cfg.getParam("chain", "output_path"), "logs")
    job_dir = log_dir
    ensure_dir(job_dir)

    job_helper = i2_job_helpers.JobHelper(config_ressources, "iota2_chain")

    iota2_main = os.path.join(
        job_dir,
        f"iota2.{job_helper.inventory_jobfile_helper[scheduler_name.lower()].extension}"
    )
    log_err = os.path.join(log_dir, "iota2_err.log")
    log_out = os.path.join(log_dir, "iota2_out.log")

    if os.path.exists(iota2_main):
        os.remove(iota2_main)

    cpu_from_cfg = job_helper.resources["cpu"]
    nb_cpu = nb_parallel_tasks if nb_parallel_tasks > cpu_from_cfg else cpu_from_cfg
    job_helper.force_resource("cpu", nb_cpu)

    scripts = os.path.join(get_iota2_project_dir(), "iota2")
    exe = (f"python {scripts}/Iota2.py "
           f"-config {cfg_file} "
           f"-starting_step {starting_step} "
           f"-ending_step {ending_step} "
           f"-nb_parallel_tasks {nb_parallel_tasks} "
           f"-scheduler_type {scheduler_type} "
           f"-tmp_directory_env {tmp_directory} ")

    if restart_from is not None:
        exe = f"{exe} -restart_from {restart_from}"
    if restart:
        exe = f"{exe} -restart"
    if config_ressources:
        exe = f"{exe} -config_ressources {config_ressources}"
    if execution_graph_files:
        exe = f"{exe} -execution_graph_files {' '.join(execution_graph_files)}"
    job_helper.write(scheduler_name.lower(),
                     "IOTA2",
                     iota2_main,
                     log_out,
                     log_err,
                     force_executable=exe)
    submit_cmd = (
        f"{job_helper.inventory_jobfile_helper[scheduler_name.lower()].submit_cmd} "
        f"{iota2_main}")
    return submit_cmd


def run_cluster(parsed_args: argparse.ArgumentParser):
    """launch iota2 on HPC
    """
    # #########################################################################
    # Temporarly check of config file
    # #########################################################################

    b_cfg = Config(open(parsed_args.config_path))
    if "outputPath" in b_cfg["chain"]:
        print("\033[31m" + "*" * 80)
        print("* Old configuration file detected. Conversion will be done.")
        print("*" * 80 + "\033[0m")
        print("\n" * 2)
        converter = ccf.ConversionConfigFile()
        out_name = os.path.splitext(parsed_args.config_path)[0] + "_conv.cfg"
        converter.convert(parsed_args.config_path, out_name)
        print("\n" * 2)
        print("*" * 80)
        print(f"Configuration conversion done.\nPlease check {out_name}.")
        print(f"{out_name} can be used to launch again the chain")
        return 0
    # #########################################################################
    if parsed_args.restart and parsed_args.restart_from:
        raise ValueError(
            "parameters 'restart' and 'restart_from' are not compatible")
    cfg = rcf.read_config_file(parsed_args.config_path)
    chains = cfg.get_builders()
    if len(chains) > 1:
        chain_to_process = workflow_merger(chains, parsed_args.config_path,
                                           parsed_args.config_ressources,
                                           parsed_args.scheduler_type,
                                           parsed_args.restart,
                                           parsed_args.restart_from)
    else:
        chain_to_process = chains[0](parsed_args.config_path,
                                     parsed_args.config_ressources,
                                     parsed_args.scheduler_type,
                                     parsed_args.restart,
                                     parsed_args.restart_from)
    if parsed_args.start == parsed_args.end == 0:
        all_steps = chain_to_process.get_steps_number()
        parsed_args.start = all_steps[0]
        parsed_args.end = all_steps[-1]

    first_step_index = parsed_args.start - 1
    last_step_index = parsed_args.end - 1

    chain_to_process.get_final_i2_exec_graph(first_step_index, last_step_index,
                                             parsed_args.graph_figures)

    print(
        chain_to_process.print_step_summarize(
            parsed_args.start, parsed_args.end, parsed_args.config_ressources
            is not None))

    if not parsed_args.only_summary:
        submit_cmd = submit_job_cmd(
            parsed_args.config_path, parsed_args.config_ressources,
            parsed_args.start, parsed_args.end, parsed_args.restart,
            parsed_args.restart_from, parsed_args.nb_parallel_tasks,
            parsed_args.scheduler_type, parsed_args.graph_figures,
            parsed_args.tmp_directory, parsed_args.scheduler_type.lower())
        Popen(submit_cmd, shell=True, stdout=PIPE, stderr=PIPE)
    return 0


def main():
    """Function to create conda entry point"""
    from iota2.Iota2 import iota2_arguments
    parser = iota2_arguments()

    # if we use a cluster, we must have a working directory
    parser.set_defaults(tmp_directory="TMPDIR")

    args = parser.parse_args()
    cluster_schedulers = ("Slurm", "PBS", "cluster")
    if args.scheduler_type in cluster_schedulers:
        run_cluster(args)
    else:
        print(
            f"'Iota2Cluster.py -scheduler_type' must be in {cluster_schedulers} "
        )


if __name__ == "__main__":
    sys.exit(main())
