#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import logging
import multiprocessing as mp
import os
import shutil

from osgeo import gdal

from iota2.common import otb_app_bank
from iota2.common.file_utils import ensure_dir
from iota2.common.otb_app_bank import executeApp
from iota2.simplification import nomenclature

LOGGER = logging.getLogger("distributed.worker")


def get_mask_regularisation(classes):
    """
    prepare different masks for adaptive regularization
    """
    nomenc = nomenclature.Iota2Nomenclature(classes, "cfg")
    dfnomenc = nomenc.hierarchical_nomenclature.to_frame().groupby(level=0)
    masks = []
    for idx, key in enumerate(dfnomenc.groups.keys()):

        exp = "(im1b1=="
        listclasses = []
        for elt in dfnomenc.groups[key].to_frame().groupby(level=1).groups.keys():
            listclasses.append(str(elt[0]))

        exp += " || im1b1==".join(listclasses)
        exp += ")?im1b1:0"
        output = "mask_%s.tif" % (idx)

        masks.append([idx, exp, output, len(dfnomenc.groups[key]) != 1])

    return masks


def prepare_band_raster_dataset(raster):
    """
    return an empty raster based on a existing raster file
    """

    if isinstance(raster, str):
        data = gdal.Open(raster)
    elif isinstance(raster, gdal.Dataset):
        data = raster
    else:
        raise Exception("Input raster file not managed")

    transform = data.GetGeoTransform()
    proj = data.GetProjection()
    drv = gdal.GetDriverByName("MEM")
    dst_ds = drv.Create("", data.RasterXSize, data.RasterYSize, 1, gdal.GDT_Byte)
    dst_ds.SetGeoTransform(transform)
    dst_ds.SetProjection(proj)
    dstband = dst_ds.GetRasterBand(1)

    return dst_ds, dstband


def rast_to_vect_recode(
    path,
    classif,
    vector,
    outputName,
    ram="10000",
    dtype="uint8",
    valvect=255,
    valrastout=255,
):
    """
    Convert vector in raster file and change background value

    Parameters
    ----------
    path : string
        working directory
    classif : string
        path to landcover classification
    vector : string
        vector file to rasterize
    outputName : string
        output filename and path
    ram : string
        ram for OTB applications
    dtype : string
        pixel type of the output raster
    valvect : integer
        value of vector to search
    valrastout : integer
        value to use to recode
    """

    # Empty raster
    tmpclassif = os.path.join(path, "temp.tif")
    bmapp = otb_app_bank.CreateBandMathApplication(
        {
            "il": [classif],
            "exp": "im1b1*0",
            "ram": str(1 * float(ram)),
            "pixType": dtype,
            "out": tmpclassif,
        }
    )

    process = mp.Process(target=executeApp, args=[bmapp])
    process.start()
    process.join()

    # Burn
    seamask = os.path.join(path, "masque_mer_recode.tif")
    rast_app = otb_app_bank.CreateRasterizationApplication(
        {
            "in": vector,
            "im": os.path.join(path, "temp.tif"),
            "background": 1,
            "pixType": dtype,
            "out": seamask,
        }
    )

    process = mp.Process(target=executeApp, args=[rast_app])
    process.start()
    process.join()

    # Differenciate inland water and sea water
    exp = "im1b1==%s?im2b1:%s" % (int(valvect), int(valrastout))
    bm_water = otb_app_bank.CreateBandMathApplication(
        {
            "il": [seamask, classif],
            "exp": exp,
            "ram": str(1 * float(ram)),
            "pixType": dtype,
            "out": outputName,
        }
    )

    process = mp.Process(target=executeApp, args=[bm_water])
    process.start()
    process.join()

    os.remove(seamask)
    os.remove(tmpclassif)


def add_nodata_to_raster(path, raster, exp, ram, output, dstnodata=0):
    """
    Apply no data value to raster
    """
    outbm = os.path.join(path, "mask.tif")
    bm_nodata = otb_app_bank.CreateBandMathApplication(
        {"il": raster, "exp": exp, "ram": str(ram), "pixType": "uint8", "out": outbm}
    )

    bm_nodata.ExecuteAndWriteOutput()

    gdal.Warp(
        output,
        outbm,
        dstNodata=dstnodata,
        multithread=True,
        format="GTiff",
        warpOptions=[["NUM_THREADS=ALL_CPUS"], ["OVERWRITE=TRUE"]],
    )

    os.remove(outbm)

    return output


def sieve_raster_memory(raster, threshold, output="", dstnodata=0, pixelConnection=8):
    """
    Sieve raster and store in a memory raster
    """
    # input band
    if isinstance(raster, str):
        data = gdal.Open(raster)
    elif isinstance(raster, gdal.Dataset):
        data = raster
    else:
        raise Exception("Input raster file not managed")

    srcband = data.GetRasterBand(1)

    # output band
    dst_ds, dstband = prepare_band_raster_dataset(raster)

    gdal.SieveFilter(srcband, srcband, dstband, threshold, pixelConnection)

    outformat = "MEM"
    if os.path.splitext(output)[1] == ".tif":
        outformat = "GTiff"

    sieved_raster = gdal.Warp(
        output,
        dst_ds,
        dstNodata=dstnodata,
        multithread=True,
        format=outformat,
        warpOptions=[["NUM_THREADS=ALL_CPUS"], ["OVERWRITE=TRUE"]],
    )
    return sieved_raster


def adaptive_regul(working_dir, raster, output, ram, rule, threshold):
    """
    adaptive regularization with memory pipeline
    """
    mask_name = rule[2]
    mask_name_no_ext = os.path.splitext(mask_name)[0]

    dir_to_rm = []
    if not working_dir:
        out_dir, _ = os.path.split(output)
        working_dir = os.path.join(out_dir, mask_name_no_ext)
        ensure_dir(working_dir)
        dir_to_rm.append(working_dir)

    mask = os.path.join(working_dir, mask_name)
    outpath = os.path.dirname(output)

    if not os.path.exists(mask):
        mask = add_nodata_to_raster(working_dir, raster, rule[1], ram, mask)

        if rule[3]:
            tmpsieve8 = sieve_raster_memory(mask, threshold, "", 0, 8)
            tmpsieve4 = sieve_raster_memory(tmpsieve8, threshold, mask, 0, 4)
            tmpsieve8 = tmpsieve4 = None

    shutil.copy(mask, outpath)
    for directory in dir_to_rm:
        shutil.rmtree(directory)
    return mask


def merge_regularization(
    path, rasters, threshold, output, ram, resample=None, water=None, working_dir=None
):
    """
    OSO-like regularization
    """
    if working_dir:
        path = working_dir

    if not os.path.exists(output):

        outpath = os.path.dirname(output)

        exp = "+".join(["im%sb1" % (idx + 1) for idx, x in enumerate(rasters)])

        merge = os.path.join(path, "merge.tif")
        merge = add_nodata_to_raster(path, rasters, exp, ram, merge)

        sieve8 = sieve_raster_memory(merge, threshold, "", 0, 8)

        outtmp = os.path.join(path, "class4.tif")

        if resample:
            sieve4 = sieve_raster_memory(sieve8, threshold, "", 0, 4)

            if water:
                tmprewater = gdal.Warp(
                    outtmp,
                    sieve4,
                    targetAlignedPixels=True,
                    resampleAlg="mode",
                    xRes=resample,
                    yRes=resample,
                    dstNodata=0,
                    multithread=True,
                    format="GTiff",
                    warpOptions=[["NUM_THREADS=ALL_CPUS"], ["OVERWRITE=TRUE"]],
                )
                rast_to_vect_recode(path, outtmp, water, output)
                os.remove(outtmp)
                tmprewater = sieve4 = None

            else:
                tmpre = gdal.Warp(
                    output,
                    sieve4,
                    targetAlignedPixels=True,
                    resampleAlg="mode",
                    xRes=resample,
                    yRes=resample,
                    dstNodata=0,
                    multithread=True,
                    format="GTiff",
                    warpOptions=[["NUM_THREADS=ALL_CPUS"], ["OVERWRITE=TRUE"]],
                )
                tmpre = sieve4 = None
        else:
            if water:
                sieve4 = sieve_raster_memory(sieve8, threshold, outtmp, 0, 4)
                sieve4 = sieve8 = None
                rast_to_vect_recode(path, outtmp, water, output)
                os.remove(outtmp)

            else:
                sieve4 = sieve_raster_memory(sieve8, threshold, output, 0, 4)
                sieve4 = sieve8 = None
                try:
                    shutil.copy(output, outpath)
                except IOError:
                    print("Output file %s already exists" % (output))

        os.remove(merge)

    else:
        print("Output file %s already exists" % (output))
