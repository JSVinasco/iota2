#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" Module interfacing scikit-learn and pytorch for iota2 """
import logging
import os
import pickle
import shutil
from collections import OrderedDict
from itertools import zip_longest
from typing import Any, Dict, List, Optional, Tuple, Union

import numpy as np
import rasterio
import torch
import torch.nn.functional as F
from osgeo import osr
from rasterio.plot import reshape_as_image
from rasterio.transform import Affine
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier
from sklearn.preprocessing import StandardScaler, binarize
from sklearn.svm import SVC

from iota2.common.custom_numpy_features import compute_custom_features
from iota2.common.otb_app_bank import CreateBandMathApplication
from iota2.common.raster_utils import ChunkConfig, reorder_proba_map
from iota2.common.utils import TimeIt
from iota2.learning.pytorch.dataloaders import sensors_from_labels_to_index
from iota2.learning.pytorch.torch_nn_bank import (Iota2NeuralNetwork,
                                                  Iota2NeuralNetworkFactory)
from iota2.learning.pytorch.torch_utils import StatsCalculator

SensorsParams = Dict[str, Union[str, List[str], int]]
FunctionNameWithParams = Tuple[str, Dict[str, Any]]

LOGGER = logging.getLogger("distributed.worker")


def reorder_features(expected_labels_order: List[str],
                     current_labels_order: List[str]) -> List[int]:
    """reorder features regarding current labels order and
    the expected one.

    Note
    ----
    if both orders are the same, return None
    """
    if len(expected_labels_order) != len(current_labels_order):
        raise ValueError(
            "input vector list must have the same length "
            f"{len(expected_labels_order)} and {len(current_labels_order)}")
    expected_lab_order = [lab.lower() for lab in expected_labels_order]
    lab_order = [lab.lower() for lab in current_labels_order]
    new_order = []
    same_order = []
    for expect_ind, lab in enumerate(expected_lab_order):
        try:
            index = lab_order.index(lab)
            new_order.append(index)
            same_order.append(index == expect_ind)
        except ValueError:
            raise ValueError(f"iota2 error : can't find the label '{lab}'")
    return None if all(same_order) else new_order


def choose_best_model(
    input_pytorch_models: List[str],
    selection_criterion: str,
    output_model_file: str,
    model_parameters_file: str,
    encoder_convert_file: str,
    hyperparameters_file: str,
    logger=LOGGER,
):
    """ """
    best_state_dict = None
    # if criterion is loss, then we want to minimize the value else maximize
    best_criterion_value = -1000
    is_better = lambda x, y: x > y
    if selection_criterion == "loss":
        best_criterion_value = 1000
        is_better = lambda x, y: x < y

    for input_pytorch_model in input_pytorch_models:
        with open(input_pytorch_model, "rb") as input_pytorch_model_file:
            pytorch_model_dict, stats, labels = pickle.load(
                input_pytorch_model_file)
            if is_better(pytorch_model_dict[selection_criterion]["score"],
                         best_criterion_value):
                best_state_dict = pytorch_model_dict[selection_criterion]
                best_criterion_value = pytorch_model_dict[selection_criterion][
                    "score"]

    if os.path.exists(output_model_file):
        os.remove(output_model_file)
    logger.info(
        f"saving : {output_model_file} with the epoch selection"
        f" criterion '{selection_criterion}' with the score "
        f"{best_state_dict['score']} that was obtained at epoch "
        f"{best_state_dict['epoch']} with learning rate = "
        f"{best_state_dict['hyperparameters']['learning_rate']} "
        f"and batch's size = {best_state_dict['hyperparameters']['batch_size']}"
    )
    with open(hyperparameters_file, "wb") as hyperparam:
        pickle.dump(
            best_state_dict["hyperparameters"],
            hyperparam,
            protocol=pickle.HIGHEST_PROTOCOL,
        )
    torch.save((best_state_dict["state_dict"], stats), output_model_file)
    Iota2NeuralNetwork.save(output_model_file, best_state_dict["state_dict"],
                            stats, labels)
    shutil.copy(best_state_dict["parameter_file"], model_parameters_file)
    shutil.copy(best_state_dict["enc"], encoder_convert_file)


def merge_sk_classifications(
    rasters_to_merge: Tuple[List[str], List[str]],
    mosaic_file: Tuple[str, str],
    epsg_code: int,
    working_dir: str,
    logger=LOGGER,
) -> None:
    """mosaic rasters

    Parameters
    ----------
    rasters_to_merge :
        tuple of list of raster to be merged
    epsg_code :
        epsg code
    working_dir :
        working direction
    logger :
        root logger
    """
    from iota2.common.raster_utils import merge_rasters

    # TODO find why the filter is needed...
    classifications = list(filter(os.path.exists, rasters_to_merge[0]))
    classif_mosaic = mosaic_file[0]
    confidences = list(filter(os.path.exists, rasters_to_merge[1]))
    confidence_mosaic = mosaic_file[1]

    logger.info("creating : {}".format(mosaic_file))
    merge_rasters(classifications, classif_mosaic, epsg_code, working_dir)
    merge_rasters(confidences, confidence_mosaic, epsg_code, working_dir,
                  "Float32")
    proba_map_mosaic = ""
    if len(rasters_to_merge) == 3 and len(mosaic_file) == 3:
        proba_map = list(filter(os.path.exists, rasters_to_merge[2]))
        proba_map_mosaic = mosaic_file[2]
        merge_rasters(proba_map, proba_map_mosaic, epsg_code, working_dir,
                      "Float32")
    if os.path.exists(classif_mosaic):
        for classif in classifications:
            os.remove(classif)
    if os.path.exists(confidence_mosaic):
        for confidence in confidences:
            os.remove(confidence)
    if proba_map_mosaic and os.path.exists(proba_map_mosaic):
        for proba in proba_map:
            os.remove(proba)


def do_predict(
    array: np.ndarray,
    model: Union[SVC, RandomForestClassifier, ExtraTreesClassifier],
    scaler: Optional[StandardScaler] = None,
    dtype: str = "float",
) -> np.ndarray:
    """perform scikit-learn prediction

    Parameters
    ----------
    array : np.array
        array of features to predict, shape = (y, x, features)
    model : SVC / RandomForestClassifier / ExtraTreesClassifier
        scikit-learn classifier
    scaler : StandardScaler
        scaler to standardize features
    dtype : str
        output array format

    Return
    ------
    np.array
        predictions
    """
    array_reshaped = array.reshape((array.shape[0] * array.shape[1]),
                                   array.shape[2])
    if scaler is not None:
        predicted_array = model.predict_proba(scaler.transform(array_reshaped))
    else:
        predicted_array = model.predict_proba(array_reshaped)

    predicted_array = predicted_array.reshape(array.shape[0], array.shape[1],
                                              predicted_array.shape[-1])
    return predicted_array.astype(dtype), []


def get_class(*args, **kwargs):
    """
    return the class
    """
    return kwargs["labels"][np.argmax(args)]


@TimeIt
def proba_to_label(
    proba_map: np.ndarray,
    out_classif: str,
    labels: List[int],
    transform: Affine,
    epsg_code: int,
    mask_arr: Optional[np.ndarray] = None,
) -> np.ndarray:
    """from the prediction probabilities, get the class' label

    Parameters
    ----------
    proba_map:
        array of predictions probabilities (one probability for each class)
    out_classif:
        output classification
    labels:
        list of class labels
    transform:
        geo transform
    epsg_code:
        epsg code
    mask_arr:
        mask
    """

    labels_map = np.apply_along_axis(func1d=get_class,
                                     axis=0,
                                     arr=proba_map,
                                     labels=labels)
    labels_map = labels_map.astype("int32")
    labels_map = np.expand_dims(labels_map, axis=0)
    if mask_arr is not None:
        mask_arr = binarize(mask_arr)
        labels_map = labels_map * mask_arr

    with rasterio.open(
            out_classif,
            "w",
            driver="GTiff",
            height=labels_map.shape[1],
            width=labels_map.shape[2],
            count=labels_map.shape[0],
            crs="EPSG:{}".format(epsg_code),
            transform=transform,
            dtype=labels_map.dtype,
    ) as dest:
        dest.write(labels_map)
    return labels_map


def probabilities_to_max_proba(
    proba_map: np.ndarray,
    transform: Affine,
    epsg_code: int,
    out_max_confidence: Optional[str] = None,
) -> np.ndarray:
    """from the prediction probabilities vector, get the max probility

    Parameters
    ----------
    proba_map: numpy.array
        array of probilities
    transform: Affine
        geo transform
    epsg_code: int
        epsg code
    out_max_confidence: str
        output raster path

    Return
    ------
    numpy.array
        confidence
    """

    max_confidence_arr = np.amax(proba_map, axis=0)
    max_confidence_arr = np.expand_dims(max_confidence_arr, axis=0)

    if out_max_confidence:
        with rasterio.open(
                out_max_confidence,
                "w",
                driver="GTiff",
                height=max_confidence_arr.shape[1],
                width=max_confidence_arr.shape[2],
                count=max_confidence_arr.shape[0],
                crs="EPSG:{}".format(epsg_code),
                transform=transform,
                dtype=max_confidence_arr.dtype,
        ) as dest:
            dest.write(max_confidence_arr)
    return max_confidence_arr


def scikit_learn_predict(
    mask: str,
    model: str,
    stat: str,
    out_classif: str,
    out_confidence: str,
    out_proba: str,
    working_dir: str,
    tile_name: str,
    sar_optical_post_fusion: bool,
    output_path: str,
    sensors_parameters: SensorsParams,
    pixel_type: str,
    number_of_chunks: Optional[int] = None,
    targeted_chunk: Optional[int] = None,
    logger=LOGGER,
) -> None:
    """perform scikit-learn prediction

    This function will produce 2 rasters, the classification map and the
    confidence map. The confidence map represent the classifier confidence in
    the chosen class.

    Parameters
    ----------
    mask:
        raster mask path
    model:
        input model path
    stat:
        statistics path
    out_classif:
        output classification raster path
    out_confidence:
        output confidence raster path
    out_proba:
        output confidence raster path
    working_dir:
        path to a working direction to store temporary data
    tile_name:
        tile's name
    sar_optical_post_fusion:
        flag to use post classification sar optical workflow
    output_path:
        iota2 output path
    sensors_parameters:
        sensors description
    pixel_type:
        output pixel type
    number_of_chunks:
        The prediction process can be done by strips. This parameter
        set the number of strips
    targeted_chunk:
        If this parameter is provided, only the targeted strip will be compute (parallelization).
    ram:
        ram (in mb) available
    logger :
        root logger
    """
    from functools import partial

    from iota2.common import raster_utils as rasterU
    from iota2.common.generate_features import generate_features

    mode = "usually" if "SAR.txt" not in model else "SAR"

    if working_dir:
        _, out_classif_name = os.path.split(out_classif)
        _, out_confidence_name = os.path.split(out_confidence)
        out_classif = os.path.join(working_dir, out_classif_name)
        out_confidence = os.path.join(working_dir, out_confidence_name)
        if out_proba:
            _, out_proba_name = os.path.split(out_proba)
            out_proba = os.path.join(working_dir, out_proba_name)

    with open(model, "rb") as model_file:
        model, scaler = pickle.load(model_file)
    # if hasattr(model, "n_jobs"):
    # model.n_jobs = -1

    if number_of_chunks and targeted_chunk:
        if targeted_chunk > number_of_chunks - 1:
            raise ValueError(
                "targeted_chunk must be inferior to the number of chunks")

    function_partial = partial(do_predict, model=model, scaler=scaler)
    classification_dir = os.path.join(output_path, "classif")
    dict_stack, labs_dict, _ = generate_features(
        working_dir,
        tile_name,
        sar_optical_post_fusion=sar_optical_post_fusion,
        output_path=output_path,
        sensors_parameters=sensors_parameters,
        mode=mode,
        logger=logger,
    )
    feat_labels = labs_dict["interp"]
    dict_stack["enable_interp"] = True
    dict_stack["enable_raw"] = False
    dict_stack["enable_masks"] = False
    logger.info("producing {}".format(out_classif))

    # ~ sk-learn provide only methods 'predict' and 'predict_proba', no proba_max.
    # ~ Then we have to compute the full probability vector to get the maximum
    # ~ confidence and generate the confidence map
    (
        predicted_proba,
        _,
        transform,
        epsg,
        masks,
        otb_img,
        projection,
        deps_pipeline,
    ) = rasterU.insert_external_function_to_pipeline(
        dict_stack,  # feat_stack,
        feat_labels,
        working_dir,
        function_partial,
        rasterU.ChunkConfig(
            chunk_size_mode="split_number",
            number_of_chunks=number_of_chunks,
            chunk_size_x=None,
            chunk_size_y=None,
        ),
        out_proba,
        mask_valid_data=mask,
        mask_value=0,
        targeted_chunk=targeted_chunk,
        output_number_of_bands=len(model.classes_),
        logger=logger,
    )
    logger.info("predictions done")
    if len(masks) > 1:
        raise ValueError("Only one mask is expected")

    if targeted_chunk is not None:
        out_classif = out_classif.replace(
            ".tif", "_SUBREGION_{}.tif".format(targeted_chunk))
        out_confidence = out_confidence.replace(
            ".tif", "_SUBREGION_{}.tif".format(targeted_chunk))

    proba_to_label(predicted_proba, out_classif, model.classes_, transform,
                   epsg, masks[0])
    probabilities_to_max_proba(predicted_proba, transform, epsg,
                               out_confidence)

    if working_dir:
        shutil.copy(out_classif, classification_dir)
        shutil.copy(out_confidence, classification_dir)
        os.remove(out_classif)
        os.remove(out_confidence)
        if out_proba:
            shutil.copy(out_proba, classification_dir)
            os.remove(out_proba)


def get_raw_data(self):
    """function use with the custom numpy features and deal
    with irregular input dates
    """
    coef, labels_refl = self.get_filled_stack()
    masks, label_masks = self.get_filled_masks()

    coef = np.concatenate((coef, masks), axis=2)
    labels = labels_refl + label_masks
    return coef, labels


def do_nothing(array: np.ndarray):
    """function use with the insert_external_function_to_pipeline workflow"""
    return array, []


def otb_pipeline_to_named_tensors(input_features, input_masks,
                                  sensors_position, device: str) -> Dict:
    """ """
    buff = {}
    for sensors_name, sensor_info in sensors_position.items():
        s_name = sensors_name.lower()
        buff[s_name] = {"data": None, "mask": None}
        buff[s_name]["data"] = torch.from_numpy(
            np.take(input_features, sensor_info["index"], axis=1).reshape(
                -1,
                int(len(sensor_info["index"]) / sensor_info["nb_date_comp"]),
                int(sensor_info["nb_date_comp"]),
            )).to(device)
        if "mask_index" in sensor_info and sensor_info["mask_index"]:
            buff[s_name]["mask"] = torch.from_numpy(
                np.take(input_masks, sensor_info["mask_index"],
                        axis=1).reshape(-1, len(sensor_info["mask_index"]),
                                        1)).to(device)
    s2_theia = buff["sentinel2"]["data"] if "sentinel2" in buff else None
    s2_s2c = buff["sentinel2s2c"]["data"] if "sentinel2s2c" in buff else None
    s2_l3a = buff["sentinel2l3a"]["data"] if "sentinel2l3a" in buff else None
    land5 = buff["landsat5old"]["data"] if "landsat5old" in buff else None
    l8_old = buff["landsat8old"]["data"] if "landsat8old" in buff else None
    land8 = buff["landsat8"]["data"] if "landsat8" in buff else None
    s1_desvv = buff["sentinel1_desvv"][
        "data"] if "sentinel1_desvv" in buff else None
    s1_desvh = buff["sentinel1_desvh"][
        "data"] if "sentinel1_desvh" in buff else None
    s1_ascvv = buff["sentinel1_ascvv"][
        "data"] if "sentinel1_ascvv" in buff else None
    s1_ascvh = buff["sentinel1_ascvh"][
        "data"] if "sentinel1_ascvh" in buff else None

    s2_theia_masks = buff["sentinel2"]["mask"] if "sentinel2" in buff else None
    s2_s2c_masks = buff["sentinel2s2c"][
        "mask"] if "sentinel2s2c" in buff else None
    s2_l3a_masks = buff["sentinel2l3a"][
        "mask"] if "sentinel2l3a" in buff else None
    l5_masks = buff["landsat5old"]["mask"] if "landsat5old" in buff else None
    l8_old_masks = buff["landsat8old"][
        "mask"] if "landsat8old" in buff else None
    l8_masks = buff["landsat8"]["mask"] if "landsat8" in buff else None
    s1_desvv_masks = (buff["sentinel1_desvv"]["mask"]
                      if "sentinel1_desvv" in buff else None)
    s1_desvh_masks = (buff["sentinel1_desvh"]["mask"]
                      if "sentinel1_desvh" in buff else None)
    s1_ascvv_masks = (buff["sentinel1_ascvv"]["mask"]
                      if "sentinel1_ascvv" in buff else None)
    s1_ascvh_masks = (buff["sentinel1_ascvh"]["mask"]
                      if "sentinel1_ascvh" in buff else None)

    params = {
        "s2_theia": s2_theia,
        "s2_s2c": s2_s2c,
        "s2_l3a": s2_l3a,
        "l5": land5,
        "l8_old": l8_old,
        "l8": land8,
        "s1_desvv": s1_desvv,
        "s1_desvh": s1_desvh,
        "s1_ascvv": s1_ascvv,
        "s1_ascvh": s1_ascvh,
        "l5_masks": l5_masks,
        "l8_masks": l8_masks,
        "l8_old_masks": l8_old_masks,
        "s2_theia_masks": s2_theia_masks,
        "s2_s2c_masks": s2_s2c_masks,
        "s2_l3a_masks": s2_l3a_masks,
        "s1_desvv_masks": s1_desvv_masks,
        "s1_desvh_masks": s1_desvh_masks,
        "s1_ascvv_masks": s1_ascvv_masks,
        "s1_ascvh_masks": s1_ascvh_masks,
    }
    return params


def do_torch_prediction(
    nn_name: str,
    feat_stack: np.ndarray,
    model_file: str,
    model_parameters_file: str,
    transform: rasterio.transform,
    classif_out: str,
    confidence_out: str,
    epsg: int,
    sensors_ind: Dict,
    actual_labels_order: List[str],
    external_nn_module: str = "",
    max_inference_size: int = 1000,
    out_proba: str = "",
    labels_converter: Dict = {},
    mask_stack: Optional[np.ndarray] = None,
    mask_spatial: Optional[np.ndarray] = None,
    mask_value: int = 0,
    nb_class: Optional[int] = None,
    coeff_vect_proba: int = 1000,
) -> None:
    """do torch predction

    Parameters
    ----------

    nn_name
        neural network name
    feat_stack
        input features stack
    model_file
        torch model save as a pickle file
    transform
        projection informations
    classif_out
        output classification file
    confidence_out
        output confidence file
    epsg
        epsg code
    sensors_ind
        dictionnary containing useful information to reshape
        the input 'feat_stack' according to sensors
    actual_labels_order
        feature's order in feat_stack numpy array
    max_inference_size
        maximum batch size for inference
    out_proba
        file to save the vector of class proabilities
    mask_stack
        input mask stack
    mask_spatial
        every output pixels are element-wise multiply by mask_spatial,
        output = output * mask_spatial
    coeff_vect_proba
        multiply vector of probabilities by this coeff
    """
    able_to_infer = bool(sensors_ind)
    device = torch.device(
        "cuda") if torch.cuda.is_available() else torch.device("cpu")
    if out_proba and nb_class is None:
        raise ValueError(
            "if parameter 'outproba' is enable, the parameter 'nb_class' is mandatory"
        )

    with open(model_parameters_file, "rb") as file_instance_nn_parameters:
        instance_param_dict = pickle.load(file_instance_nn_parameters)
    model = Iota2NeuralNetworkFactory().get_nn(
        nn_name, instance_param_dict, external_nn_module=external_nn_module)

    with open(model_file, "rb") as model_file_ctx:
        state_dict, stats, labels_order = pickle.load(model_file_ctx)

    new_feat_order = reorder_features(labels_order, actual_labels_order)
    if new_feat_order is not None:
        feat_stack = feat_stack[:, :, new_feat_order]

    model.load_state_dict(state_dict)
    model.eval()
    model.to(device)
    stats = StatsCalculator.stats_to_device(stats, device)
    model.set_stats(stats)

    feat_reshaped = feat_stack.reshape(
        (feat_stack.shape[0] * feat_stack.shape[1]), feat_stack.shape[2])
    if mask_stack is not None:
        mask_reshaped = mask_stack.reshape(
            (mask_stack.shape[0] * mask_stack.shape[1]),
            mask_stack.shape[2]).astype(np.float32)

    def max_inference_gen(tensor, slice_len):
        start = 0
        while start < tensor.shape[0]:
            yield tensor[start:start + slice_len, :]
            start += slice_len

    if able_to_infer and (mask_spatial is None or np.sum(mask_spatial) != 0):
        batch_data_gen = max_inference_gen(feat_reshaped, max_inference_size)
        if mask_stack is not None:
            batch_mask_gen = max_inference_gen(mask_reshaped,
                                               max_inference_size)
        else:
            batch_mask_gen = []

        for batch_num, (batch_data_to_infer, batch_mask_to_infer) in enumerate(
                zip_longest(batch_data_gen, batch_mask_gen)):
            kwargs_tensors = otb_pipeline_to_named_tensors(
                batch_data_to_infer, batch_mask_to_infer, sensors_ind, device)
            with torch.no_grad():
                y_hat = model(**kwargs_tensors)
                y_hat = F.softmax(y_hat, dim=1)
                confidence_batch, predicted_batch = y_hat.topk(1, dim=1)
                if out_proba:
                    nb_class = len(y_hat[0])
                    all_confidences, all_predictions = y_hat.topk(nb_class,
                                                                  dim=1)
                    sorted_idx, new_idx = torch.sort(all_predictions, dim=1)
                    all_confidences_bands = torch.gather(all_confidences,
                                                         dim=1,
                                                         index=new_idx)
                    all_confidences_array_batch = all_confidences_bands
                    all_confidences_array_batch = all_confidences_array_batch.reshape(
                        -1, nb_class).T

                if batch_num == 0:
                    predicted = predicted_batch
                    confidence = confidence_batch
                    if out_proba:
                        all_confidences_array = all_confidences_array_batch
                else:
                    predicted = torch.cat([predicted, predicted_batch], axis=0)
                    confidence = torch.cat([confidence, confidence_batch],
                                           axis=0)
                    if out_proba:
                        all_confidences_array = torch.cat([
                            all_confidences_array, all_confidences_array_batch
                        ],
                                                          axis=1)
    else:
        predicted = np.ones(
            (feat_stack.shape[0], feat_stack.shape[1])) * mask_value
        confidence = np.ones(
            (feat_stack.shape[0], feat_stack.shape[1])) * mask_value
        if out_proba:
            all_confidences_array = (np.ones(
                (nb_class, feat_stack.shape[0], feat_stack.shape[1])) *
                                     mask_value)
            all_confidences_array = all_confidences_array.astype(np.float32)
    predicted_array = predicted.reshape(feat_stack.shape[0],
                                        feat_stack.shape[1])
    confidence_array = confidence.reshape(feat_stack.shape[0],
                                          feat_stack.shape[1])
    if out_proba:
        all_confidences_array = (all_confidences_array.cpu().reshape(
            -1, feat_stack.shape[0], feat_stack.shape[1]).numpy() *
                                 coeff_vect_proba)
    if isinstance(predicted_array, torch.Tensor):
        labels_map = np.expand_dims(predicted_array.cpu(), axis=0)
        confidence_map = np.expand_dims(confidence_array.cpu(), axis=0)
    else:
        labels_map = np.expand_dims(predicted_array, axis=0)
        confidence_map = np.expand_dims(confidence_array, axis=0)
    if mask_spatial is not None:
        labels_map = labels_map * mask_spatial
        confidence_map = confidence_map * mask_spatial
        if out_proba:
            all_confidences_array = all_confidences_array * mask_spatial

    labels_map = labels_map.astype(np.uint8)
    confidence_map = confidence_map.astype(np.float32)
    with rasterio.open(
            classif_out,
            "w",
            driver="GTiff",
            height=labels_map.shape[1],
            width=labels_map.shape[2],
            count=labels_map.shape[0],
            crs="EPSG:{}".format(epsg),
            transform=transform,
            dtype=np.uint8,
    ) as dest:
        dest.write(labels_map)
    with rasterio.open(
            confidence_out,
            "w",
            driver="GTiff",
            height=labels_map.shape[1],
            width=labels_map.shape[2],
            count=labels_map.shape[0],
            crs="EPSG:{}".format(epsg),
            transform=transform,
            dtype=np.float32,
    ) as dest:
        dest.write(confidence_map)
    if out_proba:
        with rasterio.open(
                out_proba,
                "w",
                driver="GTiff",
                height=all_confidences_array.shape[1],
                width=all_confidences_array.shape[2],
                count=all_confidences_array.shape[0],
                crs="EPSG:{}".format(epsg),
                transform=transform,
                dtype=np.float32,
        ) as dest:
            dest.write(all_confidences_array)

    if labels_converter:
        # manage the case the label '0' from pytorch is the masked value in masks
        # we multiply again the output by the mask raster
        tmp_mask = classif_out.replace(".tif", "_MASK.tif")
        if mask_spatial is not None:
            mask_spatial = np.expand_dims(mask_spatial, axis=0)
            with rasterio.open(
                    tmp_mask,
                    "w",
                    driver="GTiff",
                    height=labels_map.shape[1],
                    width=labels_map.shape[2],
                    count=labels_map.shape[0],
                    crs="EPSG:{}".format(epsg),
                    transform=transform,
                    dtype=np.float32,
            ) as dest:
                dest.write(mask_spatial)

        sub_exp = []
        for torch_label, i2_label in labels_converter.items():
            sub_exp.append(f"im1b1 == {torch_label} ? {i2_label}")
        exp = f"{':'.join(sub_exp)} : 0"
        img_list = [classif_out]
        if mask_spatial is not None:
            exp = f"im2b1 * ({exp})"
            img_list.append(tmp_mask)
        bm_converter = CreateBandMathApplication({
            "il": img_list,
            "out": classif_out,
            "exp": exp,
            "pixType": "uint8"
        })
        bm_converter.ExecuteAndWriteOutput()
        if os.path.exists(tmp_mask):
            os.remove(tmp_mask)


def guess_max_inference_size(hyperparam_file: str) -> int:
    """ """
    with open(hyperparam_file, "rb") as handle:
        hyperparams = pickle.load(handle)
    return hyperparams["batch_size"]


def pytorch_predict(
    nn_name: str,
    mask: str,
    model: str,
    model_parameters_file: str,
    encoder_convert_file: str,
    out_classif: str,
    out_confidence: str,
    out_proba: str,
    working_dir: str,
    tile_name: str,
    sar_optical_post_fusion: bool,
    output_path: str,
    sensors_parameters: SensorsParams,
    hyperparam_file: str,
    external_nn_module: Optional[str] = None,
    max_inference_size: Optional[int] = None,
    all_class_labels: Optional[List[int]] = None,
    concat_mode: bool = False,
    custom_features_enable: bool = False,
    custom_features_module: str = "",
    custom_features_functions: List[FunctionNameWithParams] = [],
    number_of_chunks: Optional[int] = None,
    targeted_chunk: Optional[int] = None,
    enabled_raw: bool = False,
    enabled_gap: bool = True,
    features_from_raw_dates: bool = False,
    sensors_dates: Optional[Dict] = None,
    logger=LOGGER,
) -> None:
    """perform scikit-learn prediction

    This function will produce 2 rasters, the classification map and the
    confidence map. The confidence map represent the classifier confidence in
    the chosen class.

    Parameters
    ----------
    nn_name
        neural network name
    mask: str
        raster mask path
    model: str
        input model path
    model_parameters_file
        file containing pytorch inputs arguments (to instanciate it)
    encoder_convert_file
        file containing one hot encoder dictionary converter
    out_classif: str
        output classification raster path
    out_confidence: str
        output confidence raster path
    out_proba: str
        output confidence raster path
    working_dir: str
        path to a working direction to store temporary data
    tile_name: str
        tile's name
    sar_optical_post_fusion: bool
        flag to use post classification sar optical workflow
    output_path: str
        iota2 output path
    sensors_parameters:
        sensors description
    hyperparam_file : str
        pickle file containing model hyperparameters (batch's size...)
    external_nn_module
        python module containing the user neural network class
    max_inference_size
        maximum inference size at a time
    number_of_chunks: int
        The prediction process can be done by strips. This parameter
        set the number of strips
    targeted_chunk: int
        If this parameter is provided, only the targeted strip will be compute (parallelization).
    enabled_raw:bool
        enable custom features to access raw data (not interpolated)
    enable_gap:bool
        enable custom features to access interpolated data
    features_from_raw_dates: bool
        flag to get classify pixels from raw data
    sensors_dates:dict
        dates for each enabled sensors
    ram: int
        ram (in mb) available
    logger : logging
        root logger
    """
    from functools import partial

    from iota2.common import raster_utils as rasterU
    from iota2.common.generate_features import generate_features

    if not all_class_labels and out_proba:
        raise ValueError(
            "if proability map is asked, the parameter 'all_class_labels' become mandatory"
        )

    mode = "usually" if "SAR.txt" not in model else "SAR"

    if working_dir:
        _, out_classif_name = os.path.split(out_classif)
        _, out_confidence_name = os.path.split(out_confidence)
        out_classif = os.path.join(working_dir, out_classif_name)
        out_confidence = os.path.join(working_dir, out_confidence_name)
        if out_proba:
            _, out_proba_name = os.path.split(out_proba)
            out_proba = os.path.join(working_dir, out_proba_name)

    if number_of_chunks and targeted_chunk:
        if targeted_chunk > number_of_chunks - 1:
            raise ValueError(
                "targeted_chunk must be inferior to the number of chunks")

    # function_partial = partial(do_predict, model=model, scaler=scaler)
    classification_dir = os.path.join(output_path, "classif")
    (apps_dict, labs_dict, dep_features) = generate_features(
        working_dir,
        tile_name,
        sar_optical_post_fusion=sar_optical_post_fusion,
        output_path=output_path,
        sensors_parameters=sensors_parameters,
        mode=mode,
        logger=logger,
    )

    apps_dict["enable_interp"] = enabled_gap
    apps_dict["enable_raw"] = enabled_raw
    apps_dict["enable_masks"] = enabled_raw
    logger.info("producing {}".format(out_classif))

    # TODO : chunk_size_x and chunk_size_y must be optionnal parameters
    if features_from_raw_dates:
        functions = [("get_raw_data", {})]
        module_path = __file__
        fill_missing_dates = True
        enabled_gap = False
        apps_dict["enable_interp"] = False
    if custom_features_enable:
        functions = custom_features_functions
        module_path = custom_features_module
        fill_missing_dates = enabled_raw
    if features_from_raw_dates or custom_features_enable:
        sensors_dates_str = OrderedDict()
        for sensor_name, sensor_dates in sensors_dates.items():
            sensors_dates_str[sensor_name] = [
                str(date) for date in sensor_dates
            ]
        (
            otbimage,
            feat_labels,
            transform,
            mask_list,
            projection,
            deps_pipeline,
        ) = compute_custom_features(
            tile=tile_name,
            output_path=output_path,
            sensors_parameters=sensors_parameters,
            module_path=module_path,
            list_functions=functions,
            otb_pipelines=apps_dict,
            feat_labels=labs_dict["interp"],
            path_wd=working_dir,
            mask_valid_data=mask,
            targeted_chunk=targeted_chunk,
            chunk_config=ChunkConfig(
                chunk_size_mode="split_number",
                number_of_chunks=number_of_chunks,
                chunk_size_x=None,
                chunk_size_y=None,
            ),
            enabled_raw=enabled_raw,
            enabled_gap=enabled_gap,
            fill_missing_dates=fill_missing_dates,
            missing_refl_values=np.nan,
            all_dates_dict=sensors_dates_str,
            concat_mode=concat_mode,
            expected_output_bands=1,
            allow_nans=True,
            logger=logger,
        )
        # projection = osr.SpatialReference()
        # projection.ImportFromWkt(otbimage["metadata"]["ProjectionRef"])
        epsg = projection.GetAttrValue("AUTHORITY", 1)
        if len(mask_list) > 1:
            raise ValueError("Only one mask is expected")
        mask_array = mask_list[0]
    else:
        feat_labels = labs_dict["interp"]
        function_partial = partial(do_nothing)
        (
            data_stack,
            _,
            transform,
            epsg,
            masks,
            otbimage,
            projection,
            deps_pipeline,
        ) = rasterU.insert_external_function_to_pipeline(
            apps_dict,  # feat_stack,
            feat_labels,
            working_dir,
            function_partial,
            rasterU.ChunkConfig(
                chunk_size_mode="split_number",
                number_of_chunks=number_of_chunks,
                chunk_size_x=None,
                chunk_size_y=None,
            ),
            None,
            mask_valid_data=mask,
            mask_value=0,
            targeted_chunk=targeted_chunk,
            output_number_of_bands=len(feat_labels),
            logger=logger,
        )
        if len(masks) > 1:
            raise ValueError("Only one mask is expected")
        mask_array = masks[0]
        data_stack = reshape_as_image(data_stack)
        data_mask = None
        # TODO : segfault can append if we try to print here the otbimage variable
        # it cannot be reproductible in a unittest case...

    # then do the prediction on otbimage
    if targeted_chunk is not None:
        out_classif = out_classif.replace(
            ".tif", "_SUBREGION_{}.tif".format(targeted_chunk))
        out_confidence = out_confidence.replace(
            ".tif", "_SUBREGION_{}.tif".format(targeted_chunk))
        if out_proba:
            out_proba = out_proba.replace(
                ".tif", "_SUBREGION_{}.tif".format(targeted_chunk))
    index_mask = None
    for index_mask, label in enumerate(feat_labels):
        if "mask" in label.lower():
            break

    masks_labels = None
    if features_from_raw_dates:
        data_stack = otbimage["array"][:, :, :index_mask].astype(np.float32)
        data_mask = otbimage["array"][:, :, index_mask:]
        masks_labels = feat_labels[index_mask:]

    elif custom_features_enable:
        data_stack = otbimage["array"]
        data_mask = None

    with open(encoder_convert_file, "rb") as conv_file:
        labels_converter = pickle.load(conv_file)

    model_class = sorted([i2label for _, i2label in labels_converter.items()])
    # without copy, the torch probability vector may contain 'nan' values
    data_stack_cp = data_stack.copy()

    feat_labels_no_mask = [
        feat for feat in feat_labels if "mask" not in feat.lower()
    ]

    sensors_ind = sensors_from_labels_to_index(feat_labels_no_mask,
                                               masks_labels)
    learning_batch_size = guess_max_inference_size(hyperparam_file)
    max_inference_size = (max_inference_size
                          if max_inference_size else learning_batch_size)

    do_torch_prediction(
        nn_name=nn_name,
        feat_stack=data_stack_cp,
        mask_stack=data_mask,
        model_file=model,
        model_parameters_file=model_parameters_file,
        transform=transform,
        classif_out=out_classif,
        confidence_out=out_confidence,
        epsg=int(epsg),
        sensors_ind=sensors_ind,
        actual_labels_order=feat_labels_no_mask,
        external_nn_module=external_nn_module,
        max_inference_size=max_inference_size,
        mask_spatial=mask_array,
        labels_converter=labels_converter,
        out_proba=out_proba,
        nb_class=len(model_class),
    )

    if out_proba:
        # all_class_labels
        reorder_proba_map(out_proba, out_proba, model_class, all_class_labels,
                          "float")

    if working_dir:
        shutil.copy(out_classif, classification_dir)
        shutil.copy(out_confidence, classification_dir)
        os.remove(out_classif)
        os.remove(out_confidence)
        if out_proba:
            shutil.copy(out_proba, classification_dir)
            os.remove(out_proba)
