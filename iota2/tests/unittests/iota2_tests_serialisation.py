#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
from pathlib import Path

import numpy as np
import pytest

import iota2.tests.utils.tests_utils_vectors as TUV
from iota2.common.raster_utils import raster_to_array
from iota2.simplification import build_crown_raster as bcr
from iota2.simplification import grid_generator as gridg
from iota2.simplification import search_crown_tile as sct
from iota2.tests.utils.asserts_utils import AssertsFilesUtils

IOTA2DIR = os.environ.get("IOTA2DIR")

if IOTA2DIR is None:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR)


@pytest.mark.vecto
class Iota2TestSerialisation(AssertsFilesUtils):
    """Test serialization."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.rasterclump = str(
            str(IOTA2DIR / "data" / "references" / "posttreat" / "clump32bits.tif")
        )
        cls.raster = str(
            str(IOTA2DIR / "data" / "references" / "posttreat" / "classif_clump.tif")
        )
        cls.outfileref = str(
            str(IOTA2DIR / "data" / "references" / "posttreat" / "grid.shp")
        )
        cls.outseriaref = str(
            str(
                IOTA2DIR / "data" / "references" / "posttreat" / "tiles" / "crown_0.tif"
            )
        )
        cls.outtileref = str(
            str(IOTA2DIR / "data" / "references" / "posttreat" / "tiles" / "tile_0.tif")
        )
        cls.grasslib = os.environ.get("GRASSDIR")

        if cls.grasslib is None:
            raise Exception("GRASSDIR not initialized")

    def test_iota2_serialisation(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test Serialisation process."""
        wd = i2_tmpdir / "wd"
        Path.mkdir(wd, exist_ok=True)
        out = i2_tmpdir / "out"
        Path.mkdir(out, exist_ok=True)
        outpathtile = out / "tiles"
        Path.mkdir(outpathtile, exist_ok=True)
        outfile = str(out / "grid.shp")
        outseria = str(out / "tiles" / "crown_0.tif")
        outtile = str(out / "tiles" / "tile_0.tif")

        # Grid generation test
        gridg.grid_generate(outfile, 2, 2154, self.rasterclump)
        assert TUV.compare_vector_file(
            outfile, self.outfileref, "coordinates", "polygon", "ESRI Shapefile"
        ), "Generated grid shapefile vector does not fit with reference file"

        # Crown entities test
        sct.search_crown_tile(
            inpath=str(wd),
            raster=self.raster,
            clump=self.rasterclump,
            grid=self.outfileref,
            outpath=str(outpathtile),
            ngrid=0,
        )

        outtest = raster_to_array(str(outseria))
        outref = raster_to_array(self.outseriaref)
        assert np.array_equal(outtest, outref)

        # Crown building test
        bcr.manage_blocks(str(outpathtile), 0, 20, str(wd), str(outpathtile))

        outtest = raster_to_array(str(outtile))
        outtileref = raster_to_array(self.outtileref)
        assert np.array_equal(outtest, outtileref)
