#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import pytest

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


class Iota2TestServiceCompareImageFile:
    """Test class ServiceCompareImageFile."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.ref_data = IOTA2DIR / "data" / "references" / "ServiceCompareImageFile"

    def test_same_image(self):
        """We check if it is the same file."""
        service_compare_image_file = TUR.service_compare_image_file()
        file1 = str(self.ref_data / "raster1.tif")
        nb_diff = service_compare_image_file.gdalFileCompare(file1, file1)
        # we check if it is the same file
        assert nb_diff == 0

    def test_different_image(self):
        """We check if differences are detected."""
        service_compare_image_file = TUR.service_compare_image_file()
        file1 = str(self.ref_data / "raster1.tif")
        file2 = str(self.ref_data / "raster2.tif")
        nb_diff = service_compare_image_file.gdalFileCompare(file1, file2)
        assert nb_diff != 0

    def test_error_image(self):
        """We check if an error is detected."""
        service_compare_image_file = TUR.service_compare_image_file()
        file1 = str(self.ref_data / "rasterNotHere.tif")
        file2 = str(self.ref_data / "raster2.tif")
        pytest.raises(Exception, service_compare_image_file.gdalFileCompare,
                      file1, file2)
