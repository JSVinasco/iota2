# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Test GenResults"""
# python -m unittest Iota2TestsGenResults
import os
import shutil
from collections import OrderedDict
from pathlib import Path

import numpy as np

from iota2.common.utils import run
from iota2.validation import gen_results as GR
from iota2.validation import results_utils as resU

# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True
IOTA2DIR = os.environ.get('IOTA2DIR')
if IOTA2DIR is None:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(os.environ.get('IOTA2DIR'))


class Iota2TestGenResults:
    """Test gen results module functions."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.ref_out_dir = str(IOTA2DIR / "data" / "references" / "genResults" /
                              "Input" / "final")
        cls.nomenclature_file = str(IOTA2DIR / "data" / "references" /
                                    "genResults" / "Input" /
                                    "nomenclature.txt")

    def test_gen_results(self, i2_tmpdir, rm_tmpdir_on_success):
        '''TEST genResults method.'''
        i2_out_dir = str(i2_tmpdir / "Inputs" / "final")
        classif_final = str(Path(i2_out_dir))
        shutil.copytree(self.ref_out_dir, i2_out_dir)
        GR.find_entry_for_gen_results([i2_out_dir],
                                      i2_out_dir,
                                      self.nomenclature_file, [classif_final],
                                      [classif_final],
                                      labels_conversion_table={"11": "11"})

        outfile = os.path.join(IOTA2DIR, 'data', 'references', 'genResults',
                               'Output', 'RESULTS.txt')

        assert 0 == run(f"diff {outfile} "
                        f"{str(Path(i2_out_dir) / 'RESULTS.txt')}")

    def test_results_utils(self):
        """Test results utils."""
        conf_mat_array = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

        norm_ref_ref = np.array([[0.16666667, 0.33333333, 0.5],
                                 [0.26666667, 0.33333333, 0.4],
                                 [0.29166667, 0.33333333, 0.375]])

        norm_prod_ref = np.array([[0.08333333, 0.13333333, 0.16666667],
                                  [0.33333333, 0.33333333, 0.33333333],
                                  [0.58333333, 0.53333333, 0.5]])

        norm_ref_test = resU.normalize_conf(conf_mat_array, norm="ref")
        assert np.allclose(
            norm_ref_ref,
            norm_ref_test), "problem with the normalization by ref"

        norm_prod_test = resU.normalize_conf(conf_mat_array, norm="prod")
        assert np.allclose(
            norm_prod_ref,
            norm_prod_test), "problem with the normalization by prod"

    def test_get_coeff(self):
        """Test confusion matrix coefficients computation."""
        # construct input
        confusion_matrix = OrderedDict([
            (1, OrderedDict([(1, 50.0), (2, 78.0), (3, 41.0)])),
            (2, OrderedDict([(1, 20.0), (2, 52.0), (3, 31.0)])),
            (3, OrderedDict([(1, 27.0), (2, 72.0), (3, 98.0)]))
        ])
        k_ref = 0.15482474945066724
        oa_ref = 0.42643923240938164
        p_ref = OrderedDict([(1, 0.5154639175257731), (2, 0.25742574257425743),
                             (3, 0.5764705882352941)])
        r_ref = OrderedDict([(1, 0.2958579881656805), (2, 0.5048543689320388),
                             (3, 0.49746192893401014)])
        f_ref = OrderedDict([(1, 0.3759398496240602), (2, 0.3409836065573771),
                             (3, 0.5340599455040872)])

        k_test, oa_test, p_test, r_test, f_test = resU.get_coeff(
            confusion_matrix)

        assert k_ref == k_test, "Kappa computation is broken"
        assert oa_test == oa_ref, "Overall accuracy computation is broken"
        assert p_test == p_ref, "Precision computation is broken"
        assert r_test == r_ref, "Recall computation is broken"
        assert f_test == f_ref, "F-Score computation is broken"
