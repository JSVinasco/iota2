#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
from pathlib import Path

import pytest

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.sensors.Landsat_5_old import landsat_5_old
from iota2.sensors.Landsat_8 import landsat_8
from iota2.sensors.Landsat_8_old import landsat_8_old
from iota2.sensors.sensors_container import sensors_container
from iota2.sensors.Sentinel_2 import sentinel_2
from iota2.sensors.Sentinel_2_L3A import sentinel_2_l3a
from iota2.sensors.Sentinel_2_S2C import sentinel_2_s2c
from iota2.sensors.User_features import user_features
from iota2.tests.utils.asserts_utils import AssertsFilesUtils

IOTA2DIR = os.environ.get('IOTA2DIR')
if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")


@pytest.mark.config
@pytest.mark.sensors
class Iota2TestSensors(AssertsFilesUtils):
    """Check if sensors object can be instanciate."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.expected_s2_labels = [
            'Sentinel2_B2_20200101', 'Sentinel2_B3_20200101',
            'Sentinel2_B4_20200101', 'Sentinel2_B5_20200101',
            'Sentinel2_B6_20200101', 'Sentinel2_B7_20200101',
            'Sentinel2_B8_20200101', 'Sentinel2_B8A_20200101',
            'Sentinel2_B11_20200101', 'Sentinel2_B12_20200101',
            'Sentinel2_B2_20200111', 'Sentinel2_B3_20200111',
            'Sentinel2_B4_20200111', 'Sentinel2_B5_20200111',
            'Sentinel2_B6_20200111', 'Sentinel2_B7_20200111',
            'Sentinel2_B8_20200111', 'Sentinel2_B8A_20200111',
            'Sentinel2_B11_20200111', 'Sentinel2_B12_20200111',
            'Sentinel2_NDVI_20200101', 'Sentinel2_NDVI_20200111',
            'Sentinel2_NDWI_20200101', 'Sentinel2_NDWI_20200111',
            'Sentinel2_Brightness_20200101', 'Sentinel2_Brightness_20200111'
        ]

        cls.expected_s2_s2c_labels = [
            'Sentinel2S2C_B02_20190501', 'Sentinel2S2C_B03_20190501',
            'Sentinel2S2C_B04_20190501', 'Sentinel2S2C_B05_20190501',
            'Sentinel2S2C_B06_20190501', 'Sentinel2S2C_B07_20190501',
            'Sentinel2S2C_B08_20190501', 'Sentinel2S2C_B8A_20190501',
            'Sentinel2S2C_B11_20190501', 'Sentinel2S2C_B12_20190501',
            'Sentinel2S2C_B02_20190504', 'Sentinel2S2C_B03_20190504',
            'Sentinel2S2C_B04_20190504', 'Sentinel2S2C_B05_20190504',
            'Sentinel2S2C_B06_20190504', 'Sentinel2S2C_B07_20190504',
            'Sentinel2S2C_B08_20190504', 'Sentinel2S2C_B8A_20190504',
            'Sentinel2S2C_B11_20190504', 'Sentinel2S2C_B12_20190504',
            'Sentinel2S2C_NDVI_20190501', 'Sentinel2S2C_NDVI_20190504',
            'Sentinel2S2C_NDWI_20190501', 'Sentinel2S2C_NDWI_20190504',
            'Sentinel2S2C_Brightness_20190501',
            'Sentinel2S2C_Brightness_20190504'
        ]

        cls.expected_l8_labels = [
            'Landsat8_B1_20200101', 'Landsat8_B2_20200101',
            'Landsat8_B3_20200101', 'Landsat8_B4_20200101',
            'Landsat8_B5_20200101', 'Landsat8_B6_20200101',
            'Landsat8_B7_20200101', 'Landsat8_B1_20200111',
            'Landsat8_B2_20200111', 'Landsat8_B3_20200111',
            'Landsat8_B4_20200111', 'Landsat8_B5_20200111',
            'Landsat8_B6_20200111', 'Landsat8_B7_20200111',
            'Landsat8_NDVI_20200101', 'Landsat8_NDVI_20200111',
            'Landsat8_NDWI_20200101', 'Landsat8_NDWI_20200111',
            'Landsat8_Brightness_20200101', 'Landsat8_Brightness_20200111'
        ]

        cls.expected_s2_l3a_labels = [
            'Sentinel2L3A_B2_20200101', 'Sentinel2L3A_B3_20200101',
            'Sentinel2L3A_B4_20200101', 'Sentinel2L3A_B5_20200101',
            'Sentinel2L3A_B6_20200101', 'Sentinel2L3A_B7_20200101',
            'Sentinel2L3A_B8_20200101', 'Sentinel2L3A_B8A_20200101',
            'Sentinel2L3A_B11_20200101', 'Sentinel2L3A_B12_20200101',
            'Sentinel2L3A_B2_20200120', 'Sentinel2L3A_B3_20200120',
            'Sentinel2L3A_B4_20200120', 'Sentinel2L3A_B5_20200120',
            'Sentinel2L3A_B6_20200120', 'Sentinel2L3A_B7_20200120',
            'Sentinel2L3A_B8_20200120', 'Sentinel2L3A_B8A_20200120',
            'Sentinel2L3A_B11_20200120', 'Sentinel2L3A_B12_20200120',
            'Sentinel2L3A_NDVI_20200101', 'Sentinel2L3A_NDVI_20200120',
            'Sentinel2L3A_NDWI_20200101', 'Sentinel2L3A_NDWI_20200120',
            'Sentinel2L3A_Brightness_20200101',
            'Sentinel2L3A_Brightness_20200120'
        ]
        cls.expected_user_features_labels = [
            'NUMBER_OF_THINGS_band_0', 'LAI_band_0'
        ]

        cls.expected_l8_old_labels = [
            'Landsat8Old_B1_20200101', 'Landsat8Old_B2_20200101',
            'Landsat8Old_B3_20200101', 'Landsat8Old_B4_20200101',
            'Landsat8Old_B5_20200101', 'Landsat8Old_B6_20200101',
            'Landsat8Old_B7_20200101', 'Landsat8Old_B1_20200111',
            'Landsat8Old_B2_20200111', 'Landsat8Old_B3_20200111',
            'Landsat8Old_B4_20200111', 'Landsat8Old_B5_20200111',
            'Landsat8Old_B6_20200111', 'Landsat8Old_B7_20200111',
            'Landsat8Old_NDVI_20200101', 'Landsat8Old_NDVI_20200111',
            'Landsat8Old_NDWI_20200101', 'Landsat8Old_NDWI_20200111',
            'Landsat8Old_Brightness_20200101',
            'Landsat8Old_Brightness_20200111'
        ]
        cls.expected_l5_old_labels = [
            'Landsat5Old_B1_20200101', 'Landsat5Old_B2_20200101',
            'Landsat5Old_B3_20200101', 'Landsat5Old_B4_20200101',
            'Landsat5Old_B5_20200101', 'Landsat5Old_B6_20200101',
            'Landsat5Old_B1_20200111', 'Landsat5Old_B2_20200111',
            'Landsat5Old_B3_20200111', 'Landsat5Old_B4_20200111',
            'Landsat5Old_B5_20200111', 'Landsat5Old_B6_20200111',
            'Landsat5Old_NDVI_20200101', 'Landsat5Old_NDVI_20200111',
            'Landsat5Old_NDWI_20200101', 'Landsat5Old_NDWI_20200111',
            'Landsat5Old_Brightness_20200101',
            'Landsat5Old_Brightness_20200111'
        ]

        cls.expected_sensors = [
            "Landsat5Old", "Landsat8", "Landsat8Old", "Sentinel2",
            "Sentinel2S2C", 'Sentinel2L3A'
        ]

    # Tests definitions
    def test_instance_s2(self, i2_tmpdir, rm_tmpdir_on_success):
        """Tests if the class sentinel_2 can be instanciate."""
        tile_name = "T31TCJ"
        TUR.generate_fake_s2_data(str(i2_tmpdir),
                                  tile_name, ["20200101", "20200120"],
                                  res=10)
        args = {
            "tile_name": "T31TCJ",
            "target_proj": 2154,
            "all_tiles": "T31TCJ",
            "image_directory": str(i2_tmpdir),
            "write_dates_stack": False,
            "extract_bands_flag": False,
            "output_target_dir": None,
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": 10,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": ["NDVI", "NDWI", "Brightness"],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False,
            "working_resolution": None
        }

        s2_sensor = sentinel_2(**args)
        (features_app, _), features_labels = s2_sensor.get_features()

        features_app.ExecuteAndWriteOutput()

        assert self.expected_s2_labels == features_labels, "Sentinel-2 class broken, wrong features' labels"

        expected_output = Path(features_app.GetParameterString("out"))
        self.assert_file_exist(expected_output)

    def test_instance_s2_s2c(self, i2_tmpdir, rm_tmpdir_on_success):
        """Tests if the class sentinel_2_s2c can be instanciate."""
        tile_name = "T31TCJ"
        # generate fake input data
        mtd_files = [
            os.path.join(IOTA2DIR, "data", "MTD_MSIL2A_20190501.xml"),
            os.path.join(IOTA2DIR, "data", "MTD_MSIL2A_20190506.xml")
        ]

        TUR.generate_fake_s2_s2c_data(str(i2_tmpdir),
                                      tile_name,
                                      mtd_files,
                                      res=10.0)
        args = {
            "tile_name": tile_name,
            "target_proj": 2154,
            "all_tiles": tile_name,
            "image_directory": str(i2_tmpdir),
            "write_dates_stack": False,
            "extract_bands_flag": False,
            "output_target_dir": "",
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": 3,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": ["NDVI", "NDWI", "Brightness"],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False,
            "working_resolution": None
        }

        s2_sensor = sentinel_2_s2c(**args)
        (features_app, _), features_labels = s2_sensor.get_features()
        features_app.ExecuteAndWriteOutput()

        assert self.expected_s2_s2c_labels == features_labels, "Sentinel_2_S2C class broken, wrong features' labels"

        expected_output = Path(features_app.GetParameterString("out"))
        self.assert_file_exist(expected_output)

    def test_instance_l8(self, i2_tmpdir, rm_tmpdir_on_success):
        """Tests if the class landsat_8 can be instanciate."""
        tile_name = "T31TCJ"
        TUR.generate_fake_l8_data(str(i2_tmpdir), tile_name,
                                  ["20200101", "20200120"])
        args = {
            "tile_name": "T31TCJ",
            "target_proj": 2154,
            "all_tiles": "T31TCJ",
            "image_directory": str(i2_tmpdir),
            "write_dates_stack": False,
            "extract_bands_flag": False,
            "output_target_dir": None,
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": 10,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": ["NDVI", "NDWI", "Brightness"],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False,
            "working_resolution": None
        }

        l8_sensor = landsat_8(**args)
        (features_app, _), features_labels = l8_sensor.get_features()
        features_app.ExecuteAndWriteOutput()

        assert self.expected_l8_labels == features_labels, "Landsat_8 class broken, wrong features' labels"

        expected_output = Path(features_app.GetParameterString("out"))
        self.assert_file_exist(expected_output)

    def test_instance_user_features(self, i2_tmpdir, rm_tmpdir_on_success):
        """Tests if the class user_features can be instanciate."""
        tile_name = "T31TCJ"
        patterns = ["NUMBER_OF_THINGS", "LAI"]
        TUR.generate_fake_user_features_data(str(i2_tmpdir), tile_name,
                                             patterns)
        args = {
            "tile_name": tile_name,
            "target_proj": 2154,
            "all_tiles": "T31TCJ",
            "image_directory": str(i2_tmpdir),
            "write_dates_stack": None,
            "extract_bands_flag": False,
            "output_target_dir": None,
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": None,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": [],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False,
            "patterns": patterns,
            "working_resolution": None
        }
        user_feat_sensor = user_features(**args)
        (user_feat_stack, _), features_labels = user_feat_sensor.get_features()
        user_feat_stack.ExecuteAndWriteOutput()
        assert self.expected_user_features_labels == features_labels, "user_features class broken, wrong features' labels"

        expected_output = Path(user_feat_stack.GetParameterString("out"))
        self.assert_file_exist(expected_output)

    def test_instance_s2_l3a(self, i2_tmpdir, rm_tmpdir_on_success):
        """Tests if the class sentinel_2_l3a can be instanciate."""
        tile_name = "T31TCJ"
        TUR.generate_fake_s2_l3a_data(str(i2_tmpdir),
                                      tile_name, ["20200101", "20200120"],
                                      res=10)
        args = {
            "tile_name": "T31TCJ",
            "target_proj": 2154,
            "all_tiles": "T31TCJ",
            "image_directory": str(i2_tmpdir),
            "write_dates_stack": False,
            "extract_bands_flag": False,
            "output_target_dir": None,
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": 10,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": ["NDVI", "NDWI", "Brightness"],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False,
            "working_resolution": None
        }

        s2_l3a_sensor = sentinel_2_l3a(**args)
        (features_app, _), features_labels = s2_l3a_sensor.get_features()
        features_app.ExecuteAndWriteOutput()

        assert self.expected_s2_l3a_labels == features_labels, "Sentinel 2 L3A class broken, wrong features' labels"

        expected_output = Path(features_app.GetParameterString("out"))
        self.assert_file_exist(expected_output)

    def test_instance_l8_old(self, i2_tmpdir, rm_tmpdir_on_success):
        """Tests if the class landsat_8_old can be instanciate."""
        tile_name = "France-MetropoleD0005H0002"
        TUR.generate_fake_l8_old_data(str(i2_tmpdir), tile_name,
                                      ["20200101", "20200120"])
        args = {
            "tile_name": tile_name,
            "target_proj": 2154,
            "all_tiles": tile_name,
            "image_directory": str(i2_tmpdir),
            "write_dates_stack": False,
            "extract_bands_flag": False,
            "output_target_dir": None,
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": 10,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": ["NDVI", "NDWI", "Brightness"],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False,
            "working_resolution": None
        }

        l8_sensor = landsat_8_old(**args)
        (features_app, _), features_labels = l8_sensor.get_features()
        features_app.ExecuteAndWriteOutput()
        assert self.expected_l8_old_labels == features_labels, "Landsat_ 8 old class broken, wrong features' labels"

        expected_output = Path(features_app.GetParameterString("out"))
        self.assert_file_exist(expected_output)

    def test_instance_l5_old(self, i2_tmpdir, rm_tmpdir_on_success):
        """Tests if the class landsat_5_old can be instanciate."""
        tile_name = "France-MetropoleD0005H0002"
        TUR.generate_fake_l5_old_data(str(i2_tmpdir), tile_name,
                                      ["20200101", "20200120"])
        args = {
            "tile_name": tile_name,
            "target_proj": 2154,
            "all_tiles": tile_name,
            "image_directory": str(i2_tmpdir),
            "write_dates_stack": False,
            "extract_bands_flag": False,
            "output_target_dir": None,
            "keep_bands": True,
            "i2_output_path": str(i2_tmpdir),
            "temporal_res": 10,
            "auto_date_flag": True,
            "date_interp_min_user": "",
            "date_interp_max_user": "",
            "write_outputs_flag": False,
            "features": ["NDVI", "NDWI", "Brightness"],
            "enable_gapfilling": True,
            "hand_features_flag": False,
            "hand_features": "",
            "copy_input": True,
            "rel_refl": False,
            "keep_dupl": True,
            "vhr_path": None,
            "acor_feat": False,
            "working_resolution": None
        }

        l5_sensor = landsat_5_old(**args)
        (features_app, _), features_labels = l5_sensor.get_features()
        features_app.ExecuteAndWriteOutput()
        assert self.expected_l5_old_labels == features_labels, "Landsat_ 5 old class broken, wrong features' labels"

        expected_output = Path(features_app.GetParameterString("out"))
        self.assert_file_exist(expected_output)

    def test_sensors_container(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test if the sensors_container class enable all required sensors."""
        tile_name = "T31TCJ"

        # sentinel_2_l3a object needs data to be instantiate.
        TUR.generate_fake_s2_l3a_data(str(i2_tmpdir),
                                      tile_name, ["20200101"],
                                      res=10.0)
        args = {
            "Sentinel_2": {
                "tile_name": tile_name,
                "target_proj": 2154,
                "all_tiles": tile_name,
                "image_directory": str(i2_tmpdir),
                "write_dates_stack": False,
                "extract_bands_flag": False,
                "output_target_dir": "",
                "keep_bands": True,
                "i2_output_path": str(i2_tmpdir),
                "temporal_res": 10,
                "auto_date_flag": True,
                "date_interp_min_user": "",
                "date_interp_max_user": "",
                "write_outputs_flag": False,
                "features": ["NDVI", "NDWI", "Brightness"],
                "enable_gapfilling": True,
                "hand_features_flag": False,
                "hand_features": "",
                "copy_input": True,
                "rel_refl": False,
                "keep_dupl": True,
                "vhr_path": None,
                "acor_feat": False,
                "working_resolution": None
            },
            "Sentinel_2_S2C": {
                "tile_name": "T31TCJ",
                "target_proj": 2154,
                "all_tiles": "T31TCJ",
                "image_directory": str(i2_tmpdir),
                "write_dates_stack": False,
                "extract_bands_flag": False,
                "output_target_dir": None,
                "keep_bands": True,
                "i2_output_path": str(i2_tmpdir),
                "temporal_res": 10,
                "auto_date_flag": True,
                "date_interp_min_user": "",
                "date_interp_max_user": "",
                "write_outputs_flag": False,
                "features": ["NDVI", "NDWI", "Brightness"],
                "enable_gapfilling": True,
                "hand_features_flag": False,
                "hand_features": "",
                "copy_input": True,
                "rel_refl": False,
                "keep_dupl": True,
                "vhr_path": None,
                "acor_feat": False,
                "working_resolution": None
            },
            "Landsat8": {
                "tile_name": "T31TCJ",
                "target_proj": 2154,
                "all_tiles": "T31TCJ",
                "image_directory": str(i2_tmpdir),
                "write_dates_stack": False,
                "extract_bands_flag": False,
                "output_target_dir": None,
                "keep_bands": True,
                "i2_output_path": str(i2_tmpdir),
                "temporal_res": 10,
                "auto_date_flag": True,
                "date_interp_min_user": "",
                "date_interp_max_user": "",
                "write_outputs_flag": False,
                "features": ["NDVI", "NDWI", "Brightness"],
                "enable_gapfilling": True,
                "hand_features_flag": False,
                "hand_features": "",
                "copy_input": True,
                "rel_refl": False,
                "keep_dupl": True,
                "vhr_path": None,
                "acor_feat": False,
                "working_resolution": None
            },
            "Sentinel_2_L3A": {
                "tile_name": "T31TCJ",
                "target_proj": 2154,
                "all_tiles": "T31TCJ",
                "image_directory": str(i2_tmpdir),
                "write_dates_stack": False,
                "extract_bands_flag": False,
                "output_target_dir": None,
                "keep_bands": True,
                "i2_output_path": str(i2_tmpdir),
                "temporal_res": 10,
                "auto_date_flag": True,
                "date_interp_min_user": "",
                "date_interp_max_user": "",
                "write_outputs_flag": False,
                "features": ["NDVI", "NDWI", "Brightness"],
                "enable_gapfilling": True,
                "hand_features_flag": False,
                "hand_features": "",
                "copy_input": True,
                "rel_refl": False,
                "keep_dupl": True,
                "vhr_path": None,
                "acor_feat": False,
                "working_resolution": None
            },
            "Landsat5_old": {
                "tile_name": "T31TCJ",
                "target_proj": 2154,
                "all_tiles": "T31TCJ",
                "image_directory": str(i2_tmpdir),
                "write_dates_stack": False,
                "extract_bands_flag": False,
                "output_target_dir": None,
                "keep_bands": True,
                "i2_output_path": str(i2_tmpdir),
                "temporal_res": 10,
                "auto_date_flag": True,
                "date_interp_min_user": "",
                "date_interp_max_user": "",
                "write_outputs_flag": False,
                "features": ["NDVI", "NDWI", "Brightness"],
                "enable_gapfilling": True,
                "hand_features_flag": False,
                "hand_features": "",
                "copy_input": True,
                "rel_refl": False,
                "keep_dupl": True,
                "vhr_path": None,
                "acor_feat": False,
                "working_resolution": None
            },
            "Landsat8_old": {
                "tile_name": "T31TCJ",
                "target_proj": 2154,
                "all_tiles": "T31TCJ",
                "image_directory": str(i2_tmpdir),
                "write_dates_stack": False,
                "extract_bands_flag": False,
                "output_target_dir": None,
                "keep_bands": True,
                "i2_output_path": str(i2_tmpdir),
                "temporal_res": 10,
                "auto_date_flag": True,
                "date_interp_min_user": "",
                "date_interp_max_user": "",
                "write_outputs_flag": False,
                "features": ["NDVI", "NDWI", "Brightness"],
                "enable_gapfilling": True,
                "hand_features_flag": False,
                "hand_features": "",
                "copy_input": True,
                "rel_refl": False,
                "keep_dupl": True,
                "vhr_path": None,
                "acor_feat": False,
                "working_resolution": None
            }
        }
        sensors = sensors_container(tile_name, str(i2_tmpdir), str(i2_tmpdir),
                                    **args)
        enabled_sensors = sensors.get_enabled_sensors()
        sensors_name = [sensor.name for sensor in enabled_sensors]

        assert sensors_name == self.expected_sensors
