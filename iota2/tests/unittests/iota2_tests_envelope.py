#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import shutil
from pathlib import Path

import iota2.tests.utils.tests_utils_vectors as TUV
from iota2.sampling import tile_envelope as env

IOTA2DIR = os.environ.get("IOTA2DIR")
if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2_DATATEST = os.path.join(os.environ.get("IOTA2DIR"), "data")


class Iota2TestGenerateShapeTile:
    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.tiles = ["D0005H0002"]
        cls.path_tiles_feat = IOTA2_DATATEST + "/references/features/"

    def test_generate_shape_tile(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test envelope file genenration."""
        # Test de création des enveloppes
        features_path = str(i2_tmpdir / "features")

        masks_references = str(Path(IOTA2DIR) / "data" / "references" / "features")
        if Path(features_path).exists():
            shutil.rmtree(features_path)
        shutil.copytree(masks_references, features_path)

        # Test de création des enveloppes
        # Launch function
        env.generate_shape_tile(self.tiles, None, str(i2_tmpdir), 2154, 0)

        # For each tile test if the shapefile is ok
        for i in self.tiles:
            # generate filename
            reference_shape_file = (
                IOTA2_DATATEST + "/references/GenerateShapeTile/" + i + ".shp"
            )
            shape_file = str(i2_tmpdir / "envelope" / f"{i}.shp")
            service_compare_vector_file = TUV.ServiceCompareVectorFile()
            # Launch shapefile comparison
            assert service_compare_vector_file.test_same_shapefiles(
                reference_shape_file, shape_file
            )
