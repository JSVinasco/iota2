#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import shutil
from pathlib import Path

import pytest

from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2 import run
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


class Iota2TestBuilder:
    """Tests builders configuration file parameter."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.group_test_name = "iota_builder_runs_case"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []

        # input data
        cls.config_ref = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "i2_builder.cfg")
        cls.ground_truth_path = os.path.join(IOTA2DIR, "data", "references",
                                             "running_iota2",
                                             "ground_truth.shp")
        cls.nomenclature_path = os.path.join(IOTA2DIR, "data", "references",
                                             "running_iota2",
                                             "nomenclature.txt")
        cls.color_path = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "color.txt")

    def test_wrong_builder(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test a builder which does not exists, if a module is provided by user."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = i2_tmpdir / "test_results"
        fake_s2_theia_dir = i2_tmpdir / "s2_data"
        Path.mkdir(fake_s2_theia_dir, exist_ok=True)
        config_test = i2_tmpdir / "i2_config_s2_l2a.cfg"
        shutil.copy(self.config_ref, str(config_test))
        cfg_test = rcf.read_internal_config_file(str(config_test))
        cfg_test.cfg_as_dict["builders"]["builders_paths"] = [
            os.path.join(IOTA2DIR, "iota2", "tests", "utils")
        ]
        cfg_test.cfg_as_dict["builders"]["builders_class_name"] = [
            "i2_wrong_builder"
        ]
        cfg_test.cfg_as_dict["chain"]["output_path"] = str(running_output_path)
        cfg_test.cfg_as_dict["chain"]["s2_path"] = str(fake_s2_theia_dir)
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"][
            "nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["external_features"]["module"] = None
        cfg_test.save(str(config_test))

        # assert
        pytest.raises(TypeError, run, str(config_test), None, "debug", 1, 20,
                      False, None, [], False, 1, None)

    def test_check_invalid_builder_class_name(self, i2_tmpdir,
                                              rm_tmpdir_on_success):
        """Test a builder which does not exists, if a module is not provided by user."""
        tile_name = "T31TCJ"
        running_output_path = i2_tmpdir / "test_results"
        fake_s2_theia_dir = i2_tmpdir / "s2_data"
        Path.mkdir(fake_s2_theia_dir, exist_ok=True)
        # Invalid module name
        config_test = i2_tmpdir / "Config_TEST_invalidmodule.cfg"

        shutil.copy(self.config_ref, str(config_test))
        cfg_test = rcf.read_internal_config_file(str(config_test))
        cfg_test.cfg_as_dict["builders"]["builders_class_name"] = [
            "i2_wrong_builder"
        ]
        cfg_test.cfg_as_dict["chain"]["output_path"] = str(running_output_path)
        cfg_test.cfg_as_dict["chain"]["s2_path"] = str(fake_s2_theia_dir)
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"][
            "nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["external_features"]["module"] = None
        cfg_test.save(str(config_test))

        # Launch the chain
        pytest.raises(ValueError, run, str(config_test), None, "debug", 1, 20,
                      False, None, [], False, 1, None)
