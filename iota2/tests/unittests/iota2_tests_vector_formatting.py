#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module for testing vector formattings functions."""
import os
import shutil
from collections import Counter
from pathlib import Path

import numpy as np

import iota2.tests.utils.tests_utils_vectors as TUV
from iota2.common import file_utils as fut
from iota2.common import iota2_directory
from iota2.common.raster_utils import raster_to_array
from iota2.common.utils import run
from iota2.sampling.vector_formatting import (
    built_where_sql_exp,
    create_tile_region_masks,
    extract_maj_vote_samples,
    keep_fields,
    split_by_sets,
    split_vector_by_region,
    vector_formatting,
)
from iota2.vector_tools import vector_functions as vf
from iota2.vector_tools.change_name_field import change_name

IOTA2DIR = os.environ.get("IOTA2DIR")

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2_DATATEST = Path(os.environ.get("IOTA2DIR")) / "data"


class Iota2TestVectorFormatting:
    """Test the vector formatting functions."""

    # before launching tests
    @classmethod
    def setup_class(cls):
        """Shared variables between tests."""
        # definition of local variables
        cls.in_vector = str(
            IOTA2_DATATEST
            / "references"
            / "formatting_vectors"
            / "Input"
            / "formattingVectors"
            / "T31TCJ.shp"
        )
        cls.ref_img = str(
            IOTA2_DATATEST
            / "references"
            / "selectionSamples"
            / "Input"
            / "features"
            / "T31TCJ"
            / "tmp"
            / "MaskCommunSL.tif"
        )
        cls.ref_region = str(
            IOTA2_DATATEST
            / "references"
            / "genResults"
            / "Input"
            / "classif"
            / "MASK"
            / "Myregion_region_1_T31TCJ.shp"
        )

        # References
        cls.config_test = str(
            IOTA2_DATATEST / "config" / "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )

    # Tests definitions
    def test_vector_formatting(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """
        Test vectorFormatting function.

        random function is used in Sampling.VectorFormatting.VectorFormatting
        we can only check if there is expected number of features with
        expected fields and some features values
        """
        # define inputs
        test_output = str(i2_tmpdir / "IOTA2_dir_VectorFormatting")
        # prepare ground truth
        ground_truth = str(i2_tmpdir / "groundTruth_test.shp")
        cmd = (
            "ogr2ogr -s_srs EPSG:2154 -t_srs EPSG:2154 -dialect 'SQLite'"
            " -sql 'select GEOMETRY,code from t31tcj' "
            f"{ground_truth} {self.in_vector}"
        )
        run(cmd)

        # cfg instance
        runs = 2

        iota2_directory.generate_directories(
            test_output, True, ["D0004H0004", "D0005H0004", "D0004H0003", "D0005H0003"]
        )

        # prepare expected function inputs
        t31tcj_feat_dir = (
            i2_tmpdir / "IOTA2_dir_VectorFormatting" / "features" / "T31TCJ"
        )
        t31tcj_feat_dir.mkdir(exist_ok=True)
        t31tcj_feat_dir_tmp = (
            i2_tmpdir / "IOTA2_dir_VectorFormatting" / "features" / "T31TCJ" / "tmp"
        )
        t31tcj_feat_dir_tmp.mkdir(exist_ok=True)
        # prepare ref img
        t31tcj_ref_img = str(t31tcj_feat_dir_tmp / "MaskCommunSL.tif")
        shutil.copy(self.ref_img, t31tcj_ref_img)
        # prepare envelope
        envelope_name = "T31TCJ.shp"
        envelope_path = str(
            i2_tmpdir / "IOTA2_dir_VectorFormatting" / "envelope" / envelope_name
        )
        vf.cp_shape_file(
            self.ref_region.replace(".shp", ""),
            envelope_path.replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        change_name(envelope_path, "region", "FID")
        # prepare cloud mask
        cloud_name = "CloudThreshold_0.shp"
        cloud_path = str(
            i2_tmpdir
            / "IOTA2_dir_VectorFormatting"
            / "features"
            / "T31TCJ"
            / cloud_name
        )
        vf.cp_shape_file(
            self.ref_region.replace(".shp", ""),
            cloud_path.replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        change_name(cloud_path, "region", "cloud")

        # launch function
        ratio = 0.7
        random_seed = 1
        enable_split_ground_truth = True
        fusion_merge_all_validation = False
        merge_final_classifications = True
        merge_final_classif_ratio = 0.1

        epsg = 2154

        vf.cp_shape_file(
            self.ref_region.replace(".shp", ""),
            os.path.join(test_output, "MyRegion"),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        classif_mode = "separate"
        vector_formatting(
            tile_name="T31TCJ",
            output_path=test_output,
            ground_truth_vec=ground_truth,
            data_field="code",
            cloud_threshold=0,
            ratio=ratio,
            random_seed=random_seed,
            enable_split_ground_truth=enable_split_ground_truth,
            fusion_merge_all_validation=fusion_merge_all_validation,
            runs=runs,
            original_label_column="code",
            epsg=epsg,
            region_field="region",
            classif_mode=classif_mode,
            exterior_buffer_size=0,
            interior_buffer_size=0,
            epsilon=0,
            spatial_res=None,
            boundary_fusion_enabled=False,
            merge_final_classifications=merge_final_classifications,
            merge_final_classifications_ratio=merge_final_classif_ratio,
            region_vec=self.ref_region,
            working_directory=None,
        )

        # assert
        nb_features_origin = len(
            vf.get_field_element(
                ground_truth,
                driver_name="ESRI Shapefile",
                field="code",
                mode="all",
                elem_type="str",
            )
        )

        test_vector = fut.file_search_and(
            os.path.join(test_output, "formattingVectors"), True, "T31TCJ.shp"
        )[0]
        nb_features_test = len(
            vf.get_field_element(
                test_vector,
                driver_name="ESRI Shapefile",
                field="code",
                mode="all",
                elem_type="str",
            )
        )
        # check nb features
        assert nb_features_origin == nb_features_test, "wrong number of features"

        # check fields
        origin_fields = vf.get_all_fields_in_shape(ground_truth)
        test_fields = vf.get_all_fields_in_shape(test_vector)

        new_fields = ["region", "originfid", "seed_0", "seed_1", "tile_o"]
        expected_fields = origin_fields + new_fields
        assert len(expected_fields) == len(test_fields)
        assert all(field in test_fields for field in expected_fields)

    def test_extract_maj_vote_samples(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Test the extraction of samples by class according to a ratio."""
        # define inputs
        in_vector_name = os.path.basename(self.in_vector)
        extracted_vector_name = "extracted_samples.sqlite"
        in_vector = str(i2_tmpdir / in_vector_name)
        extracted_vector = str(i2_tmpdir / extracted_vector_name)
        vf.cp_shape_file(
            self.in_vector.replace(".shp", ""),
            in_vector.replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )

        # launch function
        data_field = "code"
        region_field = "region"
        extraction_ratio = 0.5
        extract_maj_vote_samples(
            in_vector, extracted_vector, extraction_ratio, data_field, region_field
        )
        # assert
        features_origin = vf.get_field_element(
            self.in_vector,
            driver_name="ESRI Shapefile",
            field=data_field,
            mode="all",
            elem_type="str",
        )
        by_class_origin = Counter(features_origin)

        features_in_vector = vf.get_field_element(
            in_vector,
            driver_name="ESRI Shapefile",
            field=data_field,
            mode="all",
            elem_type="str",
        )
        by_class_in_vector = Counter(features_in_vector)

        # features_extract_vector = vf.get_field_element(extracted_vector,
        #                                              driver_name="SQLite",
        #                                              field=data_field,
        #                                              mode="all",
        #                                              elem_type="str")
        # by_class_extract_vector = Counter(features_extract_vector)

        buff = []
        for class_name, class_count in list(by_class_origin.items()):
            buff.append(
                by_class_in_vector[class_name] == extraction_ratio * class_count
            )

        assert all(buff), "extraction of samples failed"

    def test_built_where_sql_exp(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """
        Test the sql clause generation.

        There is a random part in the function BuiltWhereSQL_exp,
         the returned string value can
        not be compare to a reference. That is the reason why we check
        the number of 'OR' which must be the rest of nb_id / 1000.0
        """
        nb_id = 2000
        sample_id_to_extract_low = [str(id_) for id_ in range(nb_id)]
        sql_clause = built_where_sql_exp(sample_id_to_extract_low, "in")
        nb_or_test = sql_clause.count("OR")
        assert 1 == nb_or_test, "SQLite expression failed"

    def test_split_by_sets(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Test the split of a given vector."""
        # launch function
        out_vectors = split_by_sets(
            self.in_vector,
            2,
            str(i2_tmpdir),
            2154,
            2154,
            "T31TCJ",
            split_ground_truth=True,
        )
        s0_val = out_vectors[0]
        s0_learn = out_vectors[1]
        s1_val = out_vectors[2]
        s1_learn = out_vectors[3]
        # assert
        seed_0 = vf.get_field_element(
            self.in_vector,
            driver_name="ESRI Shapefile",
            field="seed_0",
            mode="all",
            elem_type="str",
        )
        seed_1 = vf.get_field_element(
            self.in_vector,
            driver_name="ESRI Shapefile",
            field="seed_1",
            mode="all",
            elem_type="str",
        )

        seed_0_learn_ref = seed_0.count("learn")
        seed_0_val_ref = seed_0.count("validation")
        seed_1_learn_ref = seed_1.count("learn")
        seed_1_val_ref = seed_1.count("validation")

        seed_0_learn_test = len(
            vf.get_field_element(
                s0_learn,
                driver_name="SQLite",
                field="region",
                mode="all",
                elem_type="str",
            )
        )
        seed_0_val_test = len(
            vf.get_field_element(
                s0_val,
                driver_name="SQLite",
                field="region",
                mode="all",
                elem_type="str",
            )
        )
        seed_1_learn_test = len(
            vf.get_field_element(
                s1_learn,
                driver_name="SQLite",
                field="region",
                mode="all",
                elem_type="str",
            )
        )
        seed_1_val_test = len(
            vf.get_field_element(
                s1_val,
                driver_name="SQLite",
                field="region",
                mode="all",
                elem_type="str",
            )
        )

        assert (
            seed_0_learn_test == seed_0_learn_ref
        ), "wrong number of learning samples in seed 0"
        assert (
            seed_1_learn_test == seed_1_learn_ref
        ), "wrong number of learning samples in seed 1"
        assert (
            seed_0_val_test == seed_0_val_ref
        ), "wrong number of validation samples in seed 0"
        assert (
            seed_1_val_test == seed_1_val_ref
        ), "wrong number of validation samples in seed 1"

    def test_keep_fields(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Test the extraction of fields."""
        # define inputs
        fields_to_keep = ["region", "code"]
        test_vector_name = "test_vector.sqlite"
        test_vector = str(i2_tmpdir / test_vector_name)

        # launch function
        keep_fields(self.in_vector, test_vector, fields=fields_to_keep)

        # assert
        test_vector_fields = vf.get_all_fields_in_shape(test_vector, driver="SQLite")
        assert all(
            current_field in fields_to_keep for current_field in test_vector_fields
        ), "remove fields failed"

    def test_create_tile_region_masks(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Test the generation of the raster mask.

        Define the region in the tile
        """
        # define inputs
        test_vector_name = "T31TCJ.shp"
        test_vector = str(i2_tmpdir / test_vector_name)
        cmd = f"ogr2ogr -nln t31tcj -f SQLite {test_vector} {self.ref_region}"
        run(cmd)

        # launch function
        create_tile_region_masks(
            test_vector,
            "region",
            "T31TCJ",
            str(i2_tmpdir),
            "MyRegion",
            self.ref_img,
            str(i2_tmpdir),
            "separate",
            test_vector,
        )
        # assert

        raster_region = str(i2_tmpdir / "MyRegion_region_1_T31TCJ.tif")

        raster_region_arr = raster_to_array(raster_region)

        ref_array = np.ones((50, 50))

        assert np.allclose(
            ref_array, raster_region_arr
        ), "problem with the normalization by ref"

    def test_split_vector_by_region(self, i2_tmpdir, rm_tmpdir_on_success):
        # pylint: disable=unused-argument
        """Test : split a vector by the region he belongs to."""
        # define inputs
        nb_features_origin = len(
            vf.get_field_element(
                self.in_vector,
                driver_name="ESRI shapefile",
                field="region",
                mode="all",
                elem_type="str",
            )
        )
        nb_features_new_region = 5
        test_vector_name = "T31TCJ_Samples.sqlite"
        test_vector = str(i2_tmpdir / test_vector_name)
        cmd = f"ogr2ogr -nln output -f SQLite {test_vector} {self.in_vector}"
        run(cmd)

        TUV.random_update(test_vector, "output", "seed_0", "learn", nb_features_origin)
        TUV.random_update(test_vector, "output", "region", "2", nb_features_new_region)

        output_dir = str(i2_tmpdir)
        region_field = "region"

        # launch function
        split_vector_by_region(
            in_vect=test_vector,
            output_dir=output_dir,
            region_field=region_field,
            runs=1,
            sampling_validation=False,
            sample_selection_dir=None,
            driver="SQLite",
        )
        # assert
        vector_reg_1 = fut.file_search_and(str(i2_tmpdir), True, "region_1")[0]
        vector_reg_2 = fut.file_search_and(str(i2_tmpdir), True, "region_2")[0]

        feat_vect_reg_1 = len(
            vf.get_field_element(
                vector_reg_1,
                driver_name="SQLite",
                field="region",
                mode="all",
                elem_type="str",
            )
        )
        feat_vect_reg_2 = len(
            vf.get_field_element(
                vector_reg_2,
                driver_name="SQLite",
                field="region",
                mode="all",
                elem_type="str",
            )
        )

        assert nb_features_new_region == feat_vect_reg_2
        assert nb_features_origin == feat_vect_reg_1 + feat_vect_reg_2
