#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import configparser
import os
import shutil
from functools import partial
from pathlib import Path

import numpy as np
import otbApplication as otb
import pytest
import rasterio as rio
from scipy import ndimage

import iota2.tests.utils.tests_utils_rasters as TUR
from config import Config
from iota2.common import iota2_directory
from iota2.common import raster_utils as rasterU
from iota2.common.custom_numpy_features import (compute_custom_features,
                                                custom_numpy_features)
from iota2.common.file_utils import ensure_dir
from iota2.common.generate_features import generate_features
from iota2.common.i2_constants import Iota2Constants
from iota2.common.raster_utils import ChunkConfig
from iota2.common.tools.split_raster_into_tiles import reproject
from iota2.configuration_files import read_config_file as rcf
from iota2.configuration_files.sections.cfg_utils import ConfigError
from iota2.sensors.sensors_container import sensors_container
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_launcher import get_large_i2_data_test

I2_CONST = Iota2Constants()


@pytest.mark.external_features
class Iota2TestCustomNumpyFeatures(AssertsFilesUtils):
    """Check external features workflow."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        # Download i2 large data set
        get_large_i2_data_test()

        cls.config_test = str(IOTA2DIR / "data" / "numpy_features" /
                              "config_plugins.cfg")
        cls.sar_config_test = str(IOTA2DIR / "data" / "references" /
                                  "running_iota2" / "large_i2_data_test" /
                                  "s1_data" / "i2_config_sar.cfg")
        cls.srtm_db = str(IOTA2DIR / "data" / "references" / "running_iota2" /
                          "large_i2_data_test" / "s1_data" / "srtm.shp")
        cls.tiles_grid_db = str(IOTA2DIR / "data" / "references" /
                                "running_iota2" / "large_i2_data_test" /
                                "s1_data" / "Features.shp")
        cls.srtm_dir = str(IOTA2DIR / "data" / "references" / "running_iota2" /
                           "large_i2_data_test" / "s1_data" / "SRTM")
        cls.geoid_file = str(IOTA2DIR / "data" / "references" /
                             "running_iota2" / "large_i2_data_test" /
                             "s1_data" / "egm96.grd")
        cls.acquisitions = str(IOTA2DIR / "data" / "references" /
                               "running_iota2" / "large_i2_data_test" /
                               "s1_data" / "acquisitions")

    @pytest.mark.config
    def test_check_custom_features_valid_inputs(self, i2_tmpdir,
                                                rm_tmpdir_on_success):
        """Test the behaviour of check custom features."""
        s2st_data = str(i2_tmpdir)
        test_path = str(i2_tmpdir / "RUN")
        config_path_test = str(i2_tmpdir / "Config_TEST_valid.cfg")

        shutil.copy(self.config_test, config_path_test)
        cfg_test = Config(open(config_path_test)).as_dict()
        cfg_test["chain"]["output_path"] = test_path
        cfg_test["chain"]["list_tile"] = "T31TCJ"
        cfg_test["chain"]["l8_path"] = None
        cfg_test["chain"]["l8_path_old"] = None
        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = s2st_data
        cfg_test["chain"]["user_feat_path"] = None
        cfg_test["chain"]["region_path"] = None
        cfg_test["chain"]["nomenclature_path"] = config_path_test
        cfg_test["chain"]["color_table"] = config_path_test
        cfg_test["chain"]["ground_truth"] = config_path_test
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["arg_train"]["crop_mix"] = False
        cfg_test["arg_train"]["samples_classif_mix"] = False
        cfg_test["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test["sensors_data_interpolation"][
            "use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test["external_features"]["functions"] = "get_identity get_ndvi"
        # cfg_test["external_features"]["external_features_flag"] = False

        with open(config_path_test, "w") as file_cfg:
            file_cfg.write(str(cfg_test))

        cfg = rcf.read_config_file(config_path_test)
        assert cfg.getParam("external_features", "external_features_flag")
        del cfg

    @pytest.mark.config
    def test_check_invalid_module_name(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test the behaviour of check custom features."""
        s2st_data = str(i2_tmpdir)
        test_path = str(i2_tmpdir / "RUN")

        # Invalid module name
        config_path_test = str(i2_tmpdir / "Config_TEST_invalidmodule.cfg")

        shutil.copy(self.config_test, config_path_test)
        cfg_test = Config(open(config_path_test)).as_dict()
        cfg_test["chain"]["output_path"] = test_path
        cfg_test["chain"]["list_tile"] = "T31TCJ"
        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = s2st_data
        cfg_test["chain"]["user_feat_path"] = None
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["arg_train"]["crop_mix"] = False
        cfg_test["arg_train"]["samples_classif_mix"] = False
        cfg_test["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test["sensors_data_interpolation"][
            "use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test["external_features"]["module"] = os.path.join(
            IOTA2DIR, "data", "numpy_features", "dummy.py")
        cfg_test["chain"]["region_path"] = None
        cfg_test["chain"]["nomenclature_path"] = config_path_test
        cfg_test["chain"]["color_table"] = config_path_test
        cfg_test["chain"]["ground_truth"] = config_path_test
        cfg_test["chain"]["l8_path"] = None
        cfg_test["chain"]["l8_path_old"] = None

        cfg_test["external_features"]["functions"] = "get_identity get_ndvi"
        with open(config_path_test, "w") as file_cfg:
            file_cfg.write(str(cfg_test))
        pytest.raises(ConfigError, rcf.read_config_file, config_path_test)

    @pytest.mark.config
    def test_check_invalid_function_name(self, i2_tmpdir,
                                         rm_tmpdir_on_success):
        """Test the behaviour of check custom features."""
        s2st_data = str(i2_tmpdir)
        test_path = str(i2_tmpdir / "RUN")

        # Invalid function name
        config_path_test = str(i2_tmpdir / "Config_TEST_invalid_functions.cfg")

        shutil.copy(self.config_test, config_path_test)
        cfg_test = Config(open(config_path_test)).as_dict()
        cfg_test["chain"]["output_path"] = test_path
        cfg_test["chain"]["list_tile"] = "T31TCJ"
        cfg_test["chain"]["l8_path_old"] = "None"
        cfg_test["chain"]["l8_path"] = "None"
        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = s2st_data
        cfg_test["chain"]["user_feat_path"] = None
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["arg_train"]["crop_mix"] = False
        cfg_test["arg_train"]["samples_classif_mix"] = False
        cfg_test["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test["sensors_data_interpolation"][
            "use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test["external_features"]["module"] = os.path.join(
            IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test["external_features"][
            "functions"] = "dummy_function"  # " get_ndvi"
        cfg_test["chain"]["region_path"] = None
        cfg_test["chain"]["nomenclature_path"] = config_path_test
        cfg_test["chain"]["color_table"] = config_path_test
        cfg_test["chain"]["ground_truth"] = config_path_test
        cfg_test["chain"]["l8_path"] = None
        cfg_test["chain"]["l8_path_old"] = None
        with open(config_path_test, "w") as file_cfg:
            file_cfg.write(str(cfg_test))
        pytest.raises(AttributeError, rcf.read_config_file, config_path_test)

    def test_apply_function_with_custom_features(self, i2_tmpdir,
                                                 rm_tmpdir_on_success):
        """Test the whole workflow."""
        TUR.generate_fake_s2_data(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)
        S2ST_data = str(i2_tmpdir)
        testPath = str(i2_tmpdir / "RUN")
        cfg_test = Config(open(config_path_test)).as_dict()
        cfg_test["chain"]["output_path"] = testPath
        cfg_test["chain"]["list_tile"] = "T31TCJ"
        cfg_test["chain"]["l8_path_old"] = None
        cfg_test["chain"]["l8_path"] = None
        cfg_test["chain"]["l8_path_old"] = None
        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = S2ST_data
        cfg_test["chain"]["user_feat_path"] = None
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["chain"]["region_path"] = None
        cfg_test["chain"]["ground_truth"] = config_path_test
        cfg_test["chain"]["nomenclature_path"] = config_path_test
        cfg_test["chain"]["color_table"] = config_path_test
        cfg_test["arg_train"]["crop_mix"] = False
        cfg_test["arg_train"]["samples_classif_mix"] = False
        cfg_test["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test["sensors_data_interpolation"][
            "use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test["external_features"][
            "functions"] = "get_identity"  # " get_ndvi"
        cfg_test["chain"]["region_path"] = None
        cfg_test["chain"]["nomenclature_path"] = config_path_test
        cfg_test["chain"]["color_table"] = config_path_test
        cfg_test["chain"]["ground_truth"] = config_path_test
        cfg_test["chain"]["l8_path"] = None
        with open(config_path_test, "w") as file_cfg:
            file_cfg.write(str(cfg_test))
        cfg = rcf.read_config_file(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir), False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(
            rcf.read_config_file(config_path_test)).get_sensors_parameters(
                tile_name)
        sensors = sensors_container(tile_name, working_dir, str(i2_tmpdir),
                                    **sensors_param)
        sensors.sensors_preprocess()
        list_sensors = sensors.get_enabled_sensors()
        sensor = list_sensors[0]
        ((time_s_app, app_dep), features_labels) = sensor.get_time_series()

        time_s_app.ExecuteAndWriteOutput()
        time_s = sensor.get_time_series_masks()
        time_s_app, app_dep, nbdates = time_s
        time_s_app.ExecuteAndWriteOutput()

        ((time_s_app, app_dep),
         features_labels) = sensor.get_time_series_gapfilling()
        # only one sensor for test
        apps_dict, labs_dict, ori_dep = generate_features(
            working_dir, "T31TCJ", False, str(i2_tmpdir), sensors_param)
        apps_dict["enable_interp"] = True
        apps_dict["enable_masks"] = False
        apps_dict["enable_raw"] = False
        module_path = None
        list_functions = [("get_cumulative_productivity", {}),
                          ("get_seasonal_variation", {})]
        cust = custom_numpy_features(
            tile_name, str(i2_tmpdir), sensors_param,
            cfg.getParam('external_features', "module"), list_functions)
        function_partial = partial(cust.process)

        labels_features_name = ["NDVI_20200101", "NDVI_20200102"]
        new_features_path = str(i2_tmpdir / "DUMMY_test.tif")
        (test_array, new_labels, _, _, _, _, _,
         _) = rasterU.insert_external_function_to_pipeline(
             otb_pipelines=apps_dict,
             labels=labels_features_name,
             working_dir=str(i2_tmpdir),
             function=function_partial,
             chunk_config=rasterU.ChunkConfig(chunk_size_mode="user_fixed",
                                              number_of_chunks=None,
                                              chunk_size_x=5,
                                              chunk_size_y=5),
             output_path=new_features_path,
             is_custom_feature=True)

        self.assert_file_exist(new_features_path)
        assert new_labels is not None
        del cfg

    def test_custom_features_fake_data(self, i2_tmpdir, rm_tmpdir_on_success):
        """
        TEST run custom feature with fake data
        this allows to test a feature before the whole chain starts
        """
        # instantiating custom_external_features needs a full directory, even
        # if we do not intend to use it because we inject fake data

        # generate data and config
        TUR.generate_fake_s2_data(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        confpath = str(i2_tmpdir / "Config_TEST.cfg")
        shutil.copy(self.config_test, confpath)
        S2ST_data = str(i2_tmpdir)
        testPath = str(i2_tmpdir / "RUN")
        cfg_test = Config(open(confpath)).as_dict()
        cfg_test["chain"]["output_path"] = testPath
        cfg_test["chain"]["list_tile"] = "T31TCJ"
        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = S2ST_data
        cfg_test["chain"]["region_path"] = None
        cfg_test["chain"]["nomenclature_path"] = confpath
        cfg_test["chain"]["color_table"] = confpath
        cfg_test["chain"]["ground_truth"] = confpath
        cfg_test["chain"]["l8_path"] = None
        cfg_test["chain"]["l8_path_old"] = None
        with open(confpath, "w") as file_cfg:
            file_cfg.write(str(cfg_test))

        # read param
        cfg = rcf.read_config_file(confpath)
        param = rcf.iota2_parameters(cfg)

        # variable used to instantiate custom_numpy_features
        tile_name = "T31TCJ"
        output_path = str(i2_tmpdir)
        sensors_param = param.get_sensors_parameters(tile_name)
        module_name = cfg.getParam("external_features", "module")

        # should fail
        data = custom_numpy_features(
            tile_name,
            output_path,
            sensors_param,
            module_name,
            [("get_exception", {})],
        )
        failure, error = data.test_user_feature_with_fake_data()
        assert failure, ("the get_exception function was expected to"
                         "raise an exception, but apparently did not")

        # should succeed
        data = custom_numpy_features(
            tile_name,
            output_path,
            sensors_param,
            module_name,
            [("get_seasonal_variation", {})],
        )
        failure, error = data.test_user_feature_with_fake_data()
        assert failure is False, error

        # should succeed
        # testing get_filled_stack method
        data = custom_numpy_features(
            tile_name,
            output_path,
            sensors_param,
            module_name,
            [("get_raw_data", {})],
            enabled_raw=True,
        )
        failure, error = data.test_user_feature_with_fake_data()
        assert failure is False, error
        del cfg

    def test_compute_custom_features(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST : check the whole workflow."""

        TUR.generate_fake_s2_data(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)
        S2ST_data = str(i2_tmpdir)
        testPath = str(i2_tmpdir / "RUN")
        cfg_test = Config(open(config_path_test)).as_dict()
        cfg_test["chain"]["output_path"] = testPath
        cfg_test["chain"]["list_tile"] = "T31TCJ"
        cfg_test["chain"]["l8_path_old"] = "None"
        cfg_test["chain"]["l8_path"] = "None"
        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = S2ST_data
        cfg_test["chain"]["user_feat_path"] = None
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["arg_train"]["crop_mix"] = False
        cfg_test["arg_train"]["samples_classif_mix"] = False
        cfg_test["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test["sensors_data_interpolation"][
            "use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False
        # cfg_test["external_features"]["module"] = os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test["external_features"][
            "functions"] = "get_identity"  # " get_ndvi"
        cfg_test["chain"]["region_path"] = None
        cfg_test["chain"]["nomenclature_path"] = config_path_test
        cfg_test["chain"]["color_table"] = config_path_test
        cfg_test["chain"]["ground_truth"] = config_path_test
        cfg_test["chain"]["l8_path"] = None
        cfg_test["chain"]["l8_path_old"] = None
        with open(config_path_test, "w") as file_cfg:
            file_cfg.write(str(cfg_test))
        cfg = rcf.read_config_file(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir), False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(cfg).get_sensors_parameters(
            tile_name)
        sensors = sensors_container(tile_name, working_dir, str(i2_tmpdir),
                                    **sensors_param)
        sensors.sensors_preprocess()
        list_sensors = sensors.get_enabled_sensors()
        sensor = list_sensors[0]
        ((time_s_app, app_dep), features_labels) = sensor.get_time_series()

        time_s_app.ExecuteAndWriteOutput()
        time_s = sensor.get_time_series_masks()
        time_s_app, app_dep, nbdates = time_s
        time_s_app.ExecuteAndWriteOutput()

        ((time_s_app, app_dep),
         features_labels) = sensor.get_time_series_gapfilling()
        # only one sensor for test
        # sensor_name, ((time_s_app, app_dep), features_labels) = time_s[0]
        # ( (time_s_app, app_dep), features_labels) = time_s
        apps_dict, labs_dict, ori_dep = generate_features(
            working_dir, "T31TCJ", False, str(i2_tmpdir), sensors_param)
        apps_dict["enable_interp"] = True
        apps_dict["enable_raw"] = False
        apps_dict["enable_masks"] = False
        # ori_features = apps_dict["interp"]
        # ori_feat_labels = labs_dict["interp"]
        # Then apply function
        # module_path = os.path.join(IOTA2DIR, "data", "numpy_features",
        #                            "user_custom_function.py")
        # list_functions = ["get_identity", "get_ndvi", "duplicate_ndvi"]
        list_functions = [("get_cumulative_productivity", {}),
                          ("get_seasonal_variation", {})]
        cust = custom_numpy_features(
            tile_name, str(i2_tmpdir), sensors_param,
            cfg.getParam("external_features", "module"), list_functions)
        function_partial = partial(cust.process)

        labels_features_name = ["NDVI_20200101", "NDVI_20200102"]
        new_features_path = str(i2_tmpdir / "DUMMY_test.tif")

        (crop_image, new_labels, out_transform, _, proj,
         deps) = compute_custom_features(
             "T31TCJ",
             str(i2_tmpdir),
             sensors_param,
             cfg.getParam("external_features", "module"),
             list_functions,
             apps_dict, [],
             str(i2_tmpdir),
             ChunkConfig(chunk_size_mode="user_fixed",
                         number_of_chunks=1,
                         chunk_size_x=5,
                         chunk_size_y=5),
             0,
             enabled_raw=False,
             enabled_gap=True,
             fill_missing_dates=False,
             all_dates_dict=None,
             mask_valid_data=None,
             mask_value=0)

        assert new_labels is not None
        del cfg

    def test_manage_raw_data(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST : check the whole workflow."""
        TUR.generate_fake_s2_data(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)
        S2ST_data = str(i2_tmpdir)
        testPath = str(i2_tmpdir / "RUN")
        cfg_test = Config(open(config_path_test)).as_dict()
        cfg_test["chain"]["output_path"] = testPath
        cfg_test["chain"]["list_tile"] = "T31TCJ"
        cfg_test["chain"]["l8_path_old"] = "None"
        cfg_test["chain"]["l8_path"] = "None"
        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = S2ST_data
        cfg_test["chain"]["user_feat_path"] = None
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["arg_train"]["crop_mix"] = False
        cfg_test["arg_train"]["samples_classif_mix"] = False
        cfg_test["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test["sensors_data_interpolation"][
            "use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test["external_features"]["module"] = os.path.join(
            IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test["external_features"][
            "functions"] = "get_identity"  # " get_ndvi"
        cfg_test["chain"]["region_path"] = None
        cfg_test["chain"]["nomenclature_path"] = config_path_test
        cfg_test["chain"]["color_table"] = config_path_test
        cfg_test["chain"]["ground_truth"] = config_path_test
        cfg_test["chain"]["l8_path"] = None
        cfg_test["chain"]["l8_path_old"] = None
        with open(config_path_test, "w") as file_cfg:
            file_cfg.write(str(cfg_test))
        cfg = rcf.read_config_file(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir), False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(cfg).get_sensors_parameters(
            tile_name)
        sensors = sensors_container(tile_name, working_dir, str(i2_tmpdir),
                                    **sensors_param)
        sensors.sensors_preprocess()
        list_sensors = sensors.get_enabled_sensors()
        sensor = list_sensors[0]
        ((time_s_app, app_dep), features_labels) = sensor.get_time_series()

        time_s_app.ExecuteAndWriteOutput()
        time_s = sensor.get_time_series_masks()
        time_s_app, app_dep, nbdates = time_s
        time_s_app.ExecuteAndWriteOutput()

        ((time_s_app, app_dep),
         features_labels) = sensor.get_time_series_gapfilling()

        # Then apply function
        module_path = os.path.join(IOTA2DIR, "data", "numpy_features",
                                   "user_custom_function.py")
        list_functions = [("get_raw_data", {})
                          ]  #, "get_ndvi", "duplicate_ndvi"]
        all_dates_dict = {}
        all_dates_dict["Sentinel2"] = [
            "20200101", "20200212", "20200512", "20200702", "20201002",
            "20201102"
        ]
        cust = custom_numpy_features(tile_name,
                                     str(i2_tmpdir),
                                     sensors_param,
                                     module_path,
                                     list_functions,
                                     concat_mode=False,
                                     enabled_raw=True,
                                     enabled_gap=False,
                                     fill_missing_dates=True,
                                     all_dates_dict=all_dates_dict,
                                     working_dir=None)
        arr, labels = cust.process(np.ones((5, 4, 3)) * 6,
                                   binary_masks=np.ones((5, 4, 4)) * 5,
                                   raw_data=np.ones((5, 4, 40)))

        assert arr.shape == (5, 4, 66)

        assert labels == [
            'Sentinel2_B2_20200101', 'Sentinel2_B3_20200101',
            'Sentinel2_B4_20200101', 'Sentinel2_B5_20200101',
            'Sentinel2_B6_20200101', 'Sentinel2_B7_20200101',
            'Sentinel2_B8_20200101', 'Sentinel2_B8A_20200101',
            'Sentinel2_B11_20200101', 'Sentinel2_B12_20200101',
            'Sentinel2_B2_20200212', 'Sentinel2_B3_20200212',
            'Sentinel2_B4_20200212', 'Sentinel2_B5_20200212',
            'Sentinel2_B6_20200212', 'Sentinel2_B7_20200212',
            'Sentinel2_B8_20200212', 'Sentinel2_B8A_20200212',
            'Sentinel2_B11_20200212', 'Sentinel2_B12_20200212',
            'Sentinel2_B2_20200512', 'Sentinel2_B3_20200512',
            'Sentinel2_B4_20200512', 'Sentinel2_B5_20200512',
            'Sentinel2_B6_20200512', 'Sentinel2_B7_20200512',
            'Sentinel2_B8_20200512', 'Sentinel2_B8A_20200512',
            'Sentinel2_B11_20200512', 'Sentinel2_B12_20200512',
            'Sentinel2_B2_20200702', 'Sentinel2_B3_20200702',
            'Sentinel2_B4_20200702', 'Sentinel2_B5_20200702',
            'Sentinel2_B6_20200702', 'Sentinel2_B7_20200702',
            'Sentinel2_B8_20200702', 'Sentinel2_B8A_20200702',
            'Sentinel2_B11_20200702', 'Sentinel2_B12_20200702',
            'Sentinel2_B2_20201002', 'Sentinel2_B3_20201002',
            'Sentinel2_B4_20201002', 'Sentinel2_B5_20201002',
            'Sentinel2_B6_20201002', 'Sentinel2_B7_20201002',
            'Sentinel2_B8_20201002', 'Sentinel2_B8A_20201002',
            'Sentinel2_B11_20201002', 'Sentinel2_B12_20201002',
            'Sentinel2_B2_20201102', 'Sentinel2_B3_20201102',
            'Sentinel2_B4_20201102', 'Sentinel2_B5_20201102',
            'Sentinel2_B6_20201102', 'Sentinel2_B7_20201102',
            'Sentinel2_B8_20201102', 'Sentinel2_B8A_20201102',
            'Sentinel2_B11_20201102', 'Sentinel2_B12_20201102',
            'Sentinel2_MASK_20200101', 'Sentinel2_MASK_20200212',
            'Sentinel2_MASK_20200512', 'Sentinel2_MASK_20200702',
            'Sentinel2_MASK_20201002', 'Sentinel2_MASK_20201102'
        ]
        del cfg

    def test_pipeline_raise_raw_data(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST : check the whole workflow."""
        TUR.generate_fake_s2_data(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)
        S2ST_data = str(i2_tmpdir)
        testPath = str(i2_tmpdir / "RUN")
        cfg_test = Config(open(config_path_test)).as_dict()
        cfg_test["chain"]["output_path"] = testPath
        cfg_test["chain"]["list_tile"] = "T31TCJ"
        cfg_test["chain"]["l8_path_old"] = "None"
        cfg_test["chain"]["l8_path"] = "None"
        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = S2ST_data
        cfg_test["chain"]["user_feat_path"] = None
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["arg_train"]["crop_mix"] = False
        cfg_test["arg_train"]["samples_classif_mix"] = False
        cfg_test["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test["sensors_data_interpolation"][
            "use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test["external_features"]["module"] = os.path.join(
            IOTA2DIR, "data", "numpy_features", "user_custom_function.py")
        cfg_test["external_features"][
            "functions"] = "get_identity"  # " get_ndvi"
        cfg_test["chain"]["region_path"] = None
        cfg_test["chain"]["nomenclature_path"] = config_path_test
        cfg_test["chain"]["color_table"] = config_path_test
        cfg_test["chain"]["ground_truth"] = config_path_test
        cfg_test["chain"]["l8_path"] = None
        cfg_test["chain"]["l8_path_old"] = None
        with open(config_path_test, "w") as file_cfg:
            file_cfg.write(str(cfg_test))
        cfg = rcf.read_config_file(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir), False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(cfg).get_sensors_parameters(
            tile_name)

        apps_dict, labs_dict, deps = generate_features(working_dir, "T31TCJ",
                                                       False, str(i2_tmpdir),
                                                       sensors_param)
        # Then apply function
        module_path = os.path.join(IOTA2DIR, "data", "numpy_features",
                                   "user_custom_function.py")
        list_functions = [("get_raw_data", {})
                          ]  #, "get_ndvi", "duplicate_ndvi"]

        all_dates_dict = {}
        all_dates_dict["Sentinel2"] = [
            "20200101", "20200212", "20200512", "20200702", "20201002"
        ]

        apps_dict["enable_raw"] = False
        apps_dict["enable_masks"] = False
        apps_dict["enable_interp"] = True

        pytest.raises(AttributeError,
                      compute_custom_features,
                      tile=tile_name,
                      output_path=cfg.getParam("chain", "output_path"),
                      sensors_parameters=sensors_param,
                      module_path=module_path,
                      list_functions=list_functions,
                      otb_pipelines=apps_dict,
                      feat_labels=labs_dict["interp"],
                      path_wd=str(i2_tmpdir),
                      targeted_chunk=0,
                      chunk_config=ChunkConfig(chunk_size_mode="split_number",
                                               number_of_chunks=1,
                                               chunk_size_x=5,
                                               chunk_size_y=5),
                      enabled_gap=True,
                      enabled_raw=False,
                      fill_missing_dates=False,
                      all_dates_dict=None,
                      concat_mode=False)

        del cfg

    def test_pipeline_use_exogeneous_data(self, i2_tmpdir,
                                          rm_tmpdir_on_success):
        """TEST : check the whole workflow."""
        # generate multiple tiles
        for tile in ["T31TCJ", "T31TDJ"]:
            TUR.generate_fake_s2_data(
                str(i2_tmpdir),
                tile,
                ["20200101", "20200512", "20200702", "20201002"],
            )
        # make symbolic link in one folder for tiling tool
        tile_folder = str(i2_tmpdir / "sample_tiles")
        tiled_exogenous_data = str(i2_tmpdir / "tiled_exogenous_data")
        os.mkdir(tile_folder)
        for src, dst in (
            ("T31TCJ/SENTINEL2B_20200101-000000-000_L2A_T31TCJ_D_V1-7/SENTINEL2B_20200101-000000-000_L2A_T31TCJ_D_V1-7_FRE_B2.tif",
             "T31TCJ.tif"),
            ("T31TDJ/SENTINEL2B_20200101-000000-000_L2A_T31TDJ_D_V1-7/SENTINEL2B_20200101-000000-000_L2A_T31TDJ_D_V1-7_FRE_B2.tif",
             "T31TDJ.tif"),
        ):
            os.symlink(str(i2_tmpdir / src),
                       str(Path(tile_folder) / Path(dst)))
        # reproject fake exogenous data on multiple tiles
        reproject(
            IOTA2DIR / "data" / "numpy_features" / "fake_exogeneous_data.tif",
            Path(tile_folder),
            Path(tiled_exogenous_data),
            "float",
        )

        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)
        S2ST_data = str(i2_tmpdir)
        testPath = str(i2_tmpdir / "RUN")
        cfg_test = Config(open(config_path_test)).as_dict()
        cfg_test["chain"]["output_path"] = testPath
        cfg_test["chain"]["list_tile"] = "T31TCJ"
        cfg_test["chain"]["l8_path_old"] = None
        cfg_test["chain"]["l8_path"] = None
        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = S2ST_data
        cfg_test["chain"]["user_feat_path"] = None
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["arg_train"]["crop_mix"] = False
        cfg_test["arg_train"]["samples_classif_mix"] = False
        cfg_test["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test["sensors_data_interpolation"][
            "use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False

        cfg_test["external_features"][
            "functions"] = "use_exogeneous_data get_ndvi"
        # uses tiled version of exogeneous data
        cfg_test["external_features"]["exogeneous_data"] = os.path.join(
            tiled_exogenous_data, "fake_exogeneous_data_$TILE.tif")

        cfg_test["external_features"]["exogeneous_data"] = (os.path.join(
            IOTA2DIR, "data", "numpy_features", "fake_exogeneous_data.tif"))
        cfg_test["chain"]["region_path"] = None
        cfg_test["chain"]["nomenclature_path"] = config_path_test
        cfg_test["chain"]["color_table"] = config_path_test
        cfg_test["chain"]["ground_truth"] = config_path_test
        cfg_test["chain"]["l8_path"] = None
        cfg_test["chain"]["l8_path_old"] = None
        # multiband image
        # cfg_test["external_features"]exogeneous_data = (os.path.join(
        #     IOTA2DIR, "data", "numpy_features", "fake_exo_multi_band.tif"))

        with open(config_path_test, "w") as file_cfg:
            file_cfg.write(str(cfg_test))
        cfg = rcf.read_config_file(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir), False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(cfg).get_sensors_parameters(
            tile_name)
        apps_dict, labs_dict, deps = generate_features(working_dir, "T31TCJ",
                                                       False, str(i2_tmpdir),
                                                       sensors_param)

        all_dates_dict = {}
        all_dates_dict["Sentinel2"] = [
            "20200101", "20200212", "20200512", "20200702", "20201002"
        ]

        apps_dict["enable_raw"] = True
        apps_dict["enable_masks"] = True
        apps_dict["enable_interp"] = True

        (otbimage, feat_labels, out_transform, _, projection,
         deps) = compute_custom_features(
             tile=tile_name,
             output_path=cfg.getParam("chain", "output_path"),
             sensors_parameters=sensors_param,
             module_path=cfg.getParam("external_features", "module"),
             list_functions=cfg.getParam("external_features", 'functions'),
             otb_pipelines=apps_dict,
             feat_labels=labs_dict["interp"],
             path_wd=str(i2_tmpdir),
             targeted_chunk=0,
             chunk_config=ChunkConfig(chunk_size_mode="split_number",
                                      number_of_chunks=1,
                                      chunk_size_x=5,
                                      chunk_size_y=5),
             enabled_gap=True,
             enabled_raw=True,
             fill_missing_dates=True,
             all_dates_dict=all_dates_dict,
             exogeneous_data=cfg.getParam("external_features",
                                          "exogeneous_data"),
             concat_mode=False)

        assert len(feat_labels) == 56
        assert otbimage["array"].shape == (16, 86, 56)
        del cfg

    def test_raw_data_s1_s2(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST the whole workflow."""
        # prepare data
        TUR.generate_fake_s2_data(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512"],
        )
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")
        config_path_test_s1 = str(i2_tmpdir / "Config_TEST_SAR.cfg")
        S2_data = str(i2_tmpdir)

        s1_output_data = str(i2_tmpdir / "tilled_s1")
        ensure_dir(s1_output_data)
        config = configparser.ConfigParser()
        config.read(self.sar_config_test)
        paths = config["Paths"]
        paths["output"] = s1_output_data
        paths["srtm"] = self.srtm_dir
        paths["geoidfile"] = self.geoid_file
        paths["s1images"] = self.acquisitions
        processing = config["Processing"]

        processing["referencesfolder"] = S2_data
        processing["srtmshapefile"] = self.srtm_db
        processing["tilesshapefile"] = self.tiles_grid_db
        processing["TemporalResolution"] = "1"

        with open(config_path_test_s1, 'w') as configfile:
            config.write(configfile)

        shutil.copy(self.config_test, config_path_test)

        testPath = str(i2_tmpdir / "RUN")
        cfg_test = Config(open(config_path_test)).as_dict()
        cfg_test["chain"]["output_path"] = testPath
        cfg_test["chain"]["list_tile"] = "T31TCJ"
        cfg_test["chain"]["l8_path_old"] = "None"
        cfg_test["chain"]["l8_path"] = "None"
        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = S2_data
        cfg_test["chain"]["s1_path"] = config_path_test_s1
        cfg_test["chain"]["user_feat_path"] = None
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["arg_train"]["crop_mix"] = False
        cfg_test["arg_train"]["samples_classif_mix"] = False
        cfg_test["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test["sensors_data_interpolation"][
            "use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test["python_data_managing"]["number_of_chunks"] = 1
        cfg_test["python_data_managing"]["chunk_size_mode"] = "split_number"
        cfg_test["chain"]["region_path"] = None
        cfg_test["chain"]["nomenclature_path"] = config_path_test
        cfg_test["chain"]["color_table"] = config_path_test
        cfg_test["chain"]["ground_truth"] = config_path_test
        cfg_test["chain"]["l8_path"] = None
        cfg_test["chain"]["l8_path_old"] = None
        with open(config_path_test, "w") as file_cfg:
            file_cfg.write(str(cfg_test))
        cfg = rcf.read_config_file(config_path_test)

        iota2_directory.generate_directories(
            testPath, False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(cfg).get_sensors_parameters(
            tile_name)
        apps_dict, labs_dict, deps = generate_features(working_dir, "T31TCJ",
                                                       False, str(i2_tmpdir),
                                                       sensors_param)

        # availables dates == all dates
        all_dates_dict = {
            "Sentinel2": ["20200101", "20200512"],
            'Sentinel1_DES_vv': ['20151231'],
            'Sentinel1_DES_vh': ['20151231'],
            'Sentinel1_ASC_vv': ['20170518', '20170519'],
            'Sentinel1_ASC_vh': ['20170518', '20170519']
        }

        apps_dict["enable_raw"] = True
        apps_dict["enable_masks"] = True
        apps_dict["enable_interp"] = True

        functions = [("get_raw_data", {})]
        module_path = cfg.getParam('external_features', "module")

        # launch function
        (otbimage, feat_labels, out_transform, _, projection,
         deps) = compute_custom_features(
             tile=tile_name,
             output_path=cfg.getParam("chain", "output_path"),
             sensors_parameters=sensors_param,
             module_path=module_path,
             list_functions=functions,
             otb_pipelines=apps_dict,
             feat_labels=labs_dict["interp"],
             path_wd=str(i2_tmpdir),
             targeted_chunk=0,
             chunk_config=ChunkConfig(chunk_size_mode="split_number",
                                      number_of_chunks=1,
                                      chunk_size_x=5,
                                      chunk_size_y=5),
             enabled_gap=True,
             enabled_raw=True,
             fill_missing_dates=True,
             all_dates_dict=all_dates_dict,
             concat_mode=False)
        cust_arr = otbimage["array"]

        # asserts
        s2_nb_bands = 10
        s2_dates = 2
        s1_nb_bands = 6
        s1_nb_masks = 3  #(1 DES mask + 2 ASC masks)
        expected_shape = (16, 86, s2_nb_bands * s2_dates + s2_dates +
                          s1_nb_bands + s1_nb_masks)
        assert cust_arr.shape == expected_shape
        data_bands = cust_arr[:, :, 0:26]
        masks_bands = cust_arr[:, :, 26:]
        raw_ts = apps_dict["raw"]
        raw_arr = raw_ts.GetVectorImageAsNumpyArray("out")
        assert np.allclose(raw_arr, data_bands)

        masks_ts = apps_dict["masks"]
        masks_arr = masks_ts.GetVectorImageAsNumpyArray("out")
        assert np.allclose(masks_arr, masks_bands)

        # prepare data
        # availables dates != all dates : 1 fake s2 and s1 bands required
        all_dates_dict = {
            "Sentinel2": ["20200101", "20200201", "20200512"],
            'Sentinel1_DES_vv': ["20151201", '20151231'],
            'Sentinel1_DES_vh': ["20151201", '20151231'],
            'Sentinel1_ASC_vv': ['20170518', '20170519'],
            'Sentinel1_ASC_vh': ['20170518', '20170519']
        }
        # launch function
        (otbimage, feat_labels, out_transform, _, projection,
         deps) = compute_custom_features(
             tile=tile_name,
             output_path=cfg.getParam("chain", "output_path"),
             sensors_parameters=sensors_param,
             module_path=module_path,
             list_functions=functions,
             otb_pipelines=apps_dict,
             feat_labels=labs_dict["interp"],
             path_wd=str(i2_tmpdir),
             targeted_chunk=0,
             chunk_config=ChunkConfig(chunk_size_mode="split_number",
                                      number_of_chunks=1,
                                      chunk_size_x=5,
                                      chunk_size_y=5),
             enabled_gap=True,
             enabled_raw=True,
             fill_missing_dates=True,
             all_dates_dict=all_dates_dict,
             concat_mode=False)

        cust_arr = otbimage["array"]

        # asserts
        s2_nb_bands = 10
        s2_dates = 3
        s1_nb_bands = 8
        s1_nb_masks = 4  #(1 DES mask + 2 ASC masks)
        expected_shape = (16, 86, s2_nb_bands * s2_dates + s2_dates +
                          s1_nb_bands + s1_nb_masks)

        data_bands = cust_arr[:, :, 0:38]
        masks_bands = cust_arr[:, :, 38:]
        raw_ts = apps_dict["raw"]
        raw_arr = raw_ts.GetVectorImageAsNumpyArray("out")

        assert cust_arr.shape == expected_shape
        fake_bands = np.ones((16, 86, 1)) * I2_CONST.i2_missing_dates_no_data
        # s1 data then s2 data
        bands_to_insert = [0, 1, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16]
        test_otbimage = np.insert(raw_arr,
                                  obj=bands_to_insert,
                                  values=fake_bands,
                                  axis=2)
        assert np.allclose(test_otbimage,
                           data_bands), ("fake spectal bands are "
                                         "at the wrong position")

        masks_ts = apps_dict["masks"]
        masks_arr = masks_ts.GetVectorImageAsNumpyArray("out")
        fake_bands = np.ones(
            (16, 86, 1)) * I2_CONST.i2_missing_dates_no_data_mask
        # s1 data then s2 data
        bands_to_insert = [0, 4]
        test_mask = np.insert(masks_arr,
                              obj=bands_to_insert,
                              values=fake_bands,
                              axis=2)
        assert np.allclose(
            test_mask,
            masks_bands), "fake mask bands are at the wrong position"
        del cfg

    def test_s2_with_padding(self, i2_tmpdir, rm_tmpdir_on_success):
        """Check custom feature with padding"""
        # prepare data
        s2_dates = ["20200101", "20200512"]
        TUR.generate_fake_s2_data(
            str(i2_tmpdir),
            "T31TCJ",
            s2_dates,
        )
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)

        S2_data = str(i2_tmpdir)
        testPath = str(i2_tmpdir / "RUN")
        cfg_test = Config(open(config_path_test)).as_dict()
        cfg_test["chain"]["output_path"] = testPath
        cfg_test["chain"]["list_tile"] = "T31TCJ"
        cfg_test["chain"]["l8_path_old"] = "None"
        cfg_test["chain"]["l8_path"] = "None"
        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = S2_data
        cfg_test["chain"]["user_feat_path"] = None
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["arg_train"]["crop_mix"] = False
        cfg_test["arg_train"]["samples_classif_mix"] = False
        cfg_test["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test["sensors_data_interpolation"][
            "use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test["python_data_managing"]["number_of_chunks"] = 1
        cfg_test["python_data_managing"]["chunk_size_mode"] = "split_number"
        cfg_test["chain"]["region_path"] = None
        cfg_test["chain"]["nomenclature_path"] = config_path_test
        cfg_test["chain"]["color_table"] = config_path_test
        cfg_test["chain"]["ground_truth"] = config_path_test
        cfg_test["chain"]["l8_path"] = None
        cfg_test["chain"]["l8_path_old"] = None

        with open(config_path_test, "w") as file_cfg:
            file_cfg.write(str(cfg_test))
        cfg = rcf.read_config_file(config_path_test)

        iota2_directory.generate_directories(
            testPath, False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(cfg).get_sensors_parameters(
            tile_name)
        apps_dict, labs_dict, _ = generate_features(working_dir, "T31TCJ",
                                                    False, str(i2_tmpdir),
                                                    sensors_param)

        # availables dates == all dates
        all_dates_dict = {"Sentinel2": s2_dates}

        apps_dict["enable_raw"] = True
        apps_dict["enable_masks"] = True
        apps_dict["enable_interp"] = True

        module_path = __file__

        kernel = np.array([[1, 1, 1], [1, 1, 0], [1, 0, 0]])
        functions = [("convolve_raw_data", {
            "kernel": kernel,
            "mode": "constant",
            "cval": 0.0
        })]

        raw_ts = apps_dict["raw"]
        raw_arr = raw_ts.GetVectorImageAsNumpyArray("out")
        ref_proj = raw_ts.GetImageProjection("out")
        ref_x_size, ref_y_size = raw_ts.GetImageSize("out")
        ref_xres, ref_yres = raw_ts.GetImageSpacing("out")
        ref_origin_x, ref_origin_y = raw_ts.GetImageOrigin("out")

        # launch function
        otbimage, _, _, _, proj, deps = compute_custom_features(
            tile=tile_name,
            output_path=cfg.getParam("chain", "output_path"),
            sensors_parameters=sensors_param,
            module_path=module_path,
            list_functions=functions,
            otb_pipelines=apps_dict,
            feat_labels=labs_dict["interp"],
            path_wd=str(i2_tmpdir),
            targeted_chunk=None,
            chunk_config=ChunkConfig(chunk_size_mode="split_number",
                                     number_of_chunks=3,
                                     chunk_size_x=5,
                                     chunk_size_y=5,
                                     padding_size_x=3,
                                     padding_size_y=3),
            enabled_gap=True,
            enabled_raw=True,
            fill_missing_dates=True,
            all_dates_dict=all_dates_dict,
            concat_mode=False)
        # uncomment the next lines led to an error
        # raw_arr = raw_ts.GetVectorImageAsNumpyArray("out")
        cust_arr = otbimage["array"]

        # asserts
        s2_nb_bands = 10
        expected_shape = (16, 86, s2_nb_bands * len(s2_dates))
        assert cust_arr.shape == expected_shape
        assert np.allclose(
            ndimage.convolve(raw_arr,
                             np.repeat(kernel[:, :, np.newaxis],
                                       raw_arr.shape[-1],
                                       axis=2),
                             mode="constant",
                             cval=0.0), cust_arr), "padding workflow broken"

        bandmath = otb.Registry.CreateApplication("BandMath")
        bandmath.ImportVectorImage("il", otbimage)
        bandmath.Execute()
        proj = bandmath.GetImageProjection("out")
        assert proj == ref_proj
        x_size, y_size = bandmath.GetImageSize("out")
        assert (x_size, y_size) == (ref_x_size, ref_y_size)
        xres, yres = bandmath.GetImageSpacing("out")
        assert (xres, yres) == (ref_xres, ref_yres)
        origin_x, origin_y = bandmath.GetImageOrigin("out")
        assert (origin_x, origin_y) == (ref_origin_x, ref_origin_y)
        del cfg

    def test_pipeline_use_raw_and_masks(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST : check the whole workflow."""

        TUR.generate_fake_s2_data(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200512", "20200702", "20201002"],
        )
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")

        shutil.copy(self.config_test, config_path_test)
        S2ST_data = str(i2_tmpdir)
        testPath = str(i2_tmpdir / "RUN")
        cfg_test = Config(open(config_path_test)).as_dict()
        cfg_test["chain"]["output_path"] = testPath
        cfg_test["chain"]["list_tile"] = "T31TCJ"
        cfg_test["chain"]["l8_path_old"] = "None"
        cfg_test["chain"]["l8_path"] = "None"
        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = S2ST_data
        cfg_test["chain"]["user_feat_path"] = None
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["arg_train"]["crop_mix"] = False
        cfg_test["arg_train"]["samples_classif_mix"] = False
        cfg_test["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test["sensors_data_interpolation"][
            "use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False

        cfg_test["external_features"][
            "functions"] = "use_raw_and_masks_data"  # " get_ndvi"
        cfg_test["chain"]["region_path"] = None
        cfg_test["chain"]["nomenclature_path"] = config_path_test
        cfg_test["chain"]["color_table"] = config_path_test
        cfg_test["chain"]["ground_truth"] = config_path_test
        cfg_test["chain"]["l8_path"] = None
        cfg_test["chain"]["l8_path_old"] = None

        with open(config_path_test, "w") as file_cfg:
            file_cfg.write(str(cfg_test))
        cfg = rcf.read_config_file(config_path_test)

        iota2_directory.generate_directories(
            str(i2_tmpdir), False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(cfg).get_sensors_parameters(
            tile_name)
        apps_dict, labs_dict, deps = generate_features(working_dir, "T31TCJ",
                                                       False, str(i2_tmpdir),
                                                       sensors_param)

        all_dates_dict = {}
        all_dates_dict["Sentinel2"] = [
            "20200101", "20200212", "20200512", "20200702", "20201002"
        ]

        apps_dict["enable_raw"] = True
        apps_dict["enable_masks"] = True
        apps_dict["enable_interp"] = True

        (otbimage, feat_labels, out_transform, _, projection,
         deps) = compute_custom_features(
             tile=tile_name,
             output_path=cfg.getParam("chain", "output_path"),
             sensors_parameters=sensors_param,
             module_path=cfg.getParam("external_features", "module"),
             list_functions=cfg.getParam("external_features", 'functions'),
             otb_pipelines=apps_dict,
             feat_labels=labs_dict["interp"],
             path_wd=str(i2_tmpdir),
             targeted_chunk=0,
             chunk_config=ChunkConfig(chunk_size_mode="split_number",
                                      number_of_chunks=1,
                                      chunk_size_x=5,
                                      chunk_size_y=5),
             enabled_gap=True,
             enabled_raw=True,
             fill_missing_dates=True,
             all_dates_dict=all_dates_dict,
             exogeneous_data=None,
             concat_mode=False)

        unique, counts = np.unique(otbimage["array"], return_counts=True)
        dico = dict(zip(unique, counts))
        assert dico[-1000] == 2752
        assert otbimage["array"].dtype == "int16"
        assert len(feat_labels) == 4
        assert otbimage["array"].shape == (16, 86, 4)
        del cfg

    def test_interpolated_data_s1_s2(self, i2_tmpdir, rm_tmpdir_on_success):
        """Check custom feature with S1 data"""
        # prepare data
        TUR.generate_fake_s2_data(
            str(i2_tmpdir),
            "T31TCJ",
            ["20200101", "20200111"],
        )
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")
        config_path_test_s1 = str(i2_tmpdir / "Config_TEST_SAR.cfg")
        S2_data = str(i2_tmpdir)

        s1_output_data = str(i2_tmpdir / "tilled_s1")
        ensure_dir(s1_output_data)
        config = configparser.ConfigParser()
        config.read(self.sar_config_test)
        paths = config["Paths"]
        paths["output"] = s1_output_data
        paths["srtm"] = self.srtm_dir
        paths["geoidfile"] = self.geoid_file
        paths["s1images"] = self.acquisitions
        processing = config["Processing"]

        processing["referencesfolder"] = S2_data
        processing["srtmshapefile"] = self.srtm_db
        processing["tilesshapefile"] = self.tiles_grid_db
        processing["TemporalResolution"] = "1"

        with open(config_path_test_s1, 'w') as configfile:
            config.write(configfile)

        shutil.copy(self.config_test, config_path_test)

        testPath = str(i2_tmpdir / "RUN")
        cfg_test = Config(open(config_path_test)).as_dict()
        cfg_test["chain"]["output_path"] = testPath
        cfg_test["chain"]["list_tile"] = "T31TCJ"
        cfg_test["chain"]["l8_path_old"] = "None"
        cfg_test["chain"]["l8_path"] = "None"
        cfg_test["chain"]["check_inputs"] = False
        cfg_test["chain"]["s2_path"] = S2_data
        cfg_test["chain"]["s1_path"] = config_path_test_s1
        cfg_test["chain"]["user_feat_path"] = None
        cfg_test["chain"]["region_field"] = "region"
        cfg_test["arg_train"]["crop_mix"] = False
        cfg_test["arg_train"]["samples_classif_mix"] = False
        cfg_test["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test["sensors_data_interpolation"][
            "use_additional_features"] = False
        cfg_test["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test["python_data_managing"]["number_of_chunks"] = 1
        cfg_test["python_data_managing"]["chunk_size_mode"] = "split_number"
        cfg_test["chain"]["region_path"] = None
        cfg_test["chain"]["nomenclature_path"] = config_path_test
        cfg_test["chain"]["color_table"] = config_path_test
        cfg_test["chain"]["ground_truth"] = config_path_test
        cfg_test["chain"]["l8_path"] = None
        cfg_test["chain"]["l8_path_old"] = None
        with open(config_path_test, "w") as file_cfg:
            file_cfg.write(str(cfg_test))

        cfg = rcf.read_config_file(config_path_test)

        iota2_directory.generate_directories(
            testPath, False,
            cfg.getParam("chain", "list_tile").split(" "))
        tile_name = "T31TCJ"
        working_dir = None
        sensors_param = rcf.iota2_parameters(cfg).get_sensors_parameters(
            tile_name)
        apps_dict, labs_dict, deps = generate_features(working_dir, "T31TCJ",
                                                       False, str(i2_tmpdir),
                                                       sensors_param)
        apps_dict["enable_raw"] = True
        apps_dict["enable_masks"] = True
        apps_dict["enable_interp"] = True
        functions = [("get_interp_data", {})]
        module_path = __file__
        all_dates_dict = {
            "Sentinel2": ["20200101", "20200111"],
            'Sentinel1_DES_vv': ['20151231'],
            'Sentinel1_DES_vh': ['20151231'],
            'Sentinel1_ASC_vv': ['20170518', '20170519'],
            'Sentinel1_ASC_vh': ['20170518', '20170519']
        }
        # launch function
        (otbimage, feat_labels, out_transform, _, projection,
         deps) = compute_custom_features(
             tile=tile_name,
             output_path=cfg.getParam("chain", "output_path"),
             sensors_parameters=sensors_param,
             module_path=module_path,
             list_functions=functions,
             otb_pipelines=apps_dict,
             feat_labels=labs_dict["interp"],
             path_wd=str(i2_tmpdir),
             targeted_chunk=0,
             chunk_config=ChunkConfig(chunk_size_mode="split_number",
                                      number_of_chunks=1,
                                      chunk_size_x=5,
                                      chunk_size_y=5),
             enabled_gap=True,
             enabled_raw=True,
             fill_missing_dates=True,
             all_dates_dict=all_dates_dict,
             concat_mode=False)
        test_interp_arr = otbimage["array"]
        ref_interp_app = apps_dict["interp"]
        ref_interp_app.Execute()
        ref_interp_arr = ref_interp_app.GetVectorImageAsNumpyArray("out")
        assert np.allclose(ref_interp_arr,
                           test_interp_arr), "interpolated data s1 + s2 fail"
        del cfg


def convolve_raw_data(self, kernel, mode="constant", cval=0.0):
    """
    """
    raw, labels = self.get_filled_stack()
    kernel = np.repeat(kernel[:, :, np.newaxis], raw.shape[-1], axis=2)
    conv = ndimage.convolve(raw, kernel, mode=mode, cval=cval)
    return conv, labels


def get_interp_data(self):
    """must be use only with test_interpolated_data_s1_s2
    due to re-shape
    """
    des_vv = self.get_interpolated_Sentinel1_DES_vv()
    des_vh = self.get_interpolated_Sentinel1_DES_vh()
    asc_vv = self.get_interpolated_Sentinel1_ASC_vv()
    asc_vh = self.get_interpolated_Sentinel1_ASC_vh()

    s2_b2 = self.get_interpolated_Sentinel2_B2()
    s2_b3 = self.get_interpolated_Sentinel2_B3()
    s2_b4 = self.get_interpolated_Sentinel2_B4()
    s2_b5 = self.get_interpolated_Sentinel2_B5()
    s2_b6 = self.get_interpolated_Sentinel2_B6()
    s2_b7 = self.get_interpolated_Sentinel2_B7()
    s2_b8 = self.get_interpolated_Sentinel2_B8()
    s2_b8a = self.get_interpolated_Sentinel2_B8A()
    s2_b11 = self.get_interpolated_Sentinel2_B11()
    s2_b12 = self.get_interpolated_Sentinel2_B12()
    s2_ndvi = self.get_interpolated_Sentinel2_NDVI()
    s2_ndwi = self.get_interpolated_Sentinel2_NDWI()
    s2_brightness = self.get_interpolated_Sentinel2_Brightness()

    interp_stack = np.concatenate(
        (des_vv, des_vh, asc_vv, asc_vh, np.expand_dims(
            s2_b2[:, :, 0], axis=2), np.expand_dims(s2_b3[:, :, 0], axis=2),
         np.expand_dims(s2_b4[:, :, 0],
                        axis=2), np.expand_dims(s2_b5[:, :, 0], axis=2),
         np.expand_dims(s2_b6[:, :, 0],
                        axis=2), np.expand_dims(s2_b7[:, :, 0], axis=2),
         np.expand_dims(s2_b8[:, :, 0],
                        axis=2), np.expand_dims(s2_b8a[:, :, 0], axis=2),
         np.expand_dims(s2_b11[:, :, 0],
                        axis=2), np.expand_dims(s2_b12[:, :, 0], axis=2),
         np.expand_dims(s2_b2[:, :, 1],
                        axis=2), np.expand_dims(s2_b3[:, :, 1], axis=2),
         np.expand_dims(s2_b4[:, :, 1],
                        axis=2), np.expand_dims(s2_b5[:, :, 1], axis=2),
         np.expand_dims(s2_b6[:, :, 1],
                        axis=2), np.expand_dims(s2_b7[:, :, 1], axis=2),
         np.expand_dims(s2_b8[:, :, 1],
                        axis=2), np.expand_dims(s2_b8a[:, :, 1], axis=2),
         np.expand_dims(s2_b11[:, :, 1],
                        axis=2), np.expand_dims(s2_b12[:, :, 1], axis=2),
         np.expand_dims(s2_ndvi[:, :, 0],
                        axis=2), np.expand_dims(s2_ndvi[:, :, 1], axis=2),
         np.expand_dims(s2_ndwi[:, :, 0],
                        axis=2), np.expand_dims(s2_ndwi[:, :, 1], axis=2),
         np.expand_dims(s2_brightness[:, :, 0], axis=2),
         np.expand_dims(s2_brightness[:, :, 1], axis=2)),
        axis=2)
    labels = []
    return interp_stack, labels
