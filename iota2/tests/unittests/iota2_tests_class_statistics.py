# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Test class statistics concatenation called at report generation
or from command line.
"""

import shutil

from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.validation.results_utils import concatenate_sample_selection


class Iota2TestClassStatistics(AssertsFilesUtils):
    """Test class statistics generation."""

    def test_concatenate_sample_selection(self, i2_tmpdir,
                                          rm_tmpdir_on_success):
        """Test concatenate_sample_selection function."""
        ref_dir = IOTA2DIR / "data" / "references" / "class_statistics"
        test_dir = i2_tmpdir / "class_statistics"
        shutil.copytree(ref_dir, test_dir)

        # call
        concatenate_sample_selection(
            result_folder=test_dir,
            nomenclature_path=test_dir / "nomenclature.txt",
            outrates_path=test_dir,
            seed=0,
            regions={
                1,
            },
            labels_conversion={
                1: 5,
                2: 7,
            },
        )

        # compare files
        output_file = test_dir / "class_statistics_seed0_learn.csv"
        expected = test_dir / "class_statistics_seed0_learn_expected.csv"
        unexpected = test_dir / "class_statistics_seed0_learn_unexpected.csv"

        self.assert_file_exist(output_file)
        self.assert_file_equal(output_file, expected)
        self.assert_file_equal(output_file, unexpected, equal=False)

        # delete output file
        output_file.unlink()
