"""Test module."""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

from collections import Counter

from iota2.sampling.split_in_subsets import split_in_subsets
from iota2.tests.utils.tests_utils_vectors import random_ground_truth_generator
from iota2.vector_tools.vector_functions import cp_shape_file, get_field_element


class Iota2TestSamplesSplits:
    """Test class."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.random_splits = 10
        cls.data_field = "code"
        cls.region_field = "region"

    # Tests definitions
    def test_no_seed(self, i2_tmpdir, rm_tmpdir_on_success):  # pylint: disable=W0613
        """Considering 2 iota's run with no random seed provided, check if the
        two runs generate 2 times 10 independant sample-set.
        """

        vector_file_to_split = str(i2_tmpdir / "test_NoSeed.shp")
        numbler_of_class = 23
        random_ground_truth_generator(
            vector_file_to_split, self.data_field, numbler_of_class, self.region_field
        )

        random_seed = None
        # first run
        vector_file_first = str(i2_tmpdir / "test_NoSeed_first.shp")
        cp_shape_file(
            vector_file_to_split.replace(".shp", ""),
            vector_file_first.replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        split_in_subsets(
            vector_file_first,
            self.data_field,
            self.region_field,
            driver_name="ESRI shapefile",
            seeds=self.random_splits,
            random_seed=random_seed,
        )
        # second run
        vector_file_second = str(i2_tmpdir / "test_NoSeed_second.shp")
        cp_shape_file(
            vector_file_to_split.replace(".shp", ""),
            vector_file_second.replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        split_in_subsets(
            vector_file_second,
            self.data_field,
            self.region_field,
            driver_name="ESRI shapefile",
            seeds=self.random_splits,
            random_seed=random_seed,
        )

        # asserts
        seeds_runs = []
        seeds_first = []
        seeds_second = []
        for seed in range(self.random_splits):
            seeds_first.append(
                tuple(
                    get_field_element(
                        vector_file_first,
                        driver_name="ESRI Shapefile",
                        field=f"seed_{seed}",
                        mode="all",
                        elem_type="str",
                    )
                )
            )
            seeds_second.append(
                tuple(
                    get_field_element(
                        vector_file_second,
                        driver_name="ESRI Shapefile",
                        field=f"seed_{seed}",
                        mode="all",
                        elem_type="str",
                    )
                )
            )

            seeds_runs.append(seeds_first[seed] != seeds_second[seed])

        # seeds between runs must be different
        assert all(
            seeds_runs
        ), "two runs of iota², will produce same random split despite arg_train.random_seed is None"

        # each seed in the same run must be different one an other
        all_different = []
        first_seeds_counter = Counter(seeds_first)
        second_seeds_counter = Counter(seeds_second)
        for _, val1 in first_seeds_counter.items():
            all_different.append(val1 == 1)
        for _, val2 in second_seeds_counter.items():
            all_different.append(val2 == 1)
        assert all(
            all_different
        ), "two seeds have the same learning / validation split > random does not work"

    def test_seed(self, i2_tmpdir, rm_tmpdir_on_success):  # pylint: disable=W0613
        """considering 2 iota's run with no random seed provided, check if the
        two runs generate 2 times the same 10 independant sample-set.
        """

        vector_file_to_split = str(i2_tmpdir / "test_Seed.shp")
        numbler_of_class = 23
        random_ground_truth_generator(
            vector_file_to_split, self.data_field, numbler_of_class, self.region_field
        )

        random_seed = 1

        # first run
        vector_file_first = str(i2_tmpdir / "test_NoSeed_first.shp")
        cp_shape_file(
            vector_file_to_split.replace(".shp", ""),
            vector_file_first.replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        split_in_subsets(
            vector_file_first,
            self.data_field,
            self.region_field,
            driver_name="ESRI shapefile",
            seeds=self.random_splits,
            random_seed=random_seed,
        )
        # second run
        vector_file_second = str(i2_tmpdir / "test_NoSeed_second.shp")
        cp_shape_file(
            vector_file_to_split.replace(".shp", ""),
            vector_file_second.replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        split_in_subsets(
            vector_file_second,
            self.data_field,
            self.region_field,
            driver_name="ESRI shapefile",
            seeds=self.random_splits,
            random_seed=random_seed,
        )

        # asserts
        seeds_runs = []
        seeds_first = []
        seeds_second = []
        for seed in range(self.random_splits):
            seeds_first.append(
                tuple(
                    get_field_element(
                        vector_file_first,
                        driver_name="ESRI Shapefile",
                        field=f"seed_{seed}",
                        mode="all",
                        elem_type="str",
                    )
                )
            )
            seeds_second.append(
                tuple(
                    get_field_element(
                        vector_file_second,
                        driver_name="ESRI Shapefile",
                        field=f"seed_{seed}",
                        mode="all",
                        elem_type="str",
                    )
                )
            )

            seeds_runs.append(seeds_first[seed] == seeds_second[seed])

        # seeds between runs must be the same
        assert all(seeds_runs), (
            "two runs of iota², does not produce same random split "
            "despite chain.random_seed is set to an integer"
        )

        # each seed in the same run must be different one an other
        all_different = []
        first_seeds_counter = Counter(seeds_first)
        second_seeds_counter = Counter(seeds_second)
        for _, val1 in first_seeds_counter.items():
            all_different.append(val1 == 1)
        for _, val2 in second_seeds_counter.items():
            all_different.append(val2 == 1)
        assert all(all_different), (
            "two seeds have the same learning / validation "
            "split > random does not work"
        )
