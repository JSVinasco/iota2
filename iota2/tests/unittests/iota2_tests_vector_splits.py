# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Test vector split"""
import os
import shutil
from pathlib import Path

import iota2.tests.utils.tests_utils_vectors as TUV
from iota2.common import file_utils as fu
from iota2.sampling import split_in_subsets as VS
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.vector_tools import vector_functions as vf


class Iota2estVectorSplits:
    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.data_field = "CODE"
        cls.region_field = "region"
        cls.ratio = 0.5
        cls.seeds = 2
        cls.epsg = 2154

        cls.references_directory = str(IOTA2DIR / "data" / "references")
        output_ref_path = str(
            IOTA2DIR
            / "data"
            / "references"
            / "vector_splits"
            / "Output"
            / "splitInSubSets"
        )
        cls.ref_split_shp = str(Path(output_ref_path) / "T31TCJ.shp")

    def test_vector_splits(self, i2_tmpdir, rm_tmpdir_on_success):
        """"""

        test_input_dir = i2_tmpdir / "Input"
        shutil.copytree(
            str(Path(self.references_directory) / "vector_splits" / "Input"),
            str(test_input_dir),
        )

        data_app_val_dir = str(test_input_dir / "dataAppVal")
        new_regions_shapes = [str(test_input_dir / "formattingVectors" / "T31TCJ.shp")]

        for new_region_shape in new_regions_shapes:
            tile_name = os.path.splitext(os.path.basename(new_region_shape))[0]
            vectors_to_rm = fu.file_search_and(data_app_val_dir, True, tile_name)
            for vect in vectors_to_rm:
                os.remove(vect)
            VS.split_in_subsets(
                new_region_shape,
                self.data_field,
                self.region_field,
                self.ratio,
                self.seeds,
                "ESRI Shapefile",
                random_seed=0,
            )
        # We check the output
        assert TUV.compare_vector_file(
            self.ref_split_shp,
            str(test_input_dir / "formattingVectors" / "T31TCJ.shp"),
            "coordinates",
            "polygon",
            "ESRI Shapefile",
        ), "splitting vectors fails"

    def test_vector_splits_no_splits(self, i2_tmpdir, rm_tmpdir_on_success):
        """"""
        test_input_dir = i2_tmpdir / "Input"
        shutil.copytree(
            str(Path(self.references_directory) / "vector_splits" / "Input"),
            str(test_input_dir),
        )
        new_regions_shapes = [str(test_input_dir / "formattingVectors" / "T31TCJ.shp")]
        new_region_shape = new_regions_shapes[0]
        VS.split_in_subsets(
            new_region_shape,
            self.data_field,
            self.region_field,
            self.ratio,
            1,
            "ESRI Shapefile",
            split_groundtruth=False,
            random_seed=0,
        )
        seed0 = vf.get_field_element(
            new_region_shape,
            driver_name="ESRI Shapefile",
            field="seed_0",
            mode="all",
            elem_type="str",
        )

        for elem in seed0:
            assert elem in ["learn"], "flag not in ['learn']"
