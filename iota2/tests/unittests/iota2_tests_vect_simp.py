#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
from pathlib import Path

import pytest

import iota2.tests.utils.tests_utils_vectors as TUV
from iota2.simplification import vector_generalize as vas
from iota2.tests.utils.asserts_utils import AssertsFilesUtils

IOTA2DIR = os.environ.get("IOTA2DIR")

if IOTA2DIR is None:
    raise Exception("IOTA2DIR environment variable must be set")


@pytest.mark.vecto
class Iota2TestsVectSimp(AssertsFilesUtils):
    """Check vectorization/simplification workflow."""

    @classmethod
    def setup_class(cls):
        """Variables avail across tests."""
        cls.rasterreg = os.path.join(
            IOTA2DIR, "data", "references/posttreat/classif_regul.tif"
        )
        cls.vector = os.path.join(
            os.path.join(IOTA2DIR, "data", "references/posttreat/vectors/classif.shp")
        )
        cls.vectorsimp = os.path.join(
            os.path.join(
                IOTA2DIR, "data", "references/posttreat/vectors/classifsimp.shp"
            )
        )
        cls.vectorsmooth = os.path.join(
            os.path.join(
                IOTA2DIR, "data", "references/posttreat/vectors/classifsmooth.shp"
            )
        )

        cls.grasslib = os.environ.get("GRASSDIR")
        if cls.grasslib is None:
            raise Exception("GRASSDIR not initialized")

        if not os.path.exists(os.path.join(cls.grasslib, "bin")):
            raise Exception("GRASSDIR '%s' not well initialized" % (cls.grasslib))

    # Tests definitions
    def test_iota2_VectSimp(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test polygonize, simplification and smoothing operations."""
        # polygonize
        out_dir = i2_tmpdir / "out"
        Path.mkdir(out_dir, exist_ok=True)

        outfilenamesimp = str(out_dir / "classifsimp.shp")
        outfilenamesmooth = str(out_dir / "classifsmooth.shp")
        outfilename = str(out_dir / "classif.shp")
        vas.topological_polygonize(
            str(i2_tmpdir),
            self.rasterreg,
            True,
            outfilename,
            outformat="ESRI Shapefile",
        )
        assert TUV.compare_vector_file(
            self.vector, outfilename, "coordinates", "polygon", "ESRI Shapefile"
        ), ("Generated shapefile vector does not fit with " "shapefile reference file")
        # simplification
        vas.generalize_vector(
            str(i2_tmpdir),
            outfilename,
            10,
            "douglas",
            out=outfilenamesimp,
            outformat="ESRI Shapefile",
        )
        assert TUV.compare_vector_file(
            self.vectorsimp, outfilenamesimp, "coordinates", "polygon", "ESRI Shapefile"
        ), "Generated shapefile vector does not fit with shapefile reference file"

        # smoothing
        vas.generalize_vector(
            str(i2_tmpdir),
            outfilenamesimp,
            10,
            "hermite",
            out=outfilenamesmooth,
            outformat="ESRI Shapefile",
        )
        assert TUV.compare_vector_file(
            self.vectorsmooth,
            outfilenamesmooth,
            "coordinates",
            "polygon",
            "ESRI Shapefile",
        ), "Generated shapefile vector does not fit with shapefile reference file"
