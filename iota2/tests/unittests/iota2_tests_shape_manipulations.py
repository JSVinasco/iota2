# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import shutil

import iota2.common.file_utils as fu
import iota2.tests.utils.tests_utils_vectors as TUV
import iota2.vector_tools.vector_functions as vf
from iota2.common.tools import create_regions_by_tiles as RT
from iota2.sampling import tile_envelope
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


class Iota2TestShapeManipulations:
    """"""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.reference_shape = str(
            IOTA2DIR / "data" / "references" / "D5H2_groundTruth_samples.shp"
        )
        cls.nb_features = 28
        cls.fields = ["ID", "LC", "CODE", "AREA_HA"]
        cls.data_field = "CODE"
        cls.epsg = 2154
        cls.type_shape = str(IOTA2DIR / "data" / "references" / "typo.shp")
        cls.region_field = "DN"

        cls.priority_envelope_ref = str(
            IOTA2DIR / "data" / "references" / "priority_ref"
        )
        cls.split_ratio = 0.5

    def test_count_features(self):
        features = vf.get_field_element(
            self.reference_shape,
            driver_name="ESRI Shapefile",
            field="CODE",
            mode="all",
            elem_type="int",
        )
        assert len(features) == self.nb_features

    def test_multipolygons(self):
        detect_multi = TUV.multi_polygons_search(self.reference_shape)
        single = str(IOTA2DIR / "data" / "test_MultiToSinglePoly.shp")
        vf.multi_poly_to_poly(self.reference_shape, single)

        detect_no_multi = TUV.multi_polygons_search(single)
        assert detect_multi
        assert detect_no_multi is False

        test_files = fu.file_search_reg_ex(str(IOTA2DIR / "data" / "test_*"))

        for test_file in test_files:
            if os.path.isfile(test_file):
                os.remove(test_file)

    def test_getfield(self):
        all_fields = vf.get_all_fields_in_shape(self.reference_shape, "ESRI Shapefile")
        assert self.fields == all_fields

    def test_envelope(self, i2_tmpdir, rm_tmpdir_on_success):  # pylint: disable=W0613
        def gen_tile_env_prio(tests_directory, bbj_list_tile, padding):
            if os.path.exists(tests_directory):
                shutil.rmtree(tests_directory)
            os.mkdir(tests_directory)
            tile_envelope.gen_tile_env_prio(
                bbj_list_tile, tests_directory, self.epsg, padding
            )

        def check_same_envelopes(env_ref, tests_directory):
            comp = []
            for e_ref in env_ref:
                tile_number = os.path.split(e_ref)[-1].split("_")[1]
                comp.append(
                    fu.file_search_and(
                        tests_directory, True, "Tile" + tile_number + "_PRIO.shp"
                    )[0]
                )

            return [
                TUV.check_same_envelope(currentRef, test_env)
                for currentRef, test_env in zip(env_ref, comp)
            ]

        # Create a 3x3 grid (9 vectors shapes). Each tile are 110.010 km
        # with 10 km overlaping to fit L8 datas.
        TUV.gen_grid(
            str(i2_tmpdir),
            x_size=3,
            y_size=3,
            overlap=10,
            size=110.010,
            raster="True",
            pix_size=30,
        )

        tiles_path = fu.file_search_reg_ex(str(i2_tmpdir) + "/*.tif")
        bbj_list_tile = [
            tile_envelope.Tile(
                currentTile, currentTile.split("/")[-1].split(".")[0].split("_")[0]
            )
            for currentTile in tiles_path
        ]
        bbj_list_tile_sort = sorted(bbj_list_tile, key=tile_envelope.priority_key)
        env_ref = fu.file_search_reg_ex(self.priority_envelope_ref + "/*.shp")

        # Without padding
        self.priority_envelope_test = str(i2_tmpdir / "priority_test")
        gen_tile_env_prio(self.priority_envelope_test, bbj_list_tile_sort, padding=0)
        cmp_env = check_same_envelopes(env_ref, self.priority_envelope_test)
        assert all(cmp_env)

        # With padding
        self.priority_envelope_test2 = str(i2_tmpdir / "priority_test2")
        gen_tile_env_prio(self.priority_envelope_test2, bbj_list_tile_sort, padding=10)
        cmp_env = check_same_envelopes(env_ref, self.priority_envelope_test2)
        assert any(cmp_env) is False

    def test_regions_by_tile(
        self, i2_tmpdir, rm_tmpdir_on_success
    ):  # pylint: disable=W0613

        test_regions_by_tiles = str(i2_tmpdir / "test_regionsByTiles")
        if os.path.exists(test_regions_by_tiles):
            shutil.rmtree(test_regions_by_tiles)
        os.mkdir(test_regions_by_tiles)

        RT.create_regions_by_tiles(
            self.type_shape,
            self.region_field,
            self.priority_envelope_ref,
            test_regions_by_tiles,
            None,
        )
