#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

from pathlib import Path

import numpy as np
import pytest

from iota2.common import file_utils as fut
from iota2.common.raster_utils import raster_to_array
from iota2.simplification import manage_regularization as mr
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


@pytest.mark.vecto
class Iota2TestRegularisation:
    """Check regularisation workflow."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.raster10m = str(IOTA2DIR / "data" / "references" / "sampler" /
                            "final" / "Classif_Seed_0.tif")
        cls.rasterregref = str(IOTA2DIR / "data" / "references" / "posttreat" /
                               "classif_regul.tif")
        cls.nomenclature = str(IOTA2DIR / "data" / "references" / "posttreat" /
                               "nomenclature_17.cfg")

    def test_iota2_regularisation(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test regularization."""
        wd = i2_tmpdir / "wd"
        Path.mkdir(wd, exist_ok=True)
        wd = str(wd)
        out = i2_tmpdir / "out"
        Path.mkdir(out, exist_ok=True)
        out = str(out)
        tmp = i2_tmpdir / "tmp"
        Path.mkdir(tmp, exist_ok=True)
        tmp = str(tmp)
        outfile = i2_tmpdir / "classif_regul.tif"
        outfile = str(outfile)

        rules = mr.get_mask_regularisation(self.nomenclature)

        for rule in rules:
            mr.adaptive_regul(wd, self.raster10m, str(Path(tmp) / rule[2]),
                              "1000", rule, 2)

        rasters = fut.file_search_and(tmp, True, "mask", ".tif")
        mr.merge_regularization(tmp, rasters, 10, outfile, "1000")

        # test
        outtest = raster_to_array(outfile)
        outref = raster_to_array(self.rasterregref)
        assert np.array_equal(outtest, outref)
