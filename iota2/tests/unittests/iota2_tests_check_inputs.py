# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""check inputs tests."""
import os
from pathlib import Path

from iota2.common import service_error, verify_inputs
from iota2.common.tools import check_database
from iota2.tests.utils.tests_utils_rasters import generate_fake_s2_data
from iota2.tests.utils.tests_utils_vectors import random_ground_truth_generator
from iota2.vector_tools.vector_functions import cp_shape_file

IOTA2DIR = os.environ.get("IOTA2DIR")
if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(os.environ.get("IOTA2DIR"))


class Iota2CheckInputs:
    """Check the iota2's ability to detect anomaly in user-provided data."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        references_directory = IOTA2DIR / "data" / "references"
        cls.config_test = str(
            IOTA2DIR / "config" / "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )
        cls.ground_truth = str(references_directory / "ZONAGE_BV_ALL.shp")
        cls.region_target_miss = str(references_directory / "region_target_miss.shp")
        cls.region_target = str(references_directory / "region_target.shp")
        cls.gt_target_miss = str(references_directory / "gt_target_miss.shp")
        cls.gt_target = str(references_directory / "gt_target.shp")
        cls.gt_target_miss_region = str(
            Path(references_directory) / "gt_target_miss_region.shp"
        )
        cls.gt_target_miss_one_region = str(
            Path(references_directory) / "gt_target_miss_one_region.shp"
        )

    def test_check_database(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST : check the database."""
        _, ground_truth_name = os.path.split(self.ground_truth)
        test_ground_truth = str(i2_tmpdir / ground_truth_name)
        cp_shape_file(
            self.ground_truth.replace(".shp", ""),
            test_ground_truth.replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        data_field = "code"
        epsg = 2154
        gt_errors = check_database.check_ground_truth_inplace(
            test_ground_truth, data_field, epsg, do_corrections=False, display=False
        )
        # multipolygons detected
        assert len(gt_errors) == 1
        assert isinstance(
            type(gt_errors[0]), service_error.containsMultipolygon.__class__
        ), "multipolygons undetected"
        # wrong projection
        epsg = 2155
        gt_errors = check_database.check_ground_truth_inplace(
            test_ground_truth, data_field, epsg, do_corrections=False, display=False
        )
        assert len(gt_errors) == 2
        assert isinstance(
            type(gt_errors[0]), service_error.invalidProjection.__class__
        ), "invalid projection miss detected"
        assert isinstance(
            type(gt_errors[1]), service_error.containsMultipolygon.__class__
        ), "multipolygons undetected"
        # no field detected
        data_field = "error"
        epsg = 2154
        gt_errors = check_database.check_ground_truth_inplace(
            test_ground_truth, data_field, epsg, do_corrections=False, display=False
        )
        assert len(gt_errors) == 2
        assert isinstance(
            type(gt_errors[0]), service_error.missingField.__class__
        ), "missing field undetected"
        assert isinstance(
            type(gt_errors[1]), service_error.containsMultipolygon.__class__
        ), "multipolygons undetected"
        assert isinstance(
            type(gt_errors[0]), service_error.fieldType.__class__
        ), "integer field Type undetected"
        assert isinstance(
            type(gt_errors[1]), service_error.containsMultipolygon.__class__
        ), "multipolygons undetected"
        # invalid geometry
        no_geom_shape = str(i2_tmpdir / "no_geom_shape.shp")
        random_ground_truth_generator(no_geom_shape, data_field, 3, set_geom=False)
        gt_errors = check_database.check_ground_truth_inplace(
            no_geom_shape, data_field, epsg, do_corrections=False, display=False
        )
        assert len(gt_errors) == 1
        assert isinstance(
            type(gt_errors[0]), service_error.invalidGeometry.__class__
        ), "invalid geometries undetected"

        # duplicated features
        dupli_feat_path = str(i2_tmpdir / "duplicate_features.shp")
        random_ground_truth_generator(dupli_feat_path, data_field, 3)
        gt_errors = check_database.check_ground_truth_inplace(
            dupli_feat_path, data_field, epsg, do_corrections=False, display=False
        )
        assert len(gt_errors) == 1
        assert isinstance(
            type(gt_errors[0]), service_error.duplicatedFeatures.__class__
        ), "duplicated features undetected"

    def test_check_region_shape(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST : check the region shape database."""
        _, ground_truth_name = os.path.split(self.ground_truth)
        test_ground_truth = str(i2_tmpdir / ground_truth_name)
        cp_shape_file(
            self.ground_truth.replace(".shp", ""),
            test_ground_truth.replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        data_field = "region"
        epsg = 2154
        region_errors = check_database.check_region_shape(
            test_ground_truth, "", data_field, epsg, do_corrections=False, display=False
        )
        assert len(region_errors) == 1
        assert isinstance(
            type(region_errors[0]), service_error.tooSmallRegion.__class__
        ), "too small regions undetected"

    def test_check_intersections(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST : check if no intersection between geo-reference data is detected."""
        s2_data = str(i2_tmpdir / "S2_data")
        generate_fake_s2_data(s2_data, "T31TCJ", ["20190909", "20190919", "20190929"])
        # usual use case
        intersections_errors = verify_inputs.check_data_intersection(
            self.gt_target, None, "region", 2154, s2_data, ["T31TCJ"]
        )
        assert (
            len(intersections_errors) == 0
        ), "no intersections detected, but there is intersections"

        # no intersections between input rasters and the ground truth
        intersections_errors = verify_inputs.check_data_intersection(
            self.gt_target_miss, None, "region", 2154, s2_data, ["T31TCJ"]
        )

        assert len(intersections_errors) == 1
        assert isinstance(
            type(intersections_errors[0]), service_error.intersectionError.__class__
        ), "no intersections undetected"

        # no intersections between the ground truth and the region shape
        intersections_errors = verify_inputs.check_data_intersection(
            self.gt_target_miss_region,
            self.region_target,
            "region",
            2154,
            s2_data,
            ["T31TCJ"],
        )

        assert len(intersections_errors) == 1
        assert isinstance(
            type(intersections_errors[0]), service_error.intersectionError.__class__
        ), "no intersections undetected"

        # intersections between the ground truth and the region shape
        intersections_errors = verify_inputs.check_data_intersection(
            self.gt_target, self.region_target, "region", 2154, s2_data, ["T31TCJ"]
        )

        assert (
            len(intersections_errors) == 0
        ), "no intersections detected, but there is intersections"

        # no intersections between the ground truth and ONE region
        intersections_errors = verify_inputs.check_data_intersection(
            self.gt_target_miss_one_region,
            self.region_target,
            "region",
            2154,
            s2_data,
            ["T31TCJ"],
        )

        assert len(intersections_errors) == 1
        assert isinstance(
            type(intersections_errors[0]), service_error.intersectionError.__class__
        ), "no intersections undetected"
