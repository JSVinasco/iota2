#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Test sampler merge
"""
import os
import shutil
from pathlib import Path

from iota2.common.utils import run
from iota2.sampling import samples_merge

IOTA2DIR = os.environ.get('IOTA2DIR')

if IOTA2DIR is None:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR)


class Iota2TestMergeSamples:
    """Test merging samples workflow."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.expectedOutputGetModels = [('1', ['T31TCJ'], 0),
                                       ('1', ['T31TCJ'], 1)]

    def test_getModels(self, i2_tmpdir, rm_tmpdir_on_success):
        """"""
        shutil.copytree(
            IOTA2DIR / "data" / "references" / "mergeSamples" / "Input",
            i2_tmpdir / "Input")
        Path.mkdir(i2_tmpdir / "Input" / 'samplesSelection')

        output = samples_merge.get_models(
            str(i2_tmpdir / "Input" / "get_models" / 'formattingVectors'),
            'region', 2)
        assert self.expectedOutputGetModels[0][0] == output[0][0]
        assert self.expectedOutputGetModels[0][1][0] == output[0][1][0]
        assert self.expectedOutputGetModels[0][2] == output[0][2]
        assert self.expectedOutputGetModels[1][0] == output[1][0]
        assert self.expectedOutputGetModels[1][1][0] == output[1][1][0]
        assert self.expectedOutputGetModels[1][2] == output[1][2]

    def test_samplesMerge(self, i2_tmpdir, rm_tmpdir_on_success):
        """"""
        ref_folder = IOTA2DIR / "data" / "references" / "mergeSamples" / "Input"
        shutil.copytree(ref_folder, i2_tmpdir / "Input")
        sample_selection_path = i2_tmpdir / "Input" / "samples_merge" / "samplesSelection"
        Path.mkdir(sample_selection_path, exist_ok=True)

        output = samples_merge.get_models(
            str(i2_tmpdir / "Input" / "get_models" / "formattingVectors"),
            'region', 2)
        samples_merge.samples_merge(region_tiles_seed=output[0],
                                    output_path=str(i2_tmpdir / "Input" /
                                                    "samples_merge"),
                                    region_field="region",
                                    runs=1,
                                    ds_sar_opt_flag=False,
                                    working_directory=None,
                                    sampling_validation=False)

        # We check the produced files
        shape = str(IOTA2DIR / 'data' / 'references' / 'mergeSamples' /
                    'Output' / 'samples_region_1_seed_0.shp')
        proj = str(IOTA2DIR / 'data' / 'references' / 'mergeSamples' /
                   'Output' / 'samples_region_1_seed_0.prj')
        shapex = str(IOTA2DIR / 'data' / 'references' / 'mergeSamples' /
                     'Output' / 'samples_region_1_seed_0.shx')

        shape_t = str(sample_selection_path / 'samples_region_1_seed_0.shp')
        proj_t = str(sample_selection_path / 'samples_region_1_seed_0.prj')
        shapex_t = str(sample_selection_path / 'samples_region_1_seed_0.shx')
        assert 0 == run(f"diff {shape} {shape_t}")
        assert 0 == run(f"diff {proj} {proj_t}")
        assert 0 == run(f"diff {shapex} {shapex_t}")


#
