#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import shutil
from pathlib import Path

import iota2.tests.utils.tests_utils_vectors as TUV
from iota2.common import file_utils as fu
from iota2.sampling import vector_samples_merge as VSM
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


class Iota2TestsVectorSamplesMerge:
    """Check the merge of by tiles samples to an unique one"""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.ref_data = os.path.join(IOTA2DIR, "data", "references",
                                    "VectorSamplesMerge")

    def test_VectorSamplesMerge(self, i2_tmpdir, rm_tmpdir_on_success):
        """"""
        learning_samples = i2_tmpdir / "learningSamples"
        # test and creation of learningSamples
        Path.mkdir(learning_samples, exist_ok=True)
        learning_samples = str(learning_samples)

        # copy input data
        shutil.copy(
            self.ref_data +
            "/Input/D0005H0003_region_1_seed0_learn_Samples.sqlite",
            learning_samples)

        # Start test
        vector_list = fu.file_search_and(learning_samples, True, ".sqlite")
        VSM.vector_samples_merge(vector_list, str(i2_tmpdir), "code")

        # file comparison to ref file
        file1 = os.path.join(learning_samples,
                             "Samples_region_1_seed0_learn.sqlite")
        reference_file1 = os.path.join(self.ref_data, "Output",
                                       "Samples_region_1_seed0_learn.sqlite")
        assert TUV.compare_sqlite(file1,
                                  reference_file1,
                                  cmp_mode='coordinates',
                                  ignored_fields=[])
