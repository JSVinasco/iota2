#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

from pathlib import Path

import numpy as np

from iota2.classification import fusion
from iota2.common import file_utils as fut
from iota2.common import iota2_directory
from iota2.common.file_utils import get_raster_n_bands
from iota2.common.raster_utils import raster_to_array
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_rasters import array_to_raster


class Iota2TestOpticalSARFusion:

    @classmethod
    def setup_class(cls):
        # input data
        cls.config_test = str(IOTA2DIR / "config" /
                              "Config_4Tuiles_Multi_FUS_Confidence.cfg")
        cls.sar_confusion = str(IOTA2DIR / "data" / "references" /
                                "sar_confusion.csv")
        cls.opt_confusion = str(IOTA2DIR / "data" / "references" /
                                "opt_confusion.csv")
        cls.sar_classif = np.array([[11, 12, 31], [42, 51, 11], [11, 43, 11]])
        cls.optical_classif = np.array([[12, 42, 31], [11, 43, 51],
                                        [43, 51, 42]])
        cls.sar_confidence = np.array([[0.159699, 0.872120, 0.610836],
                                       [0.657606, 0.110224, 0.675240],
                                       [0.263030, 0.623490, 0.517019]])
        cls.optical_confidence = np.array([[0.208393, 0.674579, 0.507099],
                                           [0.214745, 0.962130, 0.779217],
                                           [0.858645, 0.258679, 0.015593]])
        # References
        cls.ds_fusion_ref = np.array([[11, 42, 31], [42, 43, 11], [11, 51,
                                                                   11]])
        cls.choice_map_ref = np.array([[2, 3, 1], [2, 3, 2], [2, 3, 2]])
        cls.ds_fus_confidence_ref = np.array(
            [[0.15969899, 0.67457902, 0.61083603],
             [0.65760601, 0.96213001, 0.67523998],
             [0.26302999, 0.258679, 0.51701897]])
        cls.parameter_ref = [{
            'sar_classif':
            '/classif/Classif_T31TCJ_model_1_seed_0_SAR.tif',
            'opt_model':
            '/dataAppVal/bymodels/model_1_seed_0.csv',
            'opt_classif':
            '/classif/Classif_T31TCJ_model_1_seed_0.tif',
            'sar_model':
            '/dataAppVal/bymodels/model_1_seed_0_SAR.csv'
        }, {
            'sar_classif':
            '/classif/Classif_T31TCJ_model_1_seed_1_SAR.tif',
            'opt_model':
            '/dataAppVal/bymodels/model_1_seed_1.csv',
            'opt_classif':
            '/classif/Classif_T31TCJ_model_1_seed_1.tif',
            'sar_model':
            '/dataAppVal/bymodels/model_1_seed_1_SAR.csv'
        }]
        # consts
        cls.classif_seed_pos = 5
        cls.classif_tile_pos = 1
        cls.classif_model_pos = 3
        cls.ds_choice_both = 1
        cls.ds_choice_sar = 2
        cls.ds_choice_opt = 3
        cls.ds_no_choice = 0

    def test_fusion_parameters(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST fusion.dempster_shafer_fusion_parameters."""

        # define inputs
        iota2_dir = str(i2_tmpdir / "fusionTest")
        iota2_directory.generate_directories(
            iota2_dir, False,
            ["D0004H0004", "D0005H0004", "D0004H0003", "D0005H0003"])
        iota2_ds_confusions_dir = str(
            Path(iota2_dir) / "dataAppVal" / "bymodels")
        fut.ensure_dir(iota2_ds_confusions_dir)
        # generate some fake data
        nb_seed = 2
        for i in range(nb_seed):
            fake_classif_opt = str(
                Path(iota2_dir) / "classif" /
                "Classif_T31TCJ_model_1_seed_{}.tif".format(i))
            fake_classif_sar = str(
                Path(iota2_dir) / "classif" /
                "Classif_T31TCJ_model_1_seed_{}_SAR.tif".format(i))
            fake_confidence_opt = str(
                Path(iota2_dir) / "classif" /
                "T31TCJ_model_1_confidence_seed_{}.tif".format(i))
            fake_confidence_sar = str(
                Path(iota2_dir) / "classif" /
                "T31TCJ_model_1_confidence_seed_{}_SAR.tif".format(i))
            fake_model_confusion_sar = str(
                Path(iota2_ds_confusions_dir) /
                "model_1_seed_{}_SAR.csv".format(i))
            fake_model_confusion_opt = str(
                Path(iota2_ds_confusions_dir) /
                "model_1_seed_{}.csv".format(i))

            with open(fake_classif_opt, "w") as new_file:
                new_file.write("TEST")
            with open(fake_classif_sar, "w") as new_file:
                new_file.write("TEST")
            with open(fake_confidence_opt, "w") as new_file:
                new_file.write("TEST")
            with open(fake_confidence_sar, "w") as new_file:
                new_file.write("TEST")
            with open(fake_model_confusion_sar, "w") as new_file:
                new_file.write("TEST")
            with open(fake_model_confusion_opt, "w") as new_file:
                new_file.write("TEST")

        parameters_test = fusion.dempster_shafer_fusion_parameters(iota2_dir)
        # parameters_test depend of execution environement, remove local path is necessary
        for param_group in parameters_test:
            for key, value in list(param_group.items()):
                param_group[key] = value.replace(iota2_dir, "")
        #reverse parameters_test if necessary
        if "seed_1" in parameters_test[0]["sar_model"]:
            parameters_test = parameters_test[::-1]

        # assert
        assert all(
            param_group_test == param_group_ref
            for param_group_test, param_group_ref in zip(
                parameters_test,
                self.parameter_ref)), "input parameters generation failed"

    def test_perform_fusion(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST fusion.perform_fusion."""
        # define inputs
        sar_raster = str(i2_tmpdir / "Classif_T31TCJ_model_1_seed_0_SAR.tif")
        opt_raster = str(i2_tmpdir / "Classif_T31TCJ_model_1_seed_0.tif")
        array_to_raster(self.sar_classif, sar_raster)
        array_to_raster(self.optical_classif, opt_raster)

        fusion_dic = {
            "sar_classif": sar_raster,
            "opt_classif": opt_raster,
            "sar_model": self.sar_confusion,
            "opt_model": self.opt_confusion
        }

        # launch function
        ds_fusion_test = fusion.perform_fusion(
            fusion_dic,
            mob="precision",
            classif_model_pos=self.classif_model_pos,
            classif_tile_pos=self.classif_tile_pos,
            classif_seed_pos=self.classif_seed_pos,
            working_directory=None)
        # assert
        ds_fusion_test = raster_to_array(ds_fusion_test)
        assert np.allclose(self.ds_fusion_ref,
                           ds_fusion_test), "fusion of classifications failed"

    def test_fusion_choice(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST : fusion.compute_fusion_choice."""
        sar_raster = str(i2_tmpdir / "Classif_T31TCJ_model_1_seed_0_SAR.tif")
        opt_raster = str(i2_tmpdir / "Classif_T31TCJ_model_1_seed_0.tif")
        array_to_raster(self.sar_classif, sar_raster)
        array_to_raster(self.optical_classif, opt_raster)
        iota2_dir = str(i2_tmpdir / "fusionTest")

        iota2_directory.generate_directories(
            iota2_dir, False,
            ["D0004H0004", "D0005H0004", "D0004H0003", "D0005H0003"])

        fusion_dic = {
            "sar_classif": sar_raster,
            "opt_classif": opt_raster,
            "sar_model": self.sar_confusion,
            "opt_model": self.opt_confusion
        }
        fusion_class_array = self.ds_fusion_ref
        fusion_class_raster = str(i2_tmpdir / "fusionTest.tif")
        array_to_raster(fusion_class_array, fusion_class_raster)
        workingDirectory = None

        # Launch function
        ds_choice = fusion.compute_fusion_choice(
            iota2_dir, fusion_dic, fusion_class_raster, self.classif_model_pos,
            self.classif_tile_pos, self.classif_seed_pos, self.ds_choice_both,
            self.ds_choice_sar, self.ds_choice_opt, self.ds_no_choice,
            workingDirectory)
        # assert
        ds_choice_test = raster_to_array(ds_choice)
        assert np.allclose(self.choice_map_ref,
                           ds_choice_test), "compute raster choice failed"

    def test_confidence_fusion(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST fusion.compute_confidence_fusion."""
        sar_raster = str(i2_tmpdir / "Classif_T31TCJ_model_1_seed_0_SAR.tif")
        opt_raster = str(i2_tmpdir / "Classif_T31TCJ_model_1_seed_0.tif")
        array_to_raster(self.sar_classif, sar_raster)
        array_to_raster(self.optical_classif, opt_raster)

        sar_confid_raster = str(i2_tmpdir /
                                "T31TCJ_model_1_confidence_seed_0_SAR.tif")
        opt_confid_raster = str(i2_tmpdir /
                                "T31TCJ_model_1_confidence_seed_0.tif")
        array_to_raster(self.sar_confidence,
                        sar_confid_raster,
                        output_format="float")
        array_to_raster(self.optical_confidence,
                        opt_confid_raster,
                        output_format="float")

        ds_choice = str(i2_tmpdir / "choice.tif")
        array_to_raster(self.choice_map_ref, ds_choice)

        fusion_dic = {
            "sar_classif": sar_raster,
            "opt_classif": opt_raster,
            "sar_model": self.sar_confusion,
            "opt_model": self.opt_confusion
        }
        workingDirectory = None

        # Launch function
        ds_fus_confidence_test = fusion.compute_confidence_fusion(
            fusion_dic, ds_choice, self.classif_model_pos,
            self.classif_tile_pos, self.classif_seed_pos, self.ds_choice_both,
            self.ds_choice_sar, self.ds_choice_opt, self.ds_no_choice,
            workingDirectory)
        # assert
        ds_fus_confidence_test = raster_to_array(ds_fus_confidence_test)
        assert np.allclose(
            self.ds_fus_confidence_ref,
            ds_fus_confidence_test), "fusion of confidences failed"

    def test_compute_probamap_fusion(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST fusion.compute_probamap_fusion()."""
        sar_raster = str(i2_tmpdir / "Classif_T31TCJ_model_1_seed_0_SAR.tif")
        opt_raster = str(i2_tmpdir / "Classif_T31TCJ_model_1_seed_0.tif")
        array_to_raster(self.sar_classif, sar_raster)
        array_to_raster(self.optical_classif, opt_raster)
        sar_confid_raster = str(i2_tmpdir /
                                "T31TCJ_model_1_confidence_seed_0_SAR.tif")
        opt_confid_raster = str(i2_tmpdir /
                                "T31TCJ_model_1_confidence_seed_0.tif")
        array_to_raster(self.sar_confidence,
                        sar_confid_raster,
                        output_format="float")
        array_to_raster(self.optical_confidence,
                        opt_confid_raster,
                        output_format="float")

        ds_choice = str(i2_tmpdir / "choice.tif")
        array_to_raster(self.choice_map_ref, ds_choice)

        fusion_dic = {
            "sar_classif": sar_raster,
            "opt_classif": opt_raster,
            "sar_model": self.sar_confusion,
            "opt_model": self.opt_confusion
        }
        workingDirectory = None

        # random probability maps
        sar_probamap_arr = [
            np.array([[253, 874, 600], [947, 812, 941], [580, 94, 192]]),
            np.array([[541, 711, 326], [273, 915, 698], [296, 1000, 624]]),
            np.array([[253, 290, 610], [406, 685, 333], [302, 410, 515]]),
            np.array([[216, 766, 98], [914, 288, 504], [70, 631, 161]]),
            np.array([[371, 873, 134], [477, 701, 765], [549, 301, 847]]),
            np.array([[870, 201, 85], [555, 644, 802], [98, 807, 77]])
        ]
        opt_probamap_arr = [
            np.array([[268, 528, 131], [514, 299, 252], [725, 427, 731]]),
            np.array([[119, 241, 543], [974, 629, 626], [3, 37, 819]]),
            np.array([[409, 534, 710], [916, 43, 993], [207, 68, 282]]),
            np.array([[820, 169, 423], [710, 626, 525], [377, 777, 461]]),
            np.array([[475, 116, 395], [838, 297, 262], [650, 828, 595]]),
            np.array([[940, 261, 20], [339, 934, 278], [444, 326, 219]])
        ]
        # to rasters
        sar_probamap_path = str(i2_tmpdir /
                                "PROBAMAP_T31TCJ_model_1_seed_0_SAR.tif")
        array_to_raster(sar_probamap_arr, sar_probamap_path)
        opt_probamap_path = str(i2_tmpdir /
                                "PROBAMAP_T31TCJ_model_1_seed_0.tif")
        array_to_raster(opt_probamap_arr, opt_probamap_path)

        # Launch function
        ds_fus_confidence_test = fusion.compute_probamap_fusion(
            fusion_dic, ds_choice, self.classif_model_pos,
            self.classif_tile_pos, self.classif_seed_pos, self.ds_choice_both,
            self.ds_choice_sar, self.ds_choice_opt, self.ds_no_choice,
            workingDirectory)
        # asserts

        # length assert
        assert len(sar_probamap_arr) == len(
            opt_probamap_arr) == get_raster_n_bands(ds_fus_confidence_test)

        # fusion assert
        is_ok = []
        ds_fus_confidence_test_arr = raster_to_array(ds_fus_confidence_test)
        for band_num in range(len(ds_fus_confidence_test_arr)):
            merged_band = ds_fus_confidence_test_arr[band_num]
            sar_proba_band = sar_probamap_arr[band_num]
            opt_proba_band = opt_probamap_arr[band_num]
            for (merged_proba, sar_proba, opt_proba, choice, sar_confi,
                 opt_confi) in zip(merged_band.flat, sar_proba_band.flat,
                                   opt_proba_band.flat,
                                   self.choice_map_ref.flat,
                                   self.sar_confidence.flat,
                                   self.optical_confidence.flat):
                if choice == 1:
                    if sar_confi > opt_confi:
                        is_ok.append(int(merged_proba) == int(sar_proba))
                    else:
                        is_ok.append(int(merged_proba) == int(opt_proba))
                elif choice == 2:
                    is_ok.append(int(merged_proba) == int(sar_proba))
                elif choice == 3:
                    is_ok.append(int(merged_proba) == int(opt_proba))
        assert all(is_ok)

    def test_ds_fusion(self, i2_tmpdir, rm_tmpdir_on_success):
        """
        TEST : fusion.dempster_shafer_fusion. Every functions called in fusion.dempster_shafer_fusion
               are tested above. This test check the runnability of fusion.dempster_shafer_fusion
        """

        # define inputs
        iota2_dir = str(i2_tmpdir / "fusionTest")
        iota2_directory.generate_directories(
            iota2_dir, False,
            ["D0004H0004", "D0005H0004", "D0004H0003", "D0005H0003"])

        sar_raster = str(
            Path(iota2_dir) / "classif" /
            "Classif_T31TCJ_model_1_seed_0_SAR.tif")
        opt_raster = str(
            Path(iota2_dir) / "classif" / "Classif_T31TCJ_model_1_seed_0.tif")
        array_to_raster(self.sar_classif, sar_raster)
        array_to_raster(self.optical_classif, opt_raster)

        sar_confid_raster = str(
            Path(iota2_dir) / "classif" /
            "T31TCJ_model_1_confidence_seed_0_SAR.tif")
        opt_confid_raster = str(
            Path(iota2_dir) / "classif" /
            "T31TCJ_model_1_confidence_seed_0.tif")
        array_to_raster(self.sar_confidence,
                        sar_confid_raster,
                        output_format="float")
        array_to_raster(self.optical_confidence,
                        opt_confid_raster,
                        output_format="float")

        fusion_dic = {
            "sar_classif": sar_raster,
            "opt_classif": opt_raster,
            "sar_model": self.sar_confusion,
            "opt_model": self.opt_confusion
        }
        workingDirectory = None
        # Launch function
        (fusion_path, confidence_path, probamap_path,
         choice_path) = fusion.dempster_shafer_fusion(
             iota2_dir,
             fusion_dic,
             mob="precision",
             working_directory=workingDirectory)
        assert "/classif/Classif_T31TCJ_model_1_seed_0_DS.tif" == fusion_path.replace(
            iota2_dir, "")
        assert "/classif/T31TCJ_model_1_confidence_seed_0_DS.tif" == confidence_path.replace(
            iota2_dir, "")
        assert "/final/TMP/DSchoice_T31TCJ_model_1_seed_0.tif" == choice_path.replace(
            iota2_dir, "")
        assert probamap_path is None
