#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsReadConfigFile

import os
import shutil
from pathlib import Path

import geopandas as gpd
import pandas as pd
import pytest

import iota2.common.i2_constants as i2_const
from iota2.common import iota2_directory
from iota2.segmentation import prepare_segmentation_obia as pso
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.vector_tools import vector_functions as vf

IOTA2DIR = os.environ.get("IOTA2DIR")

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")


@pytest.mark.obia
class Iota2TestObia(AssertsFilesUtils):
    """Check OBIA workflow."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.tile = "T31TCJ"
        cls.common_mask = os.path.join(IOTA2DIR, "data", "OBIA", "MaskCommunSL.shp")
        cls.region_file = os.path.join(
            IOTA2DIR, "data", "references", "running_iota2", "region.shp"
        )
        cls.i2_const = i2_const.Iota2Constants()
        cls.nomenclature_file = os.path.join(
            IOTA2DIR, "data", "references", "running_iota2", "nomenclature.txt"
        )

    def test_prepare_initial_raster_segmentation(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test prepare_initial_segmentation function (from tif)."""
        iota2_directory.generate_directories_obia(str(i2_tmpdir), False, ["T31TCJ"])
        common_mask = os.path.join(
            str(i2_tmpdir), "features", self.tile, "tmp", "MaskCommunSL.shp"
        )
        vf.cp_shape_file(
            self.common_mask.replace(".shp", ""),
            common_mask.replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx", ".tif"],
        )
        # raster slic
        pso.prepare_initial_segmentation(
            in_seg=os.path.join(IOTA2DIR, "data", "OBIA", "SLIC_T31TCJ.tif"),
            tile=self.tile,
            iota2_directory=str(i2_tmpdir),
            region_path=None,
            region_field="region",
            seg_field="DN",
            is_slic=False,
            working_directory=None,
        )
        expected_file = Path(i2_tmpdir / "segmentation" / "tmp", "seg_T31TCJ.shp")
        self.assert_file_exist(expected_file)

    def test_prepare_initial_vector_segmentation(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test prepare_initial_segmentation function (from shp)."""
        iota2_directory.generate_directories_obia(str(i2_tmpdir), False, ["T31TCJ"])
        common_mask = os.path.join(
            i2_tmpdir, "features", self.tile, "tmp", "MaskCommunSL.shp"
        )
        vf.cp_shape_file(
            self.common_mask.replace(".shp", ""),
            common_mask.replace(".shp", ""),
            [".prj", ".shp", ".dbf", ".shx"],
        )
        # raster slic
        pso.prepare_initial_segmentation(
            in_seg=os.path.join(IOTA2DIR, "data", "OBIA", "seg_T31TCJ.shp"),
            tile=self.tile,
            iota2_directory=i2_tmpdir,
            region_path=None,
            region_field="region",
            seg_field="DN",
            is_slic=False,
            working_directory=None,
        )
        expected_file = Path(i2_tmpdir / "segmentation" / "tmp", "seg_T31TCJ.shp")
        self.assert_file_exist(expected_file)

    def test_compute_intersection_segmentation_regions(
        self, i2_tmpdir, rm_tmpdir_on_success
    ):
        """Test: case N regions with priority or not."""
        in_seg = os.path.join(IOTA2DIR, "data", "OBIA", "seg_T31TCJ.shp")
        out_seg = i2_tmpdir / "out_multi_regions.shp"
        pso.compute_intersection_with_regions(
            in_seg=in_seg,
            seg_field=self.i2_const.i2_segmentation_field_name,
            out_seg=str(out_seg),
            region_path=self.region_file,
            region_priority=None,
            region_field="region",
        )
        self.assert_file_exist(out_seg)

        out_seg_prio = os.path.join(i2_tmpdir, "out_multi_regions_prio.shp")
        pso.compute_intersection_with_regions(
            in_seg=in_seg,
            seg_field=self.i2_const.i2_segmentation_field_name,
            out_seg=str(out_seg_prio),
            region_path=self.region_file,
            region_priority=["2", "1"],
            region_field="region",
        )
        self.assert_file_exist(out_seg)

        df_seg = gpd.GeoDataFrame().from_file(out_seg)
        df_prio = gpd.GeoDataFrame().from_file(out_seg_prio)

        assert df_seg["region"].iloc[1] == "1"
        assert df_prio["region"].iloc[1] == "2"

    def test_generate_grid(self):
        """Test generate_grid function."""
        df_seg = gpd.GeoDataFrame().from_file(self.common_mask)
        bsize = 10 * 2  # 10m for resolution and 20 the buffer size given by user
        grid = pso.generate_grid(bsize, df_seg)

        assert grid.shape[0] == 344
        assert grid.shape[1] == 2

    def test_compute_intersection_between_segmentation_and_app_samples(
        self, i2_tmpdir, rm_tmpdir_on_success
    ):
        """Test find_intersections_between_samples_and_segments function."""
        samples_file = os.path.join(
            IOTA2DIR, "data", "OBIA", "learn_samples_input", "T31TCJ_seed_0_learn.shp"
        )
        segmentation_file = os.path.join(
            IOTA2DIR, "data", "OBIA", "learn_samples_input", "seg_T31TCJ.shp"
        )
        work_dir = str(i2_tmpdir)
        tile = "T31TCJ"
        seed = 0
        out_name_clip = os.path.join(
            i2_tmpdir, f"learning_samples_{tile}_seed_{seed}_clipped.shp"
        )
        pso.find_intersections_between_samples_and_segments(
            samples_file=samples_file,
            segmentation_file=segmentation_file,
            work_dir=work_dir,
            region_field="region",
            tile="T31TCJ",
            seg_field=self.i2_const.i2_segmentation_field_name,
            full_segment=False,
            spatial_resolution=[10, 10],
            out_name=out_name_clip,
        )
        gdf = gpd.GeoDataFrame().from_file(out_name_clip)
        areas_expected = [
            314.8870881656894,
            593.6295769925853,
            302.7435571044915,
            1235.5472026438456,
            1519.014619743766,
            762.7119981993949,
        ]

        for area, area_ex in zip(gdf["area"], areas_expected):
            assert area == area_ex

        out_name_not_clip = os.path.join(
            i2_tmpdir, f"learning_samples_{tile}_seed_{seed}_not_clipped.shp"
        )
        pso.find_intersections_between_samples_and_segments(
            samples_file=samples_file,
            segmentation_file=segmentation_file,
            work_dir=work_dir,
            region_field="region",
            tile="T31TCJ",
            seg_field=self.i2_const.i2_segmentation_field_name,
            full_segment=True,
            spatial_resolution=[10, 10],
            out_name=out_name_not_clip,
        )
        df_seg = gpd.GeoDataFrame().from_file(segmentation_file)
        df_out = gpd.GeoDataFrame().from_file(out_name_not_clip)

        df_merge = pd.merge(df_seg, df_out, on="i2seg")
        geom1 = df_merge["geometry_x"]
        geom2 = df_merge["geometry_y"]
        for g1, g2 in zip(geom1, geom2):
            assert g1 == g2

    def test_compute_confusion_matrix_from_vector(
        self, i2_tmpdir, rm_tmpdir_on_success
    ):
        """Check if the matrix is well writen."""
        classif_shape = os.path.join(
            IOTA2DIR,
            "data",
            "OBIA",
            "validation_data",
            "Vectorized_map_tile_T31TCJ_seed_0_clipped.shp",
        )
        val_shape = os.path.join(
            IOTA2DIR, "data", "OBIA", "validation_data", "T31TCJ_seed_0_val.shp"
        )
        pso.extract_matrix_from_vector(
            classif_shape=classif_shape,
            val_shape=val_shape,
            tile=self.tile,
            output_dir=str(i2_tmpdir),
            seed=0,
            ref_label_name=self.i2_const.re_encoding_label_name,
            pred_label_name="predicted",
            seg_field=self.i2_const.i2_segmentation_field_name,
        )
        out_file = i2_tmpdir / "T31TCJ_confusion_matrix_seed_0.csv"
        df_mat = pd.read_csv(str(out_file))

        self.assert_file_exist(out_file)
        assert df_mat.shape[0] == 2
        assert df_mat.shape[1] == 2
        assert df_mat.sum(0)[1] == 10

    def test_cumulate_confusion_matrix(self, i2_tmpdir, rm_tmpdir_on_success):
        """Check if cumulate is well writen."""
        out_directory = str(i2_tmpdir)
        shutil.copy(
            os.path.join(
                IOTA2DIR,
                "data",
                "OBIA",
                "validation_data",
                "confusion_matrix_seed_0.csv",
            ),
            str(i2_tmpdir),
        )
        pso.cumulate_confusion_matrix(
            out_directory=out_directory,
            seed=0,
            data_field=self.i2_const.re_encoding_label_name,
            nomenclature_file=self.nomenclature_file,
            label_vector_table={1: 1, 2: 2, 3: 3},
            tmp_dir=out_directory,
        )

        self.assert_file_exist(i2_tmpdir / "RESULTS.txt")
        self.assert_file_exist(i2_tmpdir / "Confusion_Matrix_Classif_Seed_0.png")
        self.assert_file_exist(i2_tmpdir / "Classif_Seed_0.csv")

    def test_intersect_keep_duplicates(self, i2_tmpdir, rm_tmpdir_on_success):
        """Check that the output keeps duplicates."""
        classif_shape = os.path.join(
            IOTA2DIR,
            "data",
            "OBIA",
            "validation_data",
            "Vectorized_map_tile_T31TCJ_seed_0_clipped.shp",
        )
        val_shape = os.path.join(
            IOTA2DIR, "data", "OBIA", "validation_data", "T31TCJ_seed_0_val.shp"
        )
        output = i2_tmpdir / "intersection.shp"
        pso.intersect_keep_duplicates(
            data_base1=classif_shape,
            data_base2=val_shape,
            output=str(output),
            epsg=2154,
            keepfields=["i2label", "originfid", "i2seg", "predicted"],
        )
        gdf = gpd.GeoDataFrame().from_file(output)
        assert gdf.shape[0] == 14
        df1 = gdf[gdf["originfid"] == 4]
        assert df1.shape[0] == 4

    def test_split_classify_tiles(self, i2_tmpdir, rm_tmpdir_on_success):
        """Check if the dictionnary is well writen."""
        in_seg = os.path.join(
            IOTA2DIR, "data", "OBIA", "learn_samples_input", "seg_T31TCJ.shp"
        )
        seg_field = self.i2_const.i2_segmentation_field_name

        dico = pso.do_intersect_with_grid(
            in_seg=in_seg,
            seg_field=seg_field,
            tile=self.tile,
            prefix="",
            work_dir=i2_tmpdir,
            buffer_size=2,
            spatial_res=10,
            out_folder=i2_tmpdir,
            region_field="region",
            keepfields=["DN", "region", seg_field, "gridid"],
        )

        expected_intersection = [
            "1",
            "4",
            "22",
            "25",
            "65",
            "77",
            "99",
            "110",
            "146",
            "157",
            "191",
            "193",
            "233",
            "238",
            "250",
            "270",
            "299",
            "317",
        ]

        intersections = [shape.split("_")[-1].split(".")[0] for shape in dico[1]]
        assert intersections == expected_intersection
        assert len(dico[1]) == 18

    def test_convert_xml(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test convert_xml_to_shape function."""
        xml_files = [
            os.path.join(
                IOTA2DIR,
                "data",
                "OBIA",
                "zonal_stats_outputs",
                "Sentinel2_T31TCJ_samples_seed_0_region_1_part_1_stats.xml",
            )
        ]
        labels = [
            [
                "Sentinel2_B2_20200101",
                "Sentinel2_B3_20200101",
                "Sentinel2_B4_20200101",
                "Sentinel2_B5_20200101",
                "Sentinel2_B6_20200101",
                "Sentinel2_B7_20200101",
                "Sentinel2_B8_20200101",
                "Sentinel2_B8A_20200101",
                "Sentinel2_B11_20200101",
                "Sentinel2_B12_20200101",
                "Sentinel2_B2_20200111",
                "Sentinel2_B3_20200111",
                "Sentinel2_B4_20200111",
                "Sentinel2_B5_20200111",
                "Sentinel2_B6_20200111",
                "Sentinel2_B7_20200111",
                "Sentinel2_B8_20200111",
                "Sentinel2_B8A_20200111",
                "Sentinel2_B11_20200111",
                "Sentinel2_B12_20200111",
                "Sentinel2_B2_20200121",
                "Sentinel2_B3_20200121",
                "Sentinel2_B4_20200121",
                "Sentinel2_B5_20200121",
                "Sentinel2_B6_20200121",
                "Sentinel2_B7_20200121",
                "Sentinel2_B8_20200121",
                "Sentinel2_B8A_20200121",
                "Sentinel2_B11_20200121",
                "Sentinel2_B12_20200121",
                "Sentinel2_NDVI_20200101",
                "Sentinel2_NDVI_20200111",
                "Sentinel2_NDVI_20200121",
                "Sentinel2_NDWI_20200101",
                "Sentinel2_NDWI_20200111",
                "Sentinel2_NDWI_20200121",
                "Sentinel2_Brightness_20200101",
                "Sentinel2_Brightness_20200111",
                "Sentinel2_Brightness_20200121",
            ]
        ]
        shape = os.path.join(
            IOTA2DIR,
            "data",
            "OBIA",
            "zonal_stats_outputs",
            "seg_T31TCJ_region_1_grid_1.shp",
        )
        seed = 1
        seg_field = self.i2_const.i2_segmentation_field_name
        stats = ["mean"]
        pso.convert_xml_to_shape(
            out_directory=str(i2_tmpdir),
            xml_files=xml_files,
            labels=labels,
            shape_file=shape,
            seed=seed,
            merge_field=seg_field,
            stat_used=stats,
        )
        out_file = i2_tmpdir / "seg_T31TCJ_region_1_grid_1_seed_1.shp"
        expected_colums = [
            "DN",
            "region",
            "i2seg",
            "gridid",
            "S0mean0",
            "S0mean1",
            "S0mean2",
            "S0mean3",
            "S0mean4",
            "S0mean5",
            "S0mean6",
            "S0mean7",
            "S0mean8",
            "S0mean9",
            "S0mean10",
            "S0mean11",
            "S0mean12",
            "S0mean13",
            "S0mean14",
            "S0mean15",
            "S0mean16",
            "S0mean17",
            "S0mean18",
            "S0mean19",
            "S0mean20",
            "S0mean21",
            "S0mean22",
            "S0mean23",
            "S0mean24",
            "S0mean25",
            "S0mean26",
            "S0mean27",
            "S0mean28",
            "S0mean29",
            "S0mean30",
            "S0mean31",
            "S0mean32",
            "S0mean33",
            "S0mean34",
            "S0mean35",
            "S0mean36",
            "S0mean37",
            "S0mean38",
            "geometry",
        ]
        # test if the file is writed
        self.assert_file_exist(out_file)
        # test if columns are goodly named
        gdf = gpd.GeoDataFrame().from_file(out_file)
        columns = gdf.columns
        for cl1, cl2 in zip(columns, expected_colums):
            assert cl1 == cl2
        # Return False if no nan values in gdf
        assert not gdf.isnull().values.any()
        # ensure the file have the good shape
        assert gdf.shape[0] == 10
        assert gdf.shape[1] == 44
