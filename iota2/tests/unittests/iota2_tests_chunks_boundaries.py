# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Test Split raster"""

from iota2.common import raster_utils as ru


class Iota2TestChunksBoundaries:
    """Test chunks boundaries function"""

    @staticmethod
    def assert_boundary(boundary, startx, sizex, starty, sizey):
        assert boundary["startx"] == startx
        assert boundary["sizex"] == sizex
        assert boundary["starty"] == starty
        assert boundary["sizey"] == sizey

    @staticmethod
    def assert_padding(padding, startx, endx, starty, endy):
        assert padding["startx"] == startx
        assert padding["endx"] == endx
        assert padding["starty"] == starty
        assert padding["endy"] == endy

    @staticmethod
    def expected_padding(chunk, shape, boundary):
        return {
            "startx": chunk.padding_size_x if boundary["startx"] > 0 else 0,
            "endx": chunk.padding_size_x
            if boundary["startx"] + boundary["sizex"] < shape[0]
            else 0,
            "starty": chunk.padding_size_y if boundary["starty"] > 0 else 0,
            "endy": chunk.padding_size_y
            if boundary["starty"] + boundary["sizey"] < shape[1]
            else 0,
        }

    def test_chunks_boundaries_fixed_mode(self):
        """"""
        shape = (100, 100)
        chunk_size_mode = "user_fixed"
        number_of_chunks = None

        # No padding
        chunk = ru.ChunkConfig(
            chunk_size_mode=chunk_size_mode,
            number_of_chunks=number_of_chunks,
            chunk_size_x=65,
            chunk_size_y=80,
        )
        boundaries, paddings = ru.get_chunks_boundaries(shape, chunk)

        self.assert_boundary(boundaries[0], 0, 65, 0, 80)
        self.assert_boundary(boundaries[1], 65, 35, 0, 80)
        self.assert_boundary(boundaries[2], 0, 65, 80, 20)
        self.assert_boundary(boundaries[3], 65, 35, 80, 20)
        for padding in paddings:
            self.assert_padding(padding, 0, 0, 0, 0)

        # With padding
        chunk = ru.ChunkConfig(
            chunk_size_mode=chunk_size_mode,
            number_of_chunks=number_of_chunks,
            chunk_size_x=50,
            chunk_size_y=35,
            padding_size_x=3,
            padding_size_y=5,
        )
        boundaries, paddings = ru.get_chunks_boundaries(shape, chunk)

        for boundary, padding in zip(boundaries, paddings):
            expected_padding = self.expected_padding(chunk, shape, boundary)
            self.assert_padding(
                padding,
                expected_padding["startx"],
                expected_padding["endx"],
                expected_padding["starty"],
                expected_padding["endy"],
            )

    def test_chunks_boundaries_number_mode(self):
        """"""
        shape = (100, 100)
        chunk_size_mode = "split_number"
        chunk_size_x = None
        chunk_size_y = None

        # No padding
        number_of_chunks = 6
        chunk = ru.ChunkConfig(
            chunk_size_mode=chunk_size_mode,
            number_of_chunks=number_of_chunks,
            chunk_size_x=chunk_size_x,
            chunk_size_y=chunk_size_y,
        )
        boundaries, paddings = ru.get_chunks_boundaries(shape, chunk)

        self.assert_boundary(boundaries[0], 0, 100, 0, 16)
        self.assert_boundary(boundaries[1], 0, 100, 16, 17)
        self.assert_boundary(boundaries[2], 0, 100, 33, 17)
        self.assert_boundary(boundaries[3], 0, 100, 50, 16)
        self.assert_boundary(boundaries[4], 0, 100, 66, 17)
        self.assert_boundary(boundaries[5], 0, 100, 83, 17)
        for padding in paddings:
            self.assert_padding(padding, 0, 0, 0, 0)

        # With padding
        number_of_chunks = 7
        chunk = ru.ChunkConfig(
            chunk_size_mode=chunk_size_mode,
            number_of_chunks=number_of_chunks,
            chunk_size_x=chunk_size_x,
            chunk_size_y=chunk_size_y,
            padding_size_x=5,
            padding_size_y=2,
        )
        boundaries, paddings = ru.get_chunks_boundaries(shape, chunk)

        start_y = 0
        for boundary, padding in zip(boundaries, paddings):
            assert boundary["startx"] == 0
            assert boundary["sizex"] == shape[0]
            assert boundary["starty"] == start_y
            start_y += boundary["sizey"] - 2 * chunk.padding_size_y

            expected_padding = self.expected_padding(chunk, shape, boundary)
            self.assert_padding(
                padding,
                expected_padding["startx"],
                expected_padding["endx"],
                expected_padding["starty"],
                expected_padding["endy"],
            )

        assert boundaries[-1]["starty"] + boundaries[-1]["sizey"] == shape[1]
