#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
from pathlib import Path

import otbApplication as otb

from iota2.common.file_utils import ensure_dir
from iota2.sampling import dimensionality_reduction as DR
from iota2.tests.utils import tests_utils_rasters as TUR
from iota2.tests.utils import tests_utils_vectors as TUV
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


class Iota2TestDimensionalityReduction(AssertsFilesUtils):
    """Class dedicated to launch several sampler tests"""

    @classmethod
    def setup_class(cls):
        cls.input_sample_file_name = str(IOTA2DIR / "data" /
                                         "dim_red_samples.sqlite")

        cls.target_dimension = 6
        cls.fl_date = [
            'landsat8_b1_20140118', 'landsat8_b2_20140118',
            'landsat8_b3_20140118', 'landsat8_b4_20140118',
            'landsat8_b5_20140118', 'landsat8_b6_20140118',
            'landsat8_b7_20140118', 'landsat8_ndvi_20140118',
            'landsat8_ndwi_20140118', 'landsat8_brightness_20140118'
        ]
        cls.stats_file = str(IOTA2DIR / "data" / 'dim_red_stats.xml')
        cls.output_model_file_name = str(IOTA2DIR / "data" / 'model.pca')
        cls.test_output_model_file_name = str(IOTA2DIR / "data" / 'tmp' /
                                              'model.pca')
        cls.reduced_output_file_name = str(IOTA2DIR / "data" /
                                           'reduced.sqlite')
        cls.joint_reduced_file = str(IOTA2DIR / "data" / 'joint.sqlite')
        cls.output_sample_file_name = 'reduced_output_samples.sqlite'
        cls.output_path = str(IOTA2DIR / "data" / "tmp")
        cls.field_list = [
            'id', 'lc', 'code', 'area_ha', 'originfid', 'landsat8_b1_20140118',
            'landsat8_b2_20140118', 'landsat8_b3_20140118',
            'landsat8_b4_20140118', 'landsat8_b5_20140118',
            'landsat8_b6_20140118', 'landsat8_b7_20140118',
            'landsat8_b1_20140203', 'landsat8_b2_20140203',
            'landsat8_b3_20140203', 'landsat8_b4_20140203',
            'landsat8_b5_20140203', 'landsat8_b6_20140203',
            'landsat8_b7_20140203', 'landsat8_b1_20140219',
            'landsat8_b2_20140219', 'landsat8_b3_20140219',
            'landsat8_b4_20140219', 'landsat8_b5_20140219',
            'landsat8_b6_20140219', 'landsat8_b7_20140219',
            'landsat8_b1_20140307', 'landsat8_b2_20140307',
            'landsat8_b3_20140307', 'landsat8_b4_20140307',
            'landsat8_b5_20140307', 'landsat8_b6_20140307',
            'landsat8_b7_20140307', 'landsat8_b1_20140323',
            'landsat8_b2_20140323', 'landsat8_b3_20140323',
            'landsat8_b4_20140323', 'landsat8_b5_20140323',
            'landsat8_b6_20140323', 'landsat8_b7_20140323',
            'landsat8_b1_20140408', 'landsat8_b2_20140408',
            'landsat8_b3_20140408', 'landsat8_b4_20140408',
            'landsat8_b5_20140408', 'landsat8_b6_20140408',
            'landsat8_b7_20140408', 'landsat8_b1_20140424',
            'landsat8_b2_20140424', 'landsat8_b3_20140424',
            'landsat8_b4_20140424', 'landsat8_b5_20140424',
            'landsat8_b6_20140424', 'landsat8_b7_20140424',
            'landsat8_b1_20140510', 'landsat8_b2_20140510',
            'landsat8_b3_20140510', 'landsat8_b4_20140510',
            'landsat8_b5_20140510', 'landsat8_b6_20140510',
            'landsat8_b7_20140510', 'landsat8_b1_20140526',
            'landsat8_b2_20140526', 'landsat8_b3_20140526',
            'landsat8_b4_20140526', 'landsat8_b5_20140526',
            'landsat8_b6_20140526', 'landsat8_b7_20140526',
            'landsat8_b1_20140611', 'landsat8_b2_20140611',
            'landsat8_b3_20140611', 'landsat8_b4_20140611',
            'landsat8_b5_20140611', 'landsat8_b6_20140611',
            'landsat8_b7_20140611', 'landsat8_b1_20140627',
            'landsat8_b2_20140627', 'landsat8_b3_20140627',
            'landsat8_b4_20140627', 'landsat8_b5_20140627',
            'landsat8_b6_20140627', 'landsat8_b7_20140627',
            'landsat8_b1_20140713', 'landsat8_b2_20140713',
            'landsat8_b3_20140713', 'landsat8_b4_20140713',
            'landsat8_b5_20140713', 'landsat8_b6_20140713',
            'landsat8_b7_20140713', 'landsat8_b1_20140729',
            'landsat8_b2_20140729', 'landsat8_b3_20140729',
            'landsat8_b4_20140729', 'landsat8_b5_20140729',
            'landsat8_b6_20140729', 'landsat8_b7_20140729',
            'landsat8_b1_20140814', 'landsat8_b2_20140814',
            'landsat8_b3_20140814', 'landsat8_b4_20140814',
            'landsat8_b5_20140814', 'landsat8_b6_20140814',
            'landsat8_b7_20140814', 'landsat8_b1_20140830',
            'landsat8_b2_20140830', 'landsat8_b3_20140830',
            'landsat8_b4_20140830', 'landsat8_b5_20140830',
            'landsat8_b6_20140830', 'landsat8_b7_20140830',
            'landsat8_b1_20140915', 'landsat8_b2_20140915',
            'landsat8_b3_20140915', 'landsat8_b4_20140915',
            'landsat8_b5_20140915', 'landsat8_b6_20140915',
            'landsat8_b7_20140915', 'landsat8_b1_20141001',
            'landsat8_b2_20141001', 'landsat8_b3_20141001',
            'landsat8_b4_20141001', 'landsat8_b5_20141001',
            'landsat8_b6_20141001', 'landsat8_b7_20141001',
            'landsat8_b1_20141017', 'landsat8_b2_20141017',
            'landsat8_b3_20141017', 'landsat8_b4_20141017',
            'landsat8_b5_20141017', 'landsat8_b6_20141017',
            'landsat8_b7_20141017', 'landsat8_b1_20141102',
            'landsat8_b2_20141102', 'landsat8_b3_20141102',
            'landsat8_b4_20141102', 'landsat8_b5_20141102',
            'landsat8_b6_20141102', 'landsat8_b7_20141102',
            'landsat8_b1_20141118', 'landsat8_b2_20141118',
            'landsat8_b3_20141118', 'landsat8_b4_20141118',
            'landsat8_b5_20141118', 'landsat8_b6_20141118',
            'landsat8_b7_20141118', 'landsat8_b1_20141204',
            'landsat8_b2_20141204', 'landsat8_b3_20141204',
            'landsat8_b4_20141204', 'landsat8_b5_20141204',
            'landsat8_b6_20141204', 'landsat8_b7_20141204',
            'landsat8_b1_20141220', 'landsat8_b2_20141220',
            'landsat8_b3_20141220', 'landsat8_b4_20141220',
            'landsat8_b5_20141220', 'landsat8_b6_20141220',
            'landsat8_b7_20141220', 'landsat8_b1_20141229',
            'landsat8_b2_20141229', 'landsat8_b3_20141229',
            'landsat8_b4_20141229', 'landsat8_b5_20141229',
            'landsat8_b6_20141229', 'landsat8_b7_20141229',
            'landsat8_ndvi_20140118', 'landsat8_ndvi_20140203',
            'landsat8_ndvi_20140219', 'landsat8_ndvi_20140307',
            'landsat8_ndvi_20140323', 'landsat8_ndvi_20140408',
            'landsat8_ndvi_20140424', 'landsat8_ndvi_20140510',
            'landsat8_ndvi_20140526', 'landsat8_ndvi_20140611',
            'landsat8_ndvi_20140627', 'landsat8_ndvi_20140713',
            'landsat8_ndvi_20140729', 'landsat8_ndvi_20140814',
            'landsat8_ndvi_20140830', 'landsat8_ndvi_20140915',
            'landsat8_ndvi_20141001', 'landsat8_ndvi_20141017',
            'landsat8_ndvi_20141102', 'landsat8_ndvi_20141118',
            'landsat8_ndvi_20141204', 'landsat8_ndvi_20141220',
            'landsat8_ndvi_20141229', 'landsat8_ndwi_20140118',
            'landsat8_ndwi_20140203', 'landsat8_ndwi_20140219',
            'landsat8_ndwi_20140307', 'landsat8_ndwi_20140323',
            'landsat8_ndwi_20140408', 'landsat8_ndwi_20140424',
            'landsat8_ndwi_20140510', 'landsat8_ndwi_20140526',
            'landsat8_ndwi_20140611', 'landsat8_ndwi_20140627',
            'landsat8_ndwi_20140713', 'landsat8_ndwi_20140729',
            'landsat8_ndwi_20140814', 'landsat8_ndwi_20140830',
            'landsat8_ndwi_20140915', 'landsat8_ndwi_20141001',
            'landsat8_ndwi_20141017', 'landsat8_ndwi_20141102',
            'landsat8_ndwi_20141118', 'landsat8_ndwi_20141204',
            'landsat8_ndwi_20141220', 'landsat8_ndwi_20141229',
            'landsat8_brightness_20140118', 'landsat8_brightness_20140203',
            'landsat8_brightness_20140219', 'landsat8_brightness_20140307',
            'landsat8_brightness_20140323', 'landsat8_brightness_20140408',
            'landsat8_brightness_20140424', 'landsat8_brightness_20140510',
            'landsat8_brightness_20140526', 'landsat8_brightness_20140611',
            'landsat8_brightness_20140627', 'landsat8_brightness_20140713',
            'landsat8_brightness_20140729', 'landsat8_brightness_20140814',
            'landsat8_brightness_20140830', 'landsat8_brightness_20140915',
            'landsat8_brightness_20141001', 'landsat8_brightness_20141017',
            'landsat8_brightness_20141102', 'landsat8_brightness_20141118',
            'landsat8_brightness_20141204', 'landsat8_brightness_20141220',
            'landsat8_brightness_20141229'
        ]

    def test_get_available_features(self):
        """Test get_available_features function."""
        expected = '20140118'
        (feats, _) = DR.get_available_features(self.input_sample_file_name)
        assert feats['landsat8']['brightness'][0] == expected

        expected = 'b1'
        (feats, _) = DR.get_available_features(self.input_sample_file_name,
                                               'date', 'sensor')
        assert feats['20141017']['landsat8'][0] == expected

        expected = 'landsat8'
        (feats, _) = DR.get_available_features(self.input_sample_file_name,
                                               'date', 'band')
        assert feats['20141118']['b2'][0] == expected

    def test_generate_feature_list_global(self):
        """Test build_features_lists function, 'global' mode."""
        expected = [[
            'landsat8_b1_20140118', 'landsat8_b2_20140118',
            'landsat8_b3_20140118', 'landsat8_b4_20140118',
            'landsat8_b5_20140118', 'landsat8_b6_20140118',
            'landsat8_b7_20140118', 'landsat8_b1_20140203',
            'landsat8_b2_20140203', 'landsat8_b3_20140203',
            'landsat8_b4_20140203', 'landsat8_b5_20140203',
            'landsat8_b6_20140203', 'landsat8_b7_20140203',
            'landsat8_b1_20140219', 'landsat8_b2_20140219',
            'landsat8_b3_20140219', 'landsat8_b4_20140219',
            'landsat8_b5_20140219', 'landsat8_b6_20140219',
            'landsat8_b7_20140219', 'landsat8_b1_20140307',
            'landsat8_b2_20140307', 'landsat8_b3_20140307',
            'landsat8_b4_20140307', 'landsat8_b5_20140307',
            'landsat8_b6_20140307', 'landsat8_b7_20140307',
            'landsat8_b1_20140323', 'landsat8_b2_20140323',
            'landsat8_b3_20140323', 'landsat8_b4_20140323',
            'landsat8_b5_20140323', 'landsat8_b6_20140323',
            'landsat8_b7_20140323'
        ]]

        (feat_list, _) = DR.build_features_lists(self.input_sample_file_name,
                                                 'global')
        assert expected[0] == feat_list[0][:len(expected[0])]

    def test_generate_feature_list_date(self):
        """Test build_features_lists function, considering dates."""
        (feat_list, _) = DR.build_features_lists(self.input_sample_file_name,
                                                 'date')
        assert self.fl_date == feat_list[0]

    def test_generate_feature_list_band(self):
        """Test build_features_lists function, considering bands."""
        # second spectral band
        expected = [
            'landsat8_b2_20140118', 'landsat8_b2_20140203',
            'landsat8_b2_20140219', 'landsat8_b2_20140307',
            'landsat8_b2_20140323', 'landsat8_b2_20140408',
            'landsat8_b2_20140424', 'landsat8_b2_20140510',
            'landsat8_b2_20140526', 'landsat8_b2_20140611',
            'landsat8_b2_20140627', 'landsat8_b2_20140713',
            'landsat8_b2_20140729', 'landsat8_b2_20140814',
            'landsat8_b2_20140830', 'landsat8_b2_20140915',
            'landsat8_b2_20141001', 'landsat8_b2_20141017',
            'landsat8_b2_20141102', 'landsat8_b2_20141118',
            'landsat8_b2_20141204', 'landsat8_b2_20141220',
            'landsat8_b2_20141229'
        ]
        (feat_list, _) = DR.build_features_lists(self.input_sample_file_name,
                                                 'band')
        assert expected == feat_list[1]

    def test_compute_feature_statistics(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test compute feature statistics."""
        test_stats_file = str(i2_tmpdir / 'stats.xml')
        DR.compute_feature_statistics(self.input_sample_file_name,
                                      test_stats_file, self.fl_date)
        self.assert_file_equal(Path(test_stats_file), Path(self.stats_file))

    def test_train_dimensionality_reduction(self):
        """Test train dimensionality reduction."""
        DR.train_dimensionality_reduction(self.input_sample_file_name,
                                          self.test_output_model_file_name,
                                          self.fl_date, self.target_dimension,
                                          self.stats_file)
        self.assert_file_equal(Path(self.test_output_model_file_name),
                               Path(self.output_model_file_name))

    def test_apply_dimensionality_reduction(self, i2_tmpdir,
                                            rm_tmpdir_on_success):
        """Test apply dimensionality reduction."""
        output_features = ['reduced_' + str(x) for x in range(6)]
        test_reduced_output_file_name = str(i2_tmpdir / "reduced.sqlite")
        DR.apply_dimensionality_reduction(self.input_sample_file_name,
                                          test_reduced_output_file_name,
                                          self.output_model_file_name,
                                          self.fl_date,
                                          output_features,
                                          stats_file=self.stats_file,
                                          writing_mode='overwrite')
        assert TUV.compare_sqlite(
            test_reduced_output_file_name,
            self.reduced_output_file_name,
            cmp_mode="coordinates"), "Joined files don't match"

    def test_join_reduced_sample_files(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test join reduced sample files."""
        feat_list = [
            self.reduced_output_file_name, self.reduced_output_file_name
        ]
        output_features = [f'reduced_{x + 1}' for x in range(5)]
        test_joint_reduced_file = str(i2_tmpdir / 'joint.sqlite')
        DR.join_reduced_sample_files(feat_list, test_joint_reduced_file,
                                     output_features)
        assert TUV.compare_sqlite(
            test_joint_reduced_file,
            self.joint_reduced_file,
            cmp_mode="coordinates"), "Joined files don't match"

    def test_sample_file_pca_reduction(self):
        """Test sample file PCA reduction."""
        test_test_output_sample_file_name = str(
            IOTA2DIR / "data" / "reduced_output_samples_test.sqlite")
        DR.sample_file_pca_reduction(self.input_sample_file_name,
                                     test_test_output_sample_file_name,
                                     'date',
                                     self.target_dimension,
                                     tmp_dir=str(IOTA2DIR / "data" / "tmp"))

        assert TUV.compare_sqlite(
            test_test_output_sample_file_name,
            str(IOTA2DIR / "data" / self.output_sample_file_name),
            cmp_mode="coordinates"), "Output sample files don't match"

    def test_build_channel_groups(self):
        """Test build channel groups."""
        fake_db_dir = str(IOTA2DIR / "data" / "tmp" / "dimRed" /
                          "before_reduction")
        ensure_dir(fake_db_dir)
        fake_db_file = str(Path(fake_db_dir) / "dim_red_samples.sqlite")
        TUV.random_features_database_generator(fake_db_file, self.field_list,
                                               20)
        chan_group = DR.build_channel_groups("sensor_date", self.output_path)
        expected = [[
            'Channel1', 'Channel2', 'Channel3', 'Channel4', 'Channel5',
            'Channel6', 'Channel7', 'Channel162', 'Channel185', 'Channel208'
        ],
                    [
                        'Channel8', 'Channel9', 'Channel10', 'Channel11',
                        'Channel12', 'Channel13', 'Channel14', 'Channel163',
                        'Channel186', 'Channel209'
                    ],
                    [
                        'Channel15', 'Channel16', 'Channel17', 'Channel18',
                        'Channel19', 'Channel20', 'Channel21', 'Channel164',
                        'Channel187', 'Channel210'
                    ],
                    [
                        'Channel22', 'Channel23', 'Channel24', 'Channel25',
                        'Channel26', 'Channel27', 'Channel28', 'Channel165',
                        'Channel188', 'Channel211'
                    ],
                    [
                        'Channel29', 'Channel30', 'Channel31', 'Channel32',
                        'Channel33', 'Channel34', 'Channel35', 'Channel166',
                        'Channel189', 'Channel212'
                    ],
                    [
                        'Channel36', 'Channel37', 'Channel38', 'Channel39',
                        'Channel40', 'Channel41', 'Channel42', 'Channel167',
                        'Channel190', 'Channel213'
                    ],
                    [
                        'Channel43', 'Channel44', 'Channel45', 'Channel46',
                        'Channel47', 'Channel48', 'Channel49', 'Channel168',
                        'Channel191', 'Channel214'
                    ],
                    [
                        'Channel50', 'Channel51', 'Channel52', 'Channel53',
                        'Channel54', 'Channel55', 'Channel56', 'Channel169',
                        'Channel192', 'Channel215'
                    ],
                    [
                        'Channel57', 'Channel58', 'Channel59', 'Channel60',
                        'Channel61', 'Channel62', 'Channel63', 'Channel170',
                        'Channel193', 'Channel216'
                    ],
                    [
                        'Channel64', 'Channel65', 'Channel66', 'Channel67',
                        'Channel68', 'Channel69', 'Channel70', 'Channel171',
                        'Channel194', 'Channel217'
                    ],
                    [
                        'Channel71', 'Channel72', 'Channel73', 'Channel74',
                        'Channel75', 'Channel76', 'Channel77', 'Channel172',
                        'Channel195', 'Channel218'
                    ],
                    [
                        'Channel78', 'Channel79', 'Channel80', 'Channel81',
                        'Channel82', 'Channel83', 'Channel84', 'Channel173',
                        'Channel196', 'Channel219'
                    ],
                    [
                        'Channel85', 'Channel86', 'Channel87', 'Channel88',
                        'Channel89', 'Channel90', 'Channel91', 'Channel174',
                        'Channel197', 'Channel220'
                    ],
                    [
                        'Channel92', 'Channel93', 'Channel94', 'Channel95',
                        'Channel96', 'Channel97', 'Channel98', 'Channel175',
                        'Channel198', 'Channel221'
                    ],
                    [
                        'Channel99', 'Channel100', 'Channel101', 'Channel102',
                        'Channel103', 'Channel104', 'Channel105', 'Channel176',
                        'Channel199', 'Channel222'
                    ],
                    [
                        'Channel106', 'Channel107', 'Channel108', 'Channel109',
                        'Channel110', 'Channel111', 'Channel112', 'Channel177',
                        'Channel200', 'Channel223'
                    ],
                    [
                        'Channel113', 'Channel114', 'Channel115', 'Channel116',
                        'Channel117', 'Channel118', 'Channel119', 'Channel178',
                        'Channel201', 'Channel224'
                    ],
                    [
                        'Channel120', 'Channel121', 'Channel122', 'Channel123',
                        'Channel124', 'Channel125', 'Channel126', 'Channel179',
                        'Channel202', 'Channel225'
                    ],
                    [
                        'Channel127', 'Channel128', 'Channel129', 'Channel130',
                        'Channel131', 'Channel132', 'Channel133', 'Channel180',
                        'Channel203', 'Channel226'
                    ],
                    [
                        'Channel134', 'Channel135', 'Channel136', 'Channel137',
                        'Channel138', 'Channel139', 'Channel140', 'Channel181',
                        'Channel204', 'Channel227'
                    ],
                    [
                        'Channel141', 'Channel142', 'Channel143', 'Channel144',
                        'Channel145', 'Channel146', 'Channel147', 'Channel182',
                        'Channel205', 'Channel228'
                    ],
                    [
                        'Channel148', 'Channel149', 'Channel150', 'Channel151',
                        'Channel152', 'Channel153', 'Channel154', 'Channel183',
                        'Channel206', 'Channel229'
                    ],
                    [
                        'Channel155', 'Channel156', 'Channel157', 'Channel158',
                        'Channel159', 'Channel160', 'Channel161', 'Channel184',
                        'Channel207', 'Channel230'
                    ]]
        assert expected == chan_group

    def test_apply_dimensionality_reduction_to_feature_stack(self):
        """Test Apply Dimensionality Reduction To Feature Stack."""
        # prepare inputs
        number_of_bands = 230
        image_stack = str(IOTA2DIR / "data" / "230feats_test.tif")
        arr_pattern = TUR.fun_array("iota2_binary")
        arr_to_reduce = [
            arr_pattern * cpt for cpt in range(1, number_of_bands + 1)
        ]
        TUR.array_to_raster(arr_to_reduce, image_stack)
        fake_db_dir = str(IOTA2DIR / "data" / "tmp" / "dimRed" /
                          "before_reduction")
        ensure_dir(fake_db_dir)
        fake_db_file = str(Path(fake_db_dir) / "dim_red_samples.sqlite")
        TUV.random_features_database_generator(fake_db_file, self.field_list,
                                               20)
        # check if functions are launchable, without exceptions
        model_list = [self.output_model_file_name] * len(
            DR.build_channel_groups("sensor_date", self.output_path))
        (app, _) = DR.apply_dimensionality_reduction_to_feature_stack(
            "sensor_date", self.output_path, image_stack, model_list)
        app.SetParameterString(
            "out", str(IOTA2DIR / "data" / "tmp" / "reducedStack.tif"))
        app.ExecuteAndWriteOutput()

    def test_apply_dimensionality_reduction_to_feature_stack_pipeline(self):
        """Test Apply Dimensionality Reduction To Feature Stack (to pipeline)."""
        # prepare inputs
        number_of_bands = 230
        image_stack = str(IOTA2DIR / "data" / "230feats_test.tif")
        arr_pattern = TUR.fun_array("iota2_binary")
        arr_to_reduce = [
            arr_pattern * cpt for cpt in range(1, number_of_bands + 1)
        ]
        TUR.array_to_raster(arr_to_reduce, image_stack)
        fake_db_dir = str(IOTA2DIR / "data" / "tmp" / "dimRed" /
                          "before_reduction")
        ensure_dir(fake_db_dir)
        fake_db_file = str(Path(fake_db_dir) / "dim_red_samples.sqlite")
        TUV.random_features_database_generator(fake_db_file, self.field_list,
                                               20)
        # check if functions are launchable, without exceptions
        app = otb.Registry.CreateApplication("ExtractROI")
        app.SetParameterString("in", image_stack)
        app.Execute()
        model_list = [self.output_model_file_name] * len(
            DR.build_channel_groups("sensor_date", self.output_path))
        (appdr, _) = DR.apply_dimensionality_reduction_to_feature_stack(
            "sensor_date", self.output_path, app, model_list)
        appdr.SetParameterString(
            "out", str(IOTA2DIR / "data" / "tmp" / "reducedStackPipeline.tif"))
        appdr.ExecuteAndWriteOutput()
