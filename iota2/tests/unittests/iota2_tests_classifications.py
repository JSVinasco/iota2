# !/usr/bin/python
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
from pathlib import Path

import numpy as np

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.classification.image_classifier import Iota2Classification
from iota2.common.raster_utils import raster_to_array, reorder_proba_map

IOTA2DIR = os.environ.get('IOTA2DIR')

if IOTA2DIR is None:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR)


class Iota2TestClassifications:
    """Test reordering proba map."""

    def test_reorder_proba_map(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST probility map reordering."""
        # prepare inputs
        probamap_arr = [
            np.array([[268, 528, 131], [514, 299, 252], [725, 427, 731]]),
            np.array([[119, 241, 543], [974, 629, 626], [3, 37, 819]]),
            np.array([[409, 534, 710], [916, 43, 993], [207, 68, 282]]),
            np.array([[820, 169, 423], [710, 626, 525], [377, 777, 461]]),
            np.array([[475, 116, 395], [838, 297, 262], [650, 828, 595]]),
            np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
        ]
        probamap_path = str(i2_tmpdir / "PROBAMAP_T31TCJ_model_1_seed_0.tif")
        TUR.array_to_raster(probamap_arr, probamap_path)

        fake_model = "model_1_seed_0.txt"
        fake_tile = "T31TCJ"
        fake_output_directory = "fake_output_directory"
        _ = Iota2Classification(features_stack=None,
                                classifier_type=None,
                                model=fake_model,
                                tile=fake_tile,
                                output_directory=fake_output_directory,
                                models_class=None,
                                all_class=[])
        class_model = [1, 2, 3, 4, 6]
        all_class = [1, 2, 3, 4, 5, 6]
        proba_map_path_out = str(i2_tmpdir /
                                 "PROBAMAP_T31TCJ_model_1_seed_0_ORDERED.tif")
        reorder_proba_map(probamap_path,
                          proba_map_path_out,
                          class_model,
                          all_class,
                          pix_type="uint16")

        # assert
        probamap_arr_ref = [
            np.array([[268, 528, 131], [514, 299, 252], [725, 427, 731]]),
            np.array([[119, 241, 543], [974, 629, 626], [3, 37, 819]]),
            np.array([[409, 534, 710], [916, 43, 993], [207, 68, 282]]),
            np.array([[820, 169, 423], [710, 626, 525], [377, 777, 461]]),
            np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]]),
            np.array([[475, 116, 395], [838, 297, 262], [650, 828, 595]])
        ]
        reordered_test_arr = raster_to_array(proba_map_path_out)
        assert len(all_class) == len(reordered_test_arr)
        is_bands_ok = []
        for band in range(len(reordered_test_arr)):
            band_ref = probamap_arr_ref[band]
            band_test = reordered_test_arr[band]
            for ref_val, test_val in zip(band_ref.flat, band_test.flat):
                is_bands_ok.append(int(ref_val) == int(test_val))
        assert all(is_bands_ok), "reordering probability maps failed"

        # same test as before but the input raster does not contains
        # the last band full of 0

        # prepare inputs
        probamap_arr = [
            np.array([[268, 528, 131], [514, 299, 252], [725, 427, 731]]),
            np.array([[119, 241, 543], [974, 629, 626], [3, 37, 819]]),
            np.array([[409, 534, 710], [916, 43, 993], [207, 68, 282]]),
            np.array([[820, 169, 423], [710, 626, 525], [377, 777, 461]]),
            np.array([[475, 116, 395], [838, 297, 262], [650, 828, 595]])
        ]
        probamap_path = str(i2_tmpdir / "PROBAMAP_T31TCJ_model_1_seed_0.tif")
        TUR.array_to_raster(probamap_arr, probamap_path)

        fake_model = "model_1_seed_0.txt"
        fake_tile = "T31TCJ"
        fake_output_directory = "fake_output_directory"
        _ = Iota2Classification(features_stack=None,
                                classifier_type=None,
                                model=fake_model,
                                tile=fake_tile,
                                output_directory=fake_output_directory,
                                models_class=None,
                                all_class=[])
        class_model = [1, 2, 3, 4, 6]
        all_class = [1, 2, 3, 4, 5, 6]
        proba_map_path_out = str(i2_tmpdir /
                                 "PROBAMAP_T31TCJ_model_1_seed_0_ORDERED.tif")
        reorder_proba_map(probamap_path,
                          proba_map_path_out,
                          class_model,
                          all_class,
                          pix_type="uint16")

        # assert
        probamap_arr_ref = [
            np.array([[268, 528, 131], [514, 299, 252], [725, 427, 731]]),
            np.array([[119, 241, 543], [974, 629, 626], [3, 37, 819]]),
            np.array([[409, 534, 710], [916, 43, 993], [207, 68, 282]]),
            np.array([[820, 169, 423], [710, 626, 525], [377, 777, 461]]),
            np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]]),
            np.array([[475, 116, 395], [838, 297, 262], [650, 828, 595]])
        ]
        reordered_test_arr = raster_to_array(proba_map_path_out)
        assert len(all_class) == len(reordered_test_arr)
        is_bands_ok = []
        for band in range(len(reordered_test_arr)):
            band_ref = probamap_arr_ref[band]
            band_test = reordered_test_arr[band]
            for ref_val, test_val in zip(band_ref.flat, band_test.flat):
                is_bands_ok.append(int(ref_val) == int(test_val))
        assert all(is_bands_ok), "reordering probability maps failed"
