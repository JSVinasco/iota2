"""Test module"""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
from pathlib import Path

import pytest

from iota2.simplification import nomenclature as nom

IOTA2DIR = os.environ.get("IOTA2DIR")

if IOTA2DIR is None:
    raise Exception("IOTA2DIR environment variable must be set")
IOTA2DIR = Path(IOTA2DIR)


@pytest.mark.vecto
class Iota2NomenclatureTest:
    """Test nomenclature instanciation and exportation."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.refnomenc = str(
            IOTA2DIR / "data" / "references" / "posttreat" / "nomenclature.txt"
        )
        cls.refqml = str(IOTA2DIR / "data" / "references" / "posttreat" / "style.qml")
        cls.refqmlout = str(
            IOTA2DIR / "data" / "references" / "posttreat" / "styleraster.qml"
        )

    # Tests definitions
    def test_iota2_nomenclature(
        self, i2_tmpdir, rm_tmpdir_on_success
    ):  # pylint: disable=W0613
        """Test nomenclature instanciation and exportation."""
        qml = str(i2_tmpdir / "style.qml")

        expected_codes = [
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
        ]
        tabnom = nom.get_classes_from_vector_qml(self.refqml)
        nomenc = nom.Iota2Nomenclature(tabnom)
        nomenc.create_raster_qml(qml, "classe", 1)
        assert len(nomenc) == 23
        assert len(open(qml, "r").read()) == len(open(self.refqmlout, "r").read())
        assert nomenc.get_code(1) == expected_codes
