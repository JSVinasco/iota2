"""Unit tests about coregistration."""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os

from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR


class Iota2TestCoRegistration:
    """Unit tests about coregistration."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.config_test = os.path.join(
            IOTA2DIR, "data", "config", "test_config_coregister.cfg"
        )
        cls.datadir = os.path.join(
            IOTA2DIR, "data", "references", "CoRegister", "sensor_data"
        )
        cls.vhr_path = os.path.join(
            IOTA2DIR,
            "data",
            "references",
            "CoRegister",
            "sensor_ref",
            "ref_spot7_5m_20170216.tif",
        )

    def test_launch_coregister(self):
        """
        TEST
        Currently disabled, see:
        https://framagit.org/iota2-project/iota2/-/issues/463
        """
        print("coregistration test is disabled")
        # from config import Config
        # from iota2.common.file_utils import ensure_dir
        # from iota2.configuration_files import read_config_file as rcf
        # test_config = os.path.join(self.test_working_directory,
        #                            os.path.basename(self.config_test))
        # shutil.copy(self.config_test, test_config)

        # # prepare test's inputs
        # datadir_test = os.path.join(self.test_working_directory, "input_data")
        # shutil.copytree(self.datadir, datadir_test)

        # cfg_coregister = Config(open(test_config))
        # cfg_coregister.chain.output_path = self.test_working_directory
        # cfg_coregister.chain.s2_path = datadir_test

        # cfg_coregister.save(open(test_config, 'w'))
        # ensure_dir(
        #     os.path.join(self.test_working_directory, "features", "T38KPD"))
        # cfg = rcf.read_config_file(test_config)
        # # T38KPD's coregistration
        # sensors_parameters = rcf.iota2_parameters(
        #     test_config).get_sensors_parameters("T38KPD")
        # co_register.launch_coregister(
        #     "T38KPD", None, self.vhr_path,
        #     cfg.getParam('coregistration', 'band_src'),
        #     cfg.getParam('coregistration', 'band_ref'),
        #     cfg.getParam('coregistration', 'resample'),
        #     cfg.getParam('coregistration', 'step'),
        #     cfg.getParam('coregistration', 'minstep'),
        #     cfg.getParam('coregistration', 'minsiftpoints'),
        #     cfg.getParam('coregistration', 'iterate'),
        #     cfg.getParam('coregistration', 'prec'),
        #     cfg.getParam('coregistration', 'mode'),
        #     cfg.getParam("chain", "output_path"),
        #     cfg.getParam('coregistration', 'date_vhr'),
        #     cfg.getParam('coregistration', 'date_src'),
        #     cfg.getParam('chain', 'list_tile'), None, None,
        #     cfg.getParam('chain', 's2_path'), None, None, False,
        #     sensors_parameters)
        # date_folders = glob.glob(os.path.join(datadir_test, "T38KPD", "*"))
        # geoms_files = glob.glob(
        #     os.path.join(datadir_test, "T38KPD", "*", "*.geom"))
        # # assert
        # self.assertTrue(len(date_folders) == len(geoms_files))

        # # T38KPE's coregistration
        # ensure_dir(
        #     os.path.join(self.test_working_directory, "features", "T38KPE"))

        # sensors_parameters = rcf.iota2_parameters(
        #     test_config).get_sensors_parameters("T38KPE")
        # co_register.launch_coregister(
        #     "T38KPE", None, self.vhr_path,
        #     cfg.getParam('coregistration', 'band_src'),
        #     cfg.getParam('coregistration', 'band_ref'),
        #     cfg.getParam('coregistration', 'resample'),
        #     cfg.getParam('coregistration', 'step'),
        #     cfg.getParam('coregistration', 'minstep'),
        #     cfg.getParam('coregistration', 'minsiftpoints'),
        #     cfg.getParam('coregistration', 'iterate'),
        #     cfg.getParam('coregistration', 'prec'),
        #     cfg.getParam('coregistration', 'mode'),
        #     cfg.getParam("chain", "output_path"),
        #     cfg.getParam('coregistration', 'date_vhr'),
        #     cfg.getParam('coregistration', 'date_src'),
        #     cfg.getParam('chain', 'list_tile'), None, None,
        #     cfg.getParam('chain', 's2_path'), None, None, False,
        #     sensors_parameters)
        # # assert
        # date_folders = glob.glob(os.path.join(datadir_test, "T38KPE", "*"))
        # geoms_files = glob.glob(
        #     os.path.join(datadir_test, "T38KPE", "*", "*.geom"))
        # self.assertTrue(len(date_folders) == len(geoms_files))
