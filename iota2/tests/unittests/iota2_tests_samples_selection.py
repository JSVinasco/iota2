#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsSamplesSelection

import collections
import os
import shutil
from pathlib import Path

import iota2.tests.utils.tests_utils_vectors as TUV
from iota2.common import file_utils as fut
from iota2.common import iota2_directory
from iota2.sampling.samples_selection import (
    merge_write_stats,
    samples_selection,
    split_sel,
    update_flags,
    write_xml,
)
from iota2.tests.utils.tests_utils_files import cmp_xml_stat_files
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_vectors import rename_table
from iota2.vector_tools import vector_functions as vf
from iota2.vector_tools.vector_functions import cp_shape_file


class Iota2TestSamplesSelection:
    """Test worfklow involved to perfome a sample selection."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.config_test = str(
            IOTA2DIR / "config" / "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )
        cls.in_shape = str(
            IOTA2DIR
            / "data"
            / "references"
            / "selectionSamples"
            / "Input"
            / "samplesSelection"
            / "samples_region_1_seed_0.shp"
        )
        cls.in_xml = str(
            IOTA2DIR
            / "data"
            / "references"
            / "selectionSamples"
            / "Input"
            / "samplesSelection"
            / "samples_region_1_seed_0.xml"
        )
        cls.in_xml_merge = str(
            IOTA2DIR
            / "data"
            / "references"
            / "selectionSamples"
            / "Input"
            / "samplesSelection"
            / "merge_stats.xml"
        )
        cls.features_ref = str(
            IOTA2DIR
            / "data"
            / "references"
            / "selectionSamples"
            / "Input"
            / "features"
            / "T31TCJ"
        )
        cls.selection_ref = str(
            IOTA2DIR
            / "data"
            / "references"
            / "selectionSamples"
            / "Input"
            / "samplesSelection"
            / "T31TCJ_samples_region_1_seed_0_selection.sqlite"
        )

    def test_write_xml(self, i2_tmpdir, rm_tmpdir_on_success):
        """Writing of a statistics file."""
        samples_per_class = collections.OrderedDict(
            [
                ("11", 5),
                ("12", 6),
                ("211", 5),
                ("32", 3),
                ("31", 4),
                ("51", 8),
                ("34", 5),
                ("41", 9),
                ("222", 4),
                ("221", 4),
            ]
        )
        samples_per_vector = collections.OrderedDict(
            [
                ("1", 4),
                ("0", 5),
                ("3", 4),
                ("2", 4),
                ("5", 6),
                ("4", 5),
                ("7", 9),
                ("6", 8),
                ("9", 5),
                ("8", 3),
            ]
        )
        xml_test = str(i2_tmpdir / "test.xml")

        # launch function
        write_xml(samples_per_class, samples_per_vector, xml_test)

        assert cmp_xml_stat_files(
            self.in_xml, xml_test
        ), "merge xml statistics files failed"

    def test_merge_xml(self, i2_tmpdir, rm_tmpdir_on_success):
        """Merge statistics files."""
        samples_per_class = collections.OrderedDict(
            [
                ("11", 5),
                ("12", 6),
                ("211", 5),
                ("32", 3),
                ("31", 4),
                ("51", 8),
                ("34", 5),
                ("41", 9),
                ("222", 4),
                ("221", 4),
            ]
        )
        samples_per_vector = collections.OrderedDict(
            [
                ("1", 4),
                ("0", 5),
                ("3", 4),
                ("2", 4),
                ("5", 6),
                ("4", 5),
                ("7", 9),
                ("6", 8),
                ("9", 5),
                ("8", 3),
            ]
        )
        xml_test_1 = str(i2_tmpdir / "test_1.xml")
        xml_test_2 = str(i2_tmpdir / "test_2.xml")
        write_xml(samples_per_class, samples_per_vector, xml_test_1)
        write_xml(samples_per_class, samples_per_vector, xml_test_2)

        # launch function
        test_merge = str(i2_tmpdir / "test_merge.xml")
        merge_write_stats([xml_test_1, xml_test_2], test_merge)

        # assert
        assert cmp_xml_stat_files(
            self.in_xml_merge, test_merge
        ), "merge xml statistics files failed"

    def test_split_selection(self, i2_tmpdir, rm_tmpdir_on_success):
        """
        test dedicated to check if split_sel function works
        """
        # prepare test input
        test_vector_name = "samples_region_1_seed_0.sqlite"
        test_vector_table = "t31tcj_samples_region_1_seed_0_selection"
        test_vector = str(i2_tmpdir / test_vector_name)
        shutil.copy(self.selection_ref, test_vector)

        # update "nb_feat" features to a new "new_tile_name" tile's name
        nb_feat = 10
        new_tile_name = "T31TDJ"
        TUV.random_update(
            test_vector, test_vector_table, "tile_o", new_tile_name, nb_feat
        )
        rename_table(
            test_vector, old_table_name=test_vector_table, new_table_name="output"
        )
        # launch function
        new_files = split_sel(
            test_vector, ["T31TCJ", new_tile_name], str(i2_tmpdir), "EPSG:2154"
        )
        # assert
        nb_features_origin = len(
            vf.get_field_element(
                self.selection_ref,
                driver_name="SQLite",
                field="tile_o",
                mode="all",
                elem_type="str",
            )
        )
        nb_features_t31tcj = len(
            vf.get_field_element(
                new_files[0],
                driver_name="SQLite",
                field="tile_o",
                mode="all",
                elem_type="str",
            )
        )
        nb_features_t31tdj = len(
            vf.get_field_element(
                new_files[1],
                driver_name="SQLite",
                field="tile_o",
                mode="all",
                elem_type="str",
            )
        )
        assert nb_features_t31tdj == nb_feat, "split samples selection failed"
        assert (
            nb_features_origin == nb_features_t31tdj + nb_features_t31tcj
        ), "split samples selection failed"

    def test_update_flags(self, i2_tmpdir, rm_tmpdir_on_success):
        """ """
        # prepare test input
        test_vector_name = "T31TCJ_samples_region_1_seed_1_selection.sqlite"
        test_vector_table = "t31tcj_samples_region_1_seed_0_selection"
        test_vector = str(i2_tmpdir / test_vector_name)
        shutil.copy(self.selection_ref, test_vector)

        update_flags(test_vector, 2, table_name=test_vector_table)

        # assert
        updated_flag = "XXXX"
        nb_features_origin = len(
            vf.get_field_element(
                self.selection_ref,
                driver_name="SQLite",
                field="seed_0",
                mode="all",
                elem_type="str",
            )
        )
        features_test = vf.get_field_element(
            test_vector,
            driver_name="SQLite",
            field="seed_0",
            mode="all",
            elem_type="str",
        )
        nb_features_test_updated = features_test.count(updated_flag)
        assert nb_features_origin == nb_features_test_updated, "update features failed"

    def test_samples_selection(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test sampling of a shape file (main function of SamplesSelection.py)"""
        iota2_directory.generate_directories(
            str(i2_tmpdir / "samplesSelTest"),
            False,
            ["D0004H0004", "D0005H0004", "D0004H0003", "D0005H0003"],
        )
        shutil.copytree(
            self.features_ref, str(i2_tmpdir / "samplesSelTest" / "features" / "T31TCJ")
        )
        shutil.copy(
            self.in_xml,
            str(
                i2_tmpdir
                / "samplesSelTest"
                / "samplesSelection"
                / "T31TCJ_region_1_seed_0_stats.xml"
            ),
        )
        _, in_shape_name = os.path.split(self.in_shape)
        in_shape_dir = str(i2_tmpdir / "samplesSelTest" / "samplesSelection")
        in_shape = str(Path(in_shape_dir) / in_shape_name)
        cp_shape_file(
            self.in_shape.replace(".shp", ""),
            in_shape.replace(".shp", ""),
            extensions=[".prj", ".shp", ".dbf", ".shx"],
        )

        # launch function
        output_path = str(i2_tmpdir / "samplesSelTest")
        runs = 2
        epsg = "EPSG:2154"
        random_seed = 1
        data_field = "code"
        parameters = {"sampler": "random", "strategy": "all"}
        masks_name = "MaskCommunSL.tif"
        samples_selection(
            model=in_shape,
            working_directory=str(i2_tmpdir),
            output_path=output_path,
            runs=runs,
            epsg=epsg,
            masks_name=masks_name,
            parameters=parameters,
            data_field=data_field,
            random_seed=random_seed,
            sampling_validation=False,
            parameters_validation={},
        )
        # assert
        selection_test = fut.file_search_and(
            str(i2_tmpdir / "samplesSelTest"),
            True,
            os.path.basename(self.selection_ref),
        )[0]
        same = TUV.compare_sqlite(
            self.selection_ref, selection_test, cmp_mode="coordinates"
        )
        assert same, "sample selection generation failed"
