#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import pickle
import shutil
from functools import partial
from pathlib import Path

import numpy as np
from sklearn.datasets import make_classification
from sklearn.ensemble import RandomForestClassifier

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.common import raster_utils as rasterU
from iota2.common.file_utils import file_search_and
from iota2.common.otb_app_bank import CreateBandMathXApplication
from iota2.common.raster_utils import ChunkConfig
from iota2.learning.train_sklearn import sk_learn
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_rasters import array_to_raster


class Iota2TestsNumpyWorkflow(AssertsFilesUtils):
    """Test some numpy workflow in iota2 related to scikit ML."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.features_dataBase = str(IOTA2DIR / "data" /
                                    "reduced_output_samples.sqlite")
        cls.ref_cross_validation = ["Best Parameters:"]
        cls.ref_scale = np.array([
            2.26379081, 1.88323221, 0.7982067, 0.60191847, 0.39085819,
            0.19637141, 2.26379081, 1.88323221, 0.7982067, 0.60191847,
            0.39085819, 0.19637141, 2.2770817, 1.88356701, 0.82879011,
            0.52570417, 0.37130111, 0.18286401, 2.3113216, 1.82009071,
            0.92872819, 0.41618358, 0.32543167, 0.25472347, 2.42691728,
            1.72997482, 0.84062143, 0.38731198, 0.25782248, 0.24453248,
            2.45312165, 1.75901026, 0.72865975, 0.3548426, 0.26213068,
            0.17726742, 2.47527844, 1.73895914, 0.65199415, 0.46599839,
            0.2526496, 0.14894581, 2.47947365, 1.72583852, 0.66522993,
            0.46912169, 0.25860929, 0.15443434, 2.46829782, 1.72538802,
            0.68779437, 0.49106096, 0.26733444, 0.16386239, 2.45930121,
            1.72657813, 0.69013682, 0.50729237, 0.28364723, 0.17760912,
            2.48103485, 1.60658697, 0.8078123, 0.46817622, 0.42947051,
            0.26274128, 2.42499156, 1.72439242, 0.76069492, 0.49849193,
            0.34698139, 0.24679596, 2.38322473, 1.79774719, 0.71809114,
            0.54897445, 0.29146343, 0.220741, 2.35705107, 1.84537019,
            0.68537724, 0.57784401, 0.26225397, 0.1968307, 2.34456355,
            1.85809104, 0.69565658, 0.5909745, 0.24434703, 0.18665792,
            2.27672512, 1.91074124, 0.76340315, 0.59972296, 0.24893347,
            0.20510927, 2.28776289, 1.91855255, 0.72640125, 0.57731692,
            0.23880154, 0.21419432, 2.28240157, 1.9095699, 0.74639484,
            0.60961737, 0.23549034, 0.2126695, 2.34223592, 1.84281525,
            0.80507131, 0.45320055, 0.25136176, 0.24141408, 2.33568571,
            1.84493349, 0.85237784, 0.38862779, 0.28560659, 0.2084152,
            2.30312608, 1.85327544, 0.91991632, 0.37270513, 0.30898565,
            0.2056479, 2.24433067, 1.86446705, 1.01841375, 0.39264294,
            0.32578043, 0.20842152, 2.16757937, 1.89643401, 1.10610101,
            0.41818918, 0.34620732, 0.19259937
        ])
        cls.ref_var = np.array([
            5.12474884, 3.54656357, 0.63713394, 0.36230584, 0.15277012,
            0.03856173, 5.12474884, 3.54656357, 0.63713394, 0.36230584,
            0.15277012, 0.03856173, 5.18510106, 3.54782468, 0.68689304,
            0.27636487, 0.13786452, 0.03343925, 5.34220754, 3.3127302,
            0.86253605, 0.17320877, 0.10590577, 0.06488405, 5.88992751,
            2.99281288, 0.70664438, 0.15001057, 0.06647243, 0.05979613,
            6.01780582, 3.0941171, 0.53094504, 0.12591327, 0.06871249,
            0.03142374, 6.12700335, 3.02397889, 0.42509638, 0.2171545,
            0.06383182, 0.02218485, 6.14778957, 2.97851858, 0.44253086,
            0.22007516, 0.06687876, 0.02384996, 6.09249414, 2.97696381,
            0.47306109, 0.24114087, 0.0714677, 0.02685088, 6.04816243,
            2.98107205, 0.47628883, 0.25734555, 0.08045575, 0.031545,
            6.15553393, 2.58112168, 0.65256071, 0.21918897, 0.18444492,
            0.06903298, 5.88058406, 2.97352921, 0.57865676, 0.24849421,
            0.12039608, 0.06090825, 5.67976011, 3.23189496, 0.51565489,
            0.30137295, 0.08495093, 0.04872659, 5.55568973, 3.40539114,
            0.46974196, 0.3339037, 0.06877714, 0.03874233, 5.49697822,
            3.4525023, 0.48393808, 0.34925086, 0.05970547, 0.03484118,
            5.18347728, 3.65093209, 0.58278437, 0.35966763, 0.06196787,
            0.04206981, 5.23385904, 3.6808439, 0.52765877, 0.33329483,
            0.05702617, 0.04587921, 5.20935694, 3.64645719, 0.55710525,
            0.37163334, 0.0554557, 0.04522832, 5.48606908, 3.39596805,
            0.64813982, 0.20539074, 0.06318273, 0.05828076, 5.45542772,
            3.40377958, 0.72654798, 0.15103156, 0.08157112, 0.0434369,
            5.30438975, 3.43462987, 0.84624604, 0.13890911, 0.09547213,
            0.04229106, 5.03702016, 3.47623737, 1.03716656, 0.15416848,
            0.10613289, 0.04343953, 4.69840032, 3.59646196, 1.22345944,
            0.17488219, 0.11985951, 0.03709452
        ])

    # Tests definitions
    def test_apply_function(self, i2_tmpdir, rm_tmpdir_on_success):
        """TEST : check the whole workflow."""

        def custom_features(array):
            return array + array, []

        # First create some dummy data on disk
        dummy_raster_path = str(i2_tmpdir / "DUMMY.tif")
        array_to_rasterize = TUR.fun_array("iota2_binary")
        array_to_raster(array_to_rasterize, dummy_raster_path)

        # Get it in a otbApplication (simulating full iota2 features pipeline)
        band_math = CreateBandMathXApplication({
            "il": [dummy_raster_path],
            "exp": "im1b1;im1b1"
        })
        band_math.Execute()
        apps_dict = {
            "interp": band_math,
            "raw": band_math,
            "masks": band_math,
            "enable_interp": True,
            "enable_raw": False,
            "enable_masks": False,
        }
        # Then apply function
        function_partial = partial(custom_features)

        labels_features_name = ["NDVI_20200101"]

        new_features_path = str(i2_tmpdir / "DUMMY_test.tif")
        (test_array, new_labels, _, _, _, _, _,
         _) = rasterU.insert_external_function_to_pipeline(
             otb_pipelines=apps_dict,
             labels=labels_features_name,
             working_dir=str(i2_tmpdir),
             function=function_partial,
             output_path=new_features_path,
             chunk_config=ChunkConfig(chunk_size_mode="user_fixed",
                                      number_of_chunks=None,
                                      chunk_size_x=5,
                                      chunk_size_y=5))
        # asserts

        # check array' shape consistency
        # concerning np.array there is different convention between OTB(GDAL?)
        # and rasterio
        # OTB : [row, cols, bands]
        # rasterio : [bands, row, cols]
        band_math.Execute()
        pipeline_shape = band_math.GetVectorImageAsNumpyArray("out").shape
        pipeline_shape = (pipeline_shape[2], pipeline_shape[0],
                          pipeline_shape[1])
        assert pipeline_shape == test_array.shape

        # check if the input function is well applied
        pipeline_array = band_math.GetVectorImageAsNumpyArray("out")
        ref_array, _ = custom_features(pipeline_array)
        assert np.allclose(np.moveaxis(ref_array, -1, 0), test_array)
        assert new_labels is not None

    def test_apply_function_padding(self, i2_tmpdir, rm_tmpdir_on_success):
        """
        TEST : check the whole workflow
        """

        padding_size_x, padding_size_y = 2, 1

        # The padding is used for contextual approach.
        # In this case 2 chunks overlap each other (on the padding) and we need
        # to check these 2 chunks results are well merged
        # So here we need a test function which returns a value calculated with
        # the context of the pixel, otherwise the 2 chunks results would be
        # identical and the test irrelevant
        def custom_features(array):
            result = np.copy(array)
            for x in range(padding_size_x, array.shape[1] - padding_size_x):
                patch_boundaries_x = (x - padding_size_x, x + padding_size_x)
                for y in range(padding_size_y,
                               array.shape[0] - padding_size_y):
                    patch_boundaries_y = (y - padding_size_y,
                                          y + padding_size_y)
                    result[y, x, :] = np.mean(
                        array[patch_boundaries_y[0]:patch_boundaries_y[1],
                              patch_boundaries_x[0]:patch_boundaries_x[1], :])
            return result, []

        # First create some dummy data on disk
        dummy_raster_path = str(i2_tmpdir / "DUMMY.tif")
        array_to_rasterize = TUR.fun_array("iota2_binary")
        array_to_raster(array_to_rasterize, dummy_raster_path)

        # Get it in a otbApplication (simulating full iota2 features pipeline)
        band_math = CreateBandMathXApplication({
            "il": [dummy_raster_path],
            "exp": "im1b1;im1b1"
        })
        band_math.Execute()
        apps_dict = {
            "interp": band_math,
            "raw": band_math,
            "masks": band_math,
            "enable_interp": True,
            "enable_raw": False,
            "enable_masks": False,
        }
        # Then apply function
        function_partial = partial(custom_features)

        labels_features_name = ["NDVI_20200101"]

        new_features_path = str(i2_tmpdir / "DUMMY_test.tif")
        (test_array, new_labels, _, _, _, _, _,
         _) = rasterU.insert_external_function_to_pipeline(
             otb_pipelines=apps_dict,
             labels=labels_features_name,
             working_dir=str(i2_tmpdir),
             function=function_partial,
             output_path=new_features_path,
             chunk_config=ChunkConfig(chunk_size_mode="user_fixed",
                                      number_of_chunks=None,
                                      chunk_size_x=5,
                                      chunk_size_y=5,
                                      padding_size_x=padding_size_x,
                                      padding_size_y=padding_size_y))

        band_math.Execute()
        pipeline_shape = band_math.GetVectorImageAsNumpyArray("out").shape
        pipeline_shape = (pipeline_shape[2], pipeline_shape[0],
                          pipeline_shape[1])
        assert pipeline_shape == test_array.shape

        # check if the input function is well applied
        pipeline_array = band_math.GetVectorImageAsNumpyArray("out")
        ref_array, _ = custom_features(pipeline_array)
        assert np.allclose(np.moveaxis(ref_array, -1, 0), test_array)
        assert new_labels is not None

        # test "targeted_chunk" behaviour
        number_of_chunks = 5

        def get_mosaic(targeted_chunk):
            (test_array, _, _, _, _, _, _,
             _) = rasterU.insert_external_function_to_pipeline(
                 otb_pipelines=apps_dict,
                 labels=labels_features_name,
                 working_dir=str(i2_tmpdir),
                 function=function_partial,
                 output_path=new_features_path,
                 chunk_config=ChunkConfig(chunk_size_mode="split_number",
                                          number_of_chunks=number_of_chunks,
                                          chunk_size_x=None,
                                          chunk_size_y=None,
                                          padding_size_x=padding_size_x,
                                          padding_size_y=padding_size_y),
                 targeted_chunk=targeted_chunk)
            return test_array

        test_array = get_mosaic(targeted_chunk=0)
        assert (pipeline_shape[0], 3, pipeline_shape[2]) == test_array.shape

        test_array = get_mosaic(targeted_chunk=number_of_chunks - 1)
        assert (pipeline_shape[0], 4, pipeline_shape[2]) == test_array.shape

    def test_sk_cross_validation(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test cross validation."""
        _, db_file_name = os.path.split(self.features_dataBase)

        features_db_test = str(i2_tmpdir / db_file_name)
        shutil.copy(self.features_dataBase, features_db_test)

        test_model_path = str(i2_tmpdir / "test_model.rf")
        sk_learn(
            dataset_path=features_db_test,
            features_labels=["reduced_{}".format(cpt) for cpt in range(138)],
            model_path=test_model_path,
            data_field="code",
            sk_model_name="RandomForestClassifier",
            cv_parameters={'n_estimators': [50, 100]},
            min_samples_split=25)

        # asserts
        self.assert_file_exist(Path(test_model_path))
        test_cross_val_results = file_search_and(str(i2_tmpdir), True,
                                                 "cross_val_param.cv")[0]
        with open(test_cross_val_results, "r") as cross_val_f:
            test_cross_val = [line.rstrip() for line in cross_val_f]

        test_cv_val = all([
            val_to_find in test_cross_val
            for val_to_find in self.ref_cross_validation
        ])
        assert test_cv_val, "cross validation failed"

    def test_sk_standardization(self, i2_tmpdir, rm_tmpdir_on_success):
        """test standardization
        """

        _, db_file_name = os.path.split(self.features_dataBase)

        features_db_test = str(i2_tmpdir / db_file_name)
        shutil.copy(self.features_dataBase, features_db_test)

        test_model_path = str(i2_tmpdir / "test_model.rf")
        sk_learn(
            dataset_path=features_db_test,
            features_labels=["reduced_{}".format(cpt) for cpt in range(138)],
            model_path=test_model_path,
            data_field="code",
            sk_model_name="RandomForestClassifier",
            apply_standardization=True)

        self.assert_file_exist(Path(test_model_path))

        with open(test_model_path, "rb") as model_obj:
            model, scaler = pickle.load(model_obj)

        assert np.allclose(self.ref_scale, scaler.scale_)
        assert np.allclose(self.ref_var, scaler.var_)

    def test_machine_learning(self, i2_tmpdir, rm_tmpdir_on_success):
        """Use sci-kit learn machine learning algorithm."""

        def do_predict(array, model, dtype="int32"):

            def wrapper(*args, **kwargs):
                return model.predict([args[0]])

            predicted_array = np.apply_along_axis(func1d=wrapper,
                                                  axis=-1,
                                                  arr=array)
            return predicted_array.astype(dtype), []

        # build data to learn RF model
        X, y = make_classification(
            n_samples=1000,
            n_features=2,
            n_informative=2,
            n_redundant=0,
            random_state=0,
            shuffle=True,
        )
        # learning
        clf = RandomForestClassifier(n_estimators=100,
                                     max_depth=2,
                                     random_state=0)
        clf.fit(X, y)

        # create some data on disk in order to predict them
        dummy_raster_path = str(i2_tmpdir / "DUMMY.tif")
        array_to_rasterize = TUR.fun_array("iota2_binary")
        array_to_raster(array_to_rasterize, dummy_raster_path)

        # Get it in a otbApplication (simulating full iota2 features pipeline)
        band_math = CreateBandMathXApplication({
            "il": [dummy_raster_path],
            "exp": "im1b1;im1b1"
        })
        apps_dict = {
            "interp": band_math,
            "raw": band_math,
            "masks": band_math,
            "enable_interp": True,
            "enable_raw": False,
            "enable_masks": False,
        }
        # prediction
        function_partial = partial(do_predict, model=clf)
        prediction_path = str(i2_tmpdir / "Classif_test.tif")
        (test_array, new_labels, _, _, _, _, _,
         _) = rasterU.insert_external_function_to_pipeline(
             otb_pipelines=apps_dict,
             labels=[""],
             working_dir=str(i2_tmpdir),
             function=function_partial,
             output_path=prediction_path,
             chunk_config=ChunkConfig(chunk_size_mode="user_fixed",
                                      number_of_chunks=None,
                                      chunk_size_x=5,
                                      chunk_size_y=5))
        self.assert_file_exist(Path(prediction_path))
        assert test_array.shape == (1, 16, 86)
