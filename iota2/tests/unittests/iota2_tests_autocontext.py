#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import re
import shutil
import sys
from collections import Counter
from pathlib import Path

import numpy as np

from iota2.classification.image_classifier import autocontext_launch_classif
from iota2.common import iota2_directory
from iota2.common.file_utils import ensure_dir, file_search_and
from iota2.common.generate_features import generate_features
from iota2.common.otb_app_bank import (
    CreateBandMathApplication,
    CreatePolygonClassStatisticsApplication,
    CreateSampleExtractionApplication,
    CreateSampleSelectionApplication,
)
from iota2.common.utils import run
from iota2.configuration_files import read_config_file as rcf
from iota2.configuration_files.read_config_file import iota2_parameters
from iota2.learning.train_autocontext import train_autocontext
from iota2.sampling.split_samples import split_superpixels_and_reference
from iota2.sampling.super_pixels_selection import (
    merge_ref_super_pix,
    move_annual_samples_position,
)
from iota2.segmentation import segmentation
from iota2.sensors.sensors_container import sensors_container
from iota2.tests.utils.tests_utils_rasters import (
    array_to_raster,
    fun_array,
    generate_fake_s2_data,
    is_raster_unique_value,
)
from iota2.tests.utils.tests_utils_vectors import compare_vector_raster
from iota2.vector_tools.add_field import add_field
from iota2.vector_tools.vector_functions import get_field_element

IOTA2DIR = Path(os.environ.get("IOTA2DIR"))

if IOTA2DIR is None:
    raise Exception("IOTA2DIR environment variable must be set")

IOTA2_SCRIPT = str(IOTA2DIR / "iota2")
sys.path.append(IOTA2_SCRIPT)


class Iota2TestAutoContext:
    """Check auto-context workflow."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.originX = 566377
        cls.originY = 6284029
        cls.group_test_name = "iota_testAutoContext"

        cls.config_test = str(
            IOTA2DIR / "config" / "Config_4Tuiles_Multi_FUS_Confidence.cfg"
        )
        cls.ref_data = os.path.join(
            IOTA2DIR,
            "data",
            "references",
            "formatting_vectors",
            "Input",
            "formattingVectors",
            "T31TCJ.shp",
        )
        cls.nomenclature_path = str(
            IOTA2DIR / "data" / "references" / "nomenclature.txt"
        )
        cls.tile_name = "T31TCJ"
        cls.all_tests_ok = []
        cls.test_working_directory = None
        cls.iota2_tests_directory = str(IOTA2DIR / "data" / cls.group_test_name)
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory, ignore_errors=True)
        os.mkdir(cls.iota2_tests_directory)

        # generate permanent fake data
        cls.fake_data_dir = os.path.join(cls.iota2_tests_directory, "fake_s2")
        ensure_dir(cls.fake_data_dir)

        # TODO : fixture de class pour générer un tmpdir de scope class
        generate_fake_s2_data(
            cls.fake_data_dir, "T31TCJ", ["20190909", "20190919", "20190929"]
        )

    def generate_cfg_file(self, ref_cfg, test_cfg, i2_output_dir: Path):
        """Utils to gen auto-context configuration file."""
        shutil.copy(ref_cfg, test_cfg)

        test_path = str(i2_output_dir / "RUN")
        cfg_test = rcf.read_internal_config_file(test_cfg)
        cfg_test.cfg_as_dict["chain"]["region_path"] = None
        cfg_test.cfg_as_dict["arg_train"]["sample_management"] = None
        cfg_test.cfg_as_dict["arg_train"]["prev_features"] = None
        cfg_test.cfg_as_dict["arg_train"]["output_prev_features"] = None
        cfg_test.cfg_as_dict["simplification"]["grasslib"] = None
        cfg_test.cfg_as_dict["simplification"]["clipfile"] = None
        cfg_test.cfg_as_dict["simplification"]["bingdal"] = None
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = str(
            IOTA2DIR / "data" / "references" / "D5H2_groundTruth_samples.shp"
        )
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = test_cfg
        cfg_test.cfg_as_dict["chain"]["color_table"] = test_cfg
        cfg_test.cfg_as_dict["chain"]["output_path"] = test_path
        cfg_test.cfg_as_dict["chain"]["list_tile"] = "T31TCJ"
        cfg_test.cfg_as_dict["chain"]["l8_path_old"] = None
        cfg_test.cfg_as_dict["chain"]["l8_path"] = None
        cfg_test.cfg_as_dict["chain"]["s2_path"] = self.fake_data_dir
        cfg_test.cfg_as_dict["chain"]["s2_s2c_path"] = None
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = None
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["arg_train"]["enable_autocontext"] = True
        cfg_test.cfg_as_dict["arg_train"]["crop_mix"] = False
        cfg_test.cfg_as_dict["arg_train"]["samples_classif_mix"] = False
        cfg_test.cfg_as_dict["arg_train"]["annual_classes_extraction_source"] = None
        cfg_test.cfg_as_dict["sensors_data_interpolation"][
            "use_additional_features"
        ] = False
        cfg_test.cfg_as_dict["sensors_data_interpolation"]["write_outputs"] = False
        cfg_test.save(test_cfg)
        return test_path

    def prepare_data_ref(self, in_vector, out_vector, ref_img):
        """Genereate sample selection database."""
        stat = out_vector.replace(".sqlite", ".xml")
        CreatePolygonClassStatisticsApplication(
            {"in": ref_img, "vec": in_vector, "field": "CODE", "out": stat}
        ).ExecuteAndWriteOutput()
        CreateSampleSelectionApplication(
            {
                "in": ref_img,
                "vec": in_vector,
                "field": "CODE",
                "strategy": "all",
                "instats": stat,
                "out": out_vector,
            }
        ).ExecuteAndWriteOutput()
        os.remove(stat)

    def prepare_autoContext_data_ref(
        self, slic_seg, config_path_test, working_dir: Path
    ):
        """Genereate auto-context input database."""
        raster_ref = file_search_and(self.fake_data_dir, True, ".tif")[0]
        CreatePolygonClassStatisticsApplication(
            {
                "in": raster_ref,
                "vec": self.ref_data,
                "field": "CODE",
                "out": str(working_dir / "stats.xml"),
            }
        ).ExecuteAndWriteOutput()
        ref_sampled = str(
            working_dir / "T31TCJ_samples_region_1_seed_0_selection.sqlite"
        )
        CreateSampleSelectionApplication(
            {
                "in": raster_ref,
                "vec": self.ref_data,
                "field": "CODE",
                "strategy": "all",
                "instats": str(working_dir / "stats.xml"),
                "out": ref_sampled,
            }
        ).ExecuteAndWriteOutput()

        cfg = rcf.read_config_file(config_path_test)
        params = iota2_parameters(cfg)
        sensors_parameters = params.get_sensors_parameters("T31TCJ")
        apps_dict, labs_dict, _ = generate_features(
            None,
            "T31TCJ",
            sar_optical_post_fusion=False,
            output_path=cfg.getParam("chain", "output_path"),
            sensors_parameters=sensors_parameters,
        )
        features = apps_dict["interp"]
        feat_labels = labs_dict["interp"]
        extraction = CreateSampleExtractionApplication(
            {
                "in": features,
                "vec": ref_sampled,
                "field": "code",
                "outfield.list.names": feat_labels,
                "outfield": "list",
            }
        )
        extraction.ExecuteAndWriteOutput()

        add_field(
            ref_sampled,
            "newregion",
            value_field="1",
            value_type=str,
            driver_name="SQLite",
        )
        os.remove(str(working_dir / "stats.xml"))

        merge_ref_super_pix(
            {"selection_samples": ref_sampled, "SLIC": slic_seg},
            "code",
            "superpix",
            "is_superpix",
            "newregion",
        )

        learning_vector = str(
            working_dir / "T31TCJ_region_1_seed0_Samples_learn.sqlite"
        )
        shutil.move(ref_sampled, learning_vector)

        _, superpix = split_superpixels_and_reference(
            learning_vector, superpix_column="is_superpix"
        )

        return learning_vector, superpix

    def test_sampling_features_from_raster(self, i2_tmpdir, rm_tmpdir_on_success):
        """Non-regression test.

        Check the ability of adding features in database
        by sampling classification raster.
        """
        mask_array = fun_array("iota2_binary")
        random_classif_mask_path = str(i2_tmpdir / "Classif_Seed_0.tif")

        validity_array = np.full(mask_array.shape, 1)
        validity_path = str(i2_tmpdir / "PixelsValidity.tif")
        region_path = str(i2_tmpdir / "mask_region.tif")
        array_to_raster(
            validity_array,
            validity_path,
            pixel_size=10,
            origin_x=self.originX,
            origin_y=self.originY,
        )
        array_to_raster(
            validity_array,
            region_path,
            pixel_size=10,
            origin_x=self.originX,
            origin_y=self.originY,
        )

        ref_data_sampled = str(i2_tmpdir / "dataBase.sqlite")

        self.prepare_data_ref(
            in_vector=self.ref_data, out_vector=ref_data_sampled, ref_img=region_path
        )

        # test : all annual labels are in dataBase and classifications
        labels = [12, 31, 32, 41, 211, 222]
        annual_labels = ["12", "31"]
        annual_labels_int = [int(elem) for elem in annual_labels]
        random_classif_array = np.random.choice(
            labels, size=mask_array.shape, replace=True
        )
        random_classif_array_mask = random_classif_array * mask_array
        array_to_raster(
            random_classif_array_mask,
            random_classif_mask_path,
            pixel_size=10,
            origin_x=self.originX,
            origin_y=self.originY,
        )
        move_annual_samples_position(
            samples_position=ref_data_sampled,
            data_field="code",
            annual_labels=annual_labels,
            classification_raster=random_classif_mask_path,
            validity_raster=validity_path,
            region_mask=region_path,
            validity_threshold=0,
            tile_origin_field_value=("tile_o", "T31TCJ"),
            seed_field_value=("seed_0", "learn"),
            region_field_value=("region", "1"),
        )
        # assert
        raster_values = compare_vector_raster(
            in_vec=ref_data_sampled,
            vec_field="code",
            field_values=annual_labels_int,
            in_img=random_classif_mask_path,
        )
        values_counter = Counter(raster_values)
        exist_in_annual = [
            value in annual_labels_int for value in values_counter.keys()
        ]
        assert all(exist_in_annual)

        # test : only one annual label in dataBase
        labels = [12, 31, 32, 41, 211, 222]
        annual_labels = ["12"]
        annual_labels_int = [int(elem) for elem in annual_labels]
        random_classif_array = np.random.choice(
            labels, size=mask_array.shape, replace=True
        )
        random_classif_array_mask = random_classif_array * mask_array
        array_to_raster(
            random_classif_array_mask,
            random_classif_mask_path,
            pixel_size=10,
            origin_x=self.originX,
            origin_y=self.originY,
        )

        move_annual_samples_position(
            samples_position=ref_data_sampled,
            data_field="code",
            annual_labels=annual_labels,
            classification_raster=random_classif_mask_path,
            validity_raster=validity_path,
            region_mask=region_path,
            validity_threshold=0,
            tile_origin_field_value=("tile_o", "T31TCJ"),
            seed_field_value=("seed_0", "learn"),
            region_field_value=("region", "1"),
        )
        # assert
        raster_values = compare_vector_raster(
            in_vec=ref_data_sampled,
            vec_field="code",
            field_values=annual_labels_int,
            in_img=random_classif_mask_path,
        )
        values_counter = Counter(raster_values)
        exist_in_annual = [
            value in annual_labels_int for value in values_counter.keys()
        ]
        assert all(exist_in_annual)

        # test : no annual labels in classifications
        labels = [12, 31, 32, 41, 211, 222]
        annual_labels = ["111", "112"]
        annual_labels_int = [int(elem) for elem in annual_labels]
        random_classif_array = np.random.choice(
            labels, size=mask_array.shape, replace=True
        )
        random_classif_array_mask = random_classif_array * mask_array
        array_to_raster(
            random_classif_array_mask,
            random_classif_mask_path,
            pixel_size=10,
            origin_x=self.originX,
            origin_y=self.originY,
        )

        move_annual_samples_position(
            samples_position=ref_data_sampled,
            data_field="code",
            annual_labels=annual_labels,
            classification_raster=random_classif_mask_path,
            validity_raster=validity_path,
            region_mask=region_path,
            validity_threshold=0,
            tile_origin_field_value=("tile_o", "T31TCJ"),
            seed_field_value=("seed_0", "learn"),
            region_field_value=("region", "1"),
        )
        # assert
        raster_values = compare_vector_raster(
            in_vec=ref_data_sampled,
            vec_field="code",
            field_values=annual_labels_int,
            in_img=random_classif_mask_path,
        )
        values_counter = Counter(raster_values)
        assert len(values_counter) == 0

    def test_slic(self, i2_tmpdir, rm_tmpdir_on_success):
        """non-regression test, check if SLIC could be performed"""
        # config file
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")
        test_path = self.generate_cfg_file(
            self.config_test, config_path_test, i2_tmpdir
        )
        iota2_directory.generate_directories(
            test_path, merge_final_classifications=False, tile_list=["T31TCJ"]
        )

        slic_working_dir = str(i2_tmpdir / "slic_tmp")
        iota2_dico = rcf.iota2_parameters(
            rcf.read_config_file(config_path_test)
        ).get_sensors_parameters(self.tile_name)
        sensors = sensors_container(self.tile_name, None, i2_tmpdir, **iota2_dico)
        sensors.sensors_preprocess()

        # Launch test
        segmentation.slic_segmentation(
            self.tile_name,
            test_path,
            iota2_dico,
            ram=128,
            working_dir=slic_working_dir,
            force_spw=1,
        )

        # as SLIC algorithm contains random variables, the raster's content
        # could not be tested
        assert (
            len(
                file_search_and(
                    str(Path(test_path) / "features" / self.tile_name / "tmp"),
                    True,
                    "SLIC_{}".format(self.tile_name),
                )
            )
            == 1
        ), "SLIC algorithm failed"

    def test_train_and_classify(self, i2_tmpdir, rm_tmpdir_on_success):
        """test autoContext training"""
        # config file
        config_path_test = str(i2_tmpdir / "Config_TEST.cfg")
        test_path = self.generate_cfg_file(
            self.config_test, config_path_test, i2_tmpdir
        )
        cfg = rcf.read_config_file(config_path_test)
        iota2_directory.generate_directories(
            test_path, False, cfg.getParam("chain", "list_tile").split(" ")
        )
        autocontext_working_dir = str(i2_tmpdir / "autoContext_tmp")
        slic_working_dir = str(i2_tmpdir / "autoContext_tmp")
        iota2_dico = rcf.iota2_parameters(cfg).get_sensors_parameters(self.tile_name)
        sensors = sensors_container(self.tile_name, None, i2_tmpdir, **iota2_dico)
        sensors.sensors_preprocess()
        running_parameters = iota2_parameters(cfg)
        sensors_parameters = running_parameters.get_sensors_parameters("T31TCJ")
        segmentation.slic_segmentation(
            self.tile_name,
            test_path,
            sensors_parameters,
            ram=128,
            working_dir=slic_working_dir,
            force_spw=1,
        )

        slic_seg = file_search_and(
            str(Path(test_path) / "features" / self.tile_name / "tmp"),
            True,
            "SLIC_{}".format(self.tile_name),
        )[0]

        train_auto_data_ref, superpix_data = self.prepare_autoContext_data_ref(
            slic_seg, config_path_test, i2_tmpdir
        )

        parameter_dict = {
            "model_name": "1",
            "seed": "0",
            "list_learning_samples": [train_auto_data_ref],
            "list_superPixel_samples": [superpix_data],
            "list_tiles": ["T31TCJ"],
            "list_slic": [slic_seg],
        }

        # launch tests

        features_list_name = [
            "sentinel2_b2_20190909",
            "sentinel2_b3_20190909",
            "sentinel2_b4_20190909",
            "sentinel2_b5_20190909",
            "sentinel2_b6_20190909",
            "sentinel2_b7_20190909",
            "sentinel2_b8_20190909",
            "sentinel2_b8a_20190909",
            "sentinel2_b11_20190909",
            "sentinel2_b12_20190909",
            "sentinel2_b2_20190919",
            "sentinel2_b3_20190919",
            "sentinel2_b4_20190919",
            "sentinel2_b5_20190919",
            "sentinel2_b6_20190919",
            "sentinel2_b7_20190919",
            "sentinel2_b8_20190919",
            "sentinel2_b8a_20190919",
            "sentinel2_b11_20190919",
            "sentinel2_b12_20190919",
            "sentinel2_b2_20190929",
            "sentinel2_b3_20190929",
            "sentinel2_b4_20190929",
            "sentinel2_b5_20190929",
            "sentinel2_b6_20190929",
            "sentinel2_b7_20190929",
            "sentinel2_b8_20190929",
            "sentinel2_b8a_20190929",
            "sentinel2_b11_20190929",
            "sentinel2_b12_20190929",
            "sentinel2_ndvi_20190909",
            "sentinel2_ndvi_20190919",
            "sentinel2_ndvi_20190929",
            "sentinel2_ndwi_20190909",
            "sentinel2_ndwi_20190919",
            "sentinel2_ndwi_20190929",
            "sentinel2_brightness_20190909",
            "sentinel2_brightness_20190919",
            "sentinel2_brightness_20190929",
        ]

        # training
        e = None
        try:
            train_autocontext(
                parameter_dict,
                data_field="code",
                output_path=test_path,
                features_list_name=features_list_name,
                superpix_data_field="superpix",
                iterations=3,
                working_directory=autocontext_working_dir,
            )
        except Exception as e:
            print(e)

        # Asserts training
        assert e is None, "train_autocontext failed"

        models = file_search_and(
            str(Path(test_path) / "model"), True, "model_it_", ".rf"
        )
        assert len(models) == 4

        # classification
        tile_raster = file_search_and(self.fake_data_dir, True, "BINARY_MASK.tif")[0]
        tile_mask = str(
            Path(autocontext_working_dir) / "{}_tile_mask.tif".format(self.tile_name)
        )
        CreateBandMathApplication(
            {"il": [tile_raster], "out": tile_mask, "exp": "1"}
        ).ExecuteAndWriteOutput()

        labels = get_field_element(
            train_auto_data_ref,
            driver_name="SQLite",
            field="code",
            mode="unique",
            elem_type="str",
        )
        parameters_dict = {
            "model_name": "1",
            "seed_num": 0,
            "tile": self.tile_name,
            "tile_segmentation": slic_seg,
            "tile_mask": tile_mask,
            "labels_list": labels,
            "model_list": sorted(
                models, key=lambda x: int(re.findall("\d", os.path.basename(x))[0])
            ),
        }

        dst_sample_sel = str(
            Path(test_path) / "samplesSelection" / "samples_region_1_seed_0.shp"
        )
        run(f"ogr2ogr {dst_sample_sel} {train_auto_data_ref}")

        autocontext_launch_classif(
            parameters_dict=parameters_dict,
            classifier_type=cfg.getParam("arg_train", "classifier"),
            tile="T31TCJ",
            proba_map_expected=cfg.getParam(
                "arg_classification", "enable_probability_map"
            ),
            dimred=cfg.getParam("dim_red", "dim_red"),
            data_field="code",
            write_features=False,
            reduction_mode=False,
            iota2_run_dir=test_path,
            sar_optical_post_fusion=False,
            sensors_parameters=sensors_parameters,
            ram=128,
            working_directory=autocontext_working_dir,
        )

        # Asserts classifications
        classif = file_search_and(
            str(Path(test_path) / "classif"), True, "Classif_T31TCJ_model_1_seed_0.tif"
        )[0]
        confidence = file_search_and(
            str(Path(test_path) / "classif"),
            True,
            "T31TCJ_model_1_confidence_seed_0.tif",
        )[0]

        classif_unique_0 = is_raster_unique_value(classif, 0)
        confidence_unique_0 = is_raster_unique_value(confidence, 0)
        assert classif_unique_0 == False, (
            "AutoContext Classifications failed : classification contains"
            " only 0 values"
        )
        assert confidence_unique_0 == False, (
            "AutoContext Classifications failed : confidence contains " "only 0 values"
        )
