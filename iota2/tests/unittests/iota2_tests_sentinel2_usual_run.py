#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest running_Tests
"""
Tests dedicated to launch iota2 runs
"""
import logging
import os
import shutil
from pathlib import Path

import pytest

import iota2.tests.utils.tests_utils_rasters as TUR
from config import Config
from iota2.Iota2 import run
from iota2.tests.utils.tests_utils_files import check_expected_i2_results
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR

LOGGER = logging.getLogger("distributed.worker")


class Iota2FullS2Run:
    """Tests dedicated to launch iota2 runs."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.config_ref = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "i2_config.cfg")
        cls.config_ref_scikit = os.path.join(IOTA2DIR, "data", "references",
                                             "running_iota2",
                                             "i2_config_scikit.cfg")
        cls.ground_truth_path = os.path.join(IOTA2DIR, "data", "references",
                                             "running_iota2",
                                             "ground_truth.shp")
        cls.nomenclature_path = os.path.join(IOTA2DIR, "data", "references",
                                             "running_iota2",
                                             "nomenclature.txt")
        cls.color_path = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "color.txt")

    def test_too_few_s2_data(self, i2_tmpdir, rm_tmpdir_on_success):
        """
        Tests iota2's run using s2 (theia format)
        and perform data augmentation algorithm
        """
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0)
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test)).as_dict()
        cfg_test["chain"]["output_path"] = running_output_path
        cfg_test["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test["chain"]["data_field"] = "code"
        cfg_test["chain"]["spatial_resolution"] = 10
        cfg_test["chain"]["list_tile"] = tile_name
        cfg_test["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test["chain"]["minimum_required_dates"] = 666
        cfg_test["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test["chain"]["color_table"] = self.color_path

        with open(config_test, "w") as file_cfg:
            file_cfg.write(str(cfg_test))

        # Launch the chain
        pytest.raises(ValueError, run, config_test, None, "debug", 1, 20,
                      False, None, [], False, 1, None)

    # Tests definitions
    def test_usual_s2_theia_run(self, i2_tmpdir, rm_tmpdir_on_success):
        """
        Tests iota2's run using s2 (theia format)
        and perform data augmentation algorithm
        """
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0)
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test)).as_dict()
        cfg_test["chain"]["output_path"] = running_output_path
        cfg_test["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test["chain"]["data_field"] = "code"
        cfg_test["chain"]["spatial_resolution"] = 10
        cfg_test["chain"]["list_tile"] = tile_name
        cfg_test["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg_test["chain"]["color_table"] = self.color_path

        with open(config_test, "w") as file_cfg:
            file_cfg.write(str(cfg_test))

        env_var_name = "WDIR"
        working_directory = str(i2_tmpdir / "working_dir")
        os.mkdir(working_directory)
        os.environ[env_var_name] = working_directory
        # Launch the chain
        run(config_test, None, "debug", 1, 20, False, None, [], False, 1,
            env_var_name)
        check_expected_i2_results(Path(running_output_path))

        assert Path.exists(
            Path(running_output_path) / "learningSamples" /
            "class_statistics_seed0_learn.csv")
