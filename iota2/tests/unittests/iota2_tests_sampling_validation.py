#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import shutil
import sqlite3
from pathlib import Path

import geopandas as gpd
import xmltodict

from iota2.common.raster_utils import ChunkConfig
from iota2.common.utils import run
from iota2.configuration_files import read_config_file as rcf
from iota2.sampling import (
    samples_selection,
    samples_stat,
    vector_formatting,
    vector_sampler,
)
from iota2.tests.utils import tests_utils_vectors as TUV
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.vector_tools.vector_functions import copy_shapefile, list_value_fields


class Iota2TestSamplingWorkflow(AssertsFilesUtils):
    """Non regression tests which check stats, samples selection/extraction."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.ref_data_folder = str(
            IOTA2DIR
            / "data"
            / "references"
            / "Runs_standards"
            / "Usual_s2_theia_run"
            / "test_results"
        )
        cls.s2ref_data_folder = str(
            IOTA2DIR
            / "data"
            / "references"
            / "Runs_standards"
            / "Usual_s2_theia_run"
            / "s2_data"
        )
        cls.config_ref = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "i2_config.cfg"
        )
        cls.ground_truth_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "ground_truth.shp"
        )
        cls.ground_truth_ndvi_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "ground_truth_ndvi.shp"
        )
        cls.nomenclature_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "nomenclature.txt"
        )
        cls.color_path = str(
            IOTA2DIR / "data" / "references" / "running_iota2" / "color.txt"
        )

    def test_compute_stats(self, i2_tmpdir, rm_tmpdir_on_success):
        """Check stats from otb."""
        cmd = f"cp -r {self.ref_data_folder}/* {str(i2_tmpdir)}"
        run(cmd)

        shutil.rmtree(
            i2_tmpdir / "samplesSelection" / "T31TCJ_region_1_seed_0_stats_val.xml",
            ignore_errors=True,
        )
        samples_stat.samples_stats(
            region_seed_tile=["1", "0", "T31TCJ"],
            iota2_directory=str(i2_tmpdir),
            data_field="i2label",
            sampling_validation=True,
            working_directory=None,
        )

        self.assert_file_exist(
            i2_tmpdir / "samplesSelection" / "T31TCJ_region_1_seed_0_stats_val.xml"
        )
        with open(
            str(i2_tmpdir / "samplesSelection" / "T31TCJ_region_1_seed_0_stats_val.xml")
        ) as xml_run:
            dict_run = xmltodict.parse(xml_run.read())
        with open(
            str(
                Path(self.ref_data_folder)
                / "samplesSelection"
                / "T31TCJ_region_1_seed_0_stats_val.xml"
            )
        ) as xml_ref:
            dict_ref = xmltodict.parse(xml_ref.read())
        assert dict_ref == dict_run

    def test_selection(self, i2_tmpdir, rm_tmpdir_on_success):
        """Samples_selection.samples_selection non regression test."""
        cmd = f"cp -r {self.ref_data_folder}/* {str(i2_tmpdir)}"
        run(cmd)

        shutil.rmtree(
            i2_tmpdir
            / "samplesSelection"
            / "T31TCJ_samples_region_1_val_seed_0_selection.sqlite",
            ignore_errors=True,
        )
        samples_selection.samples_selection(
            model=str(i2_tmpdir / "samplesSelection" / "samples_region_1_seed_0.shp"),
            working_directory=str(i2_tmpdir),
            output_path=str(i2_tmpdir),
            runs=1,
            epsg="EPSG:2154",
            masks_name="MaskCommunSL.tif",
            parameters={"sampler": "random", "strategy": "all"},
            data_field="i2label",
            sampling_validation=True,
            parameters_validation={"sampler": "random", "strategy": "all"},
            random_seed=1,
        )
        con = sqlite3.connect(
            str(
                i2_tmpdir
                / "samplesSelection"
                / "T31TCJ_samples_region_1_val_seed_0_selection.sqlite"
            )
        )
        query = "SELECT * FROM T31TCJ_samples_region_1_val_seed_0_selection"
        df_run = gpd.read_postgis(
            sql=query, con=con, geom_col="geometry", crs={"init": "EPSG:2154"}
        )
        con2 = sqlite3.connect(
            str(
                Path(self.ref_data_folder)
                / "samplesSelection"
                / "T31TCJ_samples_region_1_val_seed_0_selection.sqlite"
            )
        )

        df_ref = gpd.read_postgis(
            sql=query, con=con2, geom_col="geometry", crs={"init": "EPSG:2154"}
        )

        assert len(df_ref.index) == len(df_run.index)

    def test_validation_extraction(self, i2_tmpdir, rm_tmpdir_on_success):
        """Check extraction."""
        cmd = f"cp -r {self.ref_data_folder}/* {str(i2_tmpdir)}"
        run(cmd)

        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg = rcf.read_internal_config_file(config_test)
        cfg.cfg_as_dict["chain"]["output_path"] = str(i2_tmpdir)
        cfg.cfg_as_dict["chain"]["s2_path"] = self.s2ref_data_folder
        cfg.cfg_as_dict["chain"]["data_field"] = "code"
        cfg.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg.cfg_as_dict["chain"]["list_tile"] = "T31TCJ"
        cfg.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg.cfg_as_dict["chain"]["nomenclature_path"] = self.nomenclature_path
        cfg.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg.save(config_test)

        folder_sample = str(i2_tmpdir / "learningSamples")
        shutil.rmtree(
            Path(folder_sample) / "T31TCJ_region_1_seed0_Samples_learn.sqlite",
            ignore_errors=True,
        )
        shutil.rmtree(
            Path(folder_sample) / "T31TCJ_region_1_seed0_Samples_val.sqlite",
            ignore_errors=True,
        )
        vector_sampler.generate_samples_simple(
            folder_sample=folder_sample,
            working_directory=folder_sample,
            train_shape=str(
                i2_tmpdir / "samplesSelection" / "T31TCJ_selection_merge.sqlite"
            ),
            path_wd=None,
            data_field="i2label",
            region_field="region",
            output_path=str(i2_tmpdir),
            runs=1,
            proj="EPSG:2154",
            sensors_parameters=rcf.iota2_parameters(
                rcf.read_config_file(config_test)
            ).get_sensors_parameters("T31TCJ"),
            sar_optical_post_fusion=False,
            sampling_validation=True,
            chunk_config=ChunkConfig(
                chunk_size_mode="split_number",
                number_of_chunks=50,
                chunk_size_x=None,
                chunk_size_y=None,
            ),
            ram=128,
            w_mode=False,
            folder_features=None,
            sample_sel=None,
            mode="usually",
            custom_features=False,
            module_path=None,
            list_functions=None,
            force_standard_labels=False,
            concat_mode=True,
            features_from_raw_dates=False,
            multi_param_cust={
                "enable_raw": False,
                "enable_gap": True,
                "fill_missing_dates": False,
                "all_dates_dict": None,
                "mask_valid_data": None,
                "mask_value": 0,
                "exogeneous_data": None,
            },
        )
        self.assert_file_exist(
            Path(folder_sample) / "T31TCJ_region_1_seed0_Samples_val.sqlite"
        )

        self.assert_file_exist(
            Path(folder_sample) / "T31TCJ_region_1_seed0_Samples_learn.sqlite"
        )

        assert TUV.compare_sqlite(
            str(Path(folder_sample) / "T31TCJ_region_1_seed0_Samples_val.sqlite"),
            str(
                Path(self.ref_data_folder)
                / "learningSamples"
                / "T31TCJ_region_1_seed0_Samples_val.sqlite"
            ),
            cmp_mode="table",
        )

    def test_extract_maj_vote_samples(self, i2_tmpdir, rm_tmpdir_on_success):

        ground_truth_ndvi_temp = str(i2_tmpdir / "ground_truth_ndvi.shp")
        copy_shapefile(
            self.ground_truth_ndvi_path,
            ground_truth_ndvi_temp,
        )
        vector_formatting.extract_maj_vote_samples(
            ground_truth_ndvi_temp,
            str(i2_tmpdir / "test_majvote_samples.sqlite"),
            0.5,
            data_field="split",
            region_field=0,
        )

        listid_input = list_value_fields(ground_truth_ndvi_temp, "id")
        listid_val = list_value_fields(
            str(i2_tmpdir / "test_majvote_samples.sqlite"),
            "id",
            driver="SQLite",
        )

        assert set(listid_input).isdisjoint(
            listid_val
        ), "merge validation set intersects training set"
