#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Tests dedicated to launch iota2 runs."""

import shutil
from pathlib import Path

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.configuration_files import read_config_file as rcf
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_launcher import run_cmd


class Iota2TestFeaturesMap(AssertsFilesUtils):
    """
    Tests dedicated to launch iota2 runs
    """

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.config_ref = str(IOTA2DIR / "data" / "references" /
                             "running_iota2" / "i2_config_feat_map.cfg")
        cls.dhi_module = str(IOTA2DIR / "data" / "references" /
                             "running_iota2" / "dhi.py")

    def test_dhi_run(self, i2_tmpdir, rm_tmpdir_on_success):
        """Test dhi map generation."""
        tile_name = "T31TCJ"
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0)
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        config_test = str(i2_tmpdir / "i2_config_dhi_map.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["python_data_managing"]["number_of_chunks"] = 2
        cfg_test.cfg_as_dict["external_features"]["module"] = self.dhi_module
        cfg_test.cfg_as_dict["external_features"]["functions"] = (
            "get_cumulative_productivity "
            "get_minimum_productivity "
            "get_seasonal_variation")
        cfg_test.save(config_test)

        final_dhi_raster = Path(
            running_output_path) / "final" / "features_map.tif"
        if Path.exists(final_dhi_raster):
            Path.unlink(final_dhi_raster)
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type debug"
        run_cmd(cmd)
        self.assert_file_exist(final_dhi_raster)
