#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Tests dedicated to launch iota2 runs."""
import os
import shutil
from pathlib import Path

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.configuration_files import read_config_file as rcf
from iota2.tests.utils.tests_utils_files import check_expected_i2_results
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_launcher import run_cmd


class Iota2TestSpatialRes:
    """Tests dedicated to launch iota2 runs."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.config_ref = str(IOTA2DIR / "data" / "references" /
                             "running_iota2" / "i2_config.cfg")
        cls.config_ref_scikit = str(IOTA2DIR / "data" / "references" /
                                    "running_iota2" / "i2_config_scikit.cfg")
        cls.ground_truth_path = str(IOTA2DIR / "data" / "references" /
                                    "running_iota2" / "ground_truth.shp")
        cls.nomenclature_path = str(IOTA2DIR / "data" / "references" /
                                    "running_iota2" / "nomenclature.txt")
        cls.color_path = str(IOTA2DIR / "data" / "references" /
                             "running_iota2" / "color.txt")

    def test_s2_theia_run_30m(self, i2_tmpdir, rm_tmpdir_on_success):
        """Tests iota2's run using s2 (theia format)."""
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0,
                                  array_name="iota2_binary",
                                  oversized=True)
        config_test = str(i2_tmpdir / "i2_config_s2_l2a.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"][
            "nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = [30, 30]
        cfg_test.save(config_test)

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 19 -scheduler_type debug"
        run_cmd(cmd)

        expected_files, details = check_expected_i2_results(
            Path(running_output_path) / "final")
        assert expected_files, details

    def test_spot6_run(self, i2_tmpdir, rm_tmpdir_on_success):
        """
        Tests iota2's run using spot6 (user feature)
        """
        # prepare inputs data
        tile_name = "T32TLR"
        running_output_path = str(i2_tmpdir / "test_results")
        config_test = str(i2_tmpdir / "i2_config_spot6.cfg")
        shutil.copy(str(IOTA2DIR / "data" / "spot_6_data" / "config_thrs.cfg"),
                    config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["user_feat_path"] = os.path.join(
            IOTA2DIR, "data", "spot_6_data", "images")
        cfg_test.cfg_as_dict["userFeat"]["arbo"] = "/*"
        cfg_test.cfg_as_dict["userFeat"]["patterns"] = "spot6_sub"
        cfg_test.cfg_as_dict["chain"]["data_field"] = "coden2"
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = os.path.join(
            IOTA2DIR, "data", "spot_6_data", "fake_samples.shp")
        cfg_test.cfg_as_dict["chain"]["nomenclature_path"] = os.path.join(
            IOTA2DIR, "data", "spot_6_data", "Nomenclature_thrs")
        cfg_test.cfg_as_dict["chain"]["color_table"] = os.path.join(
            IOTA2DIR, "data", "spot_6_data", "colorFile_thrs")
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = [1.5, 1.5]
        cfg_test.save(config_test)

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 19 -scheduler_type debug"
        run_cmd(cmd)
        expected_files, details = check_expected_i2_results(
            Path(running_output_path) / "final")
        assert expected_files, details
