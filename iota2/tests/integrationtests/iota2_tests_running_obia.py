#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Tests dedicated to launch iota2 runs
"""
import os
import shutil
import sys
from pathlib import Path

import pytest

import iota2.tests.utils.tests_utils_files as TUF
import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.configuration_files import read_config_file as rcf
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_launcher import run_cmd

IOTA2DIR = Path(os.environ.get('IOTA2DIR'))

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")

IOTA2_SCRIPTS = IOTA2DIR / "iota2"
sys.path.append(str(IOTA2_SCRIPTS))


@pytest.mark.obia
class Iota2OBIARunsCase(AssertsFilesUtils):
    """Tests dedicated to launch OBIA runs."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.config_ref = str(IOTA2DIR / "data" / "references" /
                             "running_iota2" / "i2_obia.cfg")

        cls.ground_truth_path = str(IOTA2DIR / "data" / "OBIA" /
                                    "reference_obia.shp")
        cls.region_path = str(IOTA2DIR / "data" / "references" /
                              "running_iota2" / "region.shp")

        cls.nomenclature_path = str(IOTA2DIR / "data" / "references" /
                                    "running_iota2" / "nomenclature.txt")
        cls.color_path = str(IOTA2DIR / "data" / "references" /
                             "running_iota2" / "color.txt")
        cls.segmentation = str(IOTA2DIR / "data" / "OBIA" / "reference_seg" /
                               "seg_T31TCJ.tif")

    def test_obia_s2_theia_runs(self, i2_tmpdir, rm_tmpdir_on_success):
        """Tests iota2's run using s2 (theia format)."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0,
                                  array_name="ones",
                                  seed=0)
        config_test = str(i2_tmpdir / "i2_obia.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["arg_train"]["random_seed"] = 0
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"][
            "nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["builders"]["builders_class_name"] = ["i2_obia"]
        cfg_test.cfg_as_dict["obia"][
            "obia_segmentation_path"] = self.segmentation
        cfg_test.cfg_as_dict["obia"]["buffer_size"] = 50
        cfg_test.save(config_test)

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type debug"
        run_cmd(cmd)

        self.assert_file_exist(
            Path(running_output_path) / "final" / "Classif_Seed_0.sqlite")
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=False,
            classif_color=False,
            confidence=False,
            pixel_val=False,
            diff_seed=False)
        assert expected_files, details

    def test_obia_regions_s2_theia_runs(self, i2_tmpdir, rm_tmpdir_on_success):
        """Tests iota2's run using s2 (theia format) with regions."""
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0,
                                  array_name="ones",
                                  seed=0)
        config_test = str(i2_tmpdir / "i2_obia.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["arg_train"]["random_seed"] = 0
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["chain"]["region_path"] = self.region_path
        cfg_test.cfg_as_dict["chain"][
            "nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["builders"]["builders_class_name"] = ["i2_obia"]
        cfg_test.cfg_as_dict["obia"]["buffer_size"] = 50
        cfg_test.cfg_as_dict["obia"][
            "obia_segmentation_path"] = self.segmentation
        cfg_test.save(config_test)

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type debug"
        run_cmd(cmd)

        self.assert_file_exist(
            Path(running_output_path) / "final" / "Classif_Seed_0.sqlite")
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=False,
            classif_color=False,
            confidence=False,
            pixel_val=False,
            diff_seed=False)
        assert expected_files, details

    def test_obia_s2_theia_full_segment_runs(self, i2_tmpdir,
                                             rm_tmpdir_on_success):
        """Tests iota2's run using s2 (theia format) and provide raster segmentation."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0,
                                  array_name="ones",
                                  seed=0)
        config_test = str(i2_tmpdir / "i2_obia.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["arg_train"]["random_seed"] = 0
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"][
            "nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["builders"]["builders_class_name"] = ["i2_obia"]
        cfg_test.cfg_as_dict["obia"]["buffer_size"] = 50
        cfg_test.cfg_as_dict["obia"][
            "obia_segmentation_path"] = self.segmentation
        cfg_test.cfg_as_dict["obia"]["full_learn_segment"] = True
        cfg_test.save(config_test)

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type debug"
        run_cmd(cmd)

        self.assert_file_exist(
            Path(running_output_path) / "final" / "Classif_Seed_0.sqlite")
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=False,
            classif_color=False,
            confidence=False,
            pixel_val=False,
            diff_seed=False)
        assert expected_files, details

    def test_obia_s2_theia_input_vector_runs(self, i2_tmpdir,
                                             rm_tmpdir_on_success):
        """Tests iota2's run using s2 (theia format) and provide vector segmentation."""
        # prepare inputs data
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0,
                                  array_name="ones",
                                  seed=0)
        config_test = str(i2_tmpdir / "i2_obia.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["arg_train"]["random_seed"] = 0
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"]["region_field"] = "region"
        cfg_test.cfg_as_dict["chain"]["region_path"] = self.region_path
        cfg_test.cfg_as_dict["chain"][
            "nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["builders"]["builders_class_name"] = ["i2_obia"]
        cfg_test.cfg_as_dict["obia"]["buffer_size"] = 50
        cfg_test.cfg_as_dict["obia"]["obia_segmentation_path"] = str(
            IOTA2DIR / "data" / "OBIA" / "reference_seg" / "seg_T31TCJ.shp")
        cfg_test.cfg_as_dict["obia"]["full_learn_segment"] = True
        cfg_test.save(config_test)

        # Launch the chain
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type debug"
        run_cmd(cmd)

        self.assert_file_exist(
            Path(running_output_path) / "final" / "Classif_Seed_0.sqlite")
        expected_files, details = TUF.check_expected_i2_results(
            Path(running_output_path) / "final",
            classif=False,
            classif_color=False,
            confidence=False,
            pixel_val=False,
            diff_seed=False)
        assert expected_files, details
