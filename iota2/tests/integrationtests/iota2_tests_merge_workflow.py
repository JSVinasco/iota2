#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Tests dedicated to launch iota2 runs."""

import shutil
from pathlib import Path

import iota2.tests.utils.tests_utils_rasters as TUR
from iota2.configuration_files import read_config_file as rcf
from iota2.tests.utils.asserts_utils import AssertsFilesUtils
from iota2.tests.utils.tests_utils_files import check_expected_i2_results
from iota2.tests.utils.tests_utils_iota2dir import IOTA2DIR
from iota2.tests.utils.tests_utils_launcher import run_cmd


class Iota2TestsClassifThenVectorization(AssertsFilesUtils):
    """Tests dedicated to launch iota2 runs."""

    @classmethod
    def setup_class(cls):
        """Variables shared between tests."""
        cls.config_ref = str(IOTA2DIR / "data" / "references" /
                             "running_iota2" / "i2_config_vecto.cfg")
        cls.nomenclature_vecto = str(IOTA2DIR / "data" / "references" /
                                     "running_iota2" / "nomenclature.cfg")
        cls.clip_file = str(IOTA2DIR / "data" / "references" /
                            "running_iota2" / "fake_region.shp")
        cls.nomenclature_path = str(IOTA2DIR / "data" / "references" /
                                    "running_iota2" / "nomenclature.txt")
        cls.ground_truth_path = str(IOTA2DIR / "data" / "references" /
                                    "running_iota2" / "ground_truth.shp")
        cls.color_path = str(IOTA2DIR / "data" / "references" /
                             "running_iota2" / "color.txt")

    def test_classification_then_vectorization(self, i2_tmpdir,
                                               rm_tmpdir_on_success):
        """Test : merge classification and vectorization workflow."""
        tile_name = "T31TCJ"
        running_output_path = str(i2_tmpdir / "test_run_results")
        fake_s2_theia_dir = str(i2_tmpdir / "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0)
        config_test = str(i2_tmpdir / "i2_config_s2_l2a_classif_vecto.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = rcf.read_internal_config_file(config_test)
        cfg_test.cfg_as_dict["chain"]["output_path"] = running_output_path
        cfg_test.cfg_as_dict["chain"]["s2_path"] = fake_s2_theia_dir
        cfg_test.cfg_as_dict["simplification"]["gridsize"] = 2
        cfg_test.cfg_as_dict["chain"]["data_field"] = "code"
        cfg_test.cfg_as_dict["chain"]["spatial_resolution"] = 10
        cfg_test.cfg_as_dict["chain"]["list_tile"] = tile_name
        cfg_test.cfg_as_dict["chain"]["ground_truth"] = self.ground_truth_path
        cfg_test.cfg_as_dict["chain"][
            "nomenclature_path"] = self.nomenclature_path
        cfg_test.cfg_as_dict["chain"]["color_table"] = self.color_path
        cfg_test.cfg_as_dict["arg_train"]["classifier"] = "sharkrf"
        cfg_test.cfg_as_dict["builders"]["builders_class_name"] = [
            "i2_classification", "i2_vectorization"
        ]
        cfg_test.cfg_as_dict["simplification"][
            "nomenclature"] = self.nomenclature_vecto
        cfg_test.cfg_as_dict["simplification"]["outprefix"] = "departement_"
        cfg_test.save(config_test)

        # run iota2
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 33 -scheduler_type debug"
        run_cmd(cmd)

        # asserts -> check if files exists
        # classification final products
        expected_files, details = check_expected_i2_results(
            Path(running_output_path) / "final")
        assert expected_files, details

        # vectorization final products
        expected_vecto_files = [
            "departement_0.prj", "departement_0.shx", "departement_0.dbf",
            "departement_0.shp", "departement_0.zip"
        ]
        for vecto_file_name in expected_vecto_files:
            expected_file = Path(
                running_output_path) / "vectors" / vecto_file_name
            self.assert_file_exist(expected_file)
