#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Generate the synthesis report from confusion matrices."""
import argparse
import logging
import os
import sys
from pathlib import Path
from typing import Dict, List, Optional

from iota2.common import file_utils as fu
from iota2.validation import results_utils as resU
from iota2.vector_tools.vector_functions import get_reverse_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


def find_entry_for_gen_results(in_folder_list: List[Path],
                               iota2_directory: Path, path_nomenclature,
                               output_txt_path_list, output_png_path_list,
                               labels_conversion_table):
    """Provide the input files from iota2 folders.

    Parameters
    ----------
    iota2_directory:
        iota2 output folder
    path_nomenclature:
        nomenclature file
    output_txt_file:
        output text report name
    output_png_file:
        fodler to store output confusion matrix as png
    labels_conversion_table:
        dict to apply the label conversion if needed
    prefix:
        name to get the correct matrices as input
    """
    # get a copy for concatenate_sample_selection
    i2_labels_to_user_labels = labels_conversion_table.copy()
    for in_folder, output_txt_path, output_png_path in zip(
            in_folder_list, output_txt_path_list, output_png_path_list):
        all_csv_in = fu.file_search_and(in_folder, True, "Classif_Seed",
                                        ".csv")
        print(all_csv_in)
        output_txt_file = os.path.join(output_txt_path, "RESULTS.txt")
        gen_results(all_csv_in, path_nomenclature, output_txt_file,
                    output_png_path, labels_conversion_table)
    # merge samples selection csv
    resU.merge_all_sample_selection(iota2_directory, path_nomenclature,
                                    i2_labels_to_user_labels)


def gen_results(all_csv_in: List[str], path_nom: str, output_txt,
                output_png_path, labels_conversion_table: Dict) -> None:
    """
    Generate IOTA² final report.

    Parameters
    ----------
    all_csv_in:
        list of input csv
    path_nom:

    labels_conversion_table
    """
    # Handle the case the originals labels are string
    # If all labels are int, the maps was already decoded
    # then the conversion table is set to None
    lab_conv_tab: Optional[Dict] = labels_conversion_table
    all_castable = []
    for _, user_label in labels_conversion_table.items():
        try:
            _ = int(user_label)
            all_castable.append(True)
        except ValueError:
            all_castable.append(False)
    re_encode_labels = all(all_castable)
    if re_encode_labels:
        lab_conv_tab = None

    resU.stats_report(all_csv_in,
                      path_nom,
                      output_txt,
                      labels_table=lab_conv_tab)

    for seed_csv in all_csv_in:
        name, _ = os.path.splitext(os.path.basename(seed_csv))
        out_png = os.path.join(output_png_path, f"Confusion_Matrix_{name}.png")
        resU.gen_confusion_matrix_fig(seed_csv,
                                      out_png,
                                      path_nom,
                                      undecidedlabel=None,
                                      dpi=200,
                                      write_conf_score=True,
                                      grid_conf=True,
                                      conf_score="count_sci",
                                      threshold=0.1,
                                      labels_table=lab_conv_tab)


def main():
    """Provide arguments to conda entry_points."""
    parser = argparse.ArgumentParser(
        description=("Merge all seed confusion matrix in a single "
                     "text file report and create a PNG matrix for each seed"))
    parser.add_argument("-input_csv_files",
                        help=("List of csv files"
                              "One per seed."),
                        dest="all_csv",
                        required=True)
    parser.add_argument("-path_nomenclature",
                        help="path to the nomenclature file",
                        dest="path_nomenclature",
                        required=True)
    parser.add_argument("-output_txt",
                        help="Output summary text file",
                        dest="output_txt",
                        required=True)
    parser.add_argument("-path_reference_data",
                        help="path to the vector reference data file",
                        dest="reference_data",
                        required=True)
    parser.add_argument("-data_field",
                        help="Label column name in the reference data file",
                        dest="output_txt",
                        required=True)

    parser.add_argument(
        "-output_png_path",
        type=lambda p: Path(p).absolute(),
        required=True,
        help="Path to directory where confusion matrix will be wrote",
    )
    args = parser.parse_args()

    i2_labels_to_user_labels = get_reverse_encoding_labels_dic(
        args.reference_data, args.reference_data_field)
    gen_results(args.all_csv, args.path_nomenclature, args.output_txt,
                args.output_png_path, i2_labels_to_user_labels)


if __name__ == "__main__":
    sys.exit(main())
