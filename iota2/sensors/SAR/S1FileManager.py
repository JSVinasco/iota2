#!/usr/bin/env python3
#-*- coding: utf-8 -*-
# =========================================================================
#   Program:   S1Processor
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
#
# Authors: Thierry KOLECK (CNES)
#
# =========================================================================

import configparser
import glob
import os

from osgeo import ogr


class S1FileManager(object):
    def __init__(self, configFile):

        config = configparser.ConfigParser()
        config.read(configFile)

        self.mgrs_shapeFile = config.get('Processing', 'TilesShapefile')
        if os.path.exists(self.mgrs_shapeFile) == False:
            print(("ERROR: " + self.mgrs_shapeFile + " is a wrong path"))
            exit(1)

        self.srtm_shapeFile = config.get('Processing', 'SRTMShapefile')
        if os.path.exists(self.srtm_shapeFile) == False:
            print(("ERROR: " + self.srtm_shapeFile + " is a wrong path"))
            exit(1)

        self.raw_directory = config.get('Paths', 'S1Images')

        self.outputPreProcess = config.get('Paths', 'Output')

        self.VH_pattern = "measurement/*vh*-???.tiff"
        self.VV_pattern = "measurement/*vv*-???.tiff"
        self.HH_pattern = "measurement/*hh*-???.tiff"
        self.HV_pattern = "measurement/*hv*-???.tiff"
        self.manifest_pattern = "manifest.safe"

        self.tilesList = config.get('Processing', 'Tiles').split(",")

        self.ProcessedFilenames = self.get_processed_filenames()
        self.get_s1_img()
        try:
            os.makedirs(self.raw_directory)
        except:
            pass

    def get_s1_img(self):

        self.rawRasterList = []
        self.NbImages = 0
        if not os.path.exists(self.raw_directory):
            os.makedirs(self.raw_directory)
            return
        content = os.listdir(self.raw_directory)
        for currentContent in content:
            safeDir = os.path.join(self.raw_directory, currentContent)
            if os.path.isdir(safeDir):
                manifest = os.path.join(safeDir, self.manifest_pattern)
                acquisition = S1DateAcquisition(manifest, [])
                vv = [
                    f
                    for f in glob.glob(os.path.join(safeDir, self.VV_pattern))
                ]
                for vvImage in vv:
                    if vvImage not in self.ProcessedFilenames:
                        acquisition.AddImage(vvImage)
                        self.NbImages += 1
                vh = [
                    f
                    for f in glob.glob(os.path.join(safeDir, self.VH_pattern))
                ]
                for vhImage in vh:
                    if vhImage not in self.ProcessedFilenames:
                        acquisition.AddImage(vhImage)
                        self.NbImages += 1
                hh = [
                    f
                    for f in glob.glob(os.path.join(safeDir, self.HH_pattern))
                ]
                for hhImage in hh:
                    if hhImage not in self.ProcessedFilenames:
                        acquisition.AddImage(hhImage)
                        self.NbImages += 1
                hv = [
                    f
                    for f in glob.glob(os.path.join(safeDir, self.HV_pattern))
                ]
                for hvImage in hv:
                    if hvImage not in self.ProcessedFilenames:
                        acquisition.AddImage(hvImage)
                        self.NbImages += 1

                self.rawRasterList.append(acquisition)

    def get_s1_intersect_by_tile(self, tileNameField):
        intersectRaster = []
        driver = ogr.GetDriverByName("ESRI Shapefile")
        dataSource = driver.Open(self.mgrs_shapeFile, 0)
        layer = dataSource.GetLayer()
        found = False
        for currentTile in layer:
            if currentTile.GetField('NAME') == tileNameField:
                found = True
                break
        if not found:
            print("Tile " + str(tileNameField) + " does not exist")
            return intersectRaster

        poly = ogr.Geometry(ogr.wkbPolygon)
        tileFootPrint = currentTile.GetGeometryRef()

        for image in self.rawRasterList:
            manifest = image.getManifest()
            NW, NE, SE, SW = self.get_origin(manifest)

            poly = ogr.Geometry(ogr.wkbPolygon)
            ring = ogr.Geometry(ogr.wkbLinearRing)
            ring.AddPoint(NW[1], NW[0], 0)
            ring.AddPoint(NE[1], NE[0], 0)
            ring.AddPoint(SE[1], SE[0], 0)
            ring.AddPoint(SW[1], SW[0], 0)
            ring.AddPoint(NW[1], NW[0], 0)
            poly.AddGeometry(ring)

            intersection = poly.Intersection(tileFootPrint)
            if intersection.GetArea() != 0:
                area_polygon = tileFootPrint.GetGeometryRef(0)
                points = area_polygon.GetPoints()
                intersectRaster.append(
                    (image, [(point[0], point[1]) for point in points[:-1]]))

        return intersectRaster

    def get_mgrs_tile_geometry_by_name(self, mgrsTileName):
        driver = ogr.GetDriverByName("ESRI Shapefile")
        mgrs_ds = driver.Open(self.mgrs_shapeFile, 0)
        mgrs_layer = mgrs_ds.GetLayer()

        for mgrsTile in mgrs_layer:
            if mgrsTile.GetField('NAME') == mgrsTileName:
                return mgrsTile.GetGeometryRef().Clone()
        raise Exception("MGRS tile does not exist " + mgrsTileName)

    def check_srtm_coverage(self, tilesToProcess):
        driver = ogr.GetDriverByName("ESRI Shapefile")
        srtm_ds = driver.Open(self.srtm_shapeFile, 0)
        srtm_layer = srtm_ds.GetLayer()

        needed_srtm_tiles = {}

        for tile in tilesToProcess:
            srtm_tiles = []
            mgrs_footprint = self.get_mgrs_tile_geometry_by_name(tile)
            area = mgrs_footprint.GetArea()
            srtm_layer.ResetReading()
            for srtm_tile in srtm_layer:
                srtm_footprint = srtm_tile.GetGeometryRef()
                intersection = mgrs_footprint.Intersection(srtm_footprint)
                if intersection.GetArea() > 0:
                    coverage = intersection.GetArea() / area
                    srtm_tiles.append((srtm_tile.GetField('FILE'), coverage))
            needed_srtm_tiles[tile] = srtm_tiles
        return needed_srtm_tiles

    def get_processed_filenames(self):
        try:
            with open(
                    os.path.join(self.outputPreProcess,
                                 "ProcessedFilenames.txt"), "r") as f:
                #return f.read().splitlines()
                return []
        except:
            pass
            return []

    def get_origin(self, manifest):
        with open(manifest, "r") as saveFile:
            for line in saveFile:
                if "<gml:coordinates>" in line:
                    coor = line.replace("                <gml:coordinates>",
                                        "").replace("</gml:coordinates>",
                                                    "").split(" ")
                    coord = [(float(val.replace("\n", "").split(",")[0]),
                              float(val.replace("\n", "").split(",")[1]))
                             for val in coor]
                    return coord[0], coord[1], coord[2], coord[3]
            raise Exception("Coordinates not found in " + str(manifest))


class S1DateAcquisition(object):
    def __init__(self, manifest, imageFilenamesList):
        self.manifest = manifest
        self.imageFilenamesList = imageFilenamesList
        self.calibrationApplication = None  #composed by [[calib,calibDependance],[...]...]

    def getManifest(self):
        return self.manifest

    def AddImage(self, ImageList):
        self.imageFilenamesList.append(ImageList)

    def GetImageList(self):
        return self.imageFilenamesList

    def SetCalibrationApplication(self, calib):
        """
      calib must be compose as the following
      calib = [(calibApplication,dependences),(...)...]
      """
        self.calibrationApplication = calib

    def GetCalibrationApplication(self):
        return self.calibrationApplication
