#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Provide function to tile the S1 acquisitions."""
import configparser
import logging
import os
import shutil
import sys
from functools import partial
from typing import List, Optional

import iota2.common.i2_constants as i2_const
from iota2.common import file_utils as fut
from iota2.common import otb_app_bank
from iota2.sensors.SAR import S1FileManager as s1_manager
from iota2.sensors.SAR import S1FilteringProcessor

# from osgeo import osr
# from osgeo.gdalconst import *

# from osgeo.gdalconst import *

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.Iota2Constants()


def get_orbit_direction(manifest: str):
    """Get the orbit from manifest file."""
    with open(manifest, "r", encoding="UTF-8") as save_file:
        for line in save_file:
            if "<s1:pass>" in line:
                if "DESCENDING" in line:
                    return "DES"
                if "ASCENDING" in line:
                    return "ASC"
        raise Exception("Orbit Directiction not found in " + str(manifest))


def get_platform_from_s1_raster(path_to_raster):
    """Get the platform from raster name."""
    return path_to_raster.split("/")[-1].split("-")[0]


class Sentinel1PreProcess:
    """Define a Sentinel 1 pipeline for preprocessing."""

    def __init__(self, config_file: str):
        """Initialize the class from a S1 config file."""
        config = configparser.ConfigParser()
        config.read(config_file)
        try:
            os.remove("S1ProcessorErr.log.log")
            os.remove("S1ProcessorOut.log")
        except:
            pass
        self.raw_directory = config.get('Paths', 'S1Images')
        # self.vh_pattern = "measurement/*vh*-???.tiff"
        # self.vv_pattern = "measurement/*vv*-???.tiff"
        self.manifest_pattern = "manifest.safe"
        self.output_preprocess = config.get('Paths', 'Output')
        self.srtm = config.get('Paths', 'SRTM')
        self.geoid = config.get('Paths', 'GeoidFile')

        # self.border_threshold = float(
        #     config.get('Processing',
        #                'BorderThreshold',
        #                fallback=I2_CONST.s1_border_threshold))
        self.ram_per_process = int(
            config.get('Processing',
                       'RAMPerProcess',
                       fallback=I2_CONST.s1_ram_per_process))

        # self.tiles_list = [
        #     s.strip() for s in config.get('Processing', 'Tiles').split(",")
        # ]

        self.references_folder = config.get('Processing', 'ReferencesFolder')
        self.raster_pattern = config.get('Processing', 'RasterPattern')


def sar_float_to_int(filter_application,
                     nb_bands: int,
                     ram_per_process: int,
                     output_format: str = "uint16",
                     db_min: int = -25,
                     db_max: int = 3):
    """Transform float SAR values to integer.

    Parameters
    ----------
    filter_application:
        OTB filtering SAR application
    nb_bands:
        number of bands in the input image of filter_application
    ram_per_process:
        The ram amount allowed for each otb application
    output_format:
        The output integer format 'uint8' or 'uint16'
    db_min:
        The minimum noise value allowed
    db_max:
        The maximum noise value allowed
    """
    min_val_scale = 0
    max_val_scale = "((2^16)-1)"
    if output_format == "uint8":
        max_val_scale = "((2^8)-1)"

    # build expression
    to_db_expression = "10*log10(im1bX)"
    scale_expression = (f"(({max_val_scale}-{min_val_scale})"
                        f"/({db_max}-{db_min}))*({to_db_expression})"
                        f"+({min_val_scale}-(({max_val_scale}-"
                        f"{min_val_scale})*{db_min})/({db_max}-{db_min}))")
    scale_max_val = (2**16) - 1
    scale_min_val = 0
    threshold_expression = (f"{to_db_expression}>{db_max}?{scale_max_val}"
                            f":{to_db_expression}<{db_min}?{scale_min_val}"
                            f":{scale_expression}")

    expression = ";".join([
        threshold_expression.replace("X", str(i + 1)) for i in range(nb_bands)
    ])

    output_path = filter_application.GetParameterValue(
        otb_app_bank.getInputParameterOutput(filter_application))

    convert = otb_app_bank.CreateBandMathXApplication({
        "il":
        filter_application,
        "out":
        output_path,
        "exp":
        expression,
        "ram":
        str(ram_per_process),
        "pixType":
        output_format
    })
    return convert


def write_output_raster(appli_otb,
                        overwrite=True,
                        working_directory=None,
                        dep=None,
                        logger=LOGGER):
    """Write a raster from an OTB application.

    Parameters
    ----------
    appli_otb:
        input otb application
    overwrite:
        enable the overwriting mode to replace existing products
    working_directory:
        temporary folder
    dep:
        the OTB pipeline dependencies
    logger:
        module logger
    """
    out_param = otb_app_bank.getInputParameterOutput(appli_otb)
    out_raster = appli_otb.GetParameterValue(out_param)
    launch_write = True
    dep_list = [appli_otb, dep]
    # Split using "?" to remove optional extended filename
    base_raster_name = out_raster.split("?")[0]
    if os.path.exists(base_raster_name) and not overwrite:
        logger.info(f"{base_raster_name} exists and will not be overwrited")
        launch_write = False

    if working_directory is None and launch_write:
        otb_app_bank.executeApp(appli_otb)
        # appli_otb.ExecuteAndWriteOutput()

    elif launch_write:
        _, out_raster_name = os.path.split(out_raster)
        out_working_dir = os.path.join(working_directory, out_raster_name)
        out_working_dir = out_working_dir.split("?")[0]
        appli_otb.SetParameterString(out_param, out_working_dir)
        # appli_otb.ExecuteAndWriteOutput
        otb_app_bank.executeApp(appli_otb)
        shutil.copy(out_working_dir, base_raster_name)
        if os.path.exists(out_working_dir.replace(".tif", ".geom")):
            shutil.copy(out_working_dir.replace(".tif", ".geom"),
                        out_raster.replace(".tif", ".geom").split("?")[0])
    if not launch_write:
        logger.info(f"{out_raster} already exists and will not be overwrited")
    # release memory
    del dep_list
    return out_raster


def generate_border_mask(data_img: str,
                         out_mask: str,
                         ram_per_process: int = 4000):
    """Generate the border mask for S1 tiled data.

    Parameters
    ----------
    data_img:
        The input image used to create the border mask
    out_mask:
        The name of created mask
    ram_per_process:
        RAM allowed for OTB application
    """
    threshold = 0.0011
    mask = otb_app_bank.CreateBandMathApplication({
        "il": data_img,
        "exp": f"im1b1<{threshold}?1:0",
        "ram": str(ram_per_process),
        "pixType": 'uint8'
    })
    mask.Execute()
    border_mask = otb_app_bank.CreateBinaryMorphologicalOperation({
        "in":
        mask,
        "out":
        out_mask,
        "ram":
        str(ram_per_process),
        "pixType":
        "uint8",
        "filter":
        "opening",
        "ballxradius":
        5,
        "ballyradius":
        5
    })
    dep = mask
    return border_mask, dep


def launch_sar_reprojection(raster_list: List[str],
                            ref_raster: str = None,
                            tile_name: str = None,
                            srtm: str = None,
                            geoid: str = None,
                            output_directory: Optional[str] = None,
                            ram_per_process: Optional[int] = None,
                            working_directory: Optional[str] = None):
    """Start S1 reprojection.

    Parameters
    ----------
    raster_list:
        List of S1 image to be reprojected
    ref_raster:
        Reference image for reprojection
    tile_name:
        Name of tile
    srtm:
        Folder containing SRTM files
    geoid:
        Path to a geoid file
    output_directory:
        Path to store reprojected data
    ram_pre_process:
        Amount of RAM available for OTB application
    working_directory:
        Temporary folder to write intermediate results
    """
    date_position = 4

    all_superi_vv = []
    all_superi_vh = []
    all_acquisition_date_vv = []
    all_acquisition_date_vh = []
    dates = []
    all_dep = []
    for date_to_concatenate in raster_list:
        # Calibration + Superimpose
        vv_dates, vh_dates = date_to_concatenate.GetImageList()
        _, sar_name = os.path.split(vv_dates)
        current_platform = get_platform_from_s1_raster(vv_dates)
        manifest = date_to_concatenate.getManifest()
        current_orbit_direction = get_orbit_direction(manifest)
        acquisition_date = sar_name.split("-")[date_position]
        dates.append(acquisition_date)
        calib_vv = otb_app_bank.CreateSarCalibration({
            "in":
            vv_dates,
            "lut":
            "gamma",
            "ram":
            str(ram_per_process)
        })
        calib_vh = otb_app_bank.CreateSarCalibration({
            "in":
            vh_dates,
            "lut":
            "gamma",
            "ram":
            str(ram_per_process)
        })
        calib_vv.Execute()
        calib_vh.Execute()
        all_dep.append(calib_vv)
        all_dep.append(calib_vh)
        orthoimagename_vv = (f"{current_platform}_{tile_name}_vv_"
                             f"{current_orbit_direction}_{acquisition_date}")

        super_vv, super_vv_dep = otb_app_bank.CreateSuperimposeApplication({
            "inr":
            ref_raster,
            "inm":
            calib_vv,
            "pixType":
            "float",
            "interpolator":
            "bco",
            "ram":
            ram_per_process,
            "elev.dem":
            srtm,
            "elev.geoid":
            geoid
        })
        orthoimagename_vh = (f"{current_platform}_{tile_name}_vh_"
                             f"{current_orbit_direction}_{acquisition_date}")

        super_vh, super_vh_dep = otb_app_bank.CreateSuperimposeApplication({
            "inr":
            ref_raster,
            "inm":
            calib_vh,
            "pixType":
            "float",
            "interpolator":
            "bco",
            "ram":
            ram_per_process,
            "elev.dem":
            srtm,
            "elev.geoid":
            geoid
        })
        # super_vv.Execute()
        # super_vh.Execute()
        all_dep.append(super_vv)
        all_dep.append(super_vh)
        all_dep.append(super_vv_dep)
        all_dep.append(super_vh_dep)
        all_superi_vv.append(super_vv)
        all_superi_vh.append(super_vh)

        all_acquisition_date_vv.append(orthoimagename_vv)
        all_acquisition_date_vh.append(orthoimagename_vh)

    all_acquisition_date_vv = "_".join(sorted(all_acquisition_date_vv))
    all_acquisition_date_vh = "_".join(sorted(all_acquisition_date_vh))
    sar_vv = os.path.join(output_directory, all_acquisition_date_vv + ".tif")
    sar_vh = os.path.join(output_directory, all_acquisition_date_vh + ".tif")

    if not os.path.exists(sar_vv):
        # Execute app
        for super_imp in all_superi_vv:
            super_imp.Execute()
        # Concatenate thanks to a BandMath
        vv_exp = ",".join([f"im{i+1}b1" for i in range(len(all_superi_vv))])
        vv_exp = f"max({vv_exp})"
        concat_appli_vv = otb_app_bank.CreateBandMathApplication({
            "il":
            all_superi_vv,
            "exp":
            vv_exp,
            "out":
            sar_vv,
            "ram":
            str(ram_per_process),
            "pixType":
            "float"
        })
        write_output_raster(concat_appli_vv,
                            overwrite=False,
                            working_directory=working_directory,
                            dep=all_dep)
    if not os.path.exists(sar_vh):
        for super_imp in all_superi_vh:
            super_imp.Execute()
        vh_exp = ",".join([f"im{i+1}b1" for i in range(len(all_superi_vh))])
        vh_exp = f"max({vh_exp})"
        concat_appli_vh = otb_app_bank.CreateBandMathApplication({
            "il":
            all_superi_vh,
            "exp":
            vh_exp,
            "out":
            sar_vh,
            "ram":
            str(ram_per_process),
            "pixType":
            "float"
        })
        write_output_raster(concat_appli_vh,
                            overwrite=False,
                            working_directory=working_directory,
                            dep=all_dep)

    # from the results generate a mask
    super_vv = os.path.join(output_directory, all_acquisition_date_vv + ".tif")
    border_mask = super_vv.replace(".tif", "_BorderMask.tif")
    if not os.path.exists(border_mask):
        mask_app, _ = generate_border_mask(super_vv,
                                           border_mask,
                                           ram_per_process=ram_per_process)
        mask_path = write_output_raster(mask_app,
                                        overwrite=False,
                                        working_directory=working_directory)
        mask_path_geom = mask_path.replace(".tif", ".geom")
        if os.path.exists(mask_path_geom):
            os.remove(mask_path_geom)

    return (sar_vv, sar_vh)


def concatenate_dates(raster_list):
    """Find raster to concatenates from a list of raster.

    Identify the same acquisition date.
    """
    date_position = 4
    date_sar = []
    for raster in raster_list:
        vv_im, _ = raster.imageFilenamesList
        _, sar_name = os.path.split(vv_im)
        acquisition_date = sar_name.split("-")[date_position].split("t")[0]
        date_sar.append((acquisition_date, raster))
    date_sar = fut.sort_by_first_elem(date_sar)
    return [
        tuple(list_of_concatenation)
        for date, list_of_concatenation in date_sar
    ]


def split_by_mode(raster_list):
    """Split the acquisition between modes and platform."""
    modes = {"s1a": {"ASC": [], "DES": []}, "s1b": {"ASC": [], "DES": []}}
    for raster, _ in raster_list:
        manifest = raster.getManifest()
        current_orbit_direction = get_orbit_direction(manifest)
        current_platform = get_platform_from_s1_raster(
            raster.imageFilenamesList[0])
        modes[current_platform][current_orbit_direction].append(raster)
    return modes["s1a"]["ASC"], modes["s1a"]["DES"], modes["s1b"][
        "ASC"], modes["s1b"]["DES"]


def get_sar_dates(raster_list):
    """Extract acquisition dates from a list of raster."""
    date_position = 4
    dates = []
    for raster in raster_list:
        vv_im, _ = raster.imageFilenamesList
        _, sar_name = os.path.split(vv_im)
        acquisition_date = sar_name.split("-")[date_position]
        dates.append(acquisition_date)
    dates = sorted(dates)
    return dates


def s1_preprocess(cfg: str,
                  process_tile: List[str],
                  working_directory: Optional[str] = None,
                  get_filtered: bool = False):
    """Launch the S1 preprocessing.

    Parameters
    ----------
    cfg:
        Path to a configuration file
    process_tile:
        List of tiles to be processed
    working_directory:
        Path to a working directory
    get_filtered:
        Enable the filtering model
    Notes
    -----
    The output is a list of otb's applications need to filter SAR images:
     [allFiltered,allDependence,allMasksOut,allTile]
    """
    if process_tile and not isinstance(process_tile, list):
        process_tile = [process_tile]

    config = configparser.ConfigParser()
    config.read(cfg)
    ram_per_process = int(
        config.get('Processing',
                   'RamPerProcess',
                   fallback=I2_CONST.s1_ram_per_process))
    s1_chain = Sentinel1PreProcess(cfg)
    s1filemanager = s1_manager.S1FileManager(cfg)
    tiles_to_process = []

    tiles_to_process = [cTile[1:] for cTile in process_tile]

    if len(tiles_to_process) == 0:
        print("No existing tiles found, exiting ...")
        sys.exit(1)

    # Analyse SRTM coverage for MGRS tiles to be processed
    srtm_tiles_check = s1filemanager.check_srtm_coverage(tiles_to_process)

    needed_srtm_tiles = []
    tiles_to_process_checked = []
    # For each MGRS tile to process
    for tile in tiles_to_process:
        # Get SRTM tiles coverage statistics
        srtm_tiles = srtm_tiles_check[tile]
        current_coverage = 0
        current_needed_srtm_tiles = []
        # Compute global coverage
        for (srtm_tile, coverage) in srtm_tiles:
            current_needed_srtm_tiles.append(srtm_tile)
            current_coverage += coverage
        # If SRTM coverage of MGRS tile is enough, process it
        if current_coverage >= 1.:
            needed_srtm_tiles += current_needed_srtm_tiles
            tiles_to_process_checked.append(tile)
        else:
            # Skip it
            print("Tile " + str(tile) + " has insuficient SRTM coverage (" +
                  str(100 * current_coverage) + "%), it will not be processed")

    # Remove duplicates
    needed_srtm_tiles = list(set(needed_srtm_tiles))

    if len(tiles_to_process_checked) == 0:
        print("No tiles to process, exiting ...")
        sys.exit(1)

    print("Required SRTM tiles: " + str(needed_srtm_tiles))

    srtm_ok = True

    for srtm_tile in needed_srtm_tiles:
        tile_path = os.path.join(s1_chain.srtm, srtm_tile)
        if not os.path.exists(tile_path):
            srtm_ok = False
            print(tile_path + " is missing")
    if not srtm_ok:
        print("Some SRTM tiles are missing, exiting ...")
        sys.exit(1)

    if not os.path.exists(s1_chain.geoid):
        print(f"Geoid file does not exists {s1_chain.geoid}, exiting ...")
        sys.exit(1)

    tiles_set = list(tiles_to_process_checked)
    raster_list = [
        elem for elem, coordinates in s1filemanager.get_s1_intersect_by_tile(
            tiles_set[0])
    ]

    # comp_per_date = 2  #VV / VH

    tile = tiles_to_process_checked[0]

    if working_directory:
        working_directory = os.path.join(working_directory, tile)
        if not os.path.exists(working_directory):
            try:
                os.mkdir(working_directory)
            except:
                pass

    ref_raster = fut.file_search_and(s1_chain.references_folder + "/T" + tile,
                                     True, s1_chain.raster_pattern)[0]

    # get SAR rasters which intersection the tile
    raster_list = s1filemanager.get_s1_intersect_by_tile(tile)
    # split SAR rasters in different groups
    (raster_list_s1aasc, raster_list_s1ades, raster_list_s1basc,
     raster_list_s1bdes) = split_by_mode(raster_list)
    # get detected dates by acquisition mode
    s1_asc_dates = get_sar_dates(raster_list_s1aasc + raster_list_s1basc)
    s1_des_dates = get_sar_dates(raster_list_s1ades + raster_list_s1bdes)

    # find which one as to be concatenate (acquisitions dates are the same)
    raster_list_s1aasc = concatenate_dates(raster_list_s1aasc)
    raster_list_s1ades = concatenate_dates(raster_list_s1ades)
    raster_list_s1basc = concatenate_dates(raster_list_s1basc)
    raster_list_s1bdes = concatenate_dates(raster_list_s1bdes)

    output_directory = os.path.join(s1_chain.output_preprocess, tile)
    if not os.path.exists(output_directory):
        try:
            os.mkdir(output_directory)
        except:
            print(f"{output_directory} already exists")
    launch_sar_reprojection_prod = partial(launch_sar_reprojection,
                                           ref_raster=ref_raster,
                                           tile_name=tile,
                                           geoid=s1_chain.geoid,
                                           srtm=s1_chain.srtm,
                                           output_directory=output_directory,
                                           ram_per_process=ram_per_process,
                                           working_directory=working_directory)

    raster_list_s1aasc_reproj = []
    for elem in raster_list_s1aasc:
        raster_list_s1aasc_reproj.append(launch_sar_reprojection_prod(elem))

    raster_list_s1ades_reproj = []
    for elem in raster_list_s1ades:
        raster_list_s1ades_reproj.append(launch_sar_reprojection_prod(elem))

    raster_list_s1basc_reproj = []
    for elem in raster_list_s1basc:
        raster_list_s1basc_reproj.append(launch_sar_reprojection_prod(elem))

    raster_list_s1bdes_reproj = []
    for elem in raster_list_s1bdes:
        raster_list_s1bdes_reproj.append(launch_sar_reprojection_prod(elem))

    raster_list_s1aasc_reproj_flat = [
        pol for sar_date in raster_list_s1aasc_reproj for pol in sar_date
    ]
    raster_list_s1ades_reproj_flat = [
        pol for sar_date in raster_list_s1ades_reproj for pol in sar_date
    ]
    raster_list_s1basc_reproj_flat = [
        pol for sar_date in raster_list_s1basc_reproj for pol in sar_date
    ]
    raster_list_s1bdes_reproj_flat = [
        pol for sar_date in raster_list_s1bdes_reproj for pol in sar_date
    ]

    all_ortho_path = (raster_list_s1aasc_reproj_flat +
                      raster_list_s1ades_reproj_flat +
                      raster_list_s1basc_reproj_flat +
                      raster_list_s1bdes_reproj_flat)

    s1aasc_masks = [
        s1aasc.replace(".tif", "_BorderMask.tif")
        for s1aasc in raster_list_s1aasc_reproj_flat if "_vv_" in s1aasc
    ]
    s1ades_masks = [
        s1ades.replace(".tif", "_BorderMask.tif")
        for s1ades in raster_list_s1ades_reproj_flat if "_vv_" in s1ades
    ]
    s1basc_masks = [
        s1basc.replace(".tif", "_BorderMask.tif")
        for s1basc in raster_list_s1basc_reproj_flat if "_vv_" in s1basc
    ]
    s1bdes_masks = [
        s1bdes.replace(".tif", "_BorderMask.tif")
        for s1bdes in raster_list_s1bdes_reproj_flat if "_vv_" in s1bdes
    ]
    allmasks = s1aasc_masks + s1ades_masks + s1basc_masks + s1bdes_masks

    date_tile = {'s1_ASC': s1_asc_dates, 's1_DES': s1_des_dates}

    # sort detected dates
    for _, value in list(date_tile.items()):
        value.sort()

    # launch outcore generation and prepare mulitemporal filtering
    filtered = S1FilteringProcessor.main(all_ortho_path, cfg, date_tile, tile)
    all_filtered = []
    all_masks_out = []

    for s1_filtered, _, _ in filtered:
        all_filtered.append(s1_filtered)

    all_masks_out.append(allmasks)

    # In order to avoid "TypeError: can't pickle SwigPyObject objects"
    if get_filtered:
        return all_filtered, all_masks_out
