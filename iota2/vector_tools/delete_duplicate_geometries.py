"""utilis which produces a new file without double geometries in a shapefile."""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sys

from iota2.vector_tools import vector_functions as vf


def percentage(part, whole):
    """Compute percentage."""
    return 100 * float(part) / float(whole)


def delete_dup_geom(infile: str):
    """Produce a new file without double geometries in a shapefile."""
    data_src = vf.open_to_write(infile)
    lyr = data_src.GetLayer()
    geoms = {}
    _ = vf.copy_shp(infile, "nodoublegeom")
    for feat in lyr:
        geom = feat.GetGeometryRef()
        f = feat.GetFID()
        if geom is not None:
            geoms[f] = geom.ExportToWkt()

    inverted = dict()
    for (k, v) in list(geoms.items()):
        if v[0] not in inverted:
            inverted[v] = k

    new_dict = dict()
    for (k, v) in list(inverted.items()):
        new_dict[v] = k
    print("Please wait ... copying features running")

    for k in new_dict:
        inFeat = lyr.GetFeature(k)
        vf.copy_feat_in_shp2(inFeat, newshp)
    print("Process done")

    return newshp


if __name__ == "__main__":
    if len(sys.argv) == 1:
        prog = os.path.basename(sys.argv[0])
        print(("      " + sys.argv[0] + " [options]"))
        print(("     Help : ", prog, " --help"))
        print(("        or : ", prog, " -h"))
        sys.exit(-1)
    else:
        parser = argparse.ArgumentParser(
            description="Delete double geometries in a shapefile"
        )
        parser.add_argument(
            "-s",
            dest="shapefile",
            action="store",
            help="Input shapefile",
            required=True,
        )
        parser.add_argument("-o", dest="outpath", action="store", help="output path")
        args = parser.parse_args()
        print("Delete duplicate geometries...")
        newshp = delete_dup_geom(args.shapefile)
        basenewshp = os.path.splitext(newshp)
        if args.outpath:
            vf.cp_shape_file(
                basenewshp, args.outpath, [".prj", ".shp", ".dbf", ".shx"], True
            )
