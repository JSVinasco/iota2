"""Module gathering most of database operations in iota2."""
# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   vector tools
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import argparse
import logging
import math
import os
import random
import shutil
import sys
import time
from shutil import copyfile
from typing import Dict, List, Union

import numpy as np
import osgeo
import xarray as xr
from osgeo import gdal, ogr
from shapely.geos import lgeos
from shapely.wkt import loads

from iota2.common import file_utils as fut
from iota2.common.utils import run

LOGGER = logging.getLogger("distributed.worker")


def get_netcdf_df_column_values(database_file: str, column: str):
    """Get column values of a netcdf file."""
    netcdf_data = xr.open_dataset(database_file)

    netcdf_data = netcdf_data.sel(
        {"bands": [column], "fid": list(netcdf_data["fid"].values)}
    )
    netcdf_data = netcdf_data.compute()
    net_array = netcdf_data.to_array()
    df_sub_sel = net_array.to_dataframe("sub_sel")
    df_sub_sel = df_sub_sel.unstack()
    df_sub_sel.columns = [str(col_name) for _, col_name in list(df_sub_sel.columns)]

    return list(df_sub_sel[column])


def get_all_fields_in_netcdf_df(netcdf_df_file: str, col_dim: str = "bands"):
    """get all fields of a pandas dataframe stored in a hdf file"""
    netcdf_data = xr.open_dataset(netcdf_df_file)
    fields = list(netcdf_data[col_dim].values)
    return fields


def re_encode_field(
    input_db: str,
    in_field: str,
    new_field: str,
    new_field_type,
    new_field_size: int,
    dict_rules: Dict,
    logger=LOGGER,
) -> None:
    """modify inplace the input database. Add a new field according the the 'in_field'
    values and the dictionary rules

    Parameters
    ----------
    input_db
        input database
    in_field
        field to perform conversion rules
    new_field
        field to create
    new_field_type
        new field type
    new_field_size
        new field size
    dict_rules:
        dict_rules[in_field] = new_value
    """

    data_src = ogr.Open(input_db)
    driver = data_src.GetDriver()
    dataset = driver.Open(input_db, 1)
    layer = dataset.GetLayer()

    all_fields = []
    in_layer_defn = layer.GetLayerDefn()
    for i in range(in_layer_defn.GetFieldCount()):
        field = in_layer_defn.GetFieldDefn(i).GetName()
        all_fields.append(field)

    if new_field not in all_fields:
        field_name = ogr.FieldDefn(new_field, new_field_type)
        field_name.SetWidth(new_field_size)
        layer.CreateField(field_name)
        for feat in layer:
            feat.SetField(new_field, dict_rules[feat.GetField(in_field)])
            layer.SetFeature(feat)
    else:
        logger.warning(
            f"field {new_field} already exists in {input_db}, skipping adding it"
        )


def get_re_encoding_labels_dic(
    ref_data: str, ref_data_field: str
) -> Dict[Union[str, int], int]:
    """
    Parameters
    ----------
    ref_data
        input reference database
    ref_data_field
        column containing labels database
    Notes
    -----
    return the labels conversion dictionary dic[old_label] = new_label
    """
    ref_field_type = get_field_type(ref_data, ref_data_field)
    label_type = None
    if int == ref_field_type:
        label_type = "int"
    elif str == ref_field_type:
        label_type = "str"
    else:
        raise ValueError(
            ("Input reference database labels must be string or integer format.")
        )
    all_labels = sorted(
        get_field_element(
            ref_data, field=ref_data_field, mode="unique", elem_type=label_type
        )
    )
    lut_labels = {}
    for new_label, old_label in zip(range(1, len(all_labels) + 1), all_labels):
        lut_labels[old_label] = new_label
    return lut_labels


def get_reverse_encoding_labels_dic(
    ref_data: str, ref_data_field: str
) -> Dict[int, Union[str, int]]:
    """gets the mapping between i2 labels and user labels
    ref_data: input reference database
    ref_data_field: column containing labels database"""
    user_labels_to_i2_labels = get_re_encoding_labels_dic(ref_data, ref_data_field)
    i2_labels_to_user_labels = {v: k for k, v in user_labels_to_i2_labels.items()}

    return i2_labels_to_user_labels


def get_vector_proj(vector_file: str, driver: str = "ESRI Shapefile"):
    """get vector projection"""
    driver = ogr.GetDriverByName(driver)
    dataset = driver.Open(vector_file)
    layer = dataset.GetLayer()
    spatial_ref = layer.GetSpatialRef()
    return spatial_ref.GetAttrValue("AUTHORITY", 1)


def get_geom_column_name(vector_file: str, driver: str = "ESRI Shapefile"):
    """
    get field's name containing features' geometries

    Parameters
    ----------
    vector_file :
        input vector file
    driver :
        ogr driver's name

    """
    column_name = None
    if os.path.exists(vector_file):
        driver = ogr.GetDriverByName(driver)
        data_src = driver.Open(vector_file, 0)
        layer = data_src.GetLayer()
        column_name = layer.GetGeometryColumn()
    else:
        raise Exception("File not found : {}".format(vector_file))

    if not column_name:
        column_name = "GEOMETRY"

    return column_name


def open_to_read(shapefile: str, driver: str = "ESRI Shapefile"):
    """
    Opens a shapefile to read it and returns the datasource in read mode

    Parameters
    ----------
    shapefile:
        the input vector file to open
    driver:
        Optional driver name for vector file
    """

    driver = ogr.GetDriverByName(driver)
    if driver.Open(shapefile, 0):
        data_source = driver.Open(shapefile, 0)
    else:
        raise ValueError("Not possible to open the file " + shapefile)

    return data_source


def open_to_write(shapefile: str, driver="ESRI Shapefile"):
    """Open a shapefile to read it and returns the datasource in write mode."""
    driver = ogr.GetDriverByName(driver)
    if driver.Open(shapefile, 1):
        data_src = driver.Open(shapefile, 1)
    else:
        print("Not possible to open the file " + shapefile)
        sys.exit(1)

    return data_src


def get_nb_feat(shapefile: str, driver="ESRI Shapefile"):
    """Return the number of features of a shapefile."""
    data_src = open_to_read(shapefile, driver)
    layer = data_src.GetLayer()
    feature_count = layer.GetFeatureCount()
    return int(feature_count)


def get_fid_list(vect: str) -> List[int]:
    """Return the list of fids in database."""
    shape = open_to_read(vect)
    lyr = shape.GetLayer()
    fidlist = []
    for feat in lyr:
        fidlist.append(feat.GetFID())

    return list(set(fidlist))


def get_geom_type(shapefile, driver="ESRI Shapefile"):
    """Return the type of geometry of the file (WKBGeometryType)."""
    data_src = open_to_read(shapefile, driver)
    layer = data_src.GetLayer()
    return layer.GetGeomType()


def get_geom_type_from_feat(shapefile, driver="ESRI Shapefile"):
    """Return the type of geometry of the file."""
    # get the data layer
    data_src = open_to_read(shapefile, driver)
    layer = data_src.GetLayer()

    # get the first feature
    feature = layer.GetNextFeature()

    geometry = feature.GetGeometryRef()

    return geometry.GetGeometryName()


def get_fid_spatial_filter(
    shapefile, shapefile_to_intersect, field=None, driver="ESRI Shapefile"
):

    data_src = open_to_read(shapefile, driver)
    lyr = data_src.GetLayer()
    dsinter = open_to_read(shapefile_to_intersect, driver)
    lyrinter = dsinter.GetLayer()

    lyr.SetSpatialFilterRect(
        lyrinter.GetExtent()[0],
        lyrinter.GetExtent()[2],
        lyrinter.GetExtent()[1],
        lyrinter.GetExtent()[3],
    )
    fidlist = []
    for feat in lyr:
        if field is None:
            fidlist.append(feat.GetFID())
        else:
            fidlist.append(feat.GetField(field))

    return fidlist


def spatial_filter(
    vect,
    clipzone,
    clipfield,
    clipvalue,
    outvect,
    driverclip="ESRI Shapefile",
    drivervect="ESRI Shapefile",
    driverout="ESRI Shapefile",
):
    """
    Return features of a vector file  which are intersected by a feature of another vector file
    """

    dsclip = open_to_read(clipzone, driverclip)
    dsvect = open_to_read(vect, drivervect)
    lyrclip = dsclip.GetLayer()
    lyrvect = dsvect.GetLayer()

    fields = get_fields(clipzone, driverclip)
    layer_dfn_clip = lyrclip.GetLayerDefn()
    field_type_code = layer_dfn_clip.GetFieldDefn(fields.index(clipfield)).GetType()

    if field_type_code == 4:
        lyrclip.SetAttributeFilter(clipfield + " = '" + str(clipvalue) + "'")
    else:
        lyrclip.SetAttributeFilter(clipfield + " = " + str(clipvalue))

    featclip = lyrclip.GetNextFeature()
    geomclip = featclip.GetGeometryRef()
    lyrvect.SetSpatialFilter(geomclip)

    if lyrvect.GetFeatureCount() != 0:
        drv = ogr.GetDriverByName(driverout)
        outds = drv.CreateDataSource(outvect)
        layer_name_out = os.path.splitext(os.path.basename(outvect))[0]
        outlyr = outds.CopyLayer(lyrvect, layer_name_out)
        del outlyr, outds, lyrclip, lyrvect, dsvect, dsclip
    else:
        print("No intersection between the two vector files")
        del lyrclip, lyrvect, dsvect, dsclip


def is_readable(db_file: str) -> bool:
    """Return True if the database is readable."""
    driver_name = get_driver(db_file)
    driver = ogr.GetDriverByName(driver_name)
    db_is_readable = bool(driver.Open(db_file, 0))
    driver = None
    return db_is_readable


def get_driver(vector):

    inds = ogr.Open(vector)
    return inds.GetDriver().GetName()


def get_layer_name(shapefile, driver="ESRI Shapefile", layernb=0):
    """
    Return the name of the nth layer of a vector file
    """
    data_src = open_to_read(shapefile, driver)
    layer = data_src.GetLayer(layernb)
    return layer.GetName()


def intersect(file_1: str, fid1: int, file_2: str, fid2: int) -> bool:
    """
    This function checks two features in a file to see if they intersect.
    It takes 4 arguments, file_1 for the first file, fid1 for the index of the
    first file's feature, file_2 for the second file, fid2 for the index of the
    second file's feature. Returns whether the intersection is True or False.
    """
    test = False
    ds1 = open_to_read(file_1)
    layer1 = ds1.GetLayer()
    feat1 = layer1.GetFeature(fid1)
    geom1 = feat1.GetGeometryRef()
    ds2 = open_to_read(file_2)
    layer2 = ds2.GetLayer()
    feat2 = layer2.GetFeature(fid2)
    geom2 = feat2.GetGeometryRef()
    if geom1.Intersect(geom2) == 1:
        print("INTERSECTION IS TRUE")
        test = True
    else:
        print("INTERSECTION IS FALSE")
        test = False
    return test


def get_fields(shp, driver="ESRI Shapefile"):
    """
    Returns the list of fields of a vector file
    """
    if not isinstance(shp, osgeo.ogr.Layer):
        data_src = open_to_read(shp, driver)
        lyr = data_src.GetLayer()
    else:
        lyr = shp

    in_layer_defn = lyr.GetLayerDefn()
    field_name_list = []
    for i in range(in_layer_defn.GetFieldCount()):
        field = in_layer_defn.GetFieldDefn(i).GetName()
        field_name_list.append(field)
    return field_name_list


# TODO: duplicate name with iota2.common.Tools.ExtractROIRaster different outputs
def get_field_type(shp, field, driver="ESRI Shapefile"):

    data_src = open_to_read(shp, driver)
    layer = data_src.GetLayer()
    layer_definition = layer.GetLayerDefn()
    dico = {"String": str, "Real": float, "Integer": int, "Integer64": int}
    for i in range(layer_definition.GetFieldCount()):
        if layer_definition.GetFieldDefn(i).GetName() == field:
            field_type_code = layer_definition.GetFieldDefn(i).GetType()
            field_type = layer_definition.GetFieldDefn(i).GetFieldTypeName(
                field_type_code
            )

    return dico[field_type]


def get_first_layer(shp, driver="ESRI Shapefile"):

    data_src = open_to_read(shp, driver)
    layer = data_src.GetLayerByIndex(0)
    layer_name = layer.GetName()

    return layer_name


def list_value_fields(shp, field, driver="ESRI Shapefile"):
    """
    Returns the list of fields of a shapefile
    """
    if not isinstance(shp, osgeo.ogr.Layer):
        data_src = open_to_read(shp, driver)
        lyr = data_src.GetLayer()
    else:
        lyr = shp

    values = []
    for feat in lyr:
        if not feat.GetField(field) in values:
            values.append(feat.GetField(field))

    return sorted(values)


def copy_shapefile(shape: str, outshape: str) -> None:
    """"""
    data_src = ogr.Open(shape)
    drv = data_src.GetDriver()
    drv.CopyDataSource(data_src, outshape)


def copy_shp2(shp, out_shapefile):
    """
    Creates an empty new layer based on the properties and attributs of an input file
    """
    data_src = open_to_read(shp)
    layer = data_src.GetLayer()
    in_layer_defn = layer.GetLayerDefn()
    field_name_target = get_fields(shp)
    out_driver = ogr.GetDriverByName("ESRI Shapefile")
    # if file already exists, delete it
    if os.path.exists(out_shapefile):
        out_driver.DeleteDataSource(out_shapefile)
    out_datasource = out_driver.CreateDataSource(out_shapefile)
    out_lyr_name = os.path.splitext(os.path.split(out_shapefile)[1])[0]
    # Get the spatial reference of the input layer
    srs_obj = layer.GetSpatialRef()
    # Creates the spatial reference of the output layer
    out_layer = out_datasource.CreateLayer(
        out_lyr_name, srs_obj, geom_type=get_geom_type(shp)
    )
    # Add input Layer Fields to the output Layer if it is the one we want
    for i in range(0, in_layer_defn.GetFieldCount()):
        field_defn = in_layer_defn.GetFieldDefn(i)
        field_name = field_defn.GetName()
        if field_name not in field_name_target:
            continue
        out_layer.CreateField(field_defn)
    # Get the output Layer's Feature Definition
    print(f"New file created : {out_shapefile}")
    return out_shapefile


def copy_shp(shp, keyname):
    """
    Creates an empty new layer based on the properties and attributs of an input file
    """
    out_shapefile = shp.split(".")[0] + "-" + keyname + ".shp"
    data_src = open_to_read(shp)
    layer = data_src.GetLayer()
    in_layer_defn = layer.GetLayerDefn()
    field_name_target = get_fields(shp)
    out_driver = ogr.GetDriverByName("ESRI Shapefile")
    # if file already exists, delete it
    if os.path.exists(out_shapefile):
        out_driver.DeleteDataSource(out_shapefile)
    out_datasource = out_driver.CreateDataSource(out_shapefile)
    out_lyr_name = os.path.splitext(os.path.split(out_shapefile)[1])[0]
    # Get the spatial reference of the input layer
    srs_obj = layer.GetSpatialRef()
    # Creates the spatial reference of the output layer
    out_layer = out_datasource.CreateLayer(
        out_lyr_name, srs_obj, geom_type=get_geom_type(shp)
    )
    # Add input Layer Fields to the output Layer if it is the one we want
    for i in range(0, in_layer_defn.GetFieldCount()):
        field_defn = in_layer_defn.GetFieldDefn(i)
        field_name = field_defn.GetName()
        if field_name not in field_name_target:
            continue
        out_layer.CreateField(field_defn)
    print(f"New file created : {out_shapefile}")
    return out_shapefile


def create_new_layer(layer: str, out_shapefile: str, outformat: str = "ESRI Shapefile"):
    """
    This function creates a new shapefile with a layer as input
    Parameters
    ----------
    layer :
        the input layer
    out_shapefile :
        the name of the output shapefile

    """
    in_layer_defn = layer.GetLayerDefn()
    field_name_target = []
    for i in range(in_layer_defn.GetFieldCount()):
        field = in_layer_defn.GetFieldDefn(i).GetName()
        field_name_target.append(field)

    out_driver = ogr.GetDriverByName(outformat)
    # if file already exists, delete it
    if os.path.exists(out_shapefile):
        out_driver.DeleteDataSource(out_shapefile)

    out_datasource = out_driver.CreateDataSource(out_shapefile)
    out_lyr_name = os.path.splitext(os.path.split(out_shapefile)[1])[0]
    # Get the spatial reference of the input layer
    srs_obj = layer.GetSpatialRef()
    # Creates the spatial reference of the output layer
    out_layer = out_datasource.CreateLayer(
        out_lyr_name, srs_obj, geom_type=layer.GetGeomType()
    )
    # Add input Layer Fields to the output Layer if it is the one we want

    for i in range(0, in_layer_defn.GetFieldCount()):
        field_defn = in_layer_defn.GetFieldDefn(i)
        field_name = field_defn.GetName()
        if field_name not in field_name_target:
            continue
        out_layer.CreateField(field_defn)
    # Get the output Layer's Feature Definition
    out_layer_defn = out_layer.GetLayerDefn()

    # Add features to the ouput Layer
    for in_feature in layer:
        # Create output Feature
        out_feature = ogr.Feature(out_layer_defn)

        # Add field values from input Layer
        for i in range(0, out_layer_defn.GetFieldCount()):
            field_defn = out_layer_defn.GetFieldDefn(i)
            field_name = field_defn.GetName()
            if field_name not in field_name_target:
                continue

            out_feature.SetField(
                out_layer_defn.GetFieldDefn(i).GetNameRef(), in_feature.GetField(i)
            )
        # Set geometry as centroid
        geom = in_feature.GetGeometryRef()
        out_feature.SetGeometry(geom.Clone())
        # Add new feature to output Layer
        out_layer.CreateFeature(out_feature)
    return out_shapefile


def copy_feat_in_shp(in_feat, shp):
    """
    Copy a feature into a new file
    (checks that it does not exist in the new file)
    """
    data_src = open_to_write(shp)
    layer = data_src.GetLayer()
    out_layer_defn = layer.GetLayerDefn()
    in_geom = in_feat.GetGeometryRef()
    layer.SetSpatialFilter(in_geom)

    if layer.GetFeatureCount() == 0:
        layer.SetSpatialFilter(None)
        field_name_list = get_fields(shp)
        out_feat = ogr.Feature(out_layer_defn)
        for field in field_name_list:
            in_value = in_feat.GetField(field)
            out_feat.SetField(field, in_value)
        geom = in_feat.GetGeometryRef()
        out_feat.SetGeometry(geom)
        layer.CreateFeature(out_feat)
        layer.SetFeature(out_feat)
        print("Feature copied")
        data_src.ExecuteSQL("REPACK " + layer.GetName())
    elif layer.GetFeatureCount() >= 1:
        layer2 = data_src.GetLayer()
        layer2.SetSpatialFilter(in_geom)
        v = 0
        for k in layer2:
            geom2 = k.GetGeometryRef()
            if geom2:
                if in_geom.Equal(geom2) is True:
                    v += 1
                elif in_geom.Equal(geom2) is False:
                    v += 0
            if v == 0:
                field_name_list = get_fields(shp)
                out_feat = ogr.Feature(out_layer_defn)
                for field in field_name_list:
                    in_value = in_feat.GetField(field)
                    out_feat.SetField(field, in_value)
                geom = in_feat.GetGeometryRef()
                out_feat.SetGeometry(geom)
                layer.CreateFeature(out_feat)
                layer.SetFeature(out_feat)
                print("Feature copied")
                data_src.ExecuteSQL("REPACK " + layer.GetName())


def copy_feat_in_shp2(in_feat, shp):
    """Copy a feature into a new file verifyng thad does not exist in the new file."""
    data_src = open_to_write(shp)
    layer = data_src.GetLayer()
    out_layer_defn = layer.GetLayerDefn()
    in_geom = in_feat.GetGeometryRef()
    layer.SetSpatialFilter(in_geom)
    field_name_list = get_fields(shp)
    out_feat = ogr.Feature(out_layer_defn)
    for field in field_name_list:
        in_value = in_feat.GetField(field)
        out_feat.SetField(field, in_value)
    geom = in_feat.GetGeometryRef()
    out_feat.SetGeometry(geom)
    layer.CreateFeature(out_feat)
    layer.SetFeature(out_feat)
    # print "Feature copied"
    data_src.ExecuteSQL("REPACK " + layer.GetName())


def delete_invalid_geom(shp):
    """Delete the invalide geometries in a file."""
    print("Verifying geometries validity")
    data_src = open_to_write(shp)
    layer = data_src.GetLayer()
    fidl = []
    # for i in range(0,nbfeat):
    for feat in layer:
        # feat = layer.GetFeature(i)
        fid = feat.GetFID()
        if feat.GetGeometryRef() is None:
            # print fid
            geom = feat.GetGeometryRef()
            if geom is None:
                fidl.append(fid)
            else:
                valid = geom.IsValid()
                if not valid:
                    fidl.append(fid)
    list_fid = []
    if len(fidl) != 0:
        for f in fidl:
            list_fid.append("FID!=" + str(f))
        chain = []
        for f in list_fid:
            chain.append(f)
            chain.append(" AND ")
        chain.pop()
        fchain = "".join(chain)
        layer.SetAttributeFilter(fchain)
        create_new_layer(layer, "valide_entities.shp")
        print("New file: valide_entities.shp was created")
    else:
        print("All geometries are valid. No file created")

    return 0


def copy_vector(vector, outpath, outname=""):
    """Copy database."""
    nametofind = os.path.splitext(os.path.basename(vector))[0]
    pathtofind = os.path.dirname(vector)
    listtocopy = fut.file_search_and(pathtofind, True, nametofind)
    outname = os.path.splitext(os.path.basename(outname))[0]
    for filetocopy in listtocopy:
        ext = os.path.splitext(filetocopy)[1]
        if outname:
            shutil.copy(filetocopy, os.path.join(outpath, outname) + ext)
        else:
            shutil.copy(filetocopy, outpath)


def check_valid_geom(
    shp, outformat="ESRI shapefile", display=True, do_corrections=True
):
    """Check the validity of geometries in a file. If geometry is not valid then
    apply buffer 0 to correct. Works for files with polygons

    Parameters
    ----------
    shp : string
        input shapeFile
    Return
    ------
    tuple
        (output_shape, count, corr) where count is the number of invalid features
        and corr the number of invalid features corrected
    """
    if display:
        print("Verifying geometries validity")

    driver = get_driver(shp)

    data_src = open_to_write(shp, driver)
    layer = data_src.GetLayer()
    nbfeat = get_nb_feat(shp, driver)
    count = 0
    corr = 0
    fidl = []

    for feat in layer:
        # feat = layer.GetFeature(i)
        fid = feat.GetFID()
        if feat.GetGeometryRef() is None:
            if display:
                print(fid)
            if not do_corrections:
                continue
            count += 1
            layer.DeleteFeature(fid)
            data_src.ExecuteSQL("REPACK " + layer.GetName())
            layer.ResetReading()
        else:
            geom = feat.GetGeometryRef()
            valid = geom.IsValid()
            if not valid and do_corrections:
                fidl.append(fid)
                buffer_test = feat.SetGeometry(geom.Buffer(0))
                layer.SetFeature(feat)
                if buffer_test == 0:
                    if display:
                        print(f"Feature {feat.GetFID()} has been corrected")
                    corr += 1
                else:
                    if display:
                        print(f"Feature {feat.GetFID()} could not be corrected")
                count += 1
    if display:
        print(f"From {count} invalid features, {corr} were corrected")

    if outformat == "ESRI shapefile":
        data_src.ExecuteSQL(f"REPACK {layer.GetName()}")
    elif outformat == "SQLite":
        data_src = layer = None
    else:
        pass

    return shp, count, corr


def check_geom_type(
    shp,
    outformat="ESRI shapefile",
    display=True,
    do_corrections=True,
    expected_geom="POLYGON",
):
    """Check the validity of geometries in a file. If geometry is not valid then
    apply buffer 0 to correct. Works for files with polygons

    Parameters
    ----------
    shp : string
        input shapeFile
    Return
    ------
    tuple
        (output_shape, count, corr) where count is the number of invalid features
        and corr the number of invalid features corrected
    """
    if display:
        print("Verifying geometries validity")

    data_src = open_to_write(shp, outformat)
    layer = data_src.GetLayer()
    count = 0
    corr = 0
    fidl = []

    for feat in layer:
        # feat = layer.GetFeature(i)
        fid = feat.GetFID()
        if feat.GetGeometryRef() is None:
            if display:
                print(fid)
            if not do_corrections:
                continue
            count += 1
            layer.DeleteFeature(fid)
            data_src.ExecuteSQL("REPACK " + layer.GetName())
            layer.ResetReading()
        else:
            geom = feat.GetGeometryRef()
            valid = geom.IsValid()
            geom_name = geom.GetGeometryName()
            if valid:
                if geom_name != expected_geom:
                    valid = False
            if not valid and do_corrections:
                fidl.append(fid)
                buffer_test = feat.SetGeometry(geom.Buffer(0))
                layer.SetFeature(feat)
                if buffer_test == 0:
                    if display:
                        print(f"Feature {feat.GetFID()} has been corrected")
                    corr += 1
                else:
                    if display:
                        print(f"Feature {feat.GetFID()} could not be corrected")
                count += 1
    if display:
        print(f"From {count} invalid features, {corr} were corrected")

    if outformat == "ESRI shapefile":
        data_src.ExecuteSQL(f"REPACK {layer.GetName()}")
    elif outformat == "SQLite":
        data_src = layer = None
    else:
        pass

    return shp, count, corr


def check_empty_geom(
    shp, informat="ESRI shapefile", do_corrections=True, output_file=None
):
    """Check if a geometry is empty, then if it does it will not be copied in output shapeFile.

    Parameters
    ----------
    input_shape : string
        input shapeFile
    do_correction : bool
        flag to remove empty geometries
    output_shape : string
        output shapeFile, if set to None output_shape = input_shape
    Return
    ------
    tuple
        (output_shape, invalid_geom_number) where invalid_geom_number is
        the number of empty geometries
    """

    data_src = open_to_read(shp, informat)
    layer = data_src.GetLayer()
    all_fid = []
    count = 0
    for feat in layer:
        fid = feat.GetFID()
        geom = feat.GetGeometryRef()
        if geom is not None:
            empty = geom.IsEmpty()
            if empty is False:
                all_fid.append(fid)
            elif empty is True:
                count += 1
    if do_corrections:
        if count == 0:
            print("No empty geometries")
            out_shapefile = shp
        else:
            for fid in all_fid:
                all_fid.append("FID=" + str(fid))
            # Add the word OR
            all_list = []
            for item in all_fid:
                all_list.append(item)
                all_list.append(" OR ")
            all_list.pop()

            ch = " ".join(all_list)
            layer.SetAttributeFilter(ch)
            out_shapefile = (
                output_file
                if output_file is not None
                else shp.split(".")[0] + "-NoEmpty.shp"
            )
            create_new_layer(layer, out_shapefile)
            print(f"{len(all_fid)} empty geometries were deleted")
    else:
        out_shapefile = shp

    return (output_file, count)


def check_is_ring_geom(shp):
    """
    Check if all the geometries in the shapefile are closed rings
    """
    data_src = open_to_write(shp)
    _ = data_src.GetLayer()
    _ = get_nb_feat(shp)


def explain_validity(shp):
    """
    Explains the validity reason of each feature in a shapefile
    """
    data_src = open_to_read(shp)
    layer = data_src.GetLayer()
    for feat in layer:
        geom = feat.GetGeometryRef()
        ob = loads(geom.ExportToWkt())
        print(lgeos.GEOSisValidReason(ob._geom))
    return 0


def check_intersect(shp, distance, fieldin):
    """
    Check if each feature intersects another feature in the same file.
    If True compute the difference. The common part is deleted.
    """
    print("Verifying intersection")
    data_src = open_to_write(shp)
    layer = data_src.GetLayer()
    nbfeat = get_nb_feat(shp)
    out_shp = copy_shp(shp, "nointersct")
    layer_def = layer.GetLayerDefn()
    fields = get_fields(shp)
    for i in range(0, nbfeat):
        print(i)
        feat1 = layer.GetFeature(i)
        geom1 = feat1.GetGeometryRef()
        centroid = geom1.Centroid()
        x = centroid.GetX()
        y = centroid.GetY()
        min_x = x - float(distance)
        min_y = y - float(distance)
        max_x = x + float(distance)
        max_y = y + float(distance)
        layer.SetSpatialFilterRect(
            float(min_x), float(min_y), float(max_x), float(max_y)
        )
        nbfeat2 = layer.GetFeatureCount()
        list_fid = []
        list_id = []
        for j in range(0, nbfeat2):
            feat2 = layer.GetFeature(j)
            # print feat1.GetFID()
            # print feat2.GetFID()
            geom1 = feat1.GetGeometryRef()
            geom2 = feat2.GetGeometryRef()
            if geom1.Intersects(geom2) is True and not geom1.Equal(geom2):
                list_fid.append(feat2.GetFID())
                list_id.append(feat2.GetField("id"))

        if len(list_fid) == 0:
            outds = open_to_read(out_shp)
            outlayer = outds.GetLayer()
            if verify_geom(geom1, outlayer) == False:
                copy_feat_in_shp(feat1, out_shp)
        elif len(list_fid) == 1:
            for f in list_fid:
                feat2 = layer.GetFeature(f)
                geom2 = feat2.GetGeometryRef()
                if feat1.GetFieldAsString(fieldin) == feat2.GetFieldAsString(fieldin):
                    print(
                        feat1.GetFieldAsString("id")
                        + " "
                        + feat2.GetFieldAsString("id")
                    )
                    newgeom = union(geom1, geom2)
                else:
                    newgeom = difference(geom1, geom2)
                # newgeom =  difference(geom1, geom2)
                newgeom2 = ogr.CreateGeometryFromWkb(newgeom.wkb)
                new_feature = ogr.Feature(layer_def)
                new_feature.SetGeometry(newgeom2)
                for field in fields:
                    new_feature.SetField(field, feat1.GetField(field))
                copy_feat_in_shp(new_feature, out_shp)
        elif len(list_fid) > 1:
            for f in list_fid:
                feat2 = layer.GetFeature(f)
                geom2 = feat2.GetGeometryRef()
                if feat1.GetFieldAsString(fieldin) == feat2.GetFieldAsString(fieldin):
                    newgeom = union(geom1, geom2)
                else:
                    newgeom = difference(geom1, geom2)
                # newgeom =  difference(geom1, geom2)
                newgeom2 = ogr.CreateGeometryFromWkb(newgeom.wkb)
                new_feature = ogr.Feature(layer_def)
                new_feature.SetGeometry(newgeom2)
                geom1 = new_feature.GetGeometryRef()
                for field in fields:
                    new_feature.SetField(field, feat1.GetField(field))
                copy_feat_in_shp(new_feature, out_shp)

    return out_shp


def check_intersect2(shp, fieldin, field_in_id):
    """
    Check if each feature intersects another feature in the same file. If True compute the difference. The common part is deleted
    """
    print("Verifying intersection")
    data_src = open_to_write(shp)
    layer = data_src.GetLayer()
    nbfeat = get_nb_feat(shp)
    out_shp = copy_shp(shp, "nointersct")
    layer_def = layer.GetLayerDefn()
    fields = get_fields(shp)
    for i in range(0, nbfeat):
        print(i)
        feat1 = layer.GetFeature(i)
        geom1 = feat1.GetGeometryRef()
        layer.SetSpatialFilter(geom1)
        nbfeat2 = layer.GetFeatureCount()
        list_fid = []
        list_id = []
        for j in range(0, nbfeat2):
            feat2 = layer.GetFeature(j)
            # print feat1.GetFID()
            # print feat2.GetFID()
            geom1 = feat1.GetGeometryRef()
            geom2 = feat2.GetGeometryRef()
            if geom1.Intersects(geom2) == True and not geom1.Equal(geom2):
                list_fid.append(feat2.GetFID())
                list_id.append(feat2.GetField(field_in_id))
        if len(list_fid) == 0:
            outds = open_to_read(out_shp)
            outlayer = outds.GetLayer()
            if verify_geom(geom1, outlayer) is False:
                copy_feat_in_shp2(feat1, out_shp)
        elif len(list_fid) == 1:
            for f in list_fid:
                feat2 = layer.GetFeature(f)
                geom2 = feat2.GetGeometryRef()
                if feat1.GetFieldAsString(fieldin) == feat2.GetFieldAsString(fieldin):
                    print(
                        feat1.GetFieldAsString(field_in_id)
                        + " "
                        + feat2.GetFieldAsString(field_in_id)
                    )
                    newgeom = union(geom1, geom2)
                else:
                    newgeom = difference(geom1, geom2)
                # newgeom =  difference(geom1, geom2)
                newgeom2 = ogr.CreateGeometryFromWkb(newgeom.wkb)
                new_feature = ogr.Feature(layer_def)
                new_feature.SetGeometry(newgeom2)
                for field in fields:
                    new_feature.SetField(field, feat1.GetField(field))
                copy_feat_in_shp2(new_feature, out_shp)
        elif len(list_fid) > 1:
            for f in list_fid:
                feat2 = layer.GetFeature(f)
                geom2 = feat2.GetGeometryRef()
                if feat1.GetFieldAsString(fieldin) == feat2.GetFieldAsString(fieldin):
                    newgeom = union(geom1, geom2)
                else:
                    newgeom = difference(geom1, geom2)
                # newgeom =  difference(geom1, geom2)
                newgeom2 = ogr.CreateGeometryFromWkb(newgeom.wkb)
                new_feature = ogr.Feature(layer_def)
                new_feature.SetGeometry(newgeom2)
                geom1 = new_feature.GetGeometryRef()
                for field in fields:
                    new_feature.SetField(field, feat1.GetField(field))
                copy_feat_in_shp2(new_feature, out_shp)

    return out_shp


def check_intersect3(shp, fieldin):
    """Check if each feature intersects another feature in the same file.

    If True compute the difference. The common part is deleted.
    """
    print("Verifying intersection")
    data_src = open_to_write(shp)
    layer = data_src.GetLayer()
    nbfeat = get_nb_feat(shp)
    layer_def = layer.GetLayerDefn()
    fields = get_fields(shp)
    list_fid = []
    for i in range(0, nbfeat):
        feat1 = layer.GetFeature(i)
        geom1 = feat1.GetGeometryRef()
        layer2 = data_src.GetLayer()
        layer2.SetSpatialFilter(None)
        layer2.SetSpatialFilter(geom1)
        for feat2 in layer2:
            geom2 = feat2.GetGeometryRef()
            if geom1.Intersects(geom2) and not geom1.Equal(geom2):
                list_fid.append(feat2.GetFID())
                if feat1.GetFieldAsString(fieldin) == feat2.GetFieldAsString(fieldin):
                    newgeom = union(geom1, geom2)
                    newgeom2 = ogr.CreateGeometryFromWkb(newgeom.wkb)
                    new_feature = ogr.Feature(layer_def)
                    new_feature.SetGeometry(newgeom2)
                    geom1 = new_feature.GetGeometryRef()
                    for field in fields:
                        new_feature.SetField(field, feat1.GetField(field))
                    layer.CreateFeature(new_feature)
                    layer.SetFeature(new_feature)
                else:
                    newgeom = difference(geom1, geom2)
                    newgeom2 = ogr.CreateGeometryFromWkb(newgeom.wkb)
                    new_feature = ogr.Feature(layer_def)
                    new_feature.SetGeometry(newgeom2)
                    geom1 = new_feature.GetGeometryRef()
                    for field in fields:
                        new_feature.SetField(field, feat1.GetField(field))
                    layer.CreateFeature(new_feature)
                    layer.SetFeature(new_feature)

    for fid in range(0, len(list_fid)):
        layer.DeleteFeature(list_fid[fid])

    data_src.ExecuteSQL("REPACK " + layer.GetName())


def verify_geom(geom, layer):
    verif = False
    for feat in layer:
        geom2 = feat.GetGeometryRef()
        if geom and geom2:
            if geom.Equal(geom2):
                verif = True

    return verif


def difference(geom1, geom2):
    """Returns the difference of 2 geometries."""
    obj1 = loads(geom1.ExportToWkt())
    obj2 = loads(geom2.ExportToWkt())
    return obj1.difference(obj2)


def union(geom1, geom2):
    """Returns the difference of 2 geometries."""
    obj1 = loads(geom1.ExportToWkt())
    obj2 = loads(geom2.ExportToWkt())
    print(obj1.union(obj2))
    return obj1.union(obj2)


def check_double_geom_two_files(shp1, shp2):
    """Priority to file No. 1."""
    ds1 = open_to_read(shp1)
    lyr1 = ds1.GetLayer()
    for feat1 in lyr1:
        if feat1.GetGeometryRef():
            geom1 = feat1.GetGeometryRef()
            centroid = geom1.Centroid()
            ds2 = open_to_write(shp2)
            lyr2 = ds2.GetLayer()
            lyr2.SetSpatialFilter(geom1)
            if lyr2.GetFeatureCount() == 0:
                lyr1.GetNextFeature()
            else:
                for feat2 in lyr2:
                    fid = feat2.GetFID()
                    geom2 = feat2.GetGeometryRef()
                    if geom1.Equal(geom2) is True:
                        lyr2.DeleteFeature(fid)
                        ds2.ExecuteSQL("REPACK " + lyr2.GetName())
            lyr2.ResetReading()


def multi_poly_to_poly(shp_multi, shp_single):
    """
    IN:
    shp_multi [string] : path to an input vector
    shp_single [string] : output vector

    OUT:
    convert all multipolygon to polygons. Add all single polygon into shp_single
    """

    def add_polygon(feat, simple_polygon, in_lyr, out_lyr):
        """
        add polygon
        """
        feature_defn = in_lyr.GetLayerDefn()
        polygon = ogr.CreateGeometryFromWkb(simple_polygon)
        out_feat = ogr.Feature(feature_defn)
        for field in field_name_list:
            in_value = feat.GetField(field)
            out_feat.SetField(field, in_value)
        out_feat.SetGeometry(polygon)
        out_lyr.CreateFeature(out_feat)
        out_lyr.SetFeature(out_feat)

    def multipoly2poly(in_lyr, out_lyr):
        for in_feat in in_lyr:
            geom = in_feat.GetGeometryRef()
            if geom.GetGeometryName() == "MULTIPOLYGON":
                for geom_part in geom:
                    add_polygon(in_feat, geom_part.ExportToWkb(), in_lyr, out_lyr)
            else:
                add_polygon(in_feat, geom.ExportToWkb(), in_lyr, out_lyr)

    gdal.UseExceptions()
    driver = ogr.GetDriverByName("ESRI Shapefile")
    field_name_list = get_all_fields_in_shape(shp_multi)
    in_ds = driver.Open(shp_multi, 0)
    in_lyr = in_ds.GetLayer()
    in_layer_defn = in_lyr.GetLayerDefn()
    srs_obj = in_lyr.GetSpatialRef()
    if os.path.exists(shp_single):
        driver.DeleteDataSource(shp_single)
    out_ds = driver.CreateDataSource(shp_single)
    out_lyr = out_ds.CreateLayer("poly", srs_obj, geom_type=ogr.wkbPolygon)
    for i in range(0, len(field_name_list)):
        field_defn = in_layer_defn.GetFieldDefn(i)
        field_name = field_defn.GetName()
        if field_name not in field_name_list:
            continue
        out_lyr.CreateField(field_defn)
    multipoly2poly(in_lyr, out_lyr)


def merge_sqlite(outname, opath, files):
    filefusion = opath + "/" + outname + ".sqlite"
    if os.path.exists(filefusion):
        os.remove(filefusion)
    if len(files) > 1:
        first = files[0]
        cmd = "ogr2ogr -f SQLite " + filefusion + " " + first
        run(cmd)
        if len(files) > 1:
            for f in range(1, len(files)):
                fusion = (
                    "ogr2ogr -f SQLite -update -append " + filefusion + " " + files[f]
                )
                print(fusion)
                run(fusion)
    else:
        shutil.copy(files[0], filefusion)


def get_field_element(
    shape: str,
    driver_name: str = "ESRI Shapefile",
    field: str = "CODE",
    mode: str = "all",
    elem_type: str = "int",
    logger=LOGGER,
):
    """
    PARAMETERS
    ----------
    shape [string] : shape to compute
    driver_name [string] : ogr driver to read the shape
    field [string] : data's field
    mode [string] : "all" or "unique"

    OUT :
    [list] containing all/unique element in shape's field

    Example :
        get_field_element("./MyShape.sqlite","SQLite","CODE",mode = "all")
        >> [1,2,2,2,2,3,4]
        get_field_element("./MyShape.sqlite","SQLite","CODE",mode = "unique")
        >> [1,2,3,4]
    """

    def get_elem(elem, elem_type):

        retour_elem = None
        if elem_type == "int":
            retour_elem = int(elem)
        elif elem_type == "str":
            retour_elem = str(elem)
        else:
            raise Exception("elem_type must be 'int' or 'str'")
        return retour_elem

    driver = ogr.GetDriverByName(driver_name)

    if driver_name.lower() == "sqlite":
        max_try = 6
        for _ in range(max_try):
            try:
                data_src = driver.Open(shape, 0)
                layer = data_src.GetLayer()
                break
            except:
                print(f"failed to open database {shape}, waiting...")
                time.sleep(random.randint(1, 10))
    else:
        data_src = driver.Open(shape, 0)
        layer = data_src.GetLayer()

    retour_mode = None

    if layer is None:
        retour_mode = []
        logger.info(
            f"{shape} is empty. Probably no intersection between"
            " learning points and the chunk."
            " Ensure you are using external_features"
        )
    elif mode == "all":
        retour_mode = [
            get_elem(currentFeat.GetField(field), elem_type) for currentFeat in layer
        ]
    elif mode == "unique":
        retour_mode = list(
            set(
                [
                    get_elem(currentFeat.GetField(field), elem_type)
                    for currentFeat in layer
                ]
            )
        )
    else:
        raise Exception("mode parameter must be 'all' or 'unique'")
    return retour_mode


def get_vector_features(
    ground_truth: str, region_field: str, InputShape: str
) -> List[str]:
    """
    PARAMETERS
    ----------
    ground_truth :
        original ground truth file
    region_field :
        the region name
    InputShape :
        path to a vector (otbcli_SampleExtraction output)

    Notes
    -----
    all_feat [list of string] : list of all feature found in InputShape.
    This vector must contain field with pattern 'value_N' N:[0,int(someInt)]
    """
    input_fields = get_all_fields_in_shape(ground_truth) + [
        region_field,
        "originfid",
        "tile_o",
    ]

    data_src = ogr.Open(InputShape)
    da_layer = data_src.GetLayer(0)
    layer_definition = da_layer.GetLayerDefn()

    all_feat = []
    for i in range(layer_definition.GetFieldCount()):
        field_name = layer_definition.GetFieldDefn(i).GetName()
        if field_name not in input_fields and field_name not in [
            input_field.lower() for input_field in input_fields
        ]:
            all_feat.append(layer_definition.GetFieldDefn(i).GetName())
    return all_feat


def get_all_fields_in_shape(vector, driver="ESRI Shapefile") -> List[str]:
    """
    Return all fields in vector

    Parameters
    ----------
    vector :
        path to vector file
    driver :
        gdal driver


    """
    driver = ogr.GetDriverByName(driver)
    data_source = driver.Open(vector, 0)
    if data_source is None:
        raise Exception("Could not open " + vector)
    layer = data_source.GetLayer()
    layer_definition = layer.GetLayerDefn()
    return [
        layer_definition.GetFieldDefn(i).GetName()
        for i in range(layer_definition.GetFieldCount())
    ]


def keep_biggest_area(shpin, shpout):
    """
    usage : from shpin, keep biggest polygon and save it in shpout
    logger = logging.getLogger("distributed.worker")
    logger.debug("Processing {}".format(shpin))
    """

    def add_polygon(feat, simple_polygon, in_lyr, out_lyr):
        """
        usage : add polygon
        """
        feature_defn = in_lyr.GetLayerDefn()
        polygon = ogr.CreateGeometryFromWkb(simple_polygon)
        out_feat = ogr.Feature(feature_defn)
        for field in field_name_list:
            in_value = feat.GetField(field)
            out_feat.SetField(field, in_value)
        out_feat.SetGeometry(polygon)
        out_lyr.CreateFeature(out_feat)
        out_lyr.SetFeature(out_feat)

    gdal.UseExceptions()
    driver = ogr.GetDriverByName("ESRI Shapefile")
    field_name_list = get_all_fields_in_shape(shpin)
    in_ds = driver.Open(shpin, 0)
    in_lyr = in_ds.GetLayer()
    in_layer_defn = in_lyr.GetLayerDefn()
    srs_obj = in_lyr.GetSpatialRef()
    if os.path.exists(shpout):
        driver.DeleteDataSource(shpout)
    out_ds = driver.CreateDataSource(shpout)
    out_lyr = out_ds.CreateLayer("poly", srs_obj, geom_type=ogr.wkbPolygon)
    for i in range(0, len(field_name_list)):
        field_defn = in_layer_defn.GetFieldDefn(i)
        field_name = field_defn.GetName()
        if field_name not in field_name_list:
            continue
        out_lyr.CreateField(field_defn)

    area = []
    all_geom = []
    for in_feat in in_lyr:
        geom = in_feat.GetGeometryRef()
        area.append(geom.GetArea())
        all_geom.append(geom.ExportToWkb())

    index_max = int(np.argmax(np.array(area)))
    add_polygon(in_lyr[index_max], all_geom[index_max], in_lyr, out_lyr)


def erode_or_dilate_shapefile(infile: str, outfile: str, buffdist: float) -> bool:
    """
    dilate or erode all features in the shapeFile In
    Return True if succeed
    Parameters
    ----------
    infile :
        the shape file
        ex : /xxx/x/x/x/x/yyy.shp
    outfile :
        the resulting shapefile
        ex : /x/x/x/x/x.shp
    buffdist :
        the distance of dilatation or erosion
        ex : -10 for erosion
             +10 for dilatation

    OUT :
    - the shapeFile outfile
    """

    retour = True
    try:
        ds = ogr.Open(infile)
        drv = ds.GetDriver()
        if os.path.exists(outfile):
            drv.DeleteDataSource(outfile)
        drv.CopyDataSource(ds, outfile)
        ds.Destroy()

        ds = ogr.Open(outfile, 1)
        lyr = ds.GetLayer(0)
        for i in range(0, lyr.GetFeatureCount()):
            feat = lyr.GetFeature(i)
            lyr.DeleteFeature(i)
            geom = feat.GetGeometryRef()
            feat.SetGeometry(geom.Buffer(float(buffdist)))
            lyr.CreateFeature(feat)
        ds.Destroy()
    except:
        retour = False
    return retour


def erode_shapefile(infile, outfile, buffdist):
    return erode_or_dilate_shapefile(infile, outfile, -math.fabs(buffdist))


# TODO: duplicate name with iota2.sampling.TileEnvelope, outputs are diferent
def get_shape_extent(shape_in):
    """
    Get shape extent of shape_in. The shape must have only one geometry
    """

    driver = ogr.GetDriverByName("ESRI Shapefile")
    data_src = driver.Open(shape_in, 0)
    layer = data_src.GetLayer()

    for feat in layer:
        geom = feat.GetGeometryRef()
    env = geom.GetEnvelope()
    return env[0], env[2], env[1], env[3]


def remove_shape(shapePath, extensions):
    """
    IN:
        shapePath : path to the shapeFile without extension.
            ex : /path/to/myShape where /path/to/myShape.* exists
        extensions : all extensions to delete
            ex : extensions = [".prj",".shp",".dbf",".shx"]
    """
    for ext in extensions:
        if os.path.exists(shapePath + ext):
            os.remove(shapePath + ext)


def cp_shape_file(inpath, outpath, extensions, spe=False):

    for ext in extensions:
        if not spe:
            shutil.copy(inpath + ext, outpath + ext)
        else:
            shutil.copy(inpath + ext, outpath)


def rename_shapefile(inpath, filename, old_suffix, new_suffix, outpath=None):
    if not outpath:
        outpath = inpath
    run(
        "cp "
        + inpath
        + "/"
        + filename
        + old_suffix
        + ".shp "
        + outpath
        + "/"
        + filename
        + new_suffix
        + ".shp"
    )
    run(
        "cp "
        + inpath
        + "/"
        + filename
        + old_suffix
        + ".shx "
        + outpath
        + "/"
        + filename
        + new_suffix
        + ".shx"
    )
    run(
        "cp "
        + inpath
        + "/"
        + filename
        + old_suffix
        + ".dbf "
        + outpath
        + "/"
        + filename
        + new_suffix
        + ".dbf"
    )
    run(
        "cp "
        + inpath
        + "/"
        + filename
        + old_suffix
        + ".prj "
        + outpath
        + "/"
        + filename
        + new_suffix
        + ".prj"
    )
    return outpath + "/" + filename + new_suffix + ".shp"


def clip_vector_data(vectorFile, cutFile, opath, nameOut=None):
    """
    Cuts a shapefile with another shapefile
    ARGs:
       INPUT:
            -vectorFile: the shapefile to be cut
            -shpMask: the other shapefile
       OUTPUT:
            -the vector file clipped
    """
    if not nameOut:
        nameVF = vectorFile.split("/")[-1].split(".")[0]
        nameCF = cutFile.split("/")[-1].split(".")[0]
        outname = opath + "/" + nameVF + "_" + nameCF + ".shp"
    else:
        outname = opath + "/" + nameOut + ".shp"

    if os.path.exists(outname):
        os.remove(outname)

    Clip = (
        "ogr2ogr -clipsrc " + cutFile + " " + outname + " " + vectorFile + " -progress"
    )
    run(Clip)
    return outname


valid = False
empty = False
intersect = False
explain = False
delete = False
none = False

if __name__ == "__main__":
    if len(sys.argv) == 1:

        prog = os.path.basename(sys.argv[0])
        print("      " + sys.argv[0] + " [options]")
        print("     Help : ", prog, " --help")
        print("        or : ", prog, " -h")
        sys.exit(-1)
    else:
        usage = "usage: %prog [options] "
        parser = argparse.ArgumentParser(
            description="Verify shapefile geometries."
            "You have to choose only one option"
        )
        parser.add_argument(
            "-s",
            dest="shapefile",
            action="store",
            help="Input shapefile",
            required=True,
        )
        parser.add_argument(
            "-v",
            action="store_true",
            help="Check the validity of geometries of input file."
            "If geometry is not valid then buffer 0 to correct",
            default=False,
        )
        parser.add_argument(
            "-e",
            action="store_true",
            help="Check if a geometry is empty and create a new file with no empty geometries",
            default=False,
        )
        parser.add_argument(
            "-i",
            action="store_true",
            help="Check if each feature intersects another feature in the same file."
            "If True compute the difference. The common part is deleted",
            default=False,
        )
        parser.add_argument(
            "-ev",
            action="store_true",
            help="Explains the validity reason of each feature in a shapefile",
            default=False,
        )
        parser.add_argument(
            "-d",
            action="store_true",
            help="Delete the invalide geometries in a file",
            default=False,
        )

        args = parser.parse_args()

        if os.path.splitext(args.shapefile)[1] == ".shp":
            output_format = "ESRI shapefile"
        elif os.path.splitext(args.shapefile)[1] == ".sqlite":
            output_format = "SQLite"
        else:
            print("Output format not managed")
            sys.exit()

        if args.v or args.e or args.i or args.ev or args.d:
            if args.v:
                check_valid = True
            if args.e:
                empty = True
            if args.i:
                intersect = True
            if args.ev:
                explain = True
            if args.d:
                delete = True
        else:
            none = True

        if check_valid:
            check_valid_geom(args.shapefile, output_format)

        if empty:
            check_empty_geom(args.shapefile, output_format)

        if intersect:
            check_intersect3(args.shapefile, "CODE")

        if explain:
            explain_validity(args.shapefile)

        if delete:
            delete_invalid_geom(args.shapefile)

        if none:
            prog = os.path.basename(sys.argv[0])
            print("      " + sys.argv[0] + " [options]")
            print("     Help : ", prog, " --help")
            print("        or : ", prog, " -h")
            sys.exit(-1)
