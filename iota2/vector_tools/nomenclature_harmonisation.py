# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import csv
import os
import sys

from osgeo import ogr

from iota2.vector_tools import conditional_field_recode
from iota2.vector_tools import delete_field as df
from iota2.vector_tools import vector_functions as vf


def label_recoding(shapefile, csvfile, delimiter, fieldin, fieldout):
    """Parse recode rules CSV file and modify input shapefile by creating a
    new field and populate it with each occurence of csv file."""
    field_list = vf.get_fields(shapefile)
    if fieldout.lower() in [x.lower() for x in field_list]:
        df.deleteField(shapefile, fieldout)

    with open(csvfile, "r") as csvfile:
        csvreader = csv.DictReader(csvfile, delimiter=delimiter)
        for row in csvreader:
            conditional_field_recode.con_field_recode(
                shapefile, fieldin, fieldout, row[fieldin], row[fieldout]
            )

    data_src = ogr.Open(shapefile, 1)
    lyr = data_src.GetLayer()
    lyr.ResetReading()
    lyr.SetAttributeFilter(fieldout + "=0 or " + fieldout + " IS NULL")
    for feat in lyr:
        lyr.DeleteFeature(feat.GetFID())


if __name__ == "__main__":
    if len(sys.argv) == 1:
        prog = os.path.basename(sys.argv[0])
        print("      " + sys.argv[0] + " [options]")
        print("     Help : ", prog, " --help")
        print("        or : ", prog, " -h")
        sys.exit(-1)
    else:
        parser = argparse.ArgumentParser(
            description="Parse recode rules CSV file"
            "and modify input shapefile by creating a new field and populate it with each"
            "occurence of csv file"
        )
        parser.add_argument(
            "-s",
            dest="shapefile",
            action="store",
            help="Input shapefile",
            required=True,
        )
        parser.add_argument(
            "-csv",
            dest="csv",
            action="store",
            help="CSV or recode rules",
            required=True,
        )
        parser.add_argument(
            "-d", dest="delimiter", action="store", help="CSV delimiter", required=True
        )
        parser.add_argument(
            "-if",
            dest="ifield",
            action="store",
            help="Existing field for rules condition",
            required=True,
        )
        parser.add_argument(
            "-of",
            dest="ofield",
            action="store",
            help="Field to create and populate",
            required=True,
        )
        args = parser.parse_args()
        label_recoding(
            args.shapefile, args.csv, args.delimiter, args.ifield, args.ofield
        )
