"""Create a field and" "populate it of an input shapefile."""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import sqlite3
import sys

from osgeo import ogr


def add_field(
    filein,
    name_field,
    value_field,
    value_type=None,
    driver_name="ESRI Shapefile",
    f_width=None,
):
    """Create a field and" "populate it of an input shapefile."""
    driver = ogr.GetDriverByName(driver_name)
    source = driver.Open(filein, 1)
    layer = source.GetLayer()
    layer_name = layer.GetName()
    if not value_type:
        try:
            int(value_field)
            new_field1 = ogr.FieldDefn(name_field, ogr.OFTInteger)
        except BaseException():
            new_field1 = ogr.FieldDefn(name_field, ogr.OFTString)
    elif value_type == str:
        new_field1 = ogr.FieldDefn(name_field, ogr.OFTString)
        sqlite_type = "varchar"
    elif value_type == int:
        new_field1 = ogr.FieldDefn(name_field, ogr.OFTInteger)
        sqlite_type = "int"
    elif value_type == "int64":
        new_field1 = ogr.FieldDefn(name_field, ogr.OFTInteger64)
        sqlite_type = "BIGINT"
    elif value_type == float:
        new_field1 = ogr.FieldDefn(name_field, ogr.OFTFLOAT)
        sqlite_type = "float"
    if f_width:
        new_field1.SetWidth(f_width)

    if driver_name == "ESRI Shapefile":
        layer.CreateField(new_field1)
        for feat in layer:
            layer.SetFeature(feat)
            feat.SetField(name_field, value_field)
            layer.SetFeature(feat)
    elif driver_name == "SQLite":
        conn = sqlite3.connect(filein)
        cursor = conn.cursor()
        cursor.execute(
            f"alter table {layer_name} add column {name_field} {sqlite_type} default '{value_field}'"
        )
        conn.commit()
        cursor.close()

    return 0


if __name__ == "__main__":
    if len(sys.argv) == 1:
        prog = os.path.basename(sys.argv[0])
        print("      " + sys.argv[0] + " [options]")
        print("     Help : ", prog, " --help")
        print("        or : ", prog, " -h")
        sys.exit(-1)
    else:
        parser = argparse.ArgumentParser(
            description="Create a field and" "populate it of an input shapefile"
        )
        parser.add_argument(
            "-s",
            dest="shapefile",
            action="store",
            help="Input shapefile",
            required=True,
        )
        parser.add_argument(
            "-f", dest="field", action="store", help="Field to add", required=True
        )
        parser.add_argument(
            "-v",
            dest="value",
            action="store",
            help="Value to insert in the field",
            required=True,
        )
        args = parser.parse_args()
        add_field(args.shapefile, args.field, args.value)
