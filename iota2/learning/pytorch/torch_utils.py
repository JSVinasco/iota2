#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import functools
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import math
import random
import warnings
from typing import Dict, List, Optional, Tuple

import numpy as np
import torch
import xarray as xr
from sklearn.preprocessing import StandardScaler

from iota2.sensors.Landsat_5_old import landsat_5_old
from iota2.sensors.Landsat_8 import landsat_8
from iota2.sensors.Landsat_8_old import landsat_8_old
from iota2.sensors.Sentinel_1 import sentinel_1
from iota2.sensors.Sentinel_2 import sentinel_2
from iota2.sensors.Sentinel_2_L3A import sentinel_2_l3a
from iota2.sensors.Sentinel_2_S2C import sentinel_2_s2c
from iota2.sensors.User_features import user_features

warnings.filterwarnings("ignore")


class StatsCalculator():
    """compute stats by batch or considering sample of the dataset
    """
    def __init__(self,
                 dataloader: torch.utils.data.DataLoader,
                 sensors_ind: Dict,
                 subsample: Optional[float] = None,
                 min_stat: bool = False,
                 max_stat: bool = False,
                 var_stat: bool = False,
                 expected_quantiles: List[float] = [0.1, 0.5, 0.95],
                 custom_stats_on_subset: Dict = {}):
        """
        Parameters
        ----------
        dataloader
            pytorch dataloader
        sensors_ind
            dictionary as :
            {'sentinel2': {'data': 0, 'mask': 1}, 'target': 2, 'weight': 3}
            which describe sensors position in data coming from dataloader
        subsample
            percentage [0;1] of the full dataset to compute stats on it
        expected_quantiles
            list of quantiles to compute (thanks to numpy.quantile).
            Quantiles will be computed using the subsample dataset.
        custom_stats_on_subset
            Dictionnary of partial function to compute stats on
            the subsample dataset. Key as the statistics label

            >>> custom_stats_on_subset["quantile"] = functools.partial(
                get_quantiles, **{"quantiles": [0.1, 0.5, 0.95]})

            if the subsample dataset contains 3 samples and 2 Sentinel-2
            dates (10 bandes each), then the function will receive
            a tensor of data with the shape (3, 20)

        Warning
        -------
        data provided by the dataloder must be in
        (batch, date, component) shape
        """
        self.dataloader = dataloader
        self.sensors_ind = sensors_ind
        self.subsample = subsample
        self.min_stat = min_stat
        self.max_stat = max_stat
        self.var_stat = var_stat
        self.mean_stat = True
        self.device = torch.device(
            "cuda") if torch.cuda.is_available() else torch.device("cpu")

        self.custom_stats_on_subset = custom_stats_on_subset
        if f"quantile_{expected_quantiles[0]}" not in custom_stats_on_subset:
            for quantile in expected_quantiles:
                self.custom_stats_on_subset[
                    f"quantile_{quantile}"] = functools.partial(
                        self.get_quantiles, **{"quantile": quantile})

    def get_quantiles(self, sensor_data: np.array, quantile):
        """compute nanquantiles (ignore nans)
        """
        return torch.tensor(np.nanquantile(sensor_data, quantile,
                                           axis=0)).to(self.device)

    def get_max(self, sensor_data: np.array) -> torch.tensor:
        """np.nanmax() -> ignore nan in array, torch 1.4 does not"""
        return torch.tensor(np.nanmax(sensor_data, axis=0)).to(self.device)

    def get_min(self, sensor_data: np.array) -> torch.tensor:
        """np.nanmin() -> ignore nan in array, torch 1.4 does not"""
        return torch.tensor(np.nanmin(sensor_data, axis=0)).to(self.device)

    def check_full_date(self, input_shape: Tuple[int, int, int],
                        indices: List[int]) -> bool:
        """according to a input shape formatted as (batch_size, dates, bands)
        check if all indexes belong to the same dates -> return True if
        indices belong to a a list of full dates.
        """
        _, dates, bands = input_shape
        hist, _ = np.histogram(indices,
                               bins=range(0, (dates * bands) + bands, bands))
        return all([val in (0, bands) for val in hist])

    @classmethod
    def stats_to_device(cls, stats: Dict, device: str) -> torch.tensor:
        """place stats tensor to the expected device"""
        tmp_stats: Dict[str, Dict[str, torch.tensor]] = {}
        for sensor_name, sensor_stats in stats.items():
            tmp_stats[sensor_name] = {}
            for stats_name, stats_tensor in sensor_stats.items():
                tmp_stats[sensor_name][stats_name] = stats_tensor.to(device)
        return tmp_stats

    def compute_sub_stats(self, stats: Dict, subset_data: Dict) -> Dict:
        """compute stats on data set aside
        """
        updated_stats = stats.copy()
        for sensor_name, sensor_data in subset_data.items():
            del updated_stats[sensor_name]["scaler"]
            means = updated_stats[sensor_name]["mean"]
            is_nan = torch.isnan(means)
            indices = is_nan.nonzero()
            excluded_indices_stats = [int(elem[0]) for elem in indices]
            if not self.check_full_date(updated_stats[sensor_name]["shape"],
                                        excluded_indices_stats):
                raise ValueError(
                    "iota2 detected a date containing a band full of NaNs"
                    ", something go wrong during the deepL stats"
                    " computation")
            del updated_stats[sensor_name]["shape"]
            if self.subsample:
                for stat_name, func in self.custom_stats_on_subset.items():
                    stat_vals = func(sensor_data)
                    updated_stats[sensor_name][stat_name] = stat_vals
        return updated_stats

    def compute(self) -> Dict:
        """
        Note
        ----
        return a dictionary of stats for each sensors as
        >>> {'s2_theia': {'min': tensor([-0.0100, ..., 0.0000]),
                          'max': tensor([-0.0100, ..., 0.0000,]),
                          'mean': tensor([-0.01, ..., 0.]),
                          'var': tensor([-0.01, ..., 0.]),
                          'quantile_0.1': tensor([-0.01, ..., 0.)],
                          'quantile_0.5': tensor([-0.01, ..., 0.])}}
        """
        stats = {}
        subset_data = {}
        for batch_num, batch in enumerate(self.dataloader):
            if len(batch[0].shape) == 3:
                values = tensors_to_kwargs_full(batch,
                                                self.sensors_ind,
                                                as_tuple=True)
            elif len(batch[0].shape) == 4:
                values = tensors_to_kwargs(batch,
                                           self.sensors_ind,
                                           as_tuple=True)
            # init
            if batch_num == 0:
                for sensor_name, (batch_data, batch_mask) in values.items():
                    if "mask" in sensor_name or batch_data is None:
                        continue
                    stats[sensor_name] = {
                        "scaler": StandardScaler(),
                        "shape": batch_data.shape
                    }
                    subset_data[sensor_name] = torch.tensor([])
            for sensor_name, (batch_data, batch_mask) in values.items():
                if batch_data is None:
                    continue
                batch_size, nb_dates, nb_comp = batch_data.shape
                batch_data = np.array(batch_data.cpu())
                # in case of the use of gapfilled data
                if batch_mask is None:
                    batch_mask = torch.ones(batch_size, nb_dates, 1) * 0.

                batch_mask = batch_mask.cpu()
                batch_mask = np.dstack([batch_mask] * nb_comp)
                # unclear values are set to np.nan
                batch_data[batch_mask != 0] = np.nan
                if self.subsample:
                    samples_ind = random.sample(
                        range(batch_size), int(self.subsample * batch_size))
                batch_reshape = batch_data.reshape(
                    (batch_size, nb_dates * nb_comp))
                stats[sensor_name]["scaler"].partial_fit(batch_reshape)
                if self.mean_stat:
                    stats[sensor_name]["mean"] = torch.tensor(
                        stats[sensor_name]["scaler"].mean_).to(self.device)
                if self.var_stat:
                    stats[sensor_name]["var"] = torch.tensor(
                        stats[sensor_name]["scaler"].var_).to(self.device)
                if self.subsample and samples_ind:
                    if len(subset_data[sensor_name]):
                        subset_data[sensor_name] = np.concatenate(
                            (subset_data[sensor_name],
                             batch_reshape[samples_ind]))
                    else:
                        subset_data[sensor_name] = batch_reshape[samples_ind]
                if self.max_stat:
                    batch_max = self.get_max(batch_reshape)
                    if "max" not in stats[sensor_name]:
                        stats[sensor_name]["max"] = batch_max
                    else:
                        stats[sensor_name]["max"] = self.get_max(
                            torch.stack([
                                stats[sensor_name]["max"].cpu(),
                                batch_max.cpu()
                            ]))
                if self.min_stat:
                    batch_min = self.get_min(batch_reshape)
                    if "min" not in stats[sensor_name]:
                        stats[sensor_name]["min"] = batch_min
                    else:
                        stats[sensor_name]["min"] = self.get_min(
                            torch.stack([
                                stats[sensor_name]["min"].cpu(),
                                batch_min.cpu()
                            ]))
        stats = self.compute_sub_stats(stats, subset_data)
        # attempt to free memory
        self.subsample = None
        return stats


def tensors_to_kwargs(batch_tensor, sensors_ind, as_tuple=False) -> Dict:
    """translate data comming from dataloader to fed the nn
       forward method
    """
    device = torch.device(
        "cuda") if torch.cuda.is_available() else torch.device("cpu")
    s2_theia = np.squeeze(
        batch_tensor[sensors_ind["sentinel2"]["data"]], axis=0).to(
            device, non_blocking=False) if "sentinel2" in sensors_ind else None
    s2_s2c = np.squeeze(
        batch_tensor[sensors_ind["sentinel2s2c"]["data"]], axis=0).to(
            device,
            non_blocking=False) if "sentinel2s2c" in sensors_ind else None
    s2_l3a = np.squeeze(
        batch_tensor[sensors_ind["sentinel2l3a"]["data"]], axis=0).to(
            device,
            non_blocking=False) if "sentinel2l3a" in sensors_ind else None
    l5 = np.squeeze(
        batch_tensor[sensors_ind["landsat5old"]["data"]], axis=0).to(
            device,
            non_blocking=False) if "landsat5old" in sensors_ind else None
    l8_old = np.squeeze(
        batch_tensor[sensors_ind["landsat8old"]["data"]], axis=0).to(
            device,
            non_blocking=False) if "landsat8old" in sensors_ind else None
    l8 = np.squeeze(batch_tensor[sensors_ind["landsat8"]["data"]], axis=0).to(
        device, non_blocking=False) if "landsat8" in sensors_ind else None
    s1_desvv = np.squeeze(
        batch_tensor[sensors_ind["sentinel1_desvv"]["data"]], axis=0).to(
            device,
            non_blocking=False) if "sentinel1_desvv" in sensors_ind else None
    s1_desvh = np.squeeze(
        batch_tensor[sensors_ind["sentinel1_desvh"]["data"]], axis=0).to(
            device,
            non_blocking=False) if "sentinel1_desvh" in sensors_ind else None
    s1_ascvv = np.squeeze(
        batch_tensor[sensors_ind["sentinel1_ascvv"]["data"]], axis=0).to(
            device,
            non_blocking=False) if "sentinel1_ascvv" in sensors_ind else None
    s1_ascvh = np.squeeze(
        batch_tensor[sensors_ind["sentinel1_ascvh"]["data"]], axis=0).to(
            device,
            non_blocking=False) if "sentinel1_ascvh" in sensors_ind else None

    s2_theia_masks = np.squeeze(
        batch_tensor[sensors_ind["sentinel2"]["mask"]], axis=0
    ).to(
        device, non_blocking=False
    ) if s2_theia is not None and sensors_ind["sentinel2"]["mask"] else None
    s2_s2c_masks = np.squeeze(
        batch_tensor[sensors_ind["sentinel2s2c"]["mask"]], axis=0
    ).to(
        device, non_blocking=False
    ) if s2_s2c is not None and sensors_ind["sentinel2s2c"]["mask"] else None
    s2_l3a_masks = np.squeeze(
        batch_tensor[sensors_ind["sentinel2l3a"]["mask"]], axis=0
    ).to(
        device, non_blocking=False
    ) if s2_l3a is not None and sensors_ind["sentinel2l3a"]["mask"] else None
    l5_masks = np.squeeze(
        batch_tensor[sensors_ind["landsat5old"]["mask"]], axis=0).to(
            device, non_blocking=False
        ) if l5 is not None and sensors_ind["landsat5old"]["mask"] else None
    l8_old_masks = np.squeeze(
        batch_tensor[sensors_ind["landsat8old"]["mask"]], axis=0
    ).to(
        device, non_blocking=False
    ) if l8_old is not None and sensors_ind["landsat8old"]["mask"] else None
    l8_masks = np.squeeze(
        batch_tensor[sensors_ind["landsat8"]["mask"]], axis=0).to(
            device, non_blocking=False
        ) if l8 is not None and sensors_ind["landsat8"]["mask"] else None
    s1_desvv_masks = np.squeeze(
        batch_tensor[sensors_ind["sentinel1_desvv"]["mask"]], axis=0).to(
            device,
            non_blocking=False) if s1_desvv is not None and sensors_ind[
                "sentinel1_desvv"]["mask"] else None
    s1_desvh_masks = np.squeeze(
        batch_tensor[sensors_ind["sentinel1_desvh"]["mask"]], axis=0).to(
            device,
            non_blocking=False) if s1_desvh is not None and sensors_ind[
                "sentinel1_desvh"]["mask"] else None
    s1_ascvv_masks = np.squeeze(
        batch_tensor[sensors_ind["sentinel1_ascvv"]["mask"]], axis=0).to(
            device,
            non_blocking=False) if s1_ascvv is not None and sensors_ind[
                "sentinel1_ascvv"]["mask"] else None
    s1_ascvh_masks = np.squeeze(
        batch_tensor[sensors_ind["sentinel1_ascvh"]["mask"]], axis=0).to(
            device,
            non_blocking=False) if s1_ascvh is not None and sensors_ind[
                "sentinel1_ascvh"]["mask"] else None

    params = {
        "s2_theia": s2_theia,
        "s2_s2c": s2_s2c,
        "s2_l3a": s2_l3a,
        "l5": l5,
        "l8_old": l8_old,
        "l8": l8,
        "s1_desvv": s1_desvv,
        "s1_desvh": s1_desvh,
        "s1_ascvv": s1_ascvv,
        "s1_ascvh": s1_ascvh,
        "l5_masks": l5_masks,
        "l8_masks": l8_masks,
        "l8_old_masks": l8_old_masks,
        "s2_theia_masks": s2_theia_masks,
        "s2_s2c_masks": s2_s2c_masks,
        "s2_l3a_masks": s2_l3a_masks,
        "s1_desvv_masks": s1_desvv_masks,
        "s1_desvh_masks": s1_desvh_masks,
        "s1_ascvv_masks": s1_ascvv_masks,
        "s1_ascvh_masks": s1_ascvh_masks
    }
    if as_tuple:
        params = {
            "s2_theia": (s2_theia, s2_theia_masks),
            "s2_s2c": (s2_s2c, s2_s2c_masks),
            "s2_l3a": (s2_l3a, s2_l3a_masks),
            "l5": (l5, l5_masks),
            "l8_old": (l8_old, l8_old_masks),
            "l8": (l8, l8_masks),
            "s1_desvv": (s1_desvv, s1_desvv_masks),
            "s1_desvh": (s1_desvh, s1_desvh_masks),
            "s1_ascvv": (s1_ascvv, s1_ascvv_masks),
            "s1_ascvh": (s1_ascvh, s1_ascvh_masks)
        }

    return params


def tensors_to_kwargs_full(batch_tensor, sensors_ind, as_tuple=False) -> Dict:
    """translate data comming from dataloader to fed the nn
       forward method
    """
    device = torch.device(
        "cuda") if torch.cuda.is_available() else torch.device("cpu")
    s2_theia = batch_tensor[sensors_ind["sentinel2"]["data"]].to(
        device, non_blocking=False) if "sentinel2" in sensors_ind else None
    s2_s2c = batch_tensor[sensors_ind["sentinel2s2c"]["data"]].to(
        device, non_blocking=False) if "sentinel2s2c" in sensors_ind else None
    s2_l3a = batch_tensor[sensors_ind["sentinel2l3a"]["data"]].to(
        device, non_blocking=False) if "sentinel2l3a" in sensors_ind else None
    l5 = batch_tensor[sensors_ind["landsat5old"]["data"]].to(
        device, non_blocking=False) if "landsat5old" in sensors_ind else None
    l8_old = batch_tensor[sensors_ind["landsat8old"]["data"]].to(
        device, non_blocking=False) if "landsat8old" in sensors_ind else None
    l8 = batch_tensor[sensors_ind["landsat8"]["data"]].to(
        device, non_blocking=False) if "landsat8" in sensors_ind else None
    s1_desvv = batch_tensor[sensors_ind["sentinel1_desvv"]["data"]].to(
        device,
        non_blocking=False) if "sentinel1_desvv" in sensors_ind else None
    s1_desvh = batch_tensor[sensors_ind["sentinel1_desvh"]["data"]].to(
        device,
        non_blocking=False) if "sentinel1_desvh" in sensors_ind else None
    s1_ascvv = batch_tensor[sensors_ind["sentinel1_ascvv"]["data"]].to(
        device,
        non_blocking=False) if "sentinel1_ascvv" in sensors_ind else None
    s1_ascvh = batch_tensor[sensors_ind["sentinel1_ascvh"]["data"]].to(
        device,
        non_blocking=False) if "sentinel1_ascvh" in sensors_ind else None

    s2_theia_masks = batch_tensor[sensors_ind["sentinel2"]["mask"]].to(
        device, non_blocking=False
    ) if s2_theia is not None and sensors_ind["sentinel2"]["mask"] else None
    s2_s2c_masks = batch_tensor[sensors_ind["sentinel2s2c"]["mask"]].to(
        device, non_blocking=False
    ) if s2_s2c is not None and sensors_ind["sentinel2s2c"]["mask"] else None
    s2_l3a_masks = batch_tensor[sensors_ind["sentinel2l3a"]["mask"]].to(
        device, non_blocking=False
    ) if s2_l3a is not None and sensors_ind["sentinel2l3a"]["mask"] else None
    l5_masks = batch_tensor[sensors_ind["landsat5old"]["mask"]].to(
        device, non_blocking=False
    ) if l5 is not None and sensors_ind["landsat5old"]["mask"] else None
    l8_old_masks = batch_tensor[sensors_ind["landsat8old"]["mask"]].to(
        device, non_blocking=False
    ) if l8_old is not None and sensors_ind["landsat8old"]["mask"] else None
    l8_masks = batch_tensor[sensors_ind["landsat8"]["mask"]].to(
        device, non_blocking=False
    ) if l8 is not None and sensors_ind["landsat8"]["mask"] else None
    s1_desvv_masks = batch_tensor[sensors_ind["sentinel1_desvv"]["mask"]].to(
        device, non_blocking=False) if s1_desvv is not None and sensors_ind[
            "sentinel1_desvv"]["mask"] else None
    s1_desvh_masks = batch_tensor[sensors_ind["sentinel1_desvh"]["mask"]].to(
        device, non_blocking=False) if s1_desvh is not None and sensors_ind[
            "sentinel1_desvh"]["mask"] else None
    s1_ascvv_masks = batch_tensor[sensors_ind["sentinel1_ascvv"]["mask"]].to(
        device, non_blocking=False) if s1_ascvv is not None and sensors_ind[
            "sentinel1_ascvv"]["mask"] else None
    s1_ascvh_masks = batch_tensor[sensors_ind["sentinel1_ascvh"]["mask"]].to(
        device, non_blocking=False) if s1_ascvh is not None and sensors_ind[
            "sentinel1_ascvh"]["mask"] else None

    params = {
        "s2_theia": s2_theia,
        "s2_s2c": s2_s2c,
        "s2_l3a": s2_l3a,
        "l5": l5,
        "l8_old": l8_old,
        "l8": l8,
        "s1_desvv": s1_desvv,
        "s1_desvh": s1_desvh,
        "s1_ascvv": s1_ascvv,
        "s1_ascvh": s1_ascvh,
        "l5_masks": l5_masks,
        "l8_masks": l8_masks,
        "l8_old_masks": l8_old_masks,
        "s2_theia_masks": s2_theia_masks,
        "s2_s2c_masks": s2_s2c_masks,
        "s2_l3a_masks": s2_l3a_masks,
        "s1_desvv_masks": s1_desvv_masks,
        "s1_desvh_masks": s1_desvh_masks,
        "s1_ascvv_masks": s1_ascvv_masks,
        "s1_ascvh_masks": s1_ascvh_masks
    }
    if as_tuple:
        params = {
            "s2_theia": (s2_theia, s2_theia_masks),
            "s2_s2c": (s2_s2c, s2_s2c_masks),
            "s2_l3a": (s2_l3a, s2_l3a_masks),
            "l5": (l5, l5_masks),
            "l8_old": (l8_old, l8_old_masks),
            "l8": (l8, l8_masks),
            "s1_desvv": (s1_desvv, s1_desvv_masks),
            "s1_desvh": (s1_desvh, s1_desvh_masks),
            "s1_ascvv": (s1_ascvv, s1_ascvv_masks),
            "s1_ascvh": (s1_ascvh, s1_ascvh_masks)
        }
    return params


class early_stop():
    """stop the learning stage on conditions
    """
    def __init__(self,
                 epoch_to_trigger: int,
                 patience: int,
                 tol: float,
                 metric: str = "val_loss"):
        """

        Parameters
        ----------
        epoch_to_trigger:
            epoch to start monitoring score
        patience:
            number of checks with no improvement after which
            the training will be stopped
        tol:
            minimum change in the monitored quantity to qualify
            as an improvement.
            if metric is 'train_loss' or 'valid_loss' then tol must be in dB.
        metric:
            metric to be monitored ['train_loss', 'val_loss',
                                    'fscore', 'oa', 'kappa']
            default is val_loss
        """
        self.epoch_to_trigger = epoch_to_trigger
        self.patience = patience
        self.tol = tol
        self.metric = metric
        self.available_metrics = [
            'train_loss', 'val_loss', 'fscore', 'oa', 'kappa'
        ]
        if self.metric not in self.available_metrics:
            raise ValueError(
                f"metric parameter must be in {self.available_metrics}")

        self.current_epoch = 0
        self.patience_counter = 0
        self.last_score: float = None

    def step(self, score: float) -> bool:
        """
        return True if an early stop is required
        """
        self.current_epoch += 1
        flag = False
        if self.current_epoch >= self.epoch_to_trigger:
            if self.current_epoch != 1:
                diff = abs(score - self.last_score)
                worst = (score -
                         self.last_score) > 0 if "loss" in self.metric else (
                             score - self.last_score) < 0
                if worst or (diff <= self.tol and not worst):
                    self.patience_counter += 1
                else:
                    self.patience_counter = 0
                self.last_score = score
                flag = self.patience_counter >= self.patience
        else:
            self.last_score = score
        return flag


class batch_provider():
    """Randomly distribute fids accros batches. This random distribution
    is done as many times as there is epochs.
    """
    def __init__(self, possible_fids_list: List[int], batch_size: int,
                 nb_epoch: int):
        """
        Parameters
        ----------

        possible_fids_lits
            list of possible fids (index in the database)
        batch_size
            size of the batch
        nb_epoch
            number of epochs

        Examples
        --------
        >>> fids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        >>> batch_size = 5
        >>> nb_epoch = 2
        >>> batch_p = batch_provider(fids, batch_size, nb_epoch)
        >>> batch_p.get_epoch(0)
        >>> batch_p.get_epoch(1)
        [[7, 9, 10, 8], [6, 4, 1, 5], [2, 3]]
        [[6, 2, 10, 1], [4, 3, 7, 5], [9, 8]]
        """
        self.possible_fids_list = possible_fids_list
        self.batch_size = batch_size

        self.batch_ep = []
        for _ in range(nb_epoch):
            random.shuffle(self.possible_fids_list)
            batch = []
            for elem in self.batch_generator(self.possible_fids_list,
                                             batch_size):
                batch.append(elem)
            self.batch_ep.append(batch)
        self.nb_batch = len(self.batch_ep[0])

    def get_epoch(self, ep_num) -> List[List[int]]:
        """
        """
        return self.batch_ep[ep_num]

    def __len__(self):
        return self.nb_batch

    def batch_generator(self, my_list: List[int], size: int):
        """generate batch of data

        Parameters
        ----------
        my_list
            list of data (usally fids)
        size
            batch's size
        """
        ind = 0
        while ind < len(my_list):
            yield my_list[ind:ind + size]
            ind += size


def update_confusion_matrix(pred, targets, conf_matrix, all_preds,
                            all_targets):
    local_cm = torch.zeros_like(conf_matrix)
    decoded_predictions = torch.argmax(pred, 1)
    decoded_targets = targets

    for t, p in zip(decoded_targets, decoded_predictions):
        local_cm[t, p] += 1
    return (conf_matrix + local_cm,
            torch.cat((all_preds.float(), decoded_predictions.float())),
            torch.cat((all_targets.float(), decoded_targets.float())))


def plot_metrics(kappa: List[float], fscore: List[float], o_acc: List[float],
                 output_path: str, learning_rate: float,
                 batch_size: int) -> None:
    """generate loss evolution accross epochs

    Parameters
    ----------

    kappa
        list of kappa for each epochs
    fscore
        list of fscore for each epochs
    o_acc
        list of overall accuracy for each epochs
    output_path: str
        output figure file
    learning_rate
        nn learning rate (use only to plot the value on the figure)
    batch_size
        nn batch size (use only to plot the value on the figure)
    """
    # imported inside function on pupose.
    # If it's not, an unexpected segfault appears
    import matplotlib
    matplotlib.get_backend()
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt

    epochs = np.arange(len(kappa))

    xsize = int(0.2 * len(kappa))
    ysize = 10
    _, ax = plt.subplots(figsize=(xsize, ysize))

    plt.plot(epochs, kappa, label="kappa")
    plt.plot(epochs, fscore, label="fscore")
    plt.plot(epochs, o_acc, label="overall accuracy")
    ax.set_ylabel('values')
    ax.set_title('metrics for each epochs for valid dataset')
    ax.set_xticks(epochs)
    ax.set_xticklabels([ep + 1 for ep in epochs], rotation=90)

    # plot hyperparameters values in legend
    plt.plot([], [], c='w', label=f"batch size {batch_size}")
    plt.plot([], [], c='w', label=f"learning rate {learning_rate}")

    ax.legend()
    plt.savefig(output_path, format="png", dpi=200)


def plot_loss(train_loss: List[float], valid_loss: List[float],
              output_path: str, learning_rate: float, batch_size: int) -> None:
    """generate loss evolution accross epochs

    Parameters
    ----------

    train_loss
        list of train loss for each epochs
    valid_loss
        list of valid loss for each epochs
    output_path: str
        output figure file
    learning_rate
        nn learning rate (use only to plot the value on the figure)
    batch_size
        nn batch size (use only to plot the value on the figure)
    """
    # imported inside function on pupose.
    # If it's not, an unexpected segfault appears
    import matplotlib
    matplotlib.get_backend()
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt

    train_loss = [math.log(loss) for loss in train_loss]
    valid_loss = [math.log(loss) for loss in valid_loss]

    epochs = np.arange(len(train_loss))

    xsize = int(0.2 * len(train_loss))
    # ysize = max(train_loss + valid_loss)
    ysize = 10
    fig, ax = plt.subplots(figsize=(xsize, ysize))

    plt.plot(epochs, train_loss, label="train loss")
    plt.plot(epochs, valid_loss, label="valid loss")
    ax.set_ylabel('loss')
    ax.set_title('log(loss) by epochs for train and valid dataset')
    ax.set_xticks(epochs)
    ax.set_xticklabels([ep + 1 for ep in epochs], rotation=90)

    plt.plot([], [], c='w', label=f"batch size {batch_size}")
    plt.plot([], [], c='w', label=f"learning rate {learning_rate}")

    ax.legend()
    plt.savefig(output_path, format="png", dpi=200)
