#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module containing all natively usable pytorch neural networks in iota2"""
import inspect
import pickle
from abc import ABC, ABCMeta, abstractmethod
from functools import reduce
from typing import Dict, List, Optional

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

from iota2.common.file_utils import verify_import
from iota2.learning.pytorch.lightweight_temporal_attention_encoder import LTAE


class NeuralNetworkMeta(ABCMeta):
    """metaclass which check every abstract methods definition

    from
    https://stackoverflow.com/questions/55309793/python-enforce-specific-method-signature-for-subclasses
    """
    def __init__(cls, name, bases, attrs):
        errors = []
        for base_cls in bases:
            for meth_name in getattr(base_cls, "__abstractmethods__", ()):
                orig_argspec = inspect.getfullargspec(
                    getattr(base_cls, meth_name))
                target_argspec = inspect.getfullargspec(getattr(
                    cls, meth_name))
                if orig_argspec != target_argspec:
                    errors.append(
                        f"Abstract method {meth_name!r} not implemented"
                        f" with correct signature in {cls.__name__!r}."
                        f" Expected {orig_argspec}.")
            if errors:
                raise TypeError("\n".join(errors))


class Iota2NeuralNetworkFactory():
    """factory to a Iota2NeuralNetwork object
    """
    def get_nn(self,
               nn_name: str,
               nn_parameters: Dict,
               nb_features: Optional[int] = None,
               nb_class: Optional[int] = None,
               pickle_file: Optional[str] = None,
               external_nn_module: Optional[str] = None,
               random_seed_number: int = 42) -> nn.Module:
        """from the neural network name, instanciate the right
        pytorch model with the input parameters

        Note
        ----
        if pickle_file, then nn_parameters is save at pickle_file as
        a pickle object
        """
        torch_model = None
        nn_parameters_feat = {}
        nn_parameters_class = {}
        if "nb_features" not in nn_parameters:
            if not nb_features:
                raise ValueError(("number of features not found in "
                                  "'dl_parameters' -> mandatory"))
            nn_parameters_feat = {"nb_features": nb_features}
        if "nb_class" not in nn_parameters:
            if not nb_class:
                raise ValueError(
                    "number of class not found in 'dl_parameters' -> mandatory"
                )
            nn_parameters_class = {"nb_class": nb_class}

        self.nn_parameters = {
            **nn_parameters_feat,
            **nn_parameters_class,
            **nn_parameters
        }
        torch.manual_seed(random_seed_number)

        torch_model = self._get_nn(nn_name, external_nn_module)
        if pickle_file:
            with open(pickle_file, "wb") as file_instance_nn_parameters:
                pickle.dump(self.nn_parameters,
                            file_instance_nn_parameters,
                            protocol=pickle.HIGHEST_PROTOCOL)
        if torch_model is None:
            raise ValueError(
                f"neural network {nn_name} is not handle by iota2")
        return torch_model

    def _get_nn(self, nn_name, external_nn_module) -> nn.Module:
        """instanciate the right nn model
        """
        if nn_name.lower() == "ann":
            model = ANN(**self.nn_parameters)
        elif nn_name.lower() == "ltaeclassifier":
            model = LTAEClassifier(**self.nn_parameters)
        elif nn_name.lower() == "mlpclassifier":
            model = MLPClassifier(**self.nn_parameters)
        elif nn_name.lower() == "simpleselfattentionclassifier":
            model = SimpleSelfAttentionClassifier(**self.nn_parameters)
        elif external_nn_module:
            external_nn_module = verify_import(external_nn_module)
            user_nn_class = getattr(external_nn_module, nn_name)
            model = user_nn_class(**self.nn_parameters)
        return model


class Iota2NeuralNetwork(nn.Module, ABC, metaclass=NeuralNetworkMeta):
    """every neural networks provided by iota2 must inherit from this
    one to ensure some specific signatures.
    """
    def __init__(self,
                 nb_features: int,
                 nb_class: int,
                 doy_sensors_dic: Optional[Dict] = None,
                 default_std: int = 1,
                 default_mean: int = 0):
        super(Iota2NeuralNetwork, self).__init__()
        self.default_std = default_std
        self.default_mean = default_mean
        self._stats: Dict[str, torch.tensor] = {}
        if torch.cuda.is_available():
            self.device = torch.device('cuda')
        else:
            self.device = torch.device('cpu')

    @abstractmethod
    def forward(self, l5, l8, l8_old, s2_theia, s2_s2c, s2_l3a, s1_desvv,
                s1_desvh, s1_ascvv, s1_ascvh, l5_masks, l8_masks, l8_old_masks,
                s2_theia_masks, s2_s2c_masks, s2_l3a_masks, s1_desvv_masks,
                s1_desvh_masks, s1_ascvv_masks, s1_ascvh_masks):
        """abstractmethod used to force the forward definition
        """
        raise NotImplementedError

    def set_stats(self, stats: Dict):
        self._stats = stats

    @staticmethod
    def nans_imputation(
            data: torch.tensor,
            imputation_values: torch.tensor,
            default_stats_values: Optional[float] = None) -> torch.tensor:
        """ replace input values

        data:
           input data formatted as batch_size, dates, bands = data.shape 
        """
        batch_size, dates, bands = data.shape
        stats_is_nan = torch.isnan(imputation_values)
        if default_stats_values is not None:
            stats_indices = stats_is_nan.nonzero(as_tuple=True)
            imputation_values[stats_indices] = default_stats_values

        stats_stack = torch.stack(batch_size *
                                  [imputation_values.reshape((dates, bands))])
        data_is_nan = torch.isnan(data)
        data_indices = data_is_nan.nonzero(as_tuple=True)

        data[data_indices] = stats_stack[data_indices].float().to(data.device)
        return data

    @staticmethod
    def standardize(features: torch.tensor,
                    mean: torch.tensor,
                    std: torch.tensor,
                    default_mean: float = 0.0,
                    default_std: float = 1.0) -> torch.tensor:
        """
        Standardize features by removing the mean and scaling to
        unit variance as (features - mean) / std

        Parameters
        ----------
        features
            tensor of features (batch_size, nb_dates, nb_bands)
        mean
            mean values of each dates (nb_dates * nb_bands)
        std
            std values of each dates (nb_dates * nb_bands)
        default_mean
            replace nan values
        default_std
            replace nan and 0 values
        """
        std[torch.isnan(std).nonzero(as_tuple=True)] = default_std
        std[std == 0] = default_std
        mean[torch.isnan(mean).nonzero(as_tuple=True)] = default_mean

        _, nb_dates, nb_bands = features.shape
        mean = mean.reshape((nb_dates, nb_bands))
        std = std.reshape((nb_dates, nb_bands))

        x = (features - mean) / std
        x = x.float()
        return x

    @classmethod
    def save(cls,
             output_file: str,
             state_dict: Dict,
             stats: Dict,
             labels: List[str],
             device="cpu") -> None:
        """class method to save the model's state
        """
        stats_on_device: Dict[str, torch.tensor] = {}
        for sensor_name, sensor_stats in stats.items():
            stats_on_device[sensor_name] = {}
            for stat_name, stat_values in sensor_stats.items():
                stats_on_device[sensor_name][stat_name] = stat_values.to(
                    device)

        with open(output_file, "wb") as model_to_save:
            pickle.dump((state_dict, stats_on_device, labels),
                        model_to_save,
                        protocol=pickle.HIGHEST_PROTOCOL)


class ANN(Iota2NeuralNetwork):
    """Same as ANN but reshape it's inputs
    """
    def __init__(self,
                 nb_features: int,
                 nb_class: int,
                 layer: int = 1,
                 doy_sensors_dic: Optional[Dict] = None,
                 default_std: int = 1,
                 default_mean: int = 0,
                 **kwargs):
        super().__init__(nb_features, nb_class, doy_sensors_dic, default_std,
                         default_mean)
        self.nb_class = nb_class
        self.layer = layer
        self.sensors_doy = doy_sensors_dic
        self.fc1 = nn.Linear(in_features=nb_features, out_features=16)
        self.fc2 = nn.Linear(in_features=16, out_features=12)
        self.output = nn.Linear(in_features=12, out_features=nb_class)

    def forward(self, l5, l8, l8_old, s2_theia, s2_s2c, s2_l3a, s1_desvv,
                s1_desvh, s1_ascvv, s1_ascvh, l5_masks, l8_masks, l8_old_masks,
                s2_theia_masks, s2_s2c_masks, s2_l3a_masks, s1_desvv_masks,
                s1_desvh_masks, s1_ascvv_masks, s1_ascvh_masks):

        mean = self._stats["s2_theia"]["mean"]
        x = self.nans_imputation(s2_theia, mean, self.default_mean)
        std = torch.sqrt(self._stats["s2_theia"]["var"])
        x = self.standardize(x, mean, std, self.default_mean, self.default_std)

        # flat x due to flat input nn first layer
        x = s2_theia.reshape(-1, x.shape[1] * x.shape[-1])
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.output(x)
        return x


class MLPClassifier(Iota2NeuralNetwork):
    """Simple Multi Layer Perceptron for Satellite Image Time Series
    classification"""
    def __init__(self,
                 nb_features: int,
                 nb_class: int,
                 nb_bands: int = 10,
                 doy_sensors_dic: Optional[Dict] = None,
                 default_std: int = 1,
                 default_mean: int = 0):
        super().__init__(nb_features, nb_class, doy_sensors_dic, default_std,
                         default_mean)
        self.nb_class = nb_class
        self.nb_bands = nb_bands
        self.nb_dates = nb_features // nb_bands
        self.internal_size = nb_class * 3
        self.embed_size = max(nb_features // 2, self.internal_size)
        self.fc1 = nn.Linear(in_features=nb_features,
                             out_features=self.embed_size)
        self.fc2 = nn.Linear(in_features=self.embed_size,
                             out_features=self.internal_size)
        self.fc1_mask = nn.Linear(in_features=self.nb_dates,
                                  out_features=self.internal_size)
        self.fc2_mask = nn.Linear(in_features=self.internal_size,
                                  out_features=self.internal_size)
        self.fc3 = nn.Linear(in_features=self.internal_size,
                             out_features=self.internal_size)
        self.fc4 = nn.Linear(in_features=self.internal_size,
                             out_features=self.internal_size)
        self.output = nn.Linear(in_features=self.internal_size,
                                out_features=nb_class)

    def _forward(self, x, m):

        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))

        if m is not None:
            m = F.relu(self.fc1_mask(m))
            m = F.relu(self.fc2_mask(m))
            x = F.relu(self.fc3(x + m))

        x = F.relu(self.fc4(x))
        x = self.output(x)
        return x

    def forward(self, l5, l8, l8_old, s2_theia, s2_s2c, s2_l3a, s1_desvv,
                s1_desvh, s1_ascvv, s1_ascvh, l5_masks, l8_masks, l8_old_masks,
                s2_theia_masks, s2_s2c_masks, s2_l3a_masks, s1_desvv_masks,
                s1_desvh_masks, s1_ascvv_masks, s1_ascvh_masks):

        mean = self._stats["s2_theia"]["mean"]
        s2_theia = self.nans_imputation(s2_theia, mean, self.default_mean)
        std = torch.sqrt(self._stats["s2_theia"]["var"])
        s2_theia = self.standardize(s2_theia, mean, std, self.default_mean,
                                    self.default_std)

        data_batch, data_dates, data_bands = s2_theia.shape
        # reshape due to nn design
        s2_theia = s2_theia.reshape(data_batch, data_dates * data_bands)
        if s2_theia_masks is not None:
            mask_batch, mask_dates, mask_bands = s2_theia_masks.shape
            s2_theia_masks = s2_theia_masks.reshape(mask_batch,
                                                    mask_dates * mask_bands)
        return self._forward(s2_theia, s2_theia_masks)


class SelfAttention(nn.Module):
    """ Self attention layer using dot product and addition """
    def __init__(self, input_size: int, output_size: int):
        super().__init__()
        self.fc = nn.Linear(in_features=input_size, out_features=output_size)
        self.bn = nn.BatchNorm1d(output_size)
        self.fc_att = nn.Linear(in_features=input_size,
                                out_features=output_size)
        self.bn_att = nn.BatchNorm1d(output_size)
        self.fc_att2 = nn.Linear(in_features=output_size,
                                 out_features=output_size)
        self.bn_att2 = nn.BatchNorm1d(output_size)

    def forward(self, x):
        x_attend = torch.softmax(self.bn_att2(
            self.fc_att2(F.relu(self.bn_att(self.fc_att(x))))),
                                 dim=1)
        x = F.relu(self.bn(self.fc(x)))
        x = x * x_attend + x / x_attend.mean()
        return x


class MaskedAttention(nn.Module):
    """ Attention layer using validity masks as context vector"""
    def __init__(self, nb_features: int, nb_bands: int):
        """ nb_features = nb_dates * nb_bands """
        super().__init__()
        self.nb_bands = nb_bands
        self.nb_dates = nb_features // self.nb_bands
        self.fc1_att = nn.Linear(in_features=self.nb_dates,
                                 out_features=self.nb_dates)
        self.bn1 = nn.BatchNorm1d(self.nb_dates)
        self.fc1_att2 = nn.Linear(in_features=self.nb_dates,
                                  out_features=self.nb_dates)
        self.bn2 = nn.BatchNorm1d(self.nb_dates)

    def forward(self, x, m):
        x_attend = torch.softmax(self.bn2(
            self.fc1_att2(self.bn1(F.relu(self.fc1_att(m))))),
                                 dim=1)
        x = x * x_attend.repeat_interleave(self.nb_bands,
                                           dim=1) + x / x_attend.max()
        return x


class SimpleSelfAttentionClassifier(Iota2NeuralNetwork):
    """ SITS classifier using validity masks and self attention. """
    def __init__(self,
                 nb_features: int,
                 nb_class: int,
                 nb_bands: int = 10,
                 nb_attention_heads: int = 3,
                 doy_sensors_dic: Optional[Dict] = None,
                 default_std: int = 1,
                 default_mean: int = 0):
        super().__init__(nb_features, nb_class, doy_sensors_dic, default_std,
                         default_mean)

        self.nb_class = nb_class
        self.nb_bands = nb_bands
        self.internal_size = nb_class * 3
        self.embed_size = max(nb_features // 2, self.internal_size)
        self.mask_attend_list = [
            MaskedAttention(nb_features, nb_bands).to(self.device)
            for _ in range(nb_attention_heads)
        ]
        self.fc1 = nn.Linear(in_features=nb_features,
                             out_features=self.embed_size)
        self.bn1 = nn.BatchNorm1d(self.embed_size)
        self.self_attend = SelfAttention(self.embed_size, self.internal_size)
        self.bnat = nn.BatchNorm1d(self.internal_size)
        self.fc2 = nn.Linear(in_features=self.internal_size,
                             out_features=self.internal_size)
        self.bn2 = nn.BatchNorm1d(self.internal_size)
        self.fc3 = nn.Linear(in_features=self.internal_size,
                             out_features=self.internal_size)
        self.bn3 = nn.BatchNorm1d(self.internal_size)
        self.fc4 = nn.Linear(in_features=self.internal_size,
                             out_features=self.internal_size)
        self.bn4 = nn.BatchNorm1d(self.internal_size)
        self.output = nn.Linear(in_features=self.internal_size,
                                out_features=nb_class)

    def _forward(self, x, m):

        if m is not None:
            x = reduce(torch.add, [sa(x, m) for sa in self.mask_attend_list])
        x = F.relu(self.bn1(self.fc1(x)))
        x = self.bnat(self.self_attend(x))
        x = F.relu(self.bn2(self.fc2(x)))
        x = F.relu(self.bn3(self.fc3(x)))
        x = F.relu(self.bn4(self.fc4(x)))
        x = self.output(x)
        return x

    def forward(self, l5, l8, l8_old, s2_theia, s2_s2c, s2_l3a, s1_desvv,
                s1_desvh, s1_ascvv, s1_ascvh, l5_masks, l8_masks, l8_old_masks,
                s2_theia_masks, s2_s2c_masks, s2_l3a_masks, s1_desvv_masks,
                s1_desvh_masks, s1_ascvv_masks, s1_ascvh_masks):

        mean = self._stats["s2_theia"]["mean"]
        s2_theia = self.nans_imputation(s2_theia, mean, self.default_mean)
        std = torch.sqrt(self._stats["s2_theia"]["var"])
        s2_theia = self.standardize(s2_theia, mean, std, self.default_mean,
                                    self.default_std)

        # reshape due to nn design
        data_batch, data_dates, data_bands = s2_theia.shape
        s2_theia = s2_theia.reshape(data_batch, data_dates * data_bands)
        if s2_theia_masks is not None:
            mask_batch, mask_dates, mask_bands = s2_theia_masks.shape
            s2_theia_masks = s2_theia_masks.reshape(mask_batch,
                                                    mask_dates * mask_bands)
        return self._forward(s2_theia, s2_theia_masks)


class LTAEClassifier(Iota2NeuralNetwork):
    """ SITS classifier using a transformer encoder"""
    def __init__(
        self,
        nb_features: int,
        nb_class: int,
        nb_bands: int = 10,
        nb_attention_heads: int = 4,
        use_masks: bool = True,
        doy_sensors_dic: Optional[Dict] = None,
        embed_size: Optional[int] = None,
        internal_size: Optional[int] = None,
        dk: Optional[int] = None,
        dropout: Optional[float] = None,
        default_std: int = 1,
        default_mean: int = 0,
    ):
        super().__init__(nb_features, nb_class, doy_sensors_dic)
        self.default_std = default_std
        self.default_mean = default_mean
        self.nb_class = nb_class
        self.nb_bands = nb_bands
        self.nb_dates = nb_features // nb_bands
        self.embed_size = embed_size or min(512, nb_features // 2)

        # embed_size has to be a multiple of the nb of attention heads
        self.embed_size = (self.embed_size //
                           nb_attention_heads) * nb_attention_heads
        self.internal_size = internal_size or max(256, self.embed_size // 2)
        self.dk = dk or 8
        self.dropout_proba = dropout
        self.nb_channels = self.nb_bands
        self.use_masks = use_masks
        self.doys = doy_sensors_dic['sentinel2']['doy']
        if use_masks:
            self.nb_channels += 1
        self.ltae_module = LTAE(self.nb_channels,
                                nb_attention_heads,
                                self.dk, [self.embed_size, self.internal_size],
                                d_model=self.embed_size,
                                len_max_seq=self.nb_dates,
                                positions=self.doys)
        self.bnhidden1 = nn.BatchNorm1d(self.internal_size)
        self.dropout = nn.Dropout(p=(self.dropout_proba or 0.0))
        self.hidden1 = nn.Linear(in_features=self.internal_size,
                                 out_features=self.internal_size)
        self.bnhidden2 = nn.BatchNorm1d(self.internal_size)
        self.hidden2 = nn.Linear(in_features=self.internal_size,
                                 out_features=self.internal_size)
        self.bnout = nn.BatchNorm1d(self.internal_size)
        self.output = nn.Linear(in_features=self.internal_size,
                                out_features=nb_class)

    def _forward(self, x, m):

        mean = self._stats["s2_theia"]["mean"]
        x = self.nans_imputation(x, mean, self.default_mean)
        std = torch.sqrt(self._stats["s2_theia"]["var"])
        x = self.standardize(x, mean, std, self.default_mean, self.default_std)

        if self.use_masks:
            x = torch.cat((x, m), 2)
        x = self.bnhidden1(self.ltae_module(x))
        if self.dropout_proba is not None and self.training:
            x = self.dropout(x)
        x = F.relu(x)
        x = F.relu(self.bnhidden2(self.hidden1(x)))
        x = F.relu(self.bnout(self.hidden2(x)))
        x = self.output(x)
        return x

    def forward(self, l5, l8, l8_old, s2_theia, s2_s2c, s2_l3a, s1_desvv,
                s1_desvh, s1_ascvv, s1_ascvh, l5_masks, l8_masks, l8_old_masks,
                s2_theia_masks, s2_s2c_masks, s2_l3a_masks, s1_desvv_masks,
                s1_desvh_masks, s1_ascvv_masks, s1_ascvh_masks):
        return self._forward(s2_theia, s2_theia_masks)
