# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import logging
import os
import shutil
from typing import Dict, List, Optional, Union

from iota2.common.file_utils import ensure_dir
from iota2.common.otb_app_bank import CreateTrainAutoContext

LOGGER = logging.getLogger("distributed.worker")
LOGGER.addHandler(logging.NullHandler())

Param = Dict[str, Union[str, List[str], int]]


def train_autocontext(
    parameter_dict: Param,
    data_field: str,
    output_path: str,
    features_list_name: List[str],
    superpix_data_field: str = "superpix",
    iterations: int = 3,
    working_directory: Optional[str] = None,
    logger: logging.Logger = LOGGER,
):
    """Launch auto-context training.

    Parameters
    ----------
    parameter_dict : dict
        dictionnary containing autoContext's input parameters
        {"model_name": string,
         "seed": integer,
         "list_learning_samples": list,
         "list_tiles": list,
         "list_slic": list,
         "list_superPixel_samples": list}
    output_path : sting
        path to a directory where will be stored models
        in a directory 'model'
    data_field : string
        class field name in learning database samples files
    superpix_data_field : string
        field in database discriminating superpixels labels
    iterations : int
        number of auto-context iterations
    RAM : integer
        available ram
    working_directory : string
        path to store temporary data
    logger : logging
        root logger
    """

    model_name = parameter_dict["model_name"]
    seed_num = parameter_dict["seed"]
    data_ref = parameter_dict["list_learning_samples"]
    data_segmented = parameter_dict["list_superPixel_samples"]

    field = data_field.lower()

    models_path = os.path.join(output_path, "model")
    model_path = os.path.join(models_path, f"model_{model_name}_seed_{seed_num}")
    ensure_dir(model_path)

    if working_directory is None:
        tmp_dir = os.path.join(model_path, "tmp")
    else:
        tmp_dir = os.path.join(
            working_directory, f"model_{model_name}_seed_{seed_num}_tmp"
        )
    ensure_dir(tmp_dir)

    feat_labels = [elem.lower() for elem in features_list_name]

    train_autocontext_app = CreateTrainAutoContext(
        {
            "refdata": data_ref,
            "reffield": field,
            "superpixdata": data_segmented,
            "superpixdatafield": superpix_data_field,
            "feat": feat_labels,
            "nit": iterations,
            "out": "{}/".format(model_path),
        }
    )
    logger.info(
        f"Start training autoContext, produce model {model_name}, seed {seed_num}"
    )
    train_autocontext_app.ExecuteAndWriteOutput()
    logger.info("training autoContext DONE")
    shutil.rmtree(tmp_dir)
