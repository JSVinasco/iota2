# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import warnings
from pathlib import Path
from typing import ClassVar, Dict, List, Literal, Optional

from pydantic import BaseModel, Field, root_validator, validator
from pydantic.fields import ModelField

from iota2.common.file_utils import verify_import
from iota2.configuration_files.check_config_parameters import verify_code_path
from iota2.configuration_files.sections.cfg_utils import (
    ConfigError,
    ConfigNotRecognisedParamWarning,
    FileParameter,
    Iota2ParamSection,
    PathParameter,
)
from iota2.learning.pytorch.torch_nn_bank import (
    Iota2NeuralNetwork,
    Iota2NeuralNetworkFactory,
)


class HyperparametersSolver(BaseModel):
    """Parameter class to feed DeepLearningParameters."""

    batch_size: List[int] = [1000]
    learning_rate: List[float] = [1e-05]


class DeepLearningParameters(BaseModel):
    """Deep learning parameters definition."""

    dl_name: Optional[str] = None
    dl_parameters: dict = {}
    model_selection_criterion: Literal["loss", "fscore", "oa", "kappa"] = "loss"
    epochs: int = 100
    weighted_labels: bool = False
    num_workers: int = 1
    hyperparameters_solver: HyperparametersSolver = HyperparametersSolver()
    dl_module: Optional[FileParameter] = None
    restart_from_checkpoint: bool = True
    dataloader_mode: Literal["stream", "full"] = "stream"
    enable_early_stop: bool = False
    epoch_to_trigger: int = 5
    early_stop_patience: int = 10
    early_stop_tol: float = 0.01
    early_stop_metric: Literal[
        "train_loss", "val_loss", "fscore", "oa", "kappa"
    ] = "val_loss"
    additional_statistics_percentage: Optional[float] = None
    adaptive_lr: Optional[dict] = None

    @validator("adaptive_lr")
    @classmethod
    def adaptive_lr_forbidden_values(cls, v):
        """Validate adaptive_lr parameter."""
        forbidden_values = ["optimizer", "mode"]
        error_mess = []
        for forbidden_value in forbidden_values:
            if forbidden_value in v:
                error_mess.append(
                    (f"parameter '{forbidden_value}' " "is forbidden in adaptive_lr")
                )
        if error_mess:
            raise ConfigError(" and ".join(error_mess))
        return v

    @validator("additional_statistics_percentage")
    @classmethod
    def additional_statistics_percentage_range(cls, v):
        """Check if the range is in ]0;1]."""
        if v > 1.0 or v < 0:
            raise ConfigError(
                "Parameter 'additional_statistics_percentage' must " "be in : ]0;1]"
            )
        return v

    @root_validator(skip_on_failure=True)
    @classmethod
    def nn_root_val(cls, values) -> Dict:
        """Validate dl parameters."""
        nn_name, nn_module = values["dl_name"], values["dl_module"]
        if nn_module and nn_name is None:
            raise ConfigError(
                (
                    "External neural network module provided "
                    "but 'dl_name' not set. Please set 'dl_name'"
                    " parameter in iota2 configuration file"
                )
            )
        if nn_module:
            verify_code_path(nn_module)
            try:
                external_nn_module = verify_import(nn_module)
            except TypeError as exception:
                raise ValueError from exception
            user_nn_class = getattr(external_nn_module, nn_name)

            if not issubclass(user_nn_class, Iota2NeuralNetwork):
                raise ConfigError(
                    (
                        f"the class {user_nn_class} must be "
                        "a subclass of 'Iota2NeuralNetwork'"
                    )
                )
        if nn_name:
            Iota2NeuralNetworkFactory().get_nn(
                nn_name=nn_name,
                nn_parameters={"doy_sensors_dic": {"sentinel2": {"doy": None}}},
                nb_features=10,
                nb_class=1,
                pickle_file=None,
                external_nn_module=nn_module,
                random_seed_number=42,
            )
        return values


class OtbClassifierParam(dict):
    """Parameters to the otb app TrainVectorClassifier."""

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value, field: ModelField):
        """Check classifier options."""
        avail_keys = dict(
            [
                ("classifier.libsvm.k", str),
                ("classifier.libsvm.m", str),
                ("classifier.libsvm.c", float),
                ("lassifier.libsvm.gamma", float),
                ("classifier.libsvm.coef0", float),
                ("classifier.libsvm.degree", int),
                ("classifier.libsvm.nu", float),
                ("classifier.libsvm.opt", bool),
                ("classifier.libsvm.prob", bool),
                ("classifier.boost.t", str),
                ("classifier.boost.w", int),
                ("classifier.boost.r", float),
                ("classifier.boost.m", int),
                ("classifier.dt.max", int),
                ("classifier.dt.min", int),
                ("classifier.dt.ra", float),
                ("classifier.dt.cat", int),
                ("classifier.dt.r", bool),
                ("classifier.dt.t", bool),
                ("classifier.ann.t", str),
                ("classifier.ann.sizes", list),
                ("classifier.ann.f", str),
                ("classifier.ann.a", float),
                ("classifier.ann.b", float),
                ("classifier.ann.bpdw", float),
                ("classifier.ann.bpms", float),
                ("classifier.ann.rdw", float),
                ("classifier.ann.rdwm", float),
                ("classifier.ann.term", str),
                ("classifier.ann.eps", float),
                ("classifier.ann.iter", int),
                ("classifier.rf.max", int),
                ("classifier.rf.min", int),
                ("classifier.rf.ra", float),
                ("classifier.rf.cat", int),
                ("classifier.rf.var", int),
                ("classifier.rf.nbtrees", int),
                ("classifier.rf.acc", float),
                ("classifier.knn.k", int),
                ("classifier.sharkrf.nbtrees", int),
                ("classifier.sharkrf.nodesize", int),
                ("classifier.sharkrf.mtry", int),
                ("classifier.sharkrf.oobr", float),
                ("classifier.sharkkm.maxiter", int),
                ("classifier.sharkkm.k", int),
                ("classifier.sharkkm.incentroids", str),
                ("classifier.sharkkm.cstats", str),
                ("classifier.sharkkm.outcentroids", str),
            ]
        )
        error_message = []
        for key in value.keys():
            if key in avail_keys:
                if not isinstance(value[key], avail_keys[key]):
                    error_message.append(
                        (
                            f"parameter '{key}' must be of "
                            f"type {avail_keys[key].__name__}. "
                            f"Value detected : {type(value[key]).__name__}"
                        )
                    )
            else:
                error_message.append(
                    (f"parameter '{key}' not " "recognised. Please remove it")
                )
        if error_message:
            raise ConfigError("\n".join(error_message))
        return value


class SampleAugmentation(dict):
    """Sample augmentation parameter definition."""

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value, field: ModelField):
        forbidden_keys = [
            "in",
            "out",
            "field",
            "layer",
            "label",
            "seed",
            "inxml",
            "progress",
            "help",
        ]
        avail_keys = [
            "target_models",
            "strategy",
            "strategy.jitter.stdfactor",
            "strategy.smote.neighbors",
            "seed",
            "samples.strategy",
            "samples.strategy.minNumber",
            "samples.strategy.byClass",
            "activate",
        ]

        forbidden_params = []
        for key in value.keys():
            if key not in avail_keys:
                warnings.warn(key, ConfigNotRecognisedParamWarning)
            if key in forbidden_keys:
                forbidden_params.append(key)
        err_message = []
        if forbidden_params:
            err_message.append(
                (
                    "sample_augmentation section, forbidden "
                    f"parameters detected : '{', '.join(forbidden_params)}'."
                    " Please remove them"
                )
            )
        if "strategy" in value:
            strategy = value["strategy"]
            if strategy not in ["replicate", "jitter", "smote"]:
                err_message.append(
                    (
                        "augmentation strategy must "
                        "be 'replicate', 'jitter' or 'smote'"
                    )
                )
        if "strategy.jitter.stdFactor" in value:
            jitter = value["strategy.jitter.stdFactor"]
            if not isinstance(jitter, int):
                err_message.append("strategy.jitter.stdFactor must an integer")
        if "strategy.smote.neighbors" in value:
            byclass = value["strategy.smote.neighbors"]
            if not isinstance(byclass, int):
                err_message.append("strategy.smote.neighbors must be an integer")
        if "samples.strategy" in value:
            samples_strategy = value["samples.strategy"]
            if samples_strategy not in ["minNumber", "balance", "byClass"]:
                err_message.append(
                    (
                        "augmentation strategy must be "
                        "'minNumber', 'balance' or 'byClass'"
                    )
                )
        if "samples.strategy.minNumber" in value:
            min_number = value["samples.strategy.minNumber"]
            if not isinstance(min_number, int):
                err_message.append("samples.strategy.minNumber must an integer")
        if "samples.strategy.byClass" in value:
            by_class = value["samples.strategy.byClass"]
            if not isinstance(by_class, str):
                err_message.append("samples.strategy.byClass must be a string")
        if "activate" in value:
            activate = value["activate"]
            if not isinstance(activate, bool):
                err_message.append("activate must be a bool")
        if "target_models" in value:
            target_models = value["target_models"]
            if not isinstance(target_models, list):
                err_message.append("target_models must a list")
            if not isinstance(target_models[0], str):
                err_message.append("target_models must constains strings")

        if err_message:
            raise ConfigError("\n" + "\n".join(err_message))
        return value

    @classmethod
    def __modify_schema__(cls, field_schema, field: Optional[ModelField]):
        if field:
            field_schema["doc_type"] = "dict"
            field_schema["available_on_builders"] = ["i2_classification"]
            field_schema["short_desc"] = "OTB parameters for sample augmentation"
            field_schema["long_desc"] = (
                "In supervised classification the balance between "
                "class samples is important. There are"
                " any ways to manage class balancing in iota2, using "
                ":ref:`refSampleSelection` or "
                "the classifier's options to limit the number of "
                "samples by class."
                "\nAn other approch is to generate synthetic "
                "samples. It is the purpose of this"
                "functionality, which is called "
                "'sample augmentation'.\n\n"
                "    .. code-block:: python\n\n"
                "        {'activate':False}\n"
                "\nExample\n"
                "^^^^^^^\n\n"
                "    .. code-block:: python\n\n"
                "        sample_augmentation : {'target_models':['1', '2'],\n"
                "                              'strategy' : 'jitter',\n"
                "                              'strategy.jitter.stdfactor' : 10,\n"
                "                              'strategy.smote.neighbors'  : 5,\n"
                "                              'samples.strategy' : 'balance',\n"
                "                              'activate' : True\n"
                "                              }\n"
                "\n\niota2 implements an interface to the OTB `SampleAugmentation"
                " <https://www.orfeo-toolbox.org/CookBook/Applications/"
                "app_SampleSelection.html>`_ application.\n"
                "There are three methods to generate samples : replicate, jitter"
                " and smote."
                "The documentation :doc:`here <sampleAugmentation_explain>`"
                " explains the difference"
                " between these approaches.\n\n"
                " ``samples.strategy`` specifies how many samples must be created."
                "There are 3 different strategies:"
                "    - minNumber\n"
                "        To set the minimum number of samples by class required\n"
                "    - balance\n"
                "        balance all classes with the same number of "
                "samples as the majority one\n"
                "    - byClass\n"
                "        augment only some of the classes"
                "\nParameters related to ``minNumber`` and ``byClass`` strategies"
                " are"
                "    - samples.strategy.minNumber\n"
                "        minimum number of samples\n"
                "    - samples.strategy.byClass\n"
                "        path to a CSV file containing in first column the class's"
                " label and \n"
                "        in the second column the minimum number of samples"
                " required.\n"
                "In the above example, classes of models '1' and '2' will be"
                " augmented to the"
                "the most represented class in the corresponding model using the"
                " jitter method."
            )


class SampleSelection(dict):
    """https://field-idempotency--pydantic-docs.netlify.app/usage/types/#custom-data-types"""

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value, field: ModelField):
        """ """

        def check_sample_selection_param(params: Dict) -> None:

            avail_strats = [
                "byclass",
                "constant",
                "percent",
                "total",
                "smallest",
                "all",
            ]
            avail_keys = [
                "sampler",
                "sampler.periodic.jitter",
                "strategy",
                "strategy.byclass.in",
                "strategy.constant.nb",
                "strategy.percent.p",
                "ram",
                "per_models",
                "target_model",
            ]
            for key in params.keys():
                if key not in avail_keys:
                    warnings.warn(key, ConfigNotRecognisedParamWarning)
            forbidden_keys = [
                "outrates",
                "in",
                "mask",
                "vec",
                "out",
                "instats",
                "field",
                "layer",
                "rand",
                "inxml",
            ]
            for forbidden_key in forbidden_keys:
                if forbidden_key in params:
                    raise ConfigError(
                        (
                            f"'{forbidden_key}' parameter must "
                            "not be set in arg_train.sample_selection"
                        )
                    )

            if "strategy.percent.p" in params and not isinstance(
                params["strategy.percent.p"], float
            ):
                raise ConfigError(
                    (
                        f"{field.name}.strategy.percent.p"
                        " must be a float. Value detected "
                        f": {params['strategy.percent.p']}"
                        f" as {type(params['strategy.percent.p'])}"
                    )
                )
            if "strategy.byclass.in" in params:
                if not isinstance(params["strategy.byclass.in"], str):
                    raise ConfigError(
                        (
                            f"{field.name}.strategy.byclass.in"
                            " must be a str. Value detected "
                            f": {params['strategy.byclass.in']}"
                            f" as {type(params['strategy.percent.p'])}"
                        )
                    )
                if not Path(params["strategy.byclass.in"]).exists():
                    raise ConfigError(
                        (
                            f"{field.name}.strategy.byclass.in"
                            " get a file which does not exists : "
                            f"'{params['strategy.byclass.in']}'"
                        )
                    )
            if "sampler" in params:
                sampler = params["sampler"]
                if sampler not in ["periodic", "random"]:
                    raise ConfigError("sampler must be 'periodic' or 'random'")
            if "sampler.periodic.jitter" in params:
                jitter = params["sampler.periodic.jitter"]
                if not isinstance(jitter, int):
                    raise ConfigError("jitter must an integer")
            if "strategy" in params:
                strategy = params["strategy"]
                if strategy not in avail_strats:
                    raise ConfigError(
                        "strategy must be {}".format(
                            " or ".join(["'{}'".format(elem) for elem in avail_strats])
                        )
                    )
            if "strategy.constant.nb" in params:
                constant = params["strategy.constant.nb"]
                if not isinstance(constant, int):
                    raise ConfigError("strategy.constant.nb must an integer")
            if "strategy.total.v" in params:
                total = params["strategy.total.v"]
                if not isinstance(total, int):
                    raise ConfigError("strategy.total.v must an integer")
            if "elev.dem" in params:
                dem = params["elev.dem"]
                if not isinstance(dem, str):
                    raise ConfigError("elev.dem must a string")
            if "elev.geoid" in params:
                geoid = params["elev.geoid"]
                if not isinstance(geoid, str):
                    raise ConfigError("elev.geoid must a string")
            if "elev.default" in params:
                default = params["elev.default"]
                if not isinstance(default, float):
                    raise ConfigError("elev.default must a float")
            if "ram" in params:
                ram = params["ram"]
                if not isinstance(ram, int):
                    raise ConfigError("ram must a int")
            if "target_model" in params:
                target_model = params["target_model"]
                if not isinstance(target_model, int):
                    raise ConfigError("target_model must an integer")

        check_sample_selection_param(value)
        if "per_models" in value:
            for model in value["per_models"]:
                check_sample_selection_param(model)

        return value

    @classmethod
    def __modify_schema__(cls, field_schema, field: Optional[ModelField]):
        if field:
            field_schema["available_on_builders"] = ["i2_classification", "i2_obia"]
            field_schema["doc_type"] = "dict"
            field_schema[
                "short_desc"
            ] = "OTB parameters for sampling the validation set"
            field_schema["long_desc"] = (
                "This field parameters the strategy of polygon "
                "sampling. It directly refers to options of "
                "OTB’s SampleSelection application."
                "\n\nExample\n"
                "-------\n\n"
                "    .. code-block:: python\n\n"
                "        sample_selection : {'sampler':'random',\n"
                "                           'strategy':'percent',\n"
                "                           'strategy.percent.p':0.2,\n"
                "                           'per_models':[{'target_model':'4',\n"
                "                                          'sampler':'periodic'}]\n"
                "                           }"
                "\n\nIn the example above, all polygons will be "
                "sampled with the 20% ratio. But "
                "the polygons which belong to the model 4 will "
                "be periodically sampled,"
                " instead of the ransom sampling used for other "
                "polygons."
                "\nNotice than ``per_models`` key contains a list of"
                " strategies. Then we can imagine the following :\n\n"
                "    .. code-block:: python\n\n"
                "        sample_selection : {'sampler':'random',\n"
                "                           'strategy':'percent',\n"
                "                           'strategy.percent.p':0.2,\n"
                "                           'per_models':[{'target_model':'4',\n"
                "                                          'sampler':'periodic'},\n"
                "                                         {'target_model':'1',\n"
                "                                          'sampler':'random',\n"
                "                                          'strategy', 'byclass',\n"
                "                                          'strategy.byclass.in',"
                " '/path/to/myCSV.csv'\n"
                "                                         }]\n"
                "                           }\n"
                " where the first column of /path/to/myCSV.csv is"
                " class label (integer), second one is the required"
                " samples number (integer)."
            )


class TrainSection(Iota2ParamSection):
    """Definition of the parameters that belong to the 'arg_train' section."""

    section_name: ClassVar[str] = "arg_train"
    mode_outside_regionsplit: float = Field(
        0.1,
        doc_type="float",
        short_desc="Set the threshold for split huge model",
        long_desc=(
            "This parameter is available if "
            "regionPath is used and arg_train.classif_mode"
            " is set to fusion. "
            "It represents the maximum size covered by a region."
            " If the regions are larger than this threshold, "
            "then N models are built by randomly"
            " selecting features inside the region."
        ),
        available_on_builders=["i2_classification"],
    )
    runs: int = Field(
        1,
        doc_type="int",
        short_desc="Number of independant runs processed",
        long_desc=(
            "Number of independant runs processed."
            " Each run has his own learning samples."
            " Must be an integer greater than 0"
        ),
        available_on_builders=["i2_classification", "i2_obia"],
    )
    split_ground_truth: bool = Field(
        True,
        doc_type="bool",
        short_desc="Enable the split of reference data",
        long_desc=(
            "If set to False, the chain"
            " use all polygons for both "
            "training and validation"
        ),
        available_on_builders=["i2_classification", "i2_obia"],
    )
    ratio: float = Field(
        0.5,
        doc_type="float",
        short_desc=(
            "Should be between 0.0 and 1.0 and represent "
            "the proportion of the dataset to include in the train split."
        ),
        available_on_builders=["i2_classification", "i2_obia"],
    )
    random_seed: int = Field(
        None,
        doc_type="int",
        short_desc="Fix the random seed for random split of reference data",
        long_desc=(
            "Fix the random seed used for random split "
            "of reference data"
            " If set, the results must be the "
            "same for a given classifier"
        ),
        available_on_builders=["i2_classification", "i2_obia"],
    )
    deep_learning_parameters: DeepLearningParameters = Field(
        {},
        doc_type="dict",
        short_desc=(
            "deep learning parameter description is available "
            ":doc:`here <deep_learning>`"
        ),
        available_on_builders=["i2_classification"],
    )
    enable_autocontext: bool = Field(
        False,
        doc_type="bool",
        short_desc="Enable the auto-context processing",
        available_on_builders=["i2_classification"],
    )
    autocontext_iterations: int = Field(
        3,
        doc_type="int",
        short_desc="Number of iterations in auto-context",
        available_on_builders=["i2_classification"],
    )
    force_standard_labels: bool = Field(
        False,
        doc_type="bool",
        short_desc="Standardize labels for feature extraction",
        long_desc=(
            "The chain label each features by the sensors name,"
            " the spectral band or indice and the date."
            " If activated this parameter use the OTB default value"
            " (`value_X`)"
        ),
        available_on_builders=["i2_classification"],
    )
    sample_selection: SampleSelection = {"sampler": "random", "strategy": "all"}
    sampling_validation: bool = Field(
        False,
        doc_type="bool",
        short_desc="Enable sampling validation",
        available_on_builders=["i2_classification"],
    )
    sample_augmentation: SampleAugmentation = {"activate": False}
    sample_management: FileParameter = Field(
        None,
        doc_type="str",
        short_desc=(
            "Absolute path to a CSV file " "containing samples transfert" " strategies"
        ),
        long_desc=(
            "The CSV must contain a row per transfert\n"
            "    .. code-block:: python\n\n"
            "        >>> cat /absolute/path/myRules.csv\n"
            "            1,2,4,2\n"
            "Meaning :\n"
            "        +--------+-------------+------------+----------+\n"
            "        | source | destination | class name | quantity |\n"
            "        +========+=============+============+==========+\n"
            "        |   1    |      2      |      4     |     2    |\n"
            "        +--------+-------------+------------+----------+\n\n"
            "Currently, setting the 'random_seed' parameter has no effect on this workflow."
        ),
        available_on_builders=["i2_classification"],
    )
    dempster_shafer_sar_opt_fusion: bool = Field(
        False,
        doc_type="bool",
        short_desc=("Enable the use of both SAR" " and optical data to train a model."),
        long_desc=(
            "Enable the use of both SAR and optical data to"
            " train a model."
            " If True then two models are trained."
            "more documentation is avalailalbe "
            ":doc:`here <SAR_Opt_postClassif_fusion>`"
        ),
        available_on_builders=["i2_classification"],
    )

    crop_mix: bool = Field(
        False,
        doc_type="bool",
        short_desc="Enable crop mix workflow",
        available_on_builders=["i2_classification"],
    )
    prev_features: FileParameter = Field(
        None,
        doc_type="str",
        short_desc=(
            "Path to a configuration file used to " "produce previous features"
        ),
        available_on_builders=["i2_classification"],
    )
    output_prev_features: PathParameter = Field(
        None,
        doc_type="str",
        short_desc="Path to previous features for crop mix",
        available_on_builders=["i2_classification"],
    )
    annual_crop: List[str] = Field(
        ["11", "12"],
        doc_type="list",
        short_desc="The list of classes to be replaced by previous data",
        available_on_builders=["i2_classification"],
    )
    a_crop_label_replacement: List[str] = Field(
        ["10", "annual_crop"],
        doc_type="list",
        short_desc="Replace a label by a string, ie ['10', 'annual_crop']",
        available_on_builders=["i2_classification"],
    )
    samples_classif_mix: bool = Field(
        False,
        doc_type="bool",
        short_desc="Enable the second step of crop mix",
        available_on_builders=["i2_classification"],
    )
    annual_classes_extraction_source: str = Field(
        None, doc_type="str", short_desc="", available_on_builders=["i2_classification"]
    )

    features_from_raw_dates: bool = Field(
        False,
        doc_type="bool",
        short_desc="learn model from raw sensor's date (no interpolations)",
        long_desc=(
            "If True, during the learning and classification step, each pixel"
            " will receive a vector of values of the size of the number of"
            " all dates detected. As the pixels were not all acquired on "
            "the same dates, the vector will contains NaNs on the unacquired "
            "dates."
        ),
        available_on_builders=["i2_classification"],
    )
    classifier: Optional[str] = Field(
        doc_type="str",
        short_desc="otb classification algorithm",
        builders={"i2_classification": True},
        available_on_builders=["i2_classification", "i2_obia"],
    )
    otb_classifier_options: OtbClassifierParam = Field(
        None,
        doc_type="dict",
        short_desc=(
            "OTB option for classifier." "If None, the OTB default values are used"
        ),
        long_desc=(
            "This parameter is a dictionnary"
            " which accepts all OTB application parameters."
            " To know the exhaustive parameter list "
            " use `otbcli_TrainVectorClassifier` in a terminal or"
            " look at the OTB documentation"
        ),
        available_on_builders=["i2_classification", "i2_obia"],
    )
    validity_threshold: int = Field(
        1,
        doc_type="int",
        short_desc="threshold above which a " "training pixel is considered valid",
        available_on_builders=["i2_classification", "i2_obia"],
    )
    features: List[str] = Field(
        ["NDVI", "NDWI", "Brightness"],
        doc_type="list",
        short_desc="List of additional features computed",
        long_desc=(
            "This parameter enable the computation of the "
            "three indices if available for the sensor used."
            "There is no choice for using only one of them"
        ),
        available_on_builders=["i2_classification", "i2_features_map"],
    )

    sample_validation: SampleSelection = {"sampler": "random", "strategy": "all"}
