"""Module that groups all the definitions of the simplification parameters"""

# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

from typing import ClassVar, Literal

from pydantic import Field

from iota2.configuration_files.sections.cfg_utils import (
    FileParameter,
    Iota2ParamSection,
    PathParameter,
)


class SimplificationSection(Iota2ParamSection):
    """Parameters of the section 'simplification'."""

    section_name: ClassVar[str] = "simplification"

    classification: FileParameter = Field(
        None,
        doc_type="str",
        short_desc="Input raster of classification",
        long_desc=(
            "This parameter is automatically set if the configuration file"
            " use the classification and vectorization builders"
        ),
        available_on_builders=["i2_vectorization"],
    )
    confidence: FileParameter = Field(
        None,
        doc_type="str",
        short_desc="Input raster of confidence",
        long_desc=(
            "This parameter is automatically set if the configuration file"
            " use the classification and vectorization builders"
        ),
        available_on_builders=["i2_vectorization"],
    )
    validity: FileParameter = Field(
        None,
        doc_type="str",
        short_desc="Input raster of validity",
        long_desc=(
            "This parameter is automatically set if the configuration file"
            " use the classification and vectorization builders"
        ),
        available_on_builders=["i2_vectorization"],
    )
    seed: int = Field(
        1,
        doc_type="int",
        short_desc="seed of input raster classification",
        long_desc=(
            "This parameter is usefull to vectorize"
            " one specific output "
            "classification seed once buidlers "
            "'i2_classification' and 'i2_vectorization' "
            "are both involved in the same run"
        ),
        available_on_builders=["i2_vectorization"],
    )
    vectorize_fusion_of_classifications: bool = Field(
        False,
        doc_type="bool",
        short_desc=(
            "flag to inform iota2 to vectorize " "the fusion of classifications"
        ),
        long_desc=(
            "This flag is only useful if the vectorization is "
            "chained with classification workflow"
        ),
        available_on_builders=["i2_vectorization"],
    )
    umc1: int = Field(
        None,
        doc_type="int",
        short_desc="MMU for the first regularization",
        long_desc=(
            "It is an interface of parameter '-st' of gdal_sieve.py function."
            " If None, classification is not regularized"
        ),
        available_on_builders=["i2_vectorization"],
    )
    umc2: int = Field(
        None,
        doc_type="int",
        short_desc="MMU for the second regularization",
        long_desc=(
            "OSO-like vectorization process requires 2 successive "
            "regularization, if you need a single regularization, "
            "let this parameter to None"
        ),
        available_on_builders=["i2_vectorization"],
    )
    inland: FileParameter = Field(
        None,
        doc_type="str",
        short_desc="inland water limit shapefile",
        long_desc=(
            "to vectorize only inland waters, " "and not unnecessary sea water areas"
        ),
        available_on_builders=["i2_vectorization"],
    )
    rssize: int = Field(
        20,
        doc_type="int",
        short_desc=(
            "Resampling size of input classification" " raster (projection unit)"
        ),
        long_desc=(
            "OSO-like vectorization requires a resampling step in order "
            "to regularize and decrease raster polygons number, "
            "If None, classification is not resampled"
        ),
        available_on_builders=["i2_vectorization"],
    )
    lib64bit: PathParameter = Field(
        None,
        doc_type="str",
        short_desc=(
            "Path of BandMath and Concatenate OTB executables "
            "returning 64-bits float pixel values"
        ),
        long_desc=(
            "Band math and concatenate OTB executables with 64 bits "
            "capabilities (only for large areas where "
            "clumps number > 2²³ bits for mantisse)"
        ),
        available_on_builders=["i2_vectorization"],
    )
    gridsize: int = Field(
        None,
        doc_type="int",
        short_desc="number of lines and columns of the serialization process",
        long_desc=(
            "This parameter is useful only for large areas for which "
            "vectorization process can not be executed (memory limitation). "
            "By 'serialization', we mean parallel vectorization processes. "
            "If not None, regularized classification raster is splitted "
            "in gridsize x gridsize rasters"
        ),
        available_on_builders=["i2_vectorization"],
    )
    grasslib: PathParameter = Field(
        None,
        doc_type="str",
        short_desc="path to grasslib",
        long_desc=(
            "Some functions of GRASS GIS software are used to "
            "vectorize, simplify and smooth vector layer. "
            "This path corresponds to GRASS install folder"
        ),
        available_on_builders=["i2_vectorization"],
    )
    douglas: int = Field(
        10,
        doc_type="int",
        short_desc=("Douglas-Peucker tolerance for" " vector-based generalization"),
        available_on_builders=["i2_vectorization"],
    )
    hermite: int = Field(
        10,
        doc_type="int",
        short_desc=("Hermite Interpolation threshold" " for vector-based smoothing"),
        available_on_builders=["i2_vectorization"],
    )
    mmu: int = Field(
        1000,
        doc_type="int",
        short_desc=(
            "MMU of output vector-based classification (projection unit),"
            "(Default : 0.1 ha)"
        ),
        available_on_builders=["i2_vectorization"],
    )
    angle: bool = Field(
        True,
        doc_type="bool",
        short_desc="if True, smoothing corners of pixels (45°)",
        available_on_builders=["i2_vectorization"],
    )
    clipfile: FileParameter = Field(
        None,
        doc_type="str",
        short_desc=("vector-based file to clip output " "vector-based classification"),
        long_desc=(
            "vector-based file can contain more than one feature "
            "(geographical/administrative areas). "
            "An output vector-based classification is produced "
            "for each feature (cf. 'clipfield' parameter)."
        ),
        available_on_builders=["i2_vectorization"],
    )
    clipfield: str = Field(
        None,
        doc_type="str",
        short_desc="field to identify distinct areas",
        long_desc=(
            "field to identify distinct geographical/administrative"
            'areas (cf. "clipfile" parameter)'
        ),
        available_on_builders=["i2_vectorization"],
    )
    clipvalue: int = Field(
        None,
        doc_type="int",
        short_desc="value of field which identify distinct areas",
        long_desc=(
            "output vector-based classification is only produced on "
            "the specific area (clipfield=clipvalue in clipfile) "
            "(cf. 'clipfield' parameter). If None, all areas are produced."
        ),
        available_on_builders=["i2_vectorization"],
    )
    outprefix: str = Field(
        "dept",
        doc_type="str",
        short_desc=("Prefix to use for naming of " "vector-based classifications"),
        long_desc=(
            "Naming of vector-based classifications is as following : "
            "prefix_clipvalue"
        ),
        available_on_builders=["i2_vectorization"],
    )
    lcfield: str = Field(
        "Class",
        doc_type="str",
        short_desc=(
            "Name of the field to store landcover class"
            " in vector-based classification"
        ),
        available_on_builders=["i2_vectorization"],
    )
    blocksize: int = Field(
        2000,
        doc_type="str",
        short_desc=("block size to split raster " "to prevent Numpy memory error"),
        long_desc=(
            "Numpy memory error may occur for large areas "
            "during serialization process. "
            "Split in sub-rasters prevents memory error"
        ),
        available_on_builders=["i2_vectorization"],
    )
    dozip: bool = Field(
        True,
        doc_type="bool",
        short_desc=("Zip output vector-based classification " "(OSO-like production)"),
        available_on_builders=["i2_vectorization"],
    )
    zonal_vector: FileParameter = Field(
        None,
        doc_type="str",
        short_desc=("vector file to compute zonal statistics " "of classification"),
        long_desc=(
            "compute the zonal statistics on the geometries of"
            "a user-provided vector file. Zonal statistics are"
            "computed on classification, confidence and raster"
            "input rasters"
        ),
        available_on_builders=["i2_vectorization"],
    )
    bingdal: PathParameter = Field(
        None,
        doc_type="str",
        short_desc="path to GDAL binaries",
        long_desc=(
            "Some GDAL lib versions (automatically set up with iota2) "
            "are not efficient to handle topology errors, use yours !"
        ),
        available_on_builders=["i2_vectorization"],
    )
    chunk: int = Field(
        10,
        doc_type="int",
        short_desc="number of chunks for statistics computing",
        long_desc=(
            "number of chunks (groups of vector-based features) "
            "for parallel computing landcover statistics"
        ),
        available_on_builders=["i2_vectorization"],
    )
    higher_stats: bool = Field(
        False,
        doc_type="bool",
        short_desc=(
            "If True, compute more complexe "
            "statistics (Shanon, majority "
            "order and difference, etc.)"
        ),
        available_on_builders=["i2_vectorization"],
    )
    systemcall: bool = Field(
        False,
        doc_type="bool",
        short_desc=("If True, use yours gdal lib (cf. bingdal)"),
        available_on_builders=["i2_vectorization"],
    )
    prod: Literal["carhab", "oso"] = Field(
        None,
        doc_type="str",
        short_desc=(
            "OSO-like output vector (aliases) is produced. "
            "Other possible value : carhab"
        ),
        available_on_builders=["i2_vectorization"],
    )
    nomenclature: FileParameter = Field(
        None,
        doc_type="configuration file which describe nomenclature",
        short_desc="configuration file which describe nomenclature",
        long_desc=(
            "This configuration file includes code, color, description "
            "and vector field alias of each class\n\n"
            ".. code-block:: bash\n\n"
            "    Classes:\n"
            "    {\n"
            "            Level1:\n"
            "            {\n"
            '                    "Urbain":\n'
            "                    {\n"
            "                    code:100\n"
            '                    alias:"Urbain"\n'
            '                    color:"#b106b1"\n'
            "                    }\n"
            "                    ...\n"
            "            }\n"
            "            Level2:\n"
            "            {\n"
            '                   "Urbain dense":\n'
            "                   {\n"
            "                   code:1\n"
            '                   alias:"UrbainDens"\n'
            '                   color:"#ff00ff"\n'
            "                   parent:100\n"
            "                   }\n"
            "                   ...\n"
            "            }\n"
            "    }\n"
        ),
        available_on_builders=["i2_vectorization"],
    )
    statslist: dict = Field(
        {1: "rate", 2: "statsmaj", 3: "statsmaj"},
        doc_type="dict",
        short_desc="dictionnary of requested landcover statistics",
        long_desc=(
            "Different landcover statistics can be computed for vector-based "
            "classification file.\nA python-like dictionnary must be provided.\n"
            "This is the OSO-like statistics :"
            '{1: "rate", 2: "statsmaj", 3: "statsmaj"}\n\n'
            "Where:\n"
            '     - {1: "rate"} : '
            "     rates of classification classes are computed "
            "     for each polygon\n\n"
            '     - {2: "statsmaj"} : '
            "     descriptive stats of classifier confidence are computed "
            "     for each polygon by using only majority class pixels\n\n"
            '     - {3: "statsmaj"} : '
            "     descriptive stats of sensor validity are computed "
            "     for each polygon by using only majority class pixels\n\n"
            "\n"
            "list of available statistics : \n"
            "    - stats : mean_b, std_b, max_b, min_b\n"
            "    - statsmaj : meanmaj, stdmaj, maxmaj, minmaj of maj. class\n"
            "    - rate : rate of each pixel value (classe names)\n"
            "    - stats_cl : mean_cl, std_cl, max_cl, min_cl of one class\n"
        ),
        available_on_builders=["i2_vectorization"],
    )
