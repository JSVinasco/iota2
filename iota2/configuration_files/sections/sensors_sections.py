# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

from typing import ClassVar, List

from pydantic import Field, validator

from iota2.configuration_files.sections.cfg_utils import (ConfigError,
                                                          Iota2ParamSection)


class Landsat8Section(Iota2ParamSection):
    """Parameters of the section 'Landsat8'."""

    section_name: ClassVar[str] = "Landsat8"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    temporal_resolution: int = Field(
        16,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc=
        "The first date of interpolated image time series : YYYYMMDD format",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc=
        "The last date of interpolated image time series : YYYYMMDD format",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    keep_bands: List[str] = Field(
        ["B1", "B2", "B3", "B4", "B5", "B6", "B7"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )


class Sentinel2TheiaSection(Iota2ParamSection):
    """Parameters of the section 'Sentinel_2'."""

    section_name: ClassVar[str] = "Sentinel_2"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    temporal_resolution: int = Field(
        10,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc=
        "The first date of interpolated image time series : YYYYMMDD format",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc=
        "The last date of interpolated image time series : YYYYMMDD format",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    keep_bands: List[str] = Field(
        ["B1", "B2", "B3", "B4", "B5", "B6", "B7"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )


class Sentinel2S2CSection(Iota2ParamSection):
    """Parameters of the section 'Sentinel_2_S2C'."""

    section_name: ClassVar[str] = "Sentinel_2_S2C"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    temporal_resolution: int = Field(
        10,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc=
        "The first date of interpolated image time series : YYYYMMDD format",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc=
        "The last date of interpolated image time series : YYYYMMDD format",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    keep_bands: List[str] = Field(
        ["B1", "B2", "B3", "B4", "B5", "B6", "B7"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )


class Sentinel2L3ASection(Iota2ParamSection):
    """Parameters of the section 'Sentinel_2_L3A'."""

    section_name: ClassVar[str] = "Sentinel_2_L3A"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    temporal_resolution: int = Field(
        10,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc=
        "The first date of interpolated image time series : YYYYMMDD format",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc=
        "The last date of interpolated image time series : YYYYMMDD format",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    keep_bands: List[str] = Field(
        ["B1", "B2", "B3", "B4", "B5", "B6", "B7"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )


class Landsat5OldSection(Iota2ParamSection):
    """Parameters of the section 'Landsat5_old'."""

    section_name: ClassVar[str] = "Landsat5_old"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    temporal_resolution: int = Field(
        10,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc=
        "The first date of interpolated image time series : YYYYMMDD format",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc=
        "The last date of interpolated image time series : YYYYMMDD format",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    keep_bands: List[str] = Field(
        ["B1", "B2", "B3", "B4", "B5", "B6", "B7"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )


class Landsat8OldSection(Iota2ParamSection):
    """Parameters of the section 'Landsat8_old'."""

    section_name: ClassVar[str] = "Landsat8_old"
    additional_features: str = Field(
        "",
        doc_type="str",
        short_desc="OTB's bandmath expressions, separated by comma",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    temporal_resolution: int = Field(
        10,
        doc_type="int",
        short_desc="The temporal gap between two interpolations",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    write_reproject_resampled_input_dates_stack: bool = Field(
        True,
        doc_type="bool",
        short_desc="flag to write of resampled stack image for each date",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    start_date: str = Field(
        "",
        doc_type="str",
        short_desc=
        "The first date of interpolated image time series : YYYYMMDD format",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    end_date: str = Field(
        "",
        doc_type="str",
        short_desc=
        "The last date of interpolated image time series : YYYYMMDD format",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    keep_bands: List[str] = Field(
        ["B1", "B2", "B3", "B4", "B5", "B6", "B7"],
        doc_type="list",
        short_desc="The list of spectral bands used for classification",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )


class UserFeatSection(Iota2ParamSection):
    """Parameters of the section 'userFeat'."""

    section_name: ClassVar[str] = "userFeat"
    arbo: str = Field(
        "/*",
        doc_type="str",
        short_desc="input folder hierarchy",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )
    patterns: str = Field(
        "ALT,ASP,SLP",
        doc_type="str",
        short_desc="key name for detect the input images",
        available_on_builders=[
            "i2_classification", "i2_obia", "i2_features_map"
        ],
    )

    @validator("patterns")
    @classmethod
    def adaptive_lr_forbidden_values(cls, v):
        """Check forbidden char."""
        if " " in v:
            raise ConfigError(f"userFeat {v} must not contains whitespaces")
        return v
