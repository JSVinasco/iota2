#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Module for initialize default parameter of any config file.
These functions return dictionnary ready to be set using the
init_section function.

If a parameter is given in a configuration file the default values is not used.
"""
import os
from typing import Any, Dict, List, Tuple

# from config import Mapping, Sequence
from iota2.common import i2_constants as i2_const
from iota2.common.file_utils import get_iota2_project_dir
from iota2.configuration_files import read_config_file as rcf

FunctionNameWithParams = Tuple[str, Dict[str, Any]]

I2_CONST = i2_const.Iota2Constants()


# Handle specific type for config module
def dico_mapping_init(my_dict):
    """
    use to init a mapping object from a dict
    """
    new_map = Mapping()
    for key, value in list(my_dict.items()):
        new_map.addMapping(key, value, "")
    return new_map


def list_sequence_init(my_list):
    """
    use to init a Sequence object from a list
    """
    new_seq = Sequence()
    for elem in my_list:
        new_seq.append(elem, "#comment")
    return new_seq


# Mother class for handle parameters
class generic_parameters():
    """ Generic class for defining parameters """

    def __init__(self):
        self.name = ""
        self.desc = ""
        self.value = ""
        self.type_exp = str
        self.long_desc = None
        self.section = None
        self.builders = None

    def get_value(self):
        """ Return the default value"""
        return self.value

    def get_desc(self):
        """ Return the short description"""
        return self.desc

    def get_name(self):
        """ Return the parameter name"""
        return self.name

    def get_type(self):
        """ Return the expected type"""
        return self.type_exp

    def get_init_values(self):
        """
        Return the section name and the expected dictionnary
        to init configuration file
        """
        return self.section, {self.name: self.value}

    def control_mapping_keys(self, param,
                             authorized_keys: List[str]) -> List[str]:
        """check if every mapping object (dictionary) keys are
        handle thanks to a list of authorized keys

        return a list of keys which are not authorized
        """
        keys = []
        for param_name in param:
            if isinstance(param[param_name],
                          Mapping) and param_name not in authorized_keys:
                keys.append(param_name)
        return keys

    def ensure_param_type(self, param):
        """
        Handle configuration parameters current misuse
        For instance: 'None' instead of None
        or '10' instead of 10

        can be overloaded by sub-class for specific needs
        (eg : check if path exits (l5_path_old) or resolution check)
        """

        if param is None:
            out_param = None
        elif isinstance(param, str) and param.lower() == "none":
            out_param = None
        elif not isinstance(param, self.type_exp):

            try:
                if self.type_exp.__name__ == "bool":

                    if param == "True":
                        out_param = True
                    elif param == "False":
                        out_param = False
                    else:
                        raise ValueError(f"{param} can not be cast as boolean")
                else:
                    out_param = self.type_exp(param)
            except TypeError:
                raise TypeError(
                    f"{param} is not compatible with type {self.type_exp}")
            except ValueError:
                raise ValueError(
                    f"{self.name} has value {param} which is not compatible"
                    f" with type {self.type_exp}")
        else:
            out_param = param
        return out_param


# Start define configuration parameters
class init_output_statistics(generic_parameters):
    """ Init output_statistics parameter"""

    def __init__(self):
        super().__init__()

        self.name = "output_statistics"
        self.desc = ("Enable the writing of PNG files "
                     "containing additional statistics")
        self.value = False
        self.type_exp = bool
        self.long_desc = (
            "Enable the writing of PNG files containing additional statistics"
            "containing the confidence by"
            " learning/validation pixels")
        self.section = 'chain'
        self.builders = {"i2_classification": False}


class init_l5_path_old(generic_parameters):
    """ Init l5_path_old parameter"""

    def __init__(self):
        super().__init__()
        self.name = "l5_path_old"
        self.desc = ("Absolute path to Landsat-5 images coming"
                     " from old THEIA format (D*H*)")
        self.value = None
        self.type_exp = str
        self.section = 'chain'
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }

    def ensure_param_type(self, param):
        out_param = super().ensure_param_type(param)
        if out_param and not os.path.exists(out_param):
            raise ValueError(f"{out_param} is not a valid path")
        return out_param


class init_l8_path(generic_parameters):
    """ Init l8_path  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "l8_path"
        self.desc = ("Absolute path to Landsat-8 images coming"
                     "from new tiled THEIA data")
        self.value = None
        self.type_exp = str
        self.section = 'chain'
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_l8_path_old(generic_parameters):
    """ Init l8_path_old parameter"""

    def __init__(self):
        super().__init__()
        self.name = "l8_path_old"
        self.desc = ("Absolute path to Landsat-8 images coming"
                     " from old THEIA format (D*H*)")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_s2_path(generic_parameters):
    """ Init s2_path parameter"""

    def __init__(self):
        super().__init__()
        self.name = "s2_path"
        self.desc = ("Absolute path to Sentinel-2 images (THEIA format)")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_s2_output_path(generic_parameters):
    """ Init s2_output_path parameter"""

    def __init__(self):
        super().__init__()
        self.name = "s2_output_path"
        self.desc = ("Absolute path to store preprocessed data "
                     "in a dedicated directory.")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_s2_s2c_path(generic_parameters):
    """ Init s2_s2c_path parameter"""

    def __init__(self):
        super().__init__()
        self.name = "s2_s2c_path"
        self.desc = "Absolute path to Sentinel-2 images (Sen2Cor format)"
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_s2_s2c_output_path(generic_parameters):
    """ Init s2_s2c_output_path  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "s2_s2c_output_path"
        self.desc = ("Absolute path to store preprocessed data "
                     "in a dedicated directory.")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_s2_l3a_path(generic_parameters):
    """ Init s2_l3a_path parameter"""

    def __init__(self):
        super().__init__()
        self.name = "s2_l3a_path"
        self.desc = ("Absolute path to Sentinel-2 L3A images (THEIA format)")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_s2_l3a_output_path(generic_parameters):
    """ Init s2_l3a_output_path parameter"""

    def __init__(self):
        super().__init__()
        self.name = "s2_l3a_output_path"
        self.desc = ("Absolute path to store preprocessed data "
                     "in a dedicated directory.")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_s1_path(generic_parameters):
    """ Init s1_path parameter"""

    def __init__(self):
        super().__init__()
        self.name = "s1_path"
        self.desc = ("Absolute path to Sentinel-1 configuration file")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_user_feat_path(generic_parameters):
    """ Init user_feat_path parameter"""

    def __init__(self):
        super().__init__()
        self.name = "user_feat_path"
        self.desc = ("Absolute path to the user's features path")
        self.long_desc = ("Absolute path to the user's features path"
                          " They must be stored by tiles")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_minimum_required_dates(generic_parameters):
    """Init minimum_required_dates"""

    def __init__(self):
        super().__init__()
        self.name = "minimum_required_dates"
        self.desc = "required minimum number of available dates for each sensor"
        self.value = 2
        self.type_exp = int
        self.section = "chain"
        self.builders = {"i2_classification": False}


class init_runs(generic_parameters):
    """ Init runs parameter"""

    def __init__(self):
        super().__init__()
        self.name = "runs"
        self.desc = "Number of independant runs processed."
        self.long_desc = ("Number of independant runs processed."
                          " Each run has his own learning samples."
                          " Must be an integer greater than 0")
        self.value = 1
        self.type_exp = int
        self.section = "arg_train"
        self.builders = {"i2_classification": False, "i2_obia": False}


class init_cloud_threshold(generic_parameters):
    """ Init cloud_threshold parameter"""

    def __init__(self):
        super().__init__()
        self.name = "cloud_threshold"
        self.desc = ("Threshold to consider that a pixel is valid")
        self.long_desc = ("Indicates the threshold for a polygon to"
                          " be used for learning. It use the validity count,"
                          " which is incremented if a cloud, "
                          "a cloud shadow or a saturated pixel is detected")
        self.value = 0
        self.type_exp = int
        self.section = "chain"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_split_ground_truth(generic_parameters):
    """ Init split_ground_truth parameter"""

    def __init__(self):
        super().__init__()
        self.name = "split_ground_truth"
        self.desc = ("Enable the split of reference data ")
        self.long_desc = (" If set to False, the chain"
                          " use all polygons for both "
                          "training and validation")
        self.value = True
        self.type_exp = bool
        self.section = "arg_train"
        self.builders = {"i2_classification": False, "i2_obia": False}


class init_ratio(generic_parameters):
    """ Init ratio parameter"""

    def __init__(self):
        super().__init__()
        self.name = "ratio"
        self.desc = (
            "Should be between 0.0 and 1.0 and represent "
            "the proportion of the dataset to include in the train split.")
        self.value = 0.5
        self.type_exp = float
        self.section = "arg_train"
        self.builders = {"i2_classification": False, "i2_obia": False}


class init_random_seed(generic_parameters):
    """ Init random_seed parameter"""

    def __init__(self):
        super().__init__()
        self.name = "random_seed"
        self.desc = ("Fix the random seed for random split of reference data")
        self.long_desc = ("Fix the random seed used for random split "
                          "of reference data"
                          " If set, the results must be the "
                          "same for a given classifier")
        self.value = None
        self.type_exp = int
        self.section = "arg_train"
        self.builders = {"i2_classification": False, "i2_obia": False}


class init_first_step(generic_parameters):
    """ Init last_step parameter"""

    def __init__(self):
        super().__init__()
        self.name = "first_step"
        self.desc = ("The step group name indicating where the chain start")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": True,
            "i2_features_map": True,
            "i2_obia": True
        }

    def ensure_param_type(self, param):
        out_param = super().ensure_param_type(param)
        if out_param is None:
            raise ValueError(f"{self.name} is mandatory")
        return out_param


class init_last_step(generic_parameters):
    """ Init last_step parameter"""

    def __init__(self):
        super().__init__()
        self.name = "last_step"
        self.desc = ("The step group name indicating where the chain ends")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": True,
            "i2_features_map": True,
            "i2_obia": True
        }

    def ensure_param_type(self, param):
        out_param = super().ensure_param_type(param)
        if out_param is None:
            raise ValueError(f"{self.name} is mandatory")
        return out_param


class init_mode_outside_region(generic_parameters):
    """ Init mode_outside_region parameter"""

    def __init__(self):
        super().__init__()
        self.name = "mode_outside_regionsplit"
        self.desc = ("Fix the threshold for split huge model")
        self.long_desc = (
            "This parameter is available if "
            "regionPath is used and arg_classification.classif_mode"
            " is set to fusion. "
            "It represents the maximum size covered by a region."
            " If the regions are larger than this threshold, "
            "then N models are built by randomly"
            " selecting features inside the region.")
        self.value = 0.1
        self.type_exp = float
        self.section = "arg_train"
        self.builders = {"i2_classification": False, "i2_obia": False}


class init_logger_level(generic_parameters):
    """ Init logger_level parameter"""

    def __init__(self):
        super().__init__()
        self.name = "logger_level"
        self.desc = ("Set the logger level: NOTSET, DEBUG,"
                     " INFO, WARNING, ERROR, CRITICAL")
        self.value = "INFO"
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_region_path(generic_parameters):
    """ Init region_path parameter"""

    def __init__(self):
        super().__init__()
        self.name = "region_path"
        self.desc = ("Absolute path to region vector file")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {"i2_classification": False, "i2_obia": False}


class init_region_field(generic_parameters):
    """ Init region_field parameter"""

    def __init__(self):
        super().__init__()
        self.name = "region_field"
        self.desc = ("The column name for region indicator in"
                     "`region_path` file")
        self.value = "region"
        self.type_exp = str
        self.section = "chain"
        self.long_desc = (
            "this column in the database must contains string which"
            " can be converted into integers. For instance '1_2' "
            "does not match this condition")
        self.builders = {"i2_classification": False, "i2_obia": False}


class init_merge_final_classifications(generic_parameters):
    """ Init merge_final_classifications parameter"""

    def __init__(self):
        super().__init__()
        self.name = "merge_final_classifications"
        self.value = False
        self.desc = ("Enable the fusion of "
                     "classifications mode, merging all "
                     "run in a unique result.")
        self.type_exp = bool
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_merge_final_classifications_method(generic_parameters):
    """ Init merge_final_classifications_method parameter"""

    def __init__(self):
        super().__init__()
        self.name = "merge_final_classifications_method"
        self.desc = ("Indicate the fusion of classification method:"
                     " 'majorityvoting' or 'dempstershafer'")
        self.value = "majorityvoting"
        self.type_exp = str
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_merge_final_classifications_undecidedlabel(generic_parameters):
    """ Init merge_final_classifications_undecidedlabel parameter"""

    def __init__(self):
        super().__init__()
        self.name = "merge_final_classifications_undecidedlabel"
        self.desc = ("Indicate the label for undecision case during fusion")
        self.value = 255
        self.type_exp = int
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_fusionofclassification_all_samples_validation(generic_parameters):
    """ Init fusionofclassification_all_samples_validation parameter"""

    def __init__(self):
        super().__init__()
        self.name = "fusionofclassification_all_samples_validation"
        self.desc = ("Enable the use of all reference data")
        self.long_desc = ("If the fusion mode is enabled, "
                          "enable the use of all reference data samples"
                          " for validation")
        self.value = False
        self.type_exp = bool
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_dempstershafer_mob(generic_parameters):
    """ Init dempstershafer_mob parameter"""

    def __init__(self):
        super().__init__()
        self.name = "dempstershafer_mob"
        self.desc = ("Choose the dempster shafer mass "
                     "of belief estimation method")
        self.long_desc = ("Two kind of indexes can be used:\n"
                          "* Global: `accuracy` or `kappa`\n"
                          "* Per class: `precision` or `recall`\n")
        self.value = "precision"
        self.type_exp = str
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_merge_final_classifications_ratio(generic_parameters):
    """ Init merge_final_classifications_ratio parameter"""

    def __init__(self):
        super().__init__()
        self.name = "merge_final_classifications_ratio"
        self.desc = ("Percentage of samples to use "
                     "in order to evaluate the fusion raster")
        self.value = 0.1
        self.type_exp = float
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_keep_runs_results(generic_parameters):
    """ Init keep_runs_results parameter"""

    def __init__(self):
        super().__init__()
        self.name = "keep_runs_results"
        self.desc = ("")
        self.long_desc = (
            "If in fusion mode, two final reports can be provided. "
            "One for each seed, and one for the classification fusion")
        self.value = True
        self.type_exp = bool
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_generate_final_probability_map(generic_parameters):
    """Init generate_final_probability_map parameter."""

    def __init__(self):
        super().__init__()
        self.name = "generate_final_probability_map"
        self.desc = ("If True produce the mosaic of probabilities maps.")
        self.long_desc = ("This operation can be very costly")
        self.value = False
        self.type_exp = bool
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_enable_boundary_fusion(generic_parameters):
    """ Init enable_boundary_fusion parameter"""

    def __init__(self):
        super().__init__()
        self.name = "enable_boundary_fusion"
        self.desc = ("Enable the fusion at boundary mode")
        self.value = False
        self.type_exp = bool
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_boundary_fusion_epsilon(generic_parameters):
    """ Init enable_boundary_fusion_epsilon parameter"""

    def __init__(self):
        super().__init__()
        self.name = "boundary_fusion_epsilon"
        self.desc = ("Choose the epsilon value for compute weights")
        self.value = 0.0001
        self.type_exp = float
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_boundary_exterior_buffer_size(generic_parameters):
    """ Init boundary_exterior_buffer_size parameter"""

    def __init__(self):
        super().__init__()
        self.name = "boundary_exterior_buffer_size"
        self.desc = ("Choose the exterior buffer size in projection unit. "
                     "Must be positive")
        self.long_desc = ("The exterior buffer is the area outside the region "
                          "where the weights for boundary classification"
                          " fusion must be computed")
        self.value = 1000
        self.type_exp = int
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_boundary_interior_buffer_size(generic_parameters):
    """ Init boundary_interior_buffer_size parameter"""

    def __init__(self):
        super().__init__()
        self.name = "boundary_interior_buffer_size"
        self.desc = ("Choose the interior buffer size in projection unit. "
                     "Must be positive")
        self.long_desc = ("The interior buffer is the area inside the region "
                          "where the weights for boundary classification"
                          " fusion must be computed. "
                          "It's mandatory to this value to be positive")
        self.value = 1000
        self.type_exp = int
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_boundary_comparison_mode(generic_parameters):
    """ Init boundary_comparison_mode parameter"""

    def __init__(self):
        super().__init__()
        self.name = "boundary_comparison_mode"
        self.desc = ("Enable the comparison mode.")
        self.long_desc = (
            "If True the maps masked by region masks "
            "and the probability based fusion maps are kept "
            "during the whole process. It allows to produce "
            "comparison metrics in the boundary area.\n"
            "If False and enable_boundary_fusion is True "
            "only the boundary products are created.\n"
            "This parameter as no impact if enable_boundary_fusion "
            "is set to False")
        self.value = False
        self.type_exp = bool
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_check_inputs(generic_parameters):
    """ Init check_inputs parameter"""

    def __init__(self):
        super().__init__()
        self.name = "check_inputs"
        self.desc = ("Enable the inputs verification.")
        self.long_desc = (
            "Enable the inputs verification."
            " It can take a lot of time for large dataset. "
            "Check if region intersect reference data for instance")
        self.value = True
        self.type_exp = bool
        self.section = "chain"
        self.builders = {"i2_classification": False}


class init_enable_autocontext(generic_parameters):
    """ Init enable_autocontext parameter"""

    def __init__(self):
        super().__init__()
        self.name = "enable_autocontext"
        self.desc = ("Enable the auto-context processing")
        self.value = False
        self.type_exp = bool
        self.section = "arg_train"
        self.builders = {"i2_classification": False}


class init_autocontext_iterations(generic_parameters):
    """ Init autocontext_iterations parameter"""

    def __init__(self):
        super(init_autocontext_iterations, self).__init__()
        self.name = "autocontext_iterations"
        self.desc = ("Number of iterations in auto-context.")
        self.value = 3
        self.type_exp = int
        self.section = "arg_train"
        self.builders = {"i2_classification": False}


class init_force_standard_labels(generic_parameters):
    """ Init force_standard_labels parameter"""

    def __init__(self):
        super().__init__()
        self.name = "force_standard_labels"
        self.desc = ("Standardize labels for feature extraction")
        self.long_desc = (
            "The chain label each features by the sensors name,"
            " the spectral band or indice and the date."
            " If activated this parameter use the OTB default value"
            " (`value_X`)")
        self.value = False
        self.type_exp = bool
        self.section = "arg_train"
        self.builders = {"i2_classification": False}


class init_spatial_resolution(generic_parameters):
    """ Init spatial_resolution parameter"""

    def __init__(self):
        super().__init__()
        self.name = "spatial_resolution"
        self.desc = ("Output spatial resolution")
        self.long_desc = ("The spatial resolution expected."
                          "It can be provided as integer or float,"
                          "or as a list containing two values"
                          " for non squared resolution")
        self.value = 10
        self.type_exp = list
        self.section = "chain"
        self.builders = {"i2_classification": False, "i2_obia": False}

    def ensure_param_type(self, param):
        # param is the readed resolution
        try:
            init_spatiale_res = param
            if isinstance(init_spatiale_res, (float, int)):
                init_spatiale_res = [init_spatiale_res, init_spatiale_res]
            elif isinstance(init_spatiale_res, Sequence):
                init_spatiale_res = init_spatiale_res.data
                if len(init_spatiale_res) == 1:
                    init_spatiale_res.append(init_spatiale_res[0])
        except Exception:
            init_spatiale_res = []
        if init_spatiale_res and len(init_spatiale_res) > 2:
            raise ValueError("spatial resolution must contains at most "
                             "2 values : [spx, spy]")
        return [float(spa_res) for spa_res in init_spatiale_res]


class init_remove_output_path(generic_parameters):
    """ Init remove_output_path parameter"""

    def __init__(self):
        super().__init__()
        self.name = "remove_output_path"
        self.desc = ("Enable the removing of complete `output_path` directory")
        self.long_desc = (
            "Enable the removing of complete `output_path` directory\n"
            "Only if the `first_step` is `init` and the folder name is valid")
        self.value = True
        self.type_exp = bool
        self.section = "chain"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_output_path(generic_parameters):
    """ Init output_path parameter"""

    def __init__(self):
        super().__init__()
        self.name = "output_path"
        self.desc = ("Absolute path to the output directory.")
        self.long_desc = ("Absolute path to the output directory."
                          "It is recommended to have one directory"
                          " per run of the chain")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": True,
            "i2_features_map": True,
            "i2_vectorization": True,
            "i2_obia": True
        }


class init_nomenclature_path(generic_parameters):
    """ Init nomenclature_path parameter"""

    def __init__(self):
        super().__init__()
        self.name = "nomenclature_path"
        self.desc = ("Absolute path to the nomenclature description file")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {"i2_classification": True, "i2_obia": True}


class init_list_tile(generic_parameters):
    """ Init list_tile parameter"""

    def __init__(self):
        super().__init__()
        self.name = "list_tile"
        self.desc = ("List of tile to process, separated by space")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": True,
            "i2_features_map": True,
            "i2_obia": True
        }
        self.delimiter = " "
        self.forbiden_chars = [",", "|", ":", ";"]

    def ensure_param_type(self, param):
        out_param = super(init_list_tile, self).ensure_param_type(param)
        if out_param is not None:
            out_param = " ".join(out_param.split())

            for forbiden_char in self.forbiden_chars:
                if forbiden_char in out_param:
                    raise ValueError(
                        "'list_tile' configuration file parameter"
                        f" delimiter must be a whitespace. "
                        f"forbiden characters :'{self.forbiden_chars}'")

        return out_param


class init_ground_truth(generic_parameters):
    """ Init ground_truth parameter"""

    def __init__(self):
        super().__init__()
        self.name = "ground_truth"
        self.desc = ("Absolute path to reference data")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {"i2_classification": True, "i2_obia": True}


class init_data_field(generic_parameters):
    """ Init data_field parameter"""

    def __init__(self):
        super().__init__()
        self.name = "data_field"
        self.desc = ("Field name indicating classes "
                     "labels in `ground_thruth`")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {"i2_classification": True, "i2_obia": True}

    def ensure_param_type(self, param):
        out_param = super(init_data_field, self).ensure_param_type(param)
        if out_param and out_param.lower() == I2_CONST.re_encoding_label_name:
            raise ValueError(
                f"data_field parameter can't be '{I2_CONST.re_encoding_label_name}'"
            )
        return out_param


class init_color_table(generic_parameters):
    """ Init color_table parameter"""

    def __init__(self):
        super().__init__()
        self.name = "color_table"
        self.desc = ("Absolute path to the file which"
                     " link classes and their colors")
        self.value = None
        self.type_exp = str
        self.section = "chain"
        self.builders = {"i2_classification": True}


# coregistration
class init_vhr_path(generic_parameters):
    """ Init vhr_path parameter"""

    def __init__(self):
        super().__init__()
        self.name = "vhr_path"
        self.desc = ("Absolute path to VHR path")
        self.value = None
        self.type_exp = str
        self.section = "coregistration"
        self.builders = {"i2_classification": False}


class init_date_vhr(generic_parameters):
    """ Init date_vhr parameter"""

    def __init__(self):
        super().__init__()
        self.name = "date_vhr"
        self.desc = ("Date `YYYYMMDD` of the VHR image")
        self.value = None
        self.type_exp = str
        self.section = "coregistration"
        self.builders = {"i2_classification": False}


class init_date_src(generic_parameters):
    """ Init date_src parameter"""

    def __init__(self):
        super().__init__()
        self.name = "date_src"
        self.desc = ("Date `YYYYMMDD` of the reference image")
        self.long_desc = ("If no `date_src` is mentionned, the best image will"
                          " be automatically choose for coregistration")
        self.value = None
        self.type_exp = str
        self.section = "coregistration"
        self.builders = {"i2_classification": False}


class init_band_ref(generic_parameters):
    """ Init band_ref parameter"""

    def __init__(self):
        super().__init__()
        self.name = "band_ref"
        self.desc = ("Number of the band of the VHR image to use"
                     " for coregistration")
        self.value = 1
        self.type_exp = int
        self.section = "coregistration"
        self.builders = {"i2_classification": False}


class init_band_src(generic_parameters):
    """ Init band_src parameter"""

    def __init__(self):
        super().__init__()
        self.name = "band_src"
        self.desc = ("Number of the band of the src raster to"
                     " use for coregistration")
        self.value = 3
        self.type_exp = int
        self.section = "coregistration"
        self.builders = {"i2_classification": False}


class init_resample(generic_parameters):
    """ Init resample parameter"""

    def __init__(self):
        super().__init__()
        self.name = "resample"
        self.desc = ("Resample the reference and the source raster"
                     "to the same resolution to find SIFT points")
        self.value = True
        self.type_exp = bool
        self.section = "coregistration"
        self.builders = {"i2_classification": False}


class init_step(generic_parameters):
    """ Init step parameter"""

    def __init__(self):
        super().__init__()
        self.name = "step"
        self.desc = ("Initial size of steps between bins in pixels")
        self.value = 256
        self.type_exp = int
        self.section = "coregistration"
        self.builders = {"i2_classification": False}


class init_minstep(generic_parameters):
    """ Init minstep parameter"""

    def __init__(self):
        super().__init__()
        self.name = "minstep"
        self.desc = ("Minimal size of steps between bins in pixels")
        self.value = 16
        self.type_exp = int
        self.section = "coregistration"
        self.builders = {"i2_classification": False}


class init_minsiftpoints(generic_parameters):
    """ init minsiftpoints parameters"""

    def __init__(self):
        super().__init__()
        self.name = "minsiftpoints"
        self.desc = ("Minimal number of SIFT points to find"
                     " to create the new RPC model")
        self.value = 40
        self.type_exp = int
        self.section = "coregistration"
        self.builders = {"i2_classification": False}


class init_iterate(generic_parameters):
    """ Init iterate parameter"""

    def __init__(self):
        super().__init__()
        self.name = "iterate"
        self.desc = ("Proceed several iteration by reducing the step"
                     " between geobin to find SIFT points")
        self.value = True
        self.type_exp = bool
        self.section = "coregistration"
        self.builders = {"i2_classification": False}


class init_prec(generic_parameters):
    """ Init prec parameter"""

    def __init__(self):
        super().__init__()
        self.name = "prec"
        self.desc = ("Estimated shift between source and reference raster in "
                     "pixel (source raster resolution)")
        self.value = 3
        self.type_exp = int
        self.section = "coregistration"
        self.builders = {"i2_classification": False}


class init_mode(generic_parameters):
    """ Init mode parameter"""

    def __init__(self):
        super().__init__()
        self.name = "mode"
        self.desc = ("Coregistration mode of the time series")
        # Be careful with this desc, all space have been counted
        # for good rst formating
        self.long_desc = (
            "+--------+-----------------------------------------"
            "---------------------------------------------------"
            "---------------------------------------------------"
            "---------------------------------------------------"
            "-----------------------------------------+\n"
            "| Mode   | Method                                  "
            "                                                   "
            "                                                   "
            "                                                   "
            "                                         |\n"
            "+========+========================================="
            "==================================================="
            "==================================================="
            "==================================================="
            "=========================================+\n"
            "|  1     |  single coregistration between one "
            "source image (and its masks) and the VHR image      "
            "                                                    "
            "                                                    "
            "                                           |\n"
            "+--------+------------------------------------------"
            "----------------------------------------------------"
            "----------------------------------------------------"
            "----------------------------------------------------"
            "-------------------------------------+\n"
            "|  2     | this mode operates a coregistration "
            "between a image of the timeseries and the VHR image"
            ", then the same RPC model is used to orthorectify "
            "every images of the timeseries                      "
            "                                             |\n"
            "+--------+------------------------------------------"
            "----------------------------------------------------"
            "----------------------------------------------------"
            "----------------------------------------------------"
            "-------------------------------------+\n"
            "|  3     | cascade mode, this mode operates a first "
            "coregistration between a source image and the VHR "
            "image, then each image of the timeseries is "
            "coregistered step by step with the closest temporal "
            "images of the timeseries already coregistered  |\n"
            "+--------+------------------------------------------"
            "----------------------------------------------------"
            "----------------------------------------------------"
            "----------------------------------------------------"
            "-------------------------------------+\n")
        self.value = 2
        self.type_exp = int
        self.section = "coregistration"
        self.builders = {"i2_classification": False}


class init_pattern(generic_parameters):
    """ Init pattern parameter"""

    def __init__(self):
        super().__init__()
        self.name = "pattern"
        self.desc = ("Pattern of the time series files to coregister")
        self.long_desc = ("By default the value is left to `None` and "
                          "the pattern depends on the sensor used "
                          "(*STACK.tif for Sentinel2, ORTHO_SURF_CORR"
                          "_PENTE*.TIF)"
                          "\n\n"
                          "Examples\n"
                          "^^^^^^^^\n"
                          "    .. code-block:: python\n\n"
                          "        pattern: '*STACK.tif'")
        self.value = None
        self.type_exp = str
        self.section = "coregistration"
        self.builders = {"i2_classification": False}


# scikit model parameters
class init_model_type(generic_parameters):
    """ Init model_type parameter"""

    def __init__(self):
        super().__init__()
        self.name = "model_type"
        self.desc = ("machine learning algorthm’s name")
        self.long_desc = ("Models comming from scikit-learn are use "
                          "if scikit_models_parameters.model_type is "
                          "different from None. More "
                          "informations about how to use "
                          "scikit-learn is available at iota2 "
                          "and scikit-learn machine learning algorithms.")
        self.value = None
        self.type_exp = str
        self.section = "scikit_models_parameters"
        self.builders = {"i2_classification": False}


class init_cross_validation_folds(generic_parameters):
    """ Init cross_validation_folds parameter"""

    def __init__(self):
        super().__init__()
        self.name = "cross_validation_folds"
        self.desc = ("The number of k-folds")
        self.value = 5
        self.type_exp = int
        self.section = "scikit_models_parameters"
        self.builders = {"i2_classification": False}


class init_cross_validation_grouped(generic_parameters):
    """ Init cross_validation_grouped parameter"""

    def __init__(self):
        super().__init__()
        self.name = "cross_validation_grouped"
        self.desc = ("")
        self.value = False
        self.type_exp = bool
        self.section = "scikit_models_parameters"
        self.builders = {"i2_classification": False}


class init_standardization(generic_parameters):
    """ Init standardization parameters"""

    def __init__(self):
        super().__init__()
        self.name = "standardization"
        self.desc = ("")
        self.value = False
        self.type_exp = bool
        self.section = "scikit_models_parameters"
        self.builders = {"i2_classification": False}


class init_cross_validation_parameters(generic_parameters):
    """Init cross_validation_parameters parameter"""

    def __init__(self):
        super().__init__()
        self.name = "cross_validation_parameters"
        self.desc = ("")
        self.value = dico_mapping_init({})
        self.type_exp = dict
        self.section = "scikit_models_parameters"
        self.builders = {"i2_classification": False}


# arg_train
class init_deep_learning_parameters(generic_parameters):
    """ Init neural network parameters"""

    def __init__(self):
        super(init_deep_learning_parameters, self).__init__()
        self.name = "deep_learning_parameters"
        self.desc = ("deep learning parameter description is available "
                     ":doc:`here <deep_learning>`")
        self.value = dico_mapping_init({})
        self.type_exp = dict
        self.section = "arg_train"
        self.mandatory = ["no"]
        self.model_selection_criterion_default = "loss"
        self.weighted_labels_default = False
        self.nn_module_default = None
        self.additional_statistics_percentage_default = None

        self.model_selection_criterion_possible_values = [
            "loss", "fscore", "oa", "kappa"
        ]
        self.early_stop_available_metrics = [
            'train_loss', 'val_loss', 'fscore', 'oa', 'kappa'
        ]
        self.adaptive_lr_forbiden_params = ["optimizer", "mode"]
        self.dataloader_possible_values = ["stream", "full"]
        self.dataloader_mode_default = "stream"
        self.hyperparameters_default = {
            "batch_size": [1000],
            "learning_rate": [0.00001]
        }
        self.hyperparameters_avail_keys = ["batch_size", "learning_rate"]
        self.builders = {"i2_classification": False}

    def ensure_param_type(self, param):
        out_param = super(init_deep_learning_parameters,
                          self).ensure_param_type(param)

        parsed_param = out_param.copy()
        additional_user_keys = self.control_mapping_keys(
            parsed_param,
            ["dl_parameters", "hyperparameters_solver", "adaptive_lr"])
        if "enable_early_stop" in out_param:
            value = out_param["enable_early_stop"]
            if not isinstance(value, bool):
                raise TypeError(
                    "arg_train.deep_learning_parameters.enable_early_stop "
                    "must be a boolean value")
        if "epoch_to_trigger" in out_param:
            value = out_param["epoch_to_trigger"]
            if not isinstance(value, int):
                raise TypeError(
                    "arg_train.deep_learning_parameters.epoch_to_trigger "
                    "must be a int value")
        if "early_stop_patience" in out_param:
            value = out_param["early_stop_patience"]
            if not isinstance(value, int):
                raise TypeError(
                    "arg_train.deep_learning_parameters.early_stop_patience "
                    "must be a int value")
        if "early_stop_metric" in out_param:
            value = out_param["early_stop_metric"]
            if not isinstance(value, str):
                raise TypeError(
                    "arg_train.deep_learning_parameters.early_stop_metric "
                    "must be a string value")
            if value not in self.early_stop_available_metrics:
                raise ValueError(
                    "arg_train.deep_learning_parameters.early_stop_metric "
                    f"must be in {self.early_stop_available_metrics}")
        if "early_stop_tol" in out_param:
            value = out_param["early_stop_tol"]
            if not isinstance(value, float):
                raise TypeError(
                    "arg_train.deep_learning_parameters.early_stop_tol "
                    "must be a float value")

        # place default values after the parsed_param variable init
        parsed_param = {}
        for key, value in out_param.items():
            if key == "dl_parameters":
                parsed_param[key] = dict(value)
            else:
                parsed_param[key] = value
        if parsed_param and "dl_parameters" not in parsed_param:
            parsed_param["dl_parameters"] = {}

        if "additional_statistics_percentage" in out_param:
            value = out_param["additional_statistics_percentage"]
            if not isinstance(value, float):
                raise TypeError(
                    f"{self.section}.{self.name}.additional_statistics_percentage"
                    " must be a float in ]0;1]")
        else:
            parsed_param[
                "additional_statistics_percentage"] = self.additional_statistics_percentage_default
        if "adaptive_lr" in out_param:
            parsed_param["adaptive_lr"] = dict(out_param["adaptive_lr"])
            forbiden_param_message = []
            for forbiden_param in self.adaptive_lr_forbiden_params:
                if forbiden_param in parsed_param["adaptive_lr"]:
                    forbiden_param_message.append(
                        f"{self.section}.{self.name}."
                        f"adaptive_lr.{forbiden_param}")
            if forbiden_param_message:
                raise ValueError(
                    f"parameter{'s' if len(forbiden_param_message)>1 else ''}"
                    f" : {', '.join(forbiden_param_message)}"
                    f" {'are' if len(forbiden_param_message)>1 else 'is' } "
                    f"forbiden, please remove {'them' if len(forbiden_param_message)>1 else 'it'}"
                    " from the iota2 configuration file")
        else:
            parsed_param["adaptive_lr"] = {}

        if "dataloader_mode" in out_param:
            value = out_param["dataloader_mode"]
            if not isinstance(value, str):
                raise TypeError(
                    "arg_train.deep_learning_parameters.dataloader_mode "
                    "must be a string value")
            if value not in self.dataloader_possible_values:
                raise ValueError(
                    "arg_train.deep_learning_parameters.dataloader_mode "
                    f"must be in : {self.dataloader_possible_values}")
        else:
            parsed_param["dataloader_mode"] = self.dataloader_mode_default

        if "hyperparameters_solver" in out_param:
            hyperparameters_keys = parsed_param["hyperparameters_solver"].keys(
            )
            # check keys
            keys_check = all(key in self.hyperparameters_avail_keys
                             for key in hyperparameters_keys)
            if not keys_check:
                raise ValueError(
                    f"hyperparameters keys must be in {', '.join(self.hyperparameters_avail_keys)}"
                )
            parsed_param["hyperparameters_solver"] = {}
            for key, val in dict(out_param["hyperparameters_solver"]).items():
                parsed_param["hyperparameters_solver"][key] = list(val)
        else:
            parsed_param[
                "hyperparameters_solver"] = self.hyperparameters_default

        if "model_selection_criterion" in out_param:
            value = parsed_param["model_selection_criterion"]
            if value not in self.model_selection_criterion_possible_values:
                raise ValueError(
                    f"configuration file parameter arg_train.deep_learning_parameters.model_selection_criterion not in accepted values : {', '.join(self.model_selection_criterion_possible_values)}"
                )
        else:
            parsed_param[
                "model_selection_criterion"] = self.model_selection_criterion_default
        if "weighted_labels" in out_param:
            value = out_param["weighted_labels"]
            if not isinstance(value, bool):
                raise TypeError(
                    "arg_train.deep_learning_parameters.weighted_labels "
                    "must be a boolean value")
        else:
            parsed_param["weighted_labels"] = self.weighted_labels_default
        if "dl_module" in out_param:
            value = out_param["dl_module"]
            if not isinstance(value, str):
                raise TypeError("arg_train.deep_learning_parameters.dl_module "
                                "must be a string value")
        else:
            parsed_param["dl_module"] = self.nn_module_default
        for key in additional_user_keys:
            parsed_param.pop(key, None)
        return parsed_param


class init_sample_selection(generic_parameters):
    """ Init sample_selection parameter"""

    def __init__(self):
        super().__init__()
        self.name = "sample_selection"
        self.desc = ("OTB parameters for sample selection")
        self.long_desc = (
            "This field parameters the strategy of polygon "
            "sampling. It directly refers to options of "
            "OTB’s `SampleSelection <https://www.orfeo-toolbox.org/CookBook/Applications/app_SampleSelection.html>`_ application."
            "\n\nExample\n"
            "^^^^^^^\n\n"
            ".. code-block:: python\n\n"
            "    sample_selection : {'sampler':'random',\n"
            "                       'strategy':'percent',\n"
            "                       'strategy.percent.p':0.2,\n"
            "                       'per_models':[{'target_model':'4',\n"
            "                                      'sampler':'periodic'}]\n"
            "                       }"
            "\n\nIn the example above, all polygons will be "
            "sampled with the 20% ratio. But "
            "the polygons which belong to the model 4 will "
            "be periodically sampled,"
            " instead of the ransom sampling used for other "
            "polygons."
            "\nNotice than ``per_models`` key contains a list of"
            " strategies. Then we can imagine the following :\n\n"
            ".. code-block:: python\n\n"
            "    sample_selection : {'sampler':'random',\n"
            "                       'strategy':'percent',\n"
            "                       'strategy.percent.p':0.2,\n"
            "                       'per_models':[{'target_model':'4',\n"
            "                                      'sampler':'periodic'},\n"
            "                                     {'target_model':'1',\n"
            "                                      'sampler':'random',\n"
            "                                      'strategy', 'byclass',\n"
            "                                      'strategy.byclass.in',"
            " '/path/to/myCSV.csv'\n"
            "                                     }]\n"
            "                       }\n"
            "\nwhere the first column of /path/to/myCSV.csv is"
            " class label (integer), second one is the required"
            " samples number (integer).")
        self.value = dico_mapping_init({
            "sampler": "random",
            "strategy": "all"
        })
        self.type_exp = dict
        self.section = "arg_train"
        self.builders = {"i2_classification": False, "i2_obia": False}


class init_sample_validation(generic_parameters):
    """ Init sample_validation parameter"""

    def __init__(self):
        super().__init__()
        self.name = "sample_validation"
        self.desc = ("OTB parameters for sampling the validation set")
        self.long_desc = (
            "This field parameters the strategy of polygon "
            "sampling. It directly refers to options of "
            "OTB’s `SampleSelection <https://www.orfeo-toolbox.org/CookBook/Applications/app_SampleSelection.html>`_ application."
            "\n\nExample\n"
            "-------\n\n"
            ".. code-block:: console\n\n"
            "    sample_validation : {'sampler':'random',\n"
            "                       'strategy':'all',\n"
            "                       }\n")
        self.value = dico_mapping_init({
            "sampler": "random",
            "strategy": "all"
        })
        self.type_exp = dict
        self.section = "arg_train"
        self.builders = {"i2_classification": False}


class init_sampling_validation(generic_parameters):
    """ Init sampling validation parameter"""

    def __init__(self):
        super().__init__()
        self.name = "sampling_validation"
        self.desc = "Enable sampling validation"
        self.value = False
        self.type_exp = bool
        self.section = "arg_train"
        self.builders = {"i2_classification": False}


class init_sample_augmentation(generic_parameters):
    """ Init sample augmentation parameter"""

    def __init__(self):
        super().__init__()
        self.name = "sample_augmentation"
        self.desc = ("OTB parameters for sample augmentation")
        self.long_desc = (
            "In supervised classification the balance between "
            "class samples is important. There are"
            " any ways to manage class balancing in iota2, using "
            ":ref:`sampleSelectionTag` or "
            "the classifier's options to limit the number of "
            "samples by class."
            "\nAn other approch is to generate synthetic "
            "samples. It is the purpose of this"
            "functionality, which is called "
            "'sample augmentation'.\n\n"
            "    .. code-block:: python\n\n"
            "        {'activate':False}\n"
            "\nExample\n"
            "^^^^^^^\n\n"
            ".. code-block:: python\n\n"
            "    sample_augmentation : {'target_models':['1', '2'],\n"
            "                          'strategy' : 'jitter',\n"
            "                          'strategy.jitter.stdfactor' : 10,\n"
            "                          'strategy.smote.neighbors'  : 5,\n"
            "                          'samples.strategy' : 'balance',\n"
            "                          'activate' : True\n"
            "                          }\n"
            "\n\niota2 implements an interface to the OTB `SampleAugmentation"
            " <https://www.orfeo-toolbox.org/CookBook/Applications/"
            "app_SampleAugmentation.html>`_ application.\n"
            "There are three methods to generate samples : replicate, jitter"
            " and smote."
            "The documentation :doc:`here <sampleAugmentation_explain>`"
            " explains the difference"
            " between these approaches.\n\n"
            " ``samples.strategy`` specifies how many samples must be created."
            "There are 3 different strategies:\n\n"
            "    - minNumber\n"
            "        To set the minimum number of samples by class required\n"
            "    - balance\n"
            "        balance all classes with the same number of "
            "samples as the majority one\n"
            "    - byClass\n"
            "        augment only some of the classes"
            "\n\nParameters related to ``minNumber`` and ``byClass`` strategies"
            " are:\n\n"
            "    - samples.strategy.minNumber\n"
            "        minimum number of samples\n"
            "    - samples.strategy.byClass\n"
            "        path to a CSV file containing in first column the class's"
            " label and \n"
            "        in the second column the minimum number of samples"
            " required.\n\n"
            "In the above example, classes of models '1' and '2' will be"
            " augmented to the"
            "the most represented class in the corresponding model using the"
            " jitter method.")
        self.value = dico_mapping_init({"activate": False})
        self.type_exp = dict
        self.section = "arg_train"
        self.builders = {"i2_classification": False, "i2_obia": False}


class init_sample_management(generic_parameters):
    """ Init sample management parameter"""

    def __init__(self):
        super().__init__()
        self.name = "sample_management"
        self.desc = ("Absolute path to a CSV file containing samples transfert"
                     " strategies")
        self.long_desc = (
            "The CSV must contain a row per transfert\n"
            "    .. code-block:: python\n\n"
            "        >>> cat /absolute/path/myRules.csv\n"
            "            1,2,4,2\n"
            "Meaning :\n"
            "        +--------+-------------+------------+----------+\n"
            "        | source | destination | class name | quantity |\n"
            "        +========+=============+============+==========+\n"
            "        |   1    |      2      |      4     |     2    |\n"
            "        +--------+-------------+------------+----------+\n")
        self.value = None
        self.type_exp = str
        self.section = "arg_train"
        self.builders = {"i2_classification": False, "i2_obia": False}


class init_dempster_shafer_sar_opt_fusion(generic_parameters):
    """ Init dempster shafer sar opt fusion parameter"""

    def __init__(self):
        super().__init__()
        self.name = "dempster_shafer_sar_opt_fusion"
        self.desc = ("Enable the use of both SAR and optical data to train"
                     " a model.")
        self.long_desc = ("Enable the use of both SAR and optical data to"
                          " train a model."
                          " If True then two models are trained."
                          "more documentation is avalailalbe "
                          ":doc:`here <SAR_Opt_postClassif_fusion>`")
        self.value = False
        self.type_exp = bool
        self.section = "arg_train"
        self.builders = {"i2_classification": False, "i2_obia": False}


class init_crop_mix(generic_parameters):
    """ Init crop mix parameter"""

    def __init__(self):
        super().__init__()
        self.name = "crop_mix"
        self.desc = ("Enable crop mix option")
        self.value = False
        self.type_exp = bool
        self.section = "arg_train"
        self.builders = {"i2_classification": False}


class init_prev_features(generic_parameters):
    """ Init prev features parameter"""

    def __init__(self):
        super(init_prev_features, self).__init__()
        self.name = "prev_features"
        self.desc = ("Path to a configuration file used to "
                     "produce previous features")
        self.long_desc = ("This config file must be launchable by iota2"
                          " (needed for crop mix)")
        self.value = None
        self.type_exp = str
        self.section = "arg_train"
        self.builders = {"i2_classification": False}


class init_output_prev_features(generic_parameters):
    """ Init output_prev_features parameter"""

    def __init__(self):
        super().__init__()
        self.name = "output_prev_features"
        self.desc = ("Path to previous features for crop mix")
        self.value = None
        self.type_exp = str
        self.section = "arg_train"
        self.builders = {"i2_classification": False}


class init_annual_crop(generic_parameters):
    """ Init annual crop  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "annual_crop"
        self.desc = ("The list of classes to be replaced by previous data")
        self.value = list_sequence_init(["11", "12"])
        self.type_exp = list
        self.section = "arg_train"
        self.builders = {"i2_classification": False}


class init_a_crop_label_replacement(generic_parameters):
    """ Init  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "a_crop_label_replacement"
        self.desc = ("Replace a label by a string")
        self.value = list_sequence_init(["10", "annual_crop"])
        self.type_exp = list
        self.section = "arg_train"
        self.builders = {"i2_classification": False}


class init_samples_classif_mix(generic_parameters):
    """ Init samples classif mix parameter"""

    def __init__(self):
        super().__init__()
        self.name = "samples_classif_mix"
        self.desc = ("Enable the second step of crop mix")
        self.value = False
        self.type_exp = bool
        self.section = "arg_train"
        self.builders = {"i2_classification": False}


class init_features_from_raw_dates(generic_parameters):
    """Init features_from_raw_dates parameter"""

    def __init__(self):
        super().__init__()
        self.name = "features_from_raw_dates"
        self.desc = ("learn model from raw sensor's date (no interpolations)")
        self.long_desc = (
            "If True, during the learning and classification step, each pixel"
            " will receive a vector of values of the size of the number of"
            " all dates detected. As the pixels were not all acquired on "
            "the same dates, the vector will contains NaNs on the unacquired "
            "dates. In iota2, we have chosen to keep these NaNs. The "
            "responsibility of managing these NaNs is delegated to the "
            "classification system (e.g. the forward method in deepLearning)."
            " Furthermore, in this use case, binary masks are provided to the"
            " classification system. These masks contains 3 types of "
            "information for 3 differents pixel values \n\n"
            "- 0 : pixel acquired by the sensor and valid\n\n"
            "- 1 : pixel acquired by the sensor and not valid (cloud,...)\n\n"
            "- 2 : pixel not acquired.")
        self.value = False
        self.type_exp = bool
        self.section = "arg_train"
        self.builders = {"i2_classification": False}


class init_classifier(generic_parameters):
    """ Init classifier parameter"""

    def __init__(self):
        super().__init__()
        self.name = "classifier"
        self.desc = ("Choose the classification algorithm")
        # self.value = "rf"
        self.value = None
        self.type_exp = str
        self.section = "arg_train"
        self.builders = {"i2_classification": False, "i2_obia": False}


class init_otb_classifier_options(generic_parameters):
    """ Init  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "otb_classifier_options"
        self.desc = ("OTB option for classifier. If None, the OTB default "
                     "values are used.")
        # self.value = dico_mapping_init({
        #     "classifier.rf.min": 5,
        #     "classifier.rf.max": 25
        # })
        self.value = None
        self.long_desc = (
            "This parameter is a dictionnary"
            " which accepts all OTB application parameters."
            " To know the exhaustive parameter list "
            " use `otbcli_TrainVectorClassifier` in a terminal or"
            " look at the OTB documentation")
        self.type_exp = dict
        self.section = "arg_train"
        self.builders = {"i2_classification": False, "i2_obia": False}


class init_annual_classes_extraction_source(generic_parameters):
    """ Init  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "annual_classes_extraction_source"
        self.desc = ("")
        self.value = None
        self.type_exp = str
        self.section = "arg_train"
        self.builders = {"i2_classification": False}


class init_validity_threshold(generic_parameters):
    """ Init  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "validity_threshold"
        self.desc = ("")
        self.value = 1
        self.type_exp = int
        self.section = "arg_train"
        self.builders = {"i2_classification": False, "i2_obia": False}


class init_no_label_management(generic_parameters):
    """ Init  no_label_management parameter"""

    def __init__(self):
        super().__init__()
        self.name = "no_label_management"
        self.desc = ("Method for choosing a label in case of fusion")
        self.value = "maxConfidence"
        self.type_exp = str
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_enable_probability_map(generic_parameters):
    """ Init  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "enable_probability_map"
        self.desc = ("Produce the probability map")
        self.long_desc = ("A probability map is a image with N bands"
                          " , where N is the number of classes in the"
                          " nomenclature file. The bands are "
                          "sorted in ascending order more information "
                          "more information :doc:`here <probability_maps>`")
        self.value = False
        self.type_exp = bool
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_fusion_options(generic_parameters):
    """ Init  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "fusion_options"
        self.desc = ("OTB FusionOfClassification options for voting method "
                     "involved if classif_mode is set to 'fusion'")
        self.value = " -nodatalabel 0 -method majorityvoting"
        self.type_exp = str
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_classif_mode(generic_parameters):
    """ Init classif_mode parameter"""

    def __init__(self):
        super().__init__()
        self.name = "classif_mode"
        self.desc = ("'separate' or 'fusion'.")
        self.long_desc = (
            " If 'fusion' : too huge models will be "
            "devided into smaller ones and will classify the same pixels."
            " The treshold between small/big models"
            " is define by the parameter 'mode_outside_regionsplit'")
        self.value = "separate"
        self.type_exp = str
        self.section = "arg_classification"
        self.builders = {"i2_classification": False}


class init_features(generic_parameters):
    """ Init features parameter"""

    def __init__(self):
        super().__init__()
        self.name = "features"
        self.desc = ("List of additional features computed")
        self.long_desc = ("This parameter enable the computation of the "
                          "three indices if available for the sensor used."
                          "There is no choice for using only one of them.")
        self.value = list_sequence_init(["NDVI", "NDWI", "Brightness"])
        self.type_exp = list
        self.section = "arg_train"
        self.builders = {"i2_classification": False, "i2_features_map": False}


class init_auto_date(generic_parameters):
    """ Init auto_date parameter"""

    def __init__(self):
        super().__init__()
        self.name = "auto_date"
        self.desc = ("Enable the use of `start_date` and `end_date`")
        self.long_desc = (
            "If True, iota2 will automatically guess the first"
            " and the last interpolation date. Else, `start_date` "
            "and `end_date` of each sensors will be used")
        self.value = True
        self.type_exp = bool
        self.section = "sensors_data_interpolation"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_write_outputs(generic_parameters):
    """ Init write_outputs parameter"""

    def __init__(self):
        super().__init__()
        self.name = "write_outputs"
        self.desc = ("Write temporary files")
        self.long_desc = (
            "Write the time series before and after gapfilling,"
            " the mask time series, and also the feature"
            " time series. This option required a large amount of"
            " free disk space.")
        self.value = False
        self.type_exp = bool
        self.section = "sensors_data_interpolation"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_use_additional_features(generic_parameters):
    """ Init use_additional_features parameter"""

    def __init__(self):
        super().__init__()
        self.name = "use_additional_features"
        self.desc = ("Enable the use of additional features")
        self.value = False
        self.type_exp = bool
        self.section = "sensors_data_interpolation"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_use_gapfilling(generic_parameters):
    """ Init use_gapfilling  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "use_gapfilling"
        self.desc = ("Enable the use of gapfilling")
        self.value = True
        self.type_exp = bool
        self.section = "sensors_data_interpolation"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_proj(generic_parameters):
    """ Init proj parameter"""

    def __init__(self):
        super().__init__()
        self.name = "proj"
        self.desc = ("The projection wanted. Format EPSG:XXXX is mandatory")
        self.value = "EPSG:2154"
        self.type_exp = str
        self.section = "chain"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_vectorization": False,
            "i2_obia": False
        }


class init_copy_input(generic_parameters):
    """ Init copy_input parameter"""

    def __init__(self):
        super().__init__()
        self.name = "copy_input"
        self.desc = ("use spectral bands as features")
        self.value = True
        self.type_exp = bool
        self.section = "iota2_feature_extraction"
        self.builders = {"i2_classification": False}


class init_rel_refl(generic_parameters):
    """ Init rel_refl parameter"""

    def __init__(self):
        super().__init__()
        self.name = "rel_refl"
        self.desc = ("Compute relative reflectances by the red band")
        self.value = False
        self.type_exp = bool
        self.section = "iota2_feature_extraction"
        self.builders = {"i2_classification": False}


class init_keep_duplicates(generic_parameters):
    """ Init keep_duplicates parameter"""

    def __init__(self):
        super().__init__()
        self.name = "keep_duplicates"
        self.desc = (
            "use 'rel_refl' can generate duplicated feature "
            "(ie: NDVI), set to False remove these duplicated features")
        self.value = True
        self.type_exp = bool
        self.section = "iota2_feature_extraction"
        self.builders = {"i2_classification": False}


class init_extract_bands(generic_parameters):
    """ Init extract_bands parameter"""

    def __init__(self):
        super().__init__()
        self.name = "extract_bands"
        self.desc = ("")
        self.value = False
        self.type_exp = bool
        self.section = "iota2_feature_extraction"
        self.builders = {"i2_classification": False}


class init_acor_feat(generic_parameters):
    """ Init acor_feat parameter"""

    def __init__(self):
        super().__init__()
        self.name = "acor_feat"
        self.desc = ("Apply atmospherically corrected features")
        self.long_desc = ("Apply atmospherically corrected features"
                          "as explained at : "
                          "http://www.cesbio.ups-tlse.fr/multitemp/?p=12746")
        self.value = False
        self.type_exp = bool
        self.section = "iota2_feature_extraction"
        self.builders = {"i2_classification": False}


class init_dim_red(generic_parameters):
    """ Init dim_red parameter"""

    def __init__(self):
        super().__init__()
        self.name = "dim_red"
        self.desc = ("Enable the dimensionality reduction mode")
        self.value = False
        self.type_exp = bool
        self.section = "dim_red"
        self.builders = {"i2_classification": False}


class init_target_dimension(generic_parameters):
    """ Init target_dimension parameter"""

    def __init__(self):
        super().__init__()
        self.name = "target_dimension"
        self.desc = ("The number of dimension required, "
                     "according to `reduction_mode`")
        self.value = 4
        self.type_exp = int
        self.section = "dim_red"
        self.builders = {"i2_classification": False}


class init_reduction_mode(generic_parameters):
    """ Init reduction_mode parameter"""

    def __init__(self):
        super().__init__()
        self.name = "reduction_mode"
        self.desc = ("The reduction mode")
        self.long_desc = ("Values authorized are: 'global' or '?'")
        self.value = "global"
        self.type_exp = str
        self.section = "dim_red"
        self.builders = {"i2_classification": False}


class init_additional_features_l8(generic_parameters):
    """ Init  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "additional_features"
        self.desc = ("OTB's bandmath expressions, separated by comma")
        self.value = ""
        self.type_exp = str
        self.section = "Landsat8"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_temporal_resolution_l8(generic_parameters):
    """ Init temporal_resolution for l8 sensor parameter"""

    def __init__(self):
        super().__init__()
        self.name = "temporal_resolution"
        self.desc = ("The temporal gap between two interpolation")
        self.value = 16
        self.type_exp = int
        self.section = "Landsat8"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_write_reproject_stack_l8(generic_parameters):
    """ Init write_reproject_resampled_input_dates_stack  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "write_reproject_resampled_input_dates_stack"
        self.desc = ("Enable the write of resampled stack image for each date")
        self.value = True
        self.type_exp = bool
        self.section = "Landsat8"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_start_date_l8(generic_parameters):
    """ Init start_date_l8 parameter"""

    def __init__(self):
        super().__init__()
        self.name = "start_date"
        self.desc = ("The first date of interpolated image time series")
        self.value = ""
        self.type_exp = str
        self.section = "Landsat8"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_end_date_l8(generic_parameters):
    """ Init end_date_l8 parameter"""

    def __init__(self):
        super().__init__()
        self.name = "end_date"
        self.desc = ("The end date of interpolated image time series")
        self.value = ""
        self.type_exp = str
        self.section = "Landsat8"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_keep_bands_l8(generic_parameters):
    """ Init end_date_l8 parameter"""

    def __init__(self):
        super().__init__()
        self.name = "keep_bands"
        self.desc = ("The list of spectral bands used for classification")
        self.value = list_sequence_init(
            ["B1", "B2", "B3", "B4", "B5", "B6", "B7"])
        self.type_exp = list
        self.section = "Landsat8"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_additional_features_s2(generic_parameters):
    """ Init  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "additional_features"
        self.desc = ("OTB's bandmath expressions, separated by comma")
        self.value = ""
        self.type_exp = str
        self.section = "Sentinel_2"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_temporal_resolution_s2(generic_parameters):
    """ Init temporal_resolution for s2 sensor parameter"""

    def __init__(self):
        super().__init__()
        self.name = "temporal_resolution"
        self.desc = ("The temporal gap between two interpolation")
        self.value = 10
        self.type_exp = int
        self.section = "Sentinel_2"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_write_reproject_stack_s2(generic_parameters):
    """ Init write_reproject_resampled_input_dates_stack  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "write_reproject_resampled_input_dates_stack"
        self.desc = ("Enable the write of resampled stack image for each date")
        self.value = True
        self.type_exp = bool
        self.section = "Sentinel_2"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_start_date_s2(generic_parameters):
    """ Init start_date_s2 parameter"""

    def __init__(self):
        super().__init__()
        self.name = "start_date"
        self.desc = ("The first date of interpolated image time series")
        self.value = ""
        self.type_exp = str
        self.section = "Sentinel_2"
        self.builders = {"i2_classification": False, "i2_features_map": False}


class init_end_date_s2(generic_parameters):
    """ Init end_date_s2 parameter"""

    def __init__(self):
        super().__init__()
        self.name = "end_date"
        self.desc = ("The end date of interpolated image time series")
        self.value = ""
        self.type_exp = str
        self.section = "Sentinel_2"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_keep_bands_s2(generic_parameters):
    """ Init end_date_s2 parameter"""

    def __init__(self):
        super().__init__()
        self.name = "keep_bands"
        self.desc = ("The list of spectral bands used for classification")
        self.value = list_sequence_init(
            ["B2", "B3", "B4", "B5", "B6", "B7", "B8", "B8A", "B11", "B12"])
        self.type_exp = list
        self.section = "Sentinel_2"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


# Sentinel 2 S2C
class init_additional_features_s2c(generic_parameters):
    """ Init  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "additional_features"
        self.desc = ("OTB's bandmath expressions, separated by comma")
        self.value = ""
        self.type_exp = str
        self.section = "Sentinel_2_S2C"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_temporal_resolution_s2c(generic_parameters):
    """ Init temporal_resolution for s2c sensor parameter"""

    def __init__(self):
        super().__init__()
        self.name = "temporal_resolution"
        self.desc = ("The temporal gap between two interpolation")
        self.value = 10
        self.type_exp = int
        self.section = "Sentinel_2_S2C"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_write_reproject_stack_s2c(generic_parameters):
    """ Init write_reproject_resampled_input_dates_stack  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "write_reproject_resampled_input_dates_stack"
        self.desc = ("Enable the write of resampled stack image for each date")
        self.value = True
        self.type_exp = bool
        self.section = "Sentinel_2_S2C"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_start_date_s2c(generic_parameters):
    """ Init start_date_s2c parameter"""

    def __init__(self):
        super().__init__()
        self.name = "start_date"
        self.desc = ("The first date of interpolated image time series")
        self.value = ""
        self.type_exp = str
        self.section = "Sentinel_2_S2C"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_end_date_s2c(generic_parameters):
    """ Init end_date_s2c parameter"""

    def __init__(self):
        super().__init__()
        self.name = "end_date"
        self.desc = ("The end date of interpolated image time series")
        self.value = ""
        self.type_exp = str
        self.section = "Sentinel_2_S2C"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_keep_bands_s2c(generic_parameters):
    """ Init end_date_s2c parameter"""

    def __init__(self):
        super().__init__()
        self.name = "keep_bands"
        self.desc = ("The list of spectral bands used for classification")
        self.value = list_sequence_init([
            "B02", "B03", "B04", "B05", "B06", "B07", "B08", "B8A", "B11",
            "B12"
        ])
        self.type_exp = list
        self.section = "Sentinel_2_S2C"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


# Sentinel 2 L3A
class init_additional_features_s2_l3a(generic_parameters):
    """ Init  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "additional_features"
        self.desc = ("OTB's bandmath expressions, separated by comma")
        self.value = ""
        self.type_exp = str
        self.section = "Sentinel_2_L3A"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_temporal_resolution_s2_l3a(generic_parameters):
    """ Init temporal_resolution for s2_l3a sensor parameter"""

    def __init__(self):
        super().__init__()
        self.name = "temporal_resolution"
        self.desc = ("The temporal gap between two interpolation")
        self.value = 16
        self.type_exp = int
        self.section = "Sentinel_2_L3A"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_write_reproject_stack_s2_l3a(generic_parameters):
    """ Init write_reproject_resampled_input_dates_stack  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "write_reproject_resampled_input_dates_stack"
        self.desc = ("Enable the write of resampled stack image for each date")
        self.value = True
        self.type_exp = bool
        self.section = "Sentinel_2_L3A"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_start_date_s2_l3a(generic_parameters):
    """ Init start_date_s2_l3a parameter"""

    def __init__(self):
        super().__init__()
        self.name = "start_date"
        self.desc = ("The first date of interpolated image time series")
        self.value = ""
        self.type_exp = str
        self.section = "Sentinel_2_L3A"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_end_date_s2_l3a(generic_parameters):
    """ Init end_date_s2_l3a parameter"""

    def __init__(self):
        super().__init__()
        self.name = "end_date"
        self.desc = ("The end date of interpolated image time series")
        self.value = ""
        self.type_exp = str
        self.section = "Sentinel_2_L3A"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_keep_bands_s2_l3a(generic_parameters):
    """ Init end_date_s2_l3a parameter"""

    def __init__(self):
        super().__init__()
        self.name = "keep_bands"
        self.desc = ("The list of spectral bands used for classification")
        self.value = list_sequence_init(
            ["B2", "B3", "B4", "B5", "B6", "B7", "B8", "B8A", "B11", "B12"])
        self.type_exp = list
        self.section = "Sentinel_2_L3A"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


# Landsat 5 old
class init_additional_features_l5_old(generic_parameters):
    """ Init  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "additional_features"
        self.desc = ("OTB's bandmath expressions, separated by comma")
        self.value = ""
        self.type_exp = str
        self.section = "Landsat5_old"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_temporal_resolution_l5_old(generic_parameters):
    """ Init temporal_resolution for l5_old sensor parameter"""

    def __init__(self):
        super().__init__()
        self.name = "temporal_resolution"
        self.desc = ("The temporal gap between two interpolation")
        self.value = 16
        self.type_exp = int
        self.section = "Landsat5_old"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_start_date_l5_old(generic_parameters):
    """ Init start_date_l5_old parameter"""

    def __init__(self):
        super().__init__()
        self.name = "start_date"
        self.desc = ("The first date of interpolated image time series")
        self.value = ""
        self.type_exp = str
        self.section = "Landsat5_old"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_end_date_l5_old(generic_parameters):
    """ Init end_date_l5_old parameter"""

    def __init__(self):
        super().__init__()
        self.name = "end_date"
        self.desc = ("The end date of interpolated image time series")
        self.value = ""
        self.type_exp = str
        self.section = "Landsat5_old"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_keep_bands_l5_old(generic_parameters):
    """ Init end_date_l5_old parameter"""

    def __init__(self):
        super().__init__()
        self.name = "keep_bands"
        self.desc = ("The list of spectral bands used for classification")
        self.value = list_sequence_init(["B1", "B2", "B3", "B4", "B5", "B6"])
        self.type_exp = list
        self.section = "Landsat5_old"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


# landsat 8_old
class init_additional_features_l8_old(generic_parameters):
    """ Init  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "additional_features"
        self.desc = ("OTB's bandmath expressions, separated by comma")
        self.value = ""
        self.type_exp = str
        self.section = "Landsat8_old"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_temporal_resolution_l8_old(generic_parameters):
    """ Init temporal_resolution for l8_old sensor parameter"""

    def __init__(self):
        super().__init__()
        self.name = "temporal_resolution"
        self.desc = ("The temporal gap between two interpolation")
        self.value = 16
        self.type_exp = int
        self.section = "Landsat8_old"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_write_reproject_stack_l8_old(generic_parameters):
    """ Init write_reproject_resampled_input_dates_stack  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "write_reproject_resampled_input_dates_stack"
        self.desc = ("Enable the write of resampled stack image for each date")
        self.value = True
        self.type_exp = bool
        self.section = "Landsat8_old"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_start_date_l8_old(generic_parameters):
    """ Init start_date_l8_old parameter"""

    def __init__(self):
        super().__init__()
        self.name = "start_date"
        self.desc = ("The first date of interpolated image time series")
        self.value = ""
        self.type_exp = str
        self.section = "Landsat8_old"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_end_date_l8_old(generic_parameters):
    """ Init end_date_l8_old parameter"""

    def __init__(self):
        super().__init__()
        self.name = "end_date"
        self.desc = ("The end date of interpolated image time series")
        self.value = ""
        self.type_exp = str
        self.section = "Landsat8_old"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_keep_bands_l8_old(generic_parameters):
    """ Init end_date_l8_old parameter"""

    def __init__(self):
        super().__init__()
        self.name = "keep_bands"
        self.desc = ("The list of spectral bands used for classification")
        self.value = list_sequence_init(["B1", "B2", "B3", "B4", "B5", "B6"])
        self.type_exp = list
        self.section = "Landsat8_old"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_arbo(generic_parameters):
    """ Init arbo parameter"""

    def __init__(self):
        super().__init__()
        self.name = "arbo"
        self.desc = ("The input folder hierarchy")
        self.value = "/*"
        self.type_exp = str
        self.section = "userFeat"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }


class init_patterns(generic_parameters):
    """ Init patterns parameter"""

    def __init__(self):
        super().__init__()
        self.name = "patterns"
        self.desc = ("key name for detect the input images")
        self.value = "ALT,ASP,SLP"
        self.type_exp = str
        self.section = "userFeat"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_obia": False
        }

    def ensure_param_type(self, param):
        out_param = super(init_patterns, self).ensure_param_type(param)
        if " " in out_param:
            raise ValueError("error in the configuration file, "
                             f"{self.section}.{self.name} must not contains"
                             " whitespaces")
        return out_param


# simplification parameters
class init_classification(generic_parameters):
    """ Init classification parameter"""

    def __init__(self):
        super().__init__()
        self.name = "classification"
        self.desc = ("Input raster of classification")
        self.value = None
        self.type_exp = str
        self.section = "simplification"
        self.builders = {"i2_vectorization": True}
        self.long_desc = (
            "This parameter is automatically set if the configuration file"
            " use the classification and vectorization builders")


class init_confidence(generic_parameters):
    """ Init confidence parameter"""

    def __init__(self):
        super().__init__()
        self.desc = ("Input raster of confidence")
        self.name = "confidence"
        self.long_desc = (
            "This parameter is automatically set if the configuration file"
            " use the classification and vectorization builders")
        self.value = None
        self.type_exp = str
        self.section = "simplification"
        self.builders = {"i2_vectorization": True}


class init_validity(generic_parameters):
    """ Init validity parameter"""

    def __init__(self):
        super().__init__()
        self.name = "validity"
        self.desc = ("Input raster of validity")
        self.long_desc = (
            "This parameter is automatically set if the configuration file"
            " use the classification and vectorization builders")
        self.value = None
        self.type_exp = str
        self.section = "simplification"
        self.builders = {"i2_vectorization": True}


class init_seed(generic_parameters):
    """ Init seed parameter"""

    def __init__(self):
        super().__init__()
        self.name = "seed"
        self.desc = ("Seed of input raster classification")
        self.long_desc = ("This parameter is usefull to vectorize"
                          " one specific output "
                          "classification seed")
        self.value = 1
        self.type_exp = int
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_vectorize_fusion_of_classifications(generic_parameters):
    """ Init seed parameter"""

    def __init__(self):
        super().__init__()
        self.name = "vectorize_fusion_of_classifications"
        self.desc = (
            "flag to inform iota2 to vectorize the fusion of classifications")
        self.long_desc = ("This flag is only useful if the vectorization is "
                          "chained with classification workflow")
        self.value = False
        self.type_exp = bool
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_umc1(generic_parameters):
    """ Init  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "umc1"
        self.desc = ("MMU for first regularization")
        self.long_desc = (
            "It is an interface of parameter '-st' of gdal_sieve.py function."
            " If None, classification is not regularized")
        self.value = None
        self.type_exp = int
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_umc2(generic_parameters):
    """ Init umc2 parameter"""

    def __init__(self):
        super().__init__()
        self.name = "umc2"
        self.desc = ("MMU for second regularization")
        self.long_desc = (
            "OSO-like vectorization process requires 2 successive "
            "regularization, if you need a single regularization, "
            "let this parameter to None")
        self.value = None
        self.type_exp = int
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_inland(generic_parameters):
    """ Init inland parameter"""

    def __init__(self):
        super().__init__()
        self.name = "inland"
        self.desc = ("Inland water limit shapefile")
        self.long_desc = ("to vectorize only inland waters, "
                          "and not unnecessary sea water areas")
        self.value = None
        self.type_exp = str
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_rssize(generic_parameters):
    """ Init rssize  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "rssize"
        self.desc = (
            "Resampling size of input classification raster (projection unit)")
        self.long_desc = (
            "OSO-like vectorization requires a resampling step in order "
            "to regularize and decrease raster polygons number, "
            "If None, classification is not resampled")
        self.value = 20
        self.type_exp = int
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_lib64bit(generic_parameters):
    """ Init lib64bit parameter"""

    def __init__(self):
        super().__init__()
        self.name = "lib64bit"
        self.desc = ("Path of BandMath and Concatenate OTB executables "
                     "returning 64-bits float pixel values")
        self.long_desc = (
            "Band math and concatenate OTB executables with 64 bits "
            "capabilities (only for large areas where "
            "clumps number > 2²³ bits for mantisse)")
        self.value = None
        self.type_exp = str
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_gridsize(generic_parameters):
    """ Init gridsize parameter"""

    def __init__(self):
        super().__init__()
        self.name = "gridsize"
        self.desc = ("Number of lines and columns of serialization process")
        self.long_desc = (
            "This parameter is useful only for large areas for which "
            "vectorization process can not be executed (memory limitation). "
            "By 'serialization', we mean parallel vectorization processes. "
            "If not None, regularized classification raster is splitted "
            "in gridsize x gridsize rasters")
        self.value = None
        self.type_exp = int
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_grasslib(generic_parameters):
    """ Init grasslib parameter"""

    def __init__(self):
        super().__init__()
        self.name = "grasslib"
        self.desc = ("path to grasslib")
        self.long_desc = ("Some functions of GRASS GIS software are used to "
                          "vectorize, simplify and smooth vector layer. "
                          "This path corresponds to GRASS install folder")
        self.value = None
        self.type_exp = str
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_douglas(generic_parameters):
    """ Init douglas parameter"""

    def __init__(self):
        super().__init__()
        self.name = "douglas"
        self.desc = ("Douglas-Peucker tolerance for"
                     " vector-based generalization")
        self.value = 10
        self.type_exp = int
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_hermite(generic_parameters):
    """ Init hermite parameter"""

    def __init__(self):
        super().__init__()
        self.name = "hermite"
        self.desc = ("Hermite Interpolation threshold"
                     " for vector-based smoothing")
        self.value = 10
        self.type_exp = int
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_mmu(generic_parameters):
    """ Init mmu parameter"""

    def __init__(self):
        super().__init__()
        self.name = "mmu"
        self.desc = (
            "MMU of output vector-based classification (projection unit),"
            "(Default : 0.1 ha)")
        self.value = 1000
        self.type_exp = int
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_angle(generic_parameters):
    """ Init angle parameter"""

    def __init__(self):
        super().__init__()
        self.name = "angle"
        self.desc = ("If True, smoothing corners of pixels (45°)")
        self.value = True
        self.type_exp = bool
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_clipfile(generic_parameters):
    """ Init clipfile parameter"""

    def __init__(self):
        super().__init__()
        self.name = "clipfile"
        self.desc = (
            "vector-based file to clip output vector-based classification")
        self.long_desc = (
            "vector-based file can contain more than one feature "
            "(geographical/administrative areas). "
            "An output vector-based classification is produced "
            "for each feature (cf. 'clipfield' parameter).")
        self.value = None
        self.type_exp = str
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_clipfield(generic_parameters):
    """ Init clipfield  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "clipfield"
        self.desc = ("field to identify distinct areas")
        self.long_desc = (
            "field to identify distinct geographical/administrative"
            "areas (cf. \"clipfile\" parameter)")
        self.value = None
        self.type_exp = str
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_clipvalue(generic_parameters):
    """ Init clipvalue  parameter"""

    def __init__(self):
        super().__init__()
        self.name = "clipvalue"
        self.desc = ("value of field which identify distinct areas")
        self.long_desc = (
            "output vector-based classification is only produced on "
            "the specific area (clipfield=clipvalue in clipfile) "
            "(cf. 'clipfield' parameter). If None, all areas are produced.")
        self.value = None
        self.type_exp = int
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_outprefix(generic_parameters):
    """ Init outprefix parameter"""

    def __init__(self):
        super().__init__()
        self.name = "outprefix"
        self.desc = (
            "Prefix to use for naming of vector-based classifications")
        self.long_desc = (
            "Naming of vector-based classifications is as following : "
            "prefix_clipvalue")
        self.value = "dept"
        self.type_exp = str
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_lcfield(generic_parameters):
    """ Init lcfield parameter"""

    def __init__(self):
        super().__init__()
        self.name = "lcfield"
        self.desc = ("Name of the field to store landcover class"
                     " in vector-based classification")
        self.value = "Class"
        self.type_exp = str
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_blocksize(generic_parameters):
    """ Init blocksize parameter"""

    def __init__(self):
        super().__init__()
        self.name = "blocksize"
        self.desc = (
            "block size to split raster to prevent Numpy memory error")
        self.long_desc = ("Numpy memory error may occur for large areas "
                          "during serialization process. "
                          "Split in sub-rasters prevents memory error")
        self.value = 2000
        self.type_exp = int
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_dozip(generic_parameters):
    """ Init dozip parameter"""

    def __init__(self):
        super().__init__()
        self.name = "dozip"
        self.desc = (
            "Zip output vector-based classification (OSO-like production)")
        self.value = True
        self.type_exp = bool
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_zonal_vector(generic_parameters):
    """ Init zonal_vector parameter"""

    def __init__(self):
        super(init_zonal_vector, self).__init__()
        self.name = "zonal_vector"
        self.desc = ("vector file to compute zonal statistics "
                     "of classification")
        self.long_desc = ("Compute the zonal statistics on the geometries of"
                          "a user-provided vector file. Zonal statistics are"
                          "computed on classification, confidence and raster"
                          "input rasters")
        self.value = None
        self.type_exp = str
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}
        self.mandatory = ["no"]


class init_bingdal(generic_parameters):
    """ Init bingdal parameter"""

    def __init__(self):
        super().__init__()
        self.name = "bingdal"
        self.desc = ("path to GDAL binaries")
        self.long_desc = (
            "Some GDAL lib versions (automatically set up with iota2) "
            "are not efficient to handle topology errors, use yours !")
        self.value = None
        self.type_exp = str
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_chunk(generic_parameters):
    """ Init chunk parameter"""

    def __init__(self):
        super().__init__()
        self.name = "chunk"
        self.desc = ("Number of chunks for statistics computing")
        self.long_desc = ("Number of chunks (groups of vector-based features) "
                          "for parallel computing landcover statistics")
        self.value = 10
        self.type_exp = int
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_higher_stats(generic_parameters):
    """ Init higher_stats parameter"""

    def __init__(self):
        super(init_higher_stats, self).__init__()
        self.name = "higher_stats"
        self.desc = (
            "If True, compute more complexe statistics (Shanon, majority "
            "order and difference, etc.)")
        self.value = False
        self.type_exp = bool
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}
        self.mandatory = ["no"]


class init_systemcall(generic_parameters):
    """ Init systemcall parameter"""

    def __init__(self):
        super().__init__()
        self.name = "systemcall"
        self.desc = ("If True, use yours gdal lib (cf. bingdal)")
        self.value = False
        self.type_exp = bool
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_prod(generic_parameters):
    """ Init prod parameter"""

    def __init__(self):
        super(init_prod, self).__init__()
        self.name = "prod"
        self.desc = (
            "OSO-like output vector (aliases) is produced. Other possible value : carhab"
        )
        self.value = None
        self.type_exp = str
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}
        self.mandatory = ["no"]


class init_nomenclature(generic_parameters):
    """ Init nomenclature parameter"""

    def __init__(self):
        super().__init__()
        self.name = "nomenclature"
        self.desc = ("configuration file which describe nomenclature")
        self.long_desc = (
            "This configuration file includes code, color, description "
            "and vector field alias of each class\n\n"
            ".. code-block:: bash\n\n"
            "    Classes:\n"
            "    {\n"
            "            Level1:\n"
            "            {\n"
            "                    \"Urbain\":\n"
            "                    {\n"
            "                    code:100\n"
            "                    alias:\"Urbain\"\n"
            "                    color:\"#b106b1\"\n"
            "                    }\n"
            "                    ...\n"
            "            }\n"
            "            Level2:\n"
            "            {\n"
            "                   \"Urbain dense\":\n"
            "                   {\n"
            "                   code:1\n"
            "                   alias:\"UrbainDens\"\n"
            "                   color:\"#ff00ff\"\n"
            "                   parent:100\n"
            "                   }\n"
            "                   ...\n"
            "            }\n"
            "    }\n")
        self.value = None
        self.type_exp = str
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


class init_statslist(generic_parameters):
    """ Init statlist parameter"""

    def __init__(self):
        super().__init__()
        self.name = "statslist"
        self.desc = ("dictionnary of requested landcover statistics")
        self.long_desc = (
            "Different landcover statistics can be computed for vector-based "
            "classification file.\nA python-like dictionnary must be provided.\n"
            "This is the OSO-like statistics :"
            "{1: \"rate\", 2: \"statsmaj\", 3: \"statsmaj\"}\n\n"
            "Where:\n"
            "     - {1: \"rate\"} : "
            "     rates of classification classes are computed "
            "     for each polygon\n\n"
            "     - {2: \"statsmaj\"} : "
            "     descriptive stats of classifier confidence are computed "
            "     for each polygon by using only majority class pixels\n\n"
            "     - {3: \"statsmaj\"} : "
            "     descriptive stats of sensor validity are computed "
            "     for each polygon by using only majority class pixels\n\n"
            "\n"
            "list of available statistics : \n"
            "    - stats : mean_b, std_b, max_b, min_b\n"
            "    - statsmaj : meanmaj, stdmaj, maxmaj, minmaj of maj. class\n"
            "    - rate : rate of each pixel value (classe names)\n"
            "    - stats_cl : mean_cl, std_cl, max_cl, min_cl of one class\n")
        self.value = {1: "rate", 2: "statsmaj", 3: "statsmaj"}
        self.type_exp = dict
        self.section = "simplification"
        self.builders = {"i2_vectorization": False}


# External features
class init_external_features_flag(generic_parameters):
    """ Init external_features_flag paramter """

    def __init__(self):
        super().__init__()
        self.name = "external_features_flag"
        self.desc = ("Enable the external features mode")
        self.value = False
        self.type_exp = bool
        self.section = "external_features"
        self.builders = {"i2_classification": False, "i2_features_map": False}


class init_module(generic_parameters):
    """ Init module parameter"""

    def __init__(self):
        super().__init__()
        self.name = "module"
        self.desc = ("Absolute path for user source code")
        self.value = os.path.join(get_iota2_project_dir(), "iota2", "common",
                                  "external_code.py")
        self.type_exp = str
        self.section = "external_features"
        self.builders = {"i2_classification": False, "i2_features_map": False}


def convert_functions(param) -> List[FunctionNameWithParams]:
    """takes a config external_features "functions" parameter and converts it
    to unified representation. raises a ValueError if type does not fit
    param: config param for external_features functions parameter"""
    out_param = []  # list of tuples
    if not param:  # empty string or list
        return out_param
    if isinstance(param, str):
        # param is a string with space-separated function names
        out_param = [(function_name, {}) for function_name in param.split(" ")]
    elif isinstance(param, Sequence):  # functions with parameters
        for elt in param:
            if isinstance(elt, str):  # only function name (no parameters)
                out_param.append((elt, {}))
            elif isinstance(elt, Sequence):  # function name plus parameters
                fn_name = ""
                kwargs = Mapping()
                if len(elt) == 0:
                    raise ValueError("please provide at least function name")
                if len(elt) >= 1:
                    fn_name = elt[0]
                if len(elt) == 2:
                    kwargs = elt[1]
                if len(elt) > 2:
                    raise ValueError("only function name and parameters"
                                     f"allowed, found {elt}")
                if not isinstance(fn_name, str):
                    raise ValueError("expected function name to be a string"
                                     f"not {fn_name}")
                if not isinstance(kwargs, Mapping):
                    raise ValueError("expected kwargs to be a Mapping"
                                     f"not {kwargs}")
                out_param.append((fn_name, dict(kwargs)))
            else:
                raise ValueError(f"invalid element {elt},"
                                 "expecting function name and kwargs")
    else:
        raise ValueError(f"invalid parameter functions {param}")
    return out_param


class init_functions(generic_parameters):
    """Init functions parameter"""

    def __init__(self):
        super().__init__()
        self.name = "functions"
        self.desc = ("The function list to be used to compute features."
                     "Can be a string of space-separated function names"
                     "Can be a list of either strings of function name"
                     "or lists of one function name and one argument mapping")
        self.value = None
        self.type_exp = list  # Any  # can be either a string or a list
        self.section = "external_features"
        self.builders = {"i2_classification": False, "i2_features_map": False}

    def ensure_param_type(self, param):
        """Overwrites parent type ensuring method to return formated result"""
        return convert_functions(param)


class init_number_of_chunks(generic_parameters):
    """ Init number_of_chunks parameter"""

    def __init__(self):
        super().__init__()
        self.name = "number_of_chunks"
        self.desc = ("The expected number of chunks")
        self.value = 50
        self.type_exp = int
        self.section = "python_data_managing"
        self.builders = {"i2_classification": False, "i2_features_map": False}


class init_max_nn_inference_size(generic_parameters):
    """ Init number_of_chunks parameter"""

    def __init__(self):
        super(init_max_nn_inference_size, self).__init__()
        self.name = "max_nn_inference_size"
        self.desc = ("maximum batch inference size")
        self.long_desc = (
            "Involved if a neural network inference is performed. "
            "If not set (None), the inference size will be the same"
            " as the one used during the learning stage")
        self.value = None
        self.type_exp = int
        self.section = "python_data_managing"
        self.builders = {"i2_classification": False}


class init_chunk_size_x(generic_parameters):
    """ Init chunk_size_x parameter"""

    def __init__(self):
        super().__init__()
        self.name = "chunk_size_x"
        self.desc = ("The number if rows for chunk")
        self.value = 50
        self.type_exp = int
        self.section = "python_data_managing"
        self.builders = {"i2_classification": False, "i2_features_map": False}


class init_chunk_size_y(generic_parameters):
    """ Init chunk_size_y parameter"""

    def __init__(self):
        super().__init__()
        self.name = "chunk_size_y"
        self.desc = ("The number if rows for chunk")
        self.value = 50
        self.type_exp = int
        self.section = "python_data_managing"
        self.builders = {"i2_classification": False, "i2_features_map": False}


class init_chunk_size_mode(generic_parameters):
    """ Init chunk_size_mode parameter"""

    def __init__(self):
        super().__init__()
        self.name = "chunk_size_mode"
        self.desc = (
            "The chunk split mode, choices are 'split_number' or 'user_fixed'")
        self.value = "split_number"
        self.type_exp = str
        self.section = "python_data_managing"
        self.builders = {"i2_classification": False, "i2_features_map": False}

    def ensure_param_type(self, param):
        param = super().ensure_param_type(param)
        if param != self.value:
            raise ValueError((f"Currently, {self.section}.{self.name} "
                              "must be equal to '{self.value}'"))
        return param


class init_concat_mode(generic_parameters):
    """ Init concat_mode parameter"""

    def __init__(self):
        super().__init__()
        self.name = "concat_mode"
        self.desc = ("Enable the use of all features")
        self.long_desc = ("If disabled, only external features are"
                          " used in the whole processing")
        self.value = True
        self.type_exp = bool
        self.section = "external_features"
        self.builders = {"i2_classification": False, "i2_features_map": False}


class init_output_name(generic_parameters):
    """ Init output_name parameter"""

    def __init__(self):
        super().__init__()
        self.name = "output_name"
        self.desc = ("Temporary chunks are written using this name as prefix")
        self.value = None
        self.type_exp = str
        self.section = "external_features"
        self.builders = {"i2_classification": False, "i2_features_map": False}


class init_no_data_value(generic_parameters):
    """ Init no_data_value parameter"""

    def __init__(self):
        super().__init__()
        self.name = "no_data_value"
        self.desc = (
            "value considered as no_data in features map mosaic (i2_features_map builder name)"
        )
        self.value = -10000
        self.type_exp = int
        self.section = "external_features"
        self.builders = {"i2_features_map": False}


# used to transform exogenous data parameter
def exogenous_data_tile_cfg(cfg: str, tilename: str):
    """reads config file and calls exogenous_data_tile"""
    # get tif filename from config file
    exogenous_data_tif = rcf.read_config_file(cfg).getParam(
        "external_features", "exogeneous_data")
    return exogenous_data_tile(exogenous_data_tif, tilename)


def exogenous_data_tile(exogenous_data_tif: str, tilename: str):
    """interpolates tile in exogenous data path if possible"""
    if not exogenous_data_tif:
        # allow null value
        return exogenous_data_tif
    else:
        # if string exists, replace $TILE by actual tile name
        return exogenous_data_tif.replace("$TILE", tilename)


class init_exogeneous_data(generic_parameters):
    """ Init exogeneous_data parameter"""

    def __init__(self):
        super().__init__()
        self.name = "exogeneous_data"
        self.desc = ("Path to a Geotiff file containing additional data"
                     " to be used in external features.")
        self.long_desc = (
            "If the =exogeneous_data= contains '$TILE', it will be replaced by the tile name being processed."
            "If you want to reproject your data on given tiles, you can use the =split_raster_into_tiles.py= command line tool.\n\n"
            "Usage: =split_raster_into_tiles.py --help=.")
        self.value = None
        self.type_exp = str
        self.section = "external_features"
        self.builders = {"i2_classification": False, "i2_features_map": False}


class init_data_mode_access(generic_parameters):
    """ Init output_name parameter"""

    def __init__(self):
        super().__init__()
        self.name = "data_mode_access"
        self.desc = ("Choose which data can be accessed in custom features")
        self.long_desc = (
            "Three values are allowed:\n"
            "- gapfilled: give access only the gapfilled data\n"
            "- raw: gives access only the original raw data\n"
            "- both: provides access to both data\n"
            "..Notes:: Data are spatialy resampled, these parameters"
            " concern only temporal interpolation")
        self.value = "gapfilled"
        self.type_exp = str
        self.section = "python_data_managing"
        self.builders = {"i2_classification": False, "i2_features_map": False}

    def ensure_param_type(self, param):
        tmp_param = super().ensure_param_type(param)
        if tmp_param == "raw":
            out_param = (False, True)
        elif tmp_param == "gapfilled":
            out_param = (True, False)
        elif tmp_param == "both":
            out_param = (True, True)
        else:
            raise ValueError(f" {tmp_param} is not a correct value for "
                             "parameter data_mode_access")
        return out_param


class init_fill_missing_dates(generic_parameters):

    def __init__(self):
        super().__init__()
        self.name = "fill_missing_dates"
        self.desc = ("Fill raw data with no data if dates are missing")
        self.long_desc = (
            "If raw data access is enabled, this option "
            "considers all unique dates for all tiles and identify"
            " which dates are missing for each tile."
            " A missing date is filled using a no data constant value."
            "Cloud or saturation are not corrected, but masks are provided"
            " Masks contain three value: 0 for valid data, 1 for cloudy "
            "or saturated pixels, 2 for a missing date")
        self.value = False
        self.type_exp = bool
        self.section = "python_data_managing"
        self.builders = {"i2_classification": False, "i2_features_map": False}


class init_builders_paths(generic_parameters):
    """ Init builder_file_path parameter"""

    def __init__(self):
        super().__init__()
        self.name = "builders_paths"
        self.desc = ("The path to user builders")
        self.long_desc = ("If not indicated, the iota2 source directory"
                          r" is used: \*/iota2/sequence_builders/")
        self.value = None
        self.type_exp = list
        self.section = "builders"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_vectorization": False,
            "i2_obia": False
        }


class init_builders_class_name(generic_parameters):
    """ Init builder_class_name parameter """

    def __init__(self):
        super().__init__()
        self.name = "builders_class_name"
        self.desc = ("The name of the class defining the builder")
        self.value = ["i2_classification"]
        self.type_exp = list
        self.section = "builders"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_vectorization": False,
            "i2_obia": False
        }

        self.long_desc = (
            "Available builders are : 'i2_classification', "
            "'i2_features_map', 'i2_obia' and 'i2_vectorization'")


class init_allowed_retry(generic_parameters):
    """ Init allowed_retry parameter """

    def __init__(self):
        super().__init__()
        self.name = "allowed_retry"
        self.desc = ("Allow dask to retry a failed job N times.")
        self.value = 0
        self.type_exp = int
        self.section = "task_retry_limits"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_vectorization": False,
            "i2_obia": False
        }


class init_maximum_ram(generic_parameters):
    """ Init maximum_ram parameter """

    def __init__(self):
        super().__init__()
        self.name = "maximum_ram"
        self.desc = ("The maximum amount of RAM available. (gB)")
        self.value = 16
        self.type_exp = int
        self.section = "task_retry_limits"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_vectorization": False,
            "i2_obia": False
        }
        self.long_desc = ("the amount of RAM will be doubled if the task"
                          " is killed due to ram overconsumption "
                          "until maximum_ram or allowed_retry are reach")


class init_maximum_cpu(generic_parameters):
    """ Init maximum_cpu parameter """

    def __init__(self):
        super().__init__()
        self.name = "maximum_cpu"
        self.desc = ("The maximum number of CPU available")
        self.value = 4
        self.type_exp = int
        self.section = "task_retry_limits"
        self.builders = {
            "i2_classification": False,
            "i2_features_map": False,
            "i2_vectorization": False,
            "i2_obia": False
        }

        self.long_desc = ("the amount of cpu will be doubled if the task "
                          "is killed due to ram overconsumption "
                          "until maximum_cpu or allowed_retry are reach")


class init_obia_segmentation_path(generic_parameters):
    """ Init obia segmentation path parameters """

    def __init__(self):
        super().__init__()
        self.name = "obia_segmentation_path"
        self.desc = "Filename for input segmentation"
        self.long_desc = ("If parameter is None then a segmentation for "
                          "each tile is processed using SLIC algorithm")
        self.value = None
        self.type_exp = str
        self.section = "obia"
        self.builders = {"i2_obia": False}


class init_buffer_size(generic_parameters):
    """ Init buffer_size parameters """

    def __init__(self):
        super().__init__()
        self.name = "buffer_size"
        self.desc = "Define the working size batch in number of pixels"
        self.long_desc = (
            "This parameter is used to avoid memory issue."
            "In case of a large temporal series,i.e one year of "
            "Sentinel2 images a recommended size is 2000."
            "For lower number of date, the buffer size can be increased."
            "If buffer_size is larger than the image size, the whole "
            "image will be processed in one time.")
        self.value = None
        self.type_exp = int
        self.section = "obia"
        self.builders = {"i2_obia": True}


class init_region_priority(generic_parameters):
    """ Init buffer_size parameters """

    def __init__(self):
        super().__init__()
        self.name = "region_priority"
        self.desc = "Define a order for region intersection"
        self.long_desc = (
            "If a list is provided, the list order is used instead"
            " of the numeric order."
            "This option can be used in case of very unbalanced region size.")
        self.value = None
        self.type_exp = list
        self.section = "obia"
        self.builders = {"i2_obia": False}


class init_full_learn_segment(generic_parameters):
    """ Init full_learn_segment parameters """

    def __init__(self):
        super().__init__()
        self.name = "full_learn_segment"
        self.desc = "Enable the use of entire segment for learning"
        self.long_desc = (
            "If True: keep each segment which intersect the learning"
            " samples. If False, the segments are clipped with learning"
            " polygon shape")
        self.value = False
        self.type_exp = bool
        self.section = "obia"
        self.builders = {"i2_obia": False}


class init_stats_used(generic_parameters):
    """ Init stats_used parameters """

    def __init__(self):
        super().__init__()
        self.name = "stats_used"
        self.desc = "List of stats used for train and classification"
        self.long_desc = (
            "This list accepts only five values:"
            " mean, count, min, max, std\n"
            "The choice of statistics used should be considered in "
            "relation to the number of dates used."
            "Because of the constraints on vector formats, one must think"
            " about the number of features this creates:"
            " nb_stats_choosen * nb_bands * nb_dates. "
            "Too many spectral bands can cause an"
            " error in the execution of the string.")
        self.value = ["mean"]
        self.type_exp = list
        self.section = "obia"
        self.builders = {"i2_obia": False}


class init_padding_size_x(generic_parameters):
    """ Init padding_size_x parameter"""

    def __init__(self):
        super().__init__()
        self.name = "padding_size_x"
        self.desc = ("The padding for chunk")
        self.value = 0
        self.type_exp = int
        self.section = "python_data_managing"
        self.builders = {"i2_classification": False, "i2_features_map": False}


class init_padding_size_y(generic_parameters):
    """ Init padding_size_y parameter"""

    def __init__(self):
        super().__init__()
        self.name = "padding_size_y"
        self.desc = ("The padding for chunk")
        self.value = 0
        self.type_exp = int
        self.section = "python_data_managing"
        self.builders = {"i2_classification": False, "i2_features_map": False}
