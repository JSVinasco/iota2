#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import shutil
from collections import OrderedDict
from functools import partial
from typing import List, Optional

from osgeo import ogr

import iota2.common.i2_constants as i2_const
from iota2.common import service_error as sErr
from iota2.common.file_utils import file_search_and, sort_by_first_elem
from iota2.common.iota2_directory import generate_directories_obia
from iota2.common.utils import run
from iota2.configuration_files import check_config_parameters as ccp
from iota2.configuration_files import read_config_file as rcf
from iota2.configuration_files.sections.cfg_utils import ConfigError
from iota2.sequence_builders.i2_sequence_builder import (
    check_databases_consistency,
    check_input_text_files,
    check_sensors_data,
    i2_builder,
)
from iota2.steps.iota2_step import Step, StepContainer
from iota2.vector_tools.vector_functions import (
    get_field_element,
    get_fields,
    get_layer_name,
    get_re_encoding_labels_dic,
)

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.Iota2Constants()


class i2_obia(i2_builder):
    """
    class use to describe steps sequence and variable to use at
    each step (config)
    """

    def __init__(
        self,
        cfg: str,
        config_resources: str,
        schduler_type: str,
        restart: bool = False,
        tasks_states_file: Optional[str] = None,
        hpc_working_directory: str = "TMPDIR",
    ):
        super().__init__(
            cfg,
            config_resources,
            schduler_type,
            restart,
            tasks_states_file,
            hpc_working_directory,
        )

        self.config_resources = config_resources
        # create dict for control variables
        self.control_var = {}
        # steps definitions
        # self.steps_group = OrderedDict()

        self.steps_group["init"] = OrderedDict()
        self.steps_group["sampling"] = OrderedDict()
        self.steps_group["dimred"] = OrderedDict()
        self.steps_group["learning"] = OrderedDict()
        self.steps_group["classification"] = OrderedDict()
        self.steps_group["mosaic"] = OrderedDict()
        self.steps_group["validation"] = OrderedDict()
        self.steps_group["regularisation"] = OrderedDict()
        self.steps_group["crown"] = OrderedDict()
        self.steps_group["mosaictiles"] = OrderedDict()
        self.steps_group["vectorisation"] = OrderedDict()
        self.steps_group["simplification"] = OrderedDict()
        self.steps_group["smoothing"] = OrderedDict()
        self.steps_group["clipvectors"] = OrderedDict()
        self.steps_group["lcstatistics"] = OrderedDict()

        # build steps
        self.sort_step()

    def get_dir(self):
        """
        usage : return iota2_directories
        """
        directories = [
            "classif",
            "config_model",
            "dataRegion",
            "envelope",
            "formattingVectors",
            "metaData",
            "samplesSelection",
            "stats",
            "dataAppVal",
            "dimRed",
            "final",
            "learningSamples",
            "model",
            "shapeRegion",
            "features",
        ]

        iota2_outputs_dir = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path"
        )

        return [os.path.join(iota2_outputs_dir, d) for d in directories]

    def models_distribution(self):
        """function to determine which models intersect which tiles
        by using intermediate iota2 results
        """
        formatting_dir = os.path.join(self.output_i2_directory, "formattingVectors")
        formatting_files = file_search_and(formatting_dir, True, ".shp")
        tiles = []
        regions_tiles = []
        for formatting_file in formatting_files:
            tile_name = os.path.splitext(os.path.basename(formatting_file))[0]
            regions_tile = get_field_element(
                formatting_file, field=self.region_field, mode="unique", elem_type="str"
            )
            if tile_name not in tiles:
                tiles.append(tile_name)
            for region in regions_tile:
                regions_tiles.append((region, tile_name))
        regions = sort_by_first_elem(regions_tiles)
        regions_distrib = {}
        for region_name, region_tiles in dict(regions).items():
            regions_distrib[region_name] = {"tiles": region_tiles}
        if not tiles:
            raise ValueError(
                (
                    "The chain stopped prematurely. Please check logs in"
                    f"'{self.log_dir}'"
                )
            )
        Step.set_models_spatial_information(tiles, regions_distrib)

        # manage tiles to classify
        shape_region_dir = os.path.join(self.output_i2_directory, "shapeRegion")
        region_tiles_files = file_search_and(shape_region_dir, False, ".shp")
        regions_to_classify = []
        for region_tile in region_tiles_files:
            region = region_tile.split("_")[-2]
            tile = region_tile.split("_")[-1]
            regions_to_classify.append((region, tile))
        regions_to_classify = sort_by_first_elem(regions_to_classify)
        dico_regions_to_classify = {}
        for region, tiles in regions_to_classify:
            dico_regions_to_classify[region] = {"tiles": tiles}

        regions_classif_distrib = {}

        for region_name, dico_tiles in regions_distrib.items():
            regions_classif_distrib[region_name] = {
                "tiles": dico_regions_to_classify[region_name.split("f")[0]]["tiles"]
            }
            if "nb_sub_model" in dico_tiles:
                regions_classif_distrib[region_name]["nb_sub_model"] = dico_tiles[
                    "nb_sub_model"
                ]

        regions_classif_distrib_no_split = {}
        for (
            region,
            region_meta,
        ) in Step.spatial_models_distribution_no_sub_splits.copy().items():
            regions_classif_distrib_no_split[region] = {
                "nb_sub_model": region_meta["nb_sub_model"],
                "tiles": dico_regions_to_classify[region]["tiles"].copy(),
            }
        Step.spatial_models_distribution_classify = regions_classif_distrib
        Step.spatial_models_distribution_no_sub_splits_classify = (
            regions_classif_distrib_no_split
        )

    def formatting_reference_data(self):
        """ """
        ref_data = self.control_var["ground_truth"]
        ref_data_field = self.control_var["data_field"]
        ref_data_i2_formatted = os.path.join(
            self.control_var["iota2_outputs_dir"], "reference_data.shp"
        )
        self.ref_data_formatting(ref_data, ref_data_field, ref_data_i2_formatted)

    def ref_data_formatting(
        self, ref_data: str, ref_data_field: str, ref_data_i2_formatted: str
    ) -> None:
        """re-encode reference data

        Parameters
        ----------
        ref_data
            input reference database
        ref_data_field
            column containing labels database
        ref_data_i2_formatted
            output formatted database
        """
        i2_label_column = I2_CONST.re_encoding_label_name
        ref_data_fields = get_fields(ref_data)
        if i2_label_column in ref_data_fields:
            raise ValueError(
                f"the reference data : {ref_data} contains the field"
                f" {i2_label_column}. Please rename the column name"
            )
        lut_labels = get_re_encoding_labels_dic(ref_data, ref_data_field)

        user_fields = get_fields(ref_data)
        layer_name = get_layer_name(ref_data)
        lower_clause = ",".join(
            [f"{user_field} AS {user_field.lower()}" for user_field in user_fields]
        )
        run(
            (
                f"ogr2ogr {ref_data_i2_formatted} {ref_data} "
                f"-sql 'SELECT {lower_clause} FROM {layer_name}'"
            )
        )

        ds = ogr.Open(ref_data_i2_formatted)
        driver = ds.GetDriver()
        data_src = driver.Open(ref_data_i2_formatted, 1)
        layer = data_src.GetLayer()
        layer.CreateField(ogr.FieldDefn(i2_label_column, ogr.OFTInteger))
        for feat in layer:
            feat.SetField(i2_label_column, lut_labels[feat.GetField(ref_data_field)])
            layer.SetFeature(feat)

    def build_steps(
        self, cfg: str, config_ressources: Optional[str] = None
    ) -> List[StepContainer]:
        """
        build steps
        """
        Step.set_models_spatial_information(self.tiles, {})

        # control variable
        self.init_dict_control_variables()

        # will contains all IOTA² steps
        s_container_pre_processing = StepContainer(name="preprocessing")
        s_container_pre_processing.prelaunch_function = partial(
            self.formatting_reference_data
        )
        s_container_init = StepContainer(name="classifications_init")

        s_container_spatial_distrib = StepContainer(
            name="classifications_spatial_distrib"
        )
        s_container_spatial_distrib.prelaunch_function = partial(
            self.models_distribution
        )

        # build chain
        # init steps
        self.init_step_group(s_container_pre_processing, s_container_init)
        # sampling steps
        self.sampling_step_group(s_container_init, s_container_spatial_distrib)
        # learning steps
        self.learning_step_group(s_container_spatial_distrib)

        # # classifications
        self.classification_step_group(s_container_spatial_distrib)

        # # mosaic step
        # self.mosaic_step_group(s_container_spatial_distrib)

        # validation steps
        self.validation_steps_group(s_container_spatial_distrib)
        return [
            s_container_pre_processing,
            s_container_init,
            s_container_spatial_distrib,
        ]

    def pre_check(self) -> None:
        """Perform some checks on data provided by the user."""
        params = rcf.iota2_parameters(self.i2_cfg_params)
        check_sensors_data(
            params,
            self.__class__.__name__,
            self.i2_cfg_params.getParam("chain", "minimum_required_dates"),
            self.i2_cfg_params.getParam("sensors_data_interpolation", "use_gapfilling"),
            self.i2_cfg_params.getParam("arg_train", "features_from_raw_dates"),
        )
        check_databases_consistency(
            self.i2_cfg_params.getParam("chain", "ground_truth"),
            self.i2_cfg_params.getParam("chain", "data_field"),
            self.i2_cfg_params.getParam("chain", "region_path"),
            self.i2_cfg_params.getParam("chain", "region_field"),
        )
        check_input_text_files(
            self.i2_cfg_params.getParam("chain", "ground_truth"),
            self.i2_cfg_params.getParam("chain", "data_field"),
            self.i2_cfg_params.getParam("chain", "nomenclature_path"),
            self.i2_cfg_params.getParam("chain", "color_table"),
        )
        self.check_config_parameters()

    def check_config_parameters(self):
        """Check parameters consistency across all sections."""
        try:
            # ensure that all tiles required are in folder
            tiles = self.i2_cfg_params.getParam("chain", "list_tile").split(" ")
            path_to_test = [
                self.i2_cfg_params.getParam("chain", "s2_path"),
                self.i2_cfg_params.getParam("chain", "s2_l3a_path"),
                self.i2_cfg_params.getParam("chain", "l8_path"),
                self.i2_cfg_params.getParam("chain", "l5_path_old"),
                self.i2_cfg_params.getParam("chain", "l8_path_old"),
                self.i2_cfg_params.getParam("chain", "s2_s2c_path"),
            ]

            for path in path_to_test:
                if path is not None:
                    for tile in tiles:
                        ccp.test_ifexists(os.path.join(path, tile))
        except sErr.configFileError as err:
            raise ConfigError(err)

        # parameters compatibilities check
        classier_probamap_avail = ["sharkrf"]
        if (
            self.i2_cfg_params.getParam("arg_classification", "enable_probability_map")
            is True
            and self.i2_cfg_params.getParam("arg_train", "classifier").lower()
            not in classier_probamap_avail
        ):
            raise ConfigError(
                "'enable_probability_map:True' only available with "
                "the 'sharkrf' classifier"
            )
        if (
            self.i2_cfg_params.getParam("chain", "region_path") is None
            and self.i2_cfg_params.getParam("arg_classification", "classif_mode")
            == "fusion"
        ):
            raise ConfigError(
                "you can't chose 'one_region' mode and ask a fusion of "
                "classifications\n"
            )
        if (
            self.i2_cfg_params.getParam(
                "arg_classification", "merge_final_classifications"
            )
            and self.i2_cfg_params.getParam("arg_train", "runs") == 1
        ):
            raise ConfigError(
                "these parameters are incompatible runs:1 and"
                " merge_final_classifications:True"
            )
        if (
            self.i2_cfg_params.getParam("arg_train", "split_ground_truth") is False
            and self.i2_cfg_params.getParam("arg_train", "runs") != 1
        ):
            raise ConfigError(
                "these parameters are incompatible split_ground_truth : False and "
                "runs different from 1"
            )
        if (
            self.i2_cfg_params.getParam(
                "arg_classification", "merge_final_classifications"
            )
            and self.i2_cfg_params.getParam("arg_train", "split_ground_truth") is False
        ):
            raise ConfigError(
                "these parameters are incompatible merge_final_classifications"
                ":True and split_ground_truth : False"
            )
        if (
            self.i2_cfg_params.getParam("arg_train", "dempster_shafer_sar_opt_fusion")
            and self.i2_cfg_params.getParam("chain", "s1_path") is None
        ):
            raise ConfigError(
                "these parameters are incompatible dempster_shafer_SAR"
                "_Opt_fusion : True and s1_path : None"
            )
        if (
            self.i2_cfg_params.getParam("arg_train", "dempster_shafer_sar_opt_fusion")
            and self.i2_cfg_params.getParam("chain", "user_feat_path") is None
            and self.i2_cfg_params.getParam("chain", "l5_path_old") is None
            and self.i2_cfg_params.getParam("chain", "l8_path") is None
            and self.i2_cfg_params.getParam("chain", "l8_path_old") is None
            and self.i2_cfg_params.getParam("chain", "s2_path") is None
            and self.i2_cfg_params.getParam("chain", "s2_s2c_path") is None
        ):
            raise ConfigError(
                "to perform post-classification fusion, optical data must be " "used"
            )
        if (
            self.i2_cfg_params.getParam("scikit_models_parameters", "model_type")
            is not None
            and self.i2_cfg_params.getParam("arg_train", "enable_autocontext") is True
        ):
            raise ConfigError(
                "these parameters are incompatible enable_autocontext : True"
                " and model_type"
            )
        if self.i2_cfg_params.getParam(
            "scikit_models_parameters", "model_type"
        ) is not None and self.i2_cfg_params.getParam(
            "external_features", "external_features_flag"
        ):
            raise ConfigError(
                "these parameters are incompatible external_features "
                "and scikit_models_parameters"
            )
        if self.i2_cfg_params.getParam(
            "external_features", "external_features_flag"
        ) and self.i2_cfg_params.getParam("arg_train", "enable_autocontext"):
            raise ConfigError(
                "these parameters are incompatible external_features "
                "and enable_autocontext"
            )

        return True

    def init_dict_control_variables(self):
        # control variable
        self.control_var["tile_list"] = self.i2_cfg_params.getParam(
            "chain", "list_tile"
        ).split(" ")
        self.control_var["check_inputs"] = self.i2_cfg_params.getParam(
            "chain", "check_inputs"
        )
        self.control_var["Sentinel1"] = self.i2_cfg_params.getParam("chain", "s1_path")
        self.control_var["shapeRegion"] = self.i2_cfg_params.getParam(
            "chain", "region_path"
        )
        self.control_var["classif_mode"] = self.i2_cfg_params.getParam(
            "arg_classification", "classif_mode"
        )
        self.control_var["sample_management"] = self.i2_cfg_params.getParam(
            "arg_train", "sample_management"
        )
        self.control_var["sample_augmentation"] = dict(
            self.i2_cfg_params.getParam("arg_train", "sample_augmentation")
        )
        self.control_var["sample_augmentation_flag"] = self.control_var[
            "sample_augmentation"
        ]["activate"]
        self.control_var["dimred"] = self.i2_cfg_params.getParam("dim_red", "dim_red")
        self.control_var["classifier"] = self.i2_cfg_params.getParam(
            "arg_train", "classifier"
        )
        self.control_var["ds_sar_opt"] = self.i2_cfg_params.getParam(
            "arg_train", "dempster_shafer_sar_opt_fusion"
        )
        self.control_var["keep_runs_results"] = self.i2_cfg_params.getParam(
            "arg_classification", "keep_runs_results"
        )
        self.control_var["merge_final_classifications"] = self.i2_cfg_params.getParam(
            "arg_classification", "merge_final_classifications"
        )
        self.control_var["ground_truth"] = self.i2_cfg_params.getParam(
            "chain", "ground_truth"
        )
        self.control_var["runs"] = self.i2_cfg_params.getParam("arg_train", "runs")
        self.control_var["data_field"] = self.i2_cfg_params.getParam(
            "chain", "data_field"
        )
        self.control_var["outStat"] = self.i2_cfg_params.getParam(
            "chain", "output_statistics"
        )
        self.control_var["VHR"] = self.i2_cfg_params.getParam(
            "coregistration", "vhr_path"
        )
        self.control_var["gridsize"] = self.i2_cfg_params.getParam(
            "simplification", "gridsize"
        )
        self.control_var["umc1"] = self.i2_cfg_params.getParam("simplification", "umc1")
        self.control_var["umc2"] = self.i2_cfg_params.getParam("simplification", "umc2")
        self.control_var["rssize"] = self.i2_cfg_params.getParam(
            "simplification", "rssize"
        )
        self.control_var["inland"] = self.i2_cfg_params.getParam(
            "simplification", "inland"
        )
        self.control_var["iota2_outputs_dir"] = self.i2_cfg_params.getParam(
            "chain", "output_path"
        )
        self.control_var["use_scikitlearn"] = (
            self.i2_cfg_params.getParam("scikit_models_parameters", "model_type")
            is not None
        )
        self.control_var["nomenclature"] = self.i2_cfg_params.getParam(
            "simplification", "nomenclature"
        )
        if self.i2_cfg_params.getParam("obia", "obia_segmentation_path") is None:
            self.control_var["provided_segmentation"] = False
        else:
            self.control_var["provided_segmentation"] = True
        self.control_var["enable_external_features"] = self.i2_cfg_params.getParam(
            "external_features", "external_features_flag"
        )
        self.control_var["remove_output_path"] = self.i2_cfg_params.getParam(
            "chain", "remove_output_path"
        )

    def init_step_group(
        self, pre_processing_steps: StepContainer, s_container: StepContainer
    ):
        """
        Initialize initialisation steps
        """
        from iota2.steps import (
            check_inputs_step,
            common_masks,
            compute_intersection_seg_regions,
            pixel_validity,
            prepare_obia_seg,
            sensors_preprocess,
            slic_segmentation,
        )

        if self.control_var["check_inputs"]:
            pre_processing_steps.append(
                partial(
                    check_inputs_step.CheckInputsClassifWorkflow,
                    self.cfg,
                    self.config_resources,
                ),
                "init",
            )
        pre_processing_steps.append(
            partial(
                sensors_preprocess.SensorsPreprocess,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "init",
        )
        s_container.append(
            partial(
                common_masks.CommonMasks,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "init",
        )
        s_container.append(
            partial(
                pixel_validity.PixelValidity,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "init",
        )
        if not self.control_var["provided_segmentation"]:
            s_container.append(
                partial(
                    slic_segmentation.SlicSegmentation,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "init",
            )
        s_container.append(
            partial(
                prepare_obia_seg.PrepareObiaSeg,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "init",
        )
        s_container.append(
            partial(
                compute_intersection_seg_regions.ComputeIntersectionSegRegions,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "init",
        )

    def sampling_step_group(
        self, first_container: StepContainer, second_container: StepContainer
    ):
        """
        Declare each step for samples management
        """
        from iota2.steps import (
            envelope,
            gen_region_vector,
            intersect_seg_learn,
            learning_zonal_statistics,
            samples_merge,
            split_samples,
            vector_formatting,
        )

        first_container.append(
            partial(
                envelope.Envelope,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "sampling",
        )
        if not self.control_var["shapeRegion"]:
            first_container.append(
                partial(
                    gen_region_vector.GenRegionVector,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "sampling",
            )
        first_container.append(
            partial(
                vector_formatting.VectorFormatting,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "sampling",
        )
        huge_models = False
        if (
            self.control_var["shapeRegion"]
            and self.control_var["classif_mode"] == "fusion"
        ):
            huge_models = True
            first_container.append(
                partial(
                    split_samples.SplitSamples,
                    self.cfg,
                    self.config_resources,
                    self.workingDirectory,
                ),
                "sampling",
            )

        second_container.append(
            partial(
                samples_merge.SamplesMerge,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
                huge_models,
            ),
            "sampling",
        )
        second_container.append(
            partial(
                intersect_seg_learn.IntersectSegLearn,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "sampling",
        )
        second_container.append(
            partial(
                learning_zonal_statistics.LearningZonalStatistics,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "sampling",
        )

    def learning_step_group(self, s_container: StepContainer):
        """
        Initialize learning step
        """
        from iota2.steps import learning_obia

        s_container.append(
            partial(
                learning_obia.ObiaLearning,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "learning",
        )

    def classification_step_group(self, s_container: StepContainer):
        """
        Initialize classification steps
        """
        from iota2.steps import classification_obia

        s_container.append(
            partial(
                classification_obia.ObiaClassification,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "classification",
        )

    def validation_steps_group(self, s_container: StepContainer):
        """ """
        from iota2.steps import (
            compute_metrics_obia,
            merge_tiles_obia,
            obia_final_metrics,
        )

        s_container.append(
            partial(
                compute_metrics_obia.ComputeMetricsObia,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "validation",
        )
        s_container.append(
            partial(
                merge_tiles_obia.MergeTilesObia,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "validation",
        )
        s_container.append(
            partial(
                obia_final_metrics.MergeFinalMetrics,
                self.cfg,
                self.config_resources,
                self.workingDirectory,
            ),
            "validation",
        )

    def generate_output_directories(self, first_step_index: int, restart: bool):
        """ """
        i2_output_dir = self.control_var["iota2_outputs_dir"]
        rm_if_exists = self.control_var["remove_output_path"]

        task_status_file = os.path.join(
            i2_output_dir, I2_CONST.i2_tasks_status_filename
        )

        restart = restart and os.path.exists(task_status_file)
        rm_if_exists = rm_if_exists and os.path.exists(i2_output_dir)

        if rm_if_exists and first_step_index != 0 or restart:
            pass
        elif rm_if_exists:
            shutil.rmtree(i2_output_dir)

        if restart:
            pass
        elif (first_step_index == 0 and not restart) or not os.path.exists(
            i2_output_dir
        ):
            generate_directories_obia(
                i2_output_dir,
                self.control_var["merge_final_classifications"],
                self.control_var["tile_list"],
            )
        else:
            pass
