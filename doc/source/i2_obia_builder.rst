i2_obia
#######

.. _i2_obia.Landsat5_old:

Landsat5_old
************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_obia.Landsat5_old.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_obia.Landsat5_old.end_date:
      * - end_date
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - end_date


          .. _i2_obia.Landsat5_old.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_obia.Landsat5_old.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - start_date


          .. _i2_obia.Landsat5_old.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _i2_obia.Landsat5_old.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_obia.Landsat8:

Landsat8
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_obia.Landsat8.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_obia.Landsat8.end_date:
      * - end_date
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - end_date


          .. _i2_obia.Landsat8.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_obia.Landsat8.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - start_date


          .. _i2_obia.Landsat8.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _i2_obia.Landsat8.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_obia.Landsat8_old:

Landsat8_old
************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_obia.Landsat8_old.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_obia.Landsat8_old.end_date:
      * - end_date
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - end_date


          .. _i2_obia.Landsat8_old.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_obia.Landsat8_old.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - start_date


          .. _i2_obia.Landsat8_old.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _i2_obia.Landsat8_old.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_obia.Sentinel_2:

Sentinel_2
**********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_obia.Sentinel_2.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_obia.Sentinel_2.end_date:
      * - end_date
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - end_date


          .. _i2_obia.Sentinel_2.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_obia.Sentinel_2.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - start_date


          .. _i2_obia.Sentinel_2.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _i2_obia.Sentinel_2.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_obia.Sentinel_2_L3A:

Sentinel_2_L3A
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_obia.Sentinel_2_L3A.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_obia.Sentinel_2_L3A.end_date:
      * - end_date
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - end_date


          .. _i2_obia.Sentinel_2_L3A.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_obia.Sentinel_2_L3A.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - start_date


          .. _i2_obia.Sentinel_2_L3A.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _i2_obia.Sentinel_2_L3A.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_obia.Sentinel_2_S2C:

Sentinel_2_S2C
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_obia.Sentinel_2_S2C.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_obia.Sentinel_2_S2C.end_date:
      * - end_date
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - end_date


          .. _i2_obia.Sentinel_2_S2C.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_obia.Sentinel_2_S2C.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - start_date


          .. _i2_obia.Sentinel_2_S2C.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _i2_obia.Sentinel_2_S2C.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_obia.arg_train:

arg_train
*********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_obia.arg_train.classifier:
      * - classifier
        - None
        - otb classification algorithm
        - str
        - False
        - classifier


          .. _i2_obia.arg_train.otb_classifier_options:
      * - :ref:`otb_classifier_options <desc_i2_obia.arg_train.otb_classifier_options>`
        - None
        - OTB option for classifier.If None, the OTB default values are used
        - dict
        - False
        - :ref:`otb_classifier_options <desc_i2_obia.arg_train.otb_classifier_options>`


          .. _i2_obia.arg_train.random_seed:
      * - :ref:`random_seed <desc_i2_obia.arg_train.random_seed>`
        - None
        - Fix the random seed for random split of reference data
        - int
        - False
        - :ref:`random_seed <desc_i2_obia.arg_train.random_seed>`


          .. _i2_obia.arg_train.ratio:
      * - ratio
        - 0.5
        - Should be between 0.0 and 1.0 and represent the proportion of the dataset to include in the train split.
        - float
        - False
        - ratio


          .. _i2_obia.arg_train.runs:
      * - :ref:`runs <desc_i2_obia.arg_train.runs>`
        - 1
        - Number of independant runs processed
        - int
        - False
        - :ref:`runs <desc_i2_obia.arg_train.runs>`


          .. _i2_obia.arg_train.sample_selection:
      * - :ref:`sample_selection <desc_i2_obia.arg_train.sample_selection>`
        - {'sampler': 'random', 'strategy': 'all'}
        - OTB parameters for sampling the validation set
        - dict
        - False
        - :ref:`sample_selection <desc_i2_obia.arg_train.sample_selection>`


          .. _i2_obia.arg_train.sample_validation:
      * - :ref:`sample_validation <desc_i2_obia.arg_train.sample_validation>`
        - {'sampler': 'random', 'strategy': 'all'}
        - OTB parameters for sampling the validation set
        - dict
        - False
        - :ref:`sample_validation <desc_i2_obia.arg_train.sample_validation>`


          .. _i2_obia.arg_train.split_ground_truth:
      * - :ref:`split_ground_truth <desc_i2_obia.arg_train.split_ground_truth>`
        - True
        - Enable the split of reference data
        - bool
        - False
        - :ref:`split_ground_truth <desc_i2_obia.arg_train.split_ground_truth>`


          .. _i2_obia.arg_train.validity_threshold:
      * - validity_threshold
        - 1
        - threshold above which a training pixel is considered valid
        - int
        - False
        - validity_threshold





Notes
=====

.. _desc_i2_obia.arg_train.otb_classifier_options:

:ref:`otb_classifier_options <i2_obia.arg_train.otb_classifier_options>`
------------------------------------------------------------------------
This parameter is a dictionnary which accepts all OTB application parameters. To know the exhaustive parameter list  use `otbcli_TrainVectorClassifier` in a terminal or look at the OTB documentation

.. _desc_i2_obia.arg_train.random_seed:

:ref:`random_seed <i2_obia.arg_train.random_seed>`
--------------------------------------------------
Fix the random seed used for random split of reference data If set, the results must be the same for a given classifier

.. _desc_i2_obia.arg_train.runs:

:ref:`runs <i2_obia.arg_train.runs>`
------------------------------------
Number of independant runs processed. Each run has his own learning samples. Must be an integer greater than 0

.. _desc_i2_obia.arg_train.sample_selection:

:ref:`sample_selection <i2_obia.arg_train.sample_selection>`
------------------------------------------------------------
This field parameters the strategy of polygon sampling. It directly refers to options of OTB’s SampleSelection application.

Example
-------

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'}]
                           }

In the example above, all polygons will be sampled with the 20% ratio. But the polygons which belong to the model 4 will be periodically sampled, instead of the ransom sampling used for other polygons.
Notice than ``per_models`` key contains a list of strategies. Then we can imagine the following :

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'},
                                         {'target_model':'1',
                                          'sampler':'random',
                                          'strategy', 'byclass',
                                          'strategy.byclass.in', '/path/to/myCSV.csv'
                                         }]
                           }
 where the first column of /path/to/myCSV.csv is class label (integer), second one is the required samples number (integer).

.. _desc_i2_obia.arg_train.sample_validation:

:ref:`sample_validation <i2_obia.arg_train.sample_validation>`
--------------------------------------------------------------
This field parameters the strategy of polygon sampling. It directly refers to options of OTB’s SampleSelection application.

Example
-------

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'}]
                           }

In the example above, all polygons will be sampled with the 20% ratio. But the polygons which belong to the model 4 will be periodically sampled, instead of the ransom sampling used for other polygons.
Notice than ``per_models`` key contains a list of strategies. Then we can imagine the following :

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'},
                                         {'target_model':'1',
                                          'sampler':'random',
                                          'strategy', 'byclass',
                                          'strategy.byclass.in', '/path/to/myCSV.csv'
                                         }]
                           }
 where the first column of /path/to/myCSV.csv is class label (integer), second one is the required samples number (integer).

.. _desc_i2_obia.arg_train.split_ground_truth:

:ref:`split_ground_truth <i2_obia.arg_train.split_ground_truth>`
----------------------------------------------------------------
If set to False, the chain use all polygons for both training and validation

.. _i2_obia.builders:

builders
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_obia.builders.builders_class_name:
      * - :ref:`builders_class_name <desc_i2_obia.builders.builders_class_name>`
        - ['i2_classification']
        - The name of the class defining the builder
        - list
        - False
        - :ref:`builders_class_name <desc_i2_obia.builders.builders_class_name>`


          .. _i2_obia.builders.builders_paths:
      * - :ref:`builders_paths <desc_i2_obia.builders.builders_paths>`
        - /path/to/iota2/sources
        - The path to user builders
        - str
        - False
        - :ref:`builders_paths <desc_i2_obia.builders.builders_paths>`





Notes
=====

.. _desc_i2_obia.builders.builders_class_name:

:ref:`builders_class_name <i2_obia.builders.builders_class_name>`
-----------------------------------------------------------------
Available builders are : 'i2_classification', 'i2_features_map', 'i2_obia' and 'i2_vectorization'

.. _desc_i2_obia.builders.builders_paths:

:ref:`builders_paths <i2_obia.builders.builders_paths>`
-------------------------------------------------------
If not indicated, the iota2 source directory is used: */iota2/sequence_builders/

.. _i2_obia.chain:

chain
*****

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_obia.chain.check_inputs:
      * - :ref:`check_inputs <desc_i2_obia.chain.check_inputs>`
        - True
        - Enable the inputs verification
        - bool
        - False
        - :ref:`check_inputs <desc_i2_obia.chain.check_inputs>`


          .. _i2_obia.chain.cloud_threshold:
      * - :ref:`cloud_threshold <desc_i2_obia.chain.cloud_threshold>`
        - 0
        - Threshold to consider that a pixel is valid
        - int
        - False
        - :ref:`cloud_threshold <desc_i2_obia.chain.cloud_threshold>`


          .. _i2_obia.chain.color_table:
      * - color_table
        - None
        - Absolute path to the file that links the classes and their colours
        - str
        - True
        - color_table


          .. _i2_obia.chain.data_field:
      * - data_field
        - None
        - Field name indicating classes labels in `ground_thruth`
        - str
        - True
        - data_field


          .. _i2_obia.chain.first_step:
      * - first_step
        - None
        - The step group name indicating where the chain start
        - str
        - False
        - first_step


          .. _i2_obia.chain.ground_truth:
      * - ground_truth
        - None
        - Absolute path to reference data
        - str
        - True
        - ground_truth


          .. _i2_obia.chain.l5_path_old:
      * - l5_path_old
        - None
        - Absolute path to Landsat-5 images coming from old THEIA format (D*H*)
        - str
        - False
        - l5_path_old


          .. _i2_obia.chain.l8_path:
      * - l8_path
        - None
        - Absolute path to Landsat-8 images comingfrom new tiled THEIA data
        - str
        - False
        - l8_path


          .. _i2_obia.chain.l8_path_old:
      * - l8_path_old
        - None
        - Absolute path to Landsat-8 images coming from old THEIA format (D*H*)
        - str
        - False
        - l8_path_old


          .. _i2_obia.chain.last_step:
      * - last_step
        - None
        - The step group name indicating where the chain ends
        - str
        - False
        - last_step


          .. _i2_obia.chain.list_tile:
      * - list_tile
        - None
        - List of tile to process, separated by space
        - str
        - True
        - list_tile


          .. _i2_obia.chain.logger_level:
      * - logger_level
        - INFO
        - Set the logger level: NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL
        - str
        - False
        - logger_level


          .. _i2_obia.chain.minimum_required_dates:
      * - minimum_required_dates
        - 2
        - required minimum number of available dates for each sensor
        - int
        - False
        - minimum_required_dates


          .. _i2_obia.chain.nomenclature_path:
      * - nomenclature_path
        - None
        - Absolute path to the nomenclature description file
        - str
        - True
        - nomenclature_path


          .. _i2_obia.chain.output_path:
      * - :ref:`output_path <desc_i2_obia.chain.output_path>`
        - None
        - Absolute path to the output directory
        - str
        - True
        - :ref:`output_path <desc_i2_obia.chain.output_path>`


          .. _i2_obia.chain.proj:
      * - proj
        - None
        - The projection wanted. Format EPSG:XXXX is mandatory
        - str
        - True
        - proj


          .. _i2_obia.chain.region_field:
      * - :ref:`region_field <desc_i2_obia.chain.region_field>`
        - region
        - The column name for region indicator in`region_path` file
        - str
        - False
        - :ref:`region_field <desc_i2_obia.chain.region_field>`


          .. _i2_obia.chain.region_path:
      * - region_path
        - None
        - Absolute path to a region vector file
        - str
        - False
        - region_path


          .. _i2_obia.chain.remove_output_path:
      * - :ref:`remove_output_path <desc_i2_obia.chain.remove_output_path>`
        - True
        - Before the launch of iota2, remove the content of `output_path`
        - bool
        - False
        - :ref:`remove_output_path <desc_i2_obia.chain.remove_output_path>`


          .. _i2_obia.chain.s1_path:
      * - s1_path
        - None
        - Absolute path to Sentinel-1 configuration file
        - str
        - False
        - s1_path


          .. _i2_obia.chain.s2_l3a_output_path:
      * - s2_l3a_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_l3a_output_path


          .. _i2_obia.chain.s2_l3a_path:
      * - s2_l3a_path
        - None
        - Absolute path to Sentinel-2 L3A images (THEIA format)
        - str
        - False
        - s2_l3a_path


          .. _i2_obia.chain.s2_output_path:
      * - s2_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_output_path


          .. _i2_obia.chain.s2_path:
      * - s2_path
        - None
        - Absolute path to Sentinel-2 images (THEIA format)
        - str
        - False
        - s2_path


          .. _i2_obia.chain.s2_s2c_output_path:
      * - s2_s2c_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_s2c_output_path


          .. _i2_obia.chain.s2_s2c_path:
      * - s2_s2c_path
        - None
        - Absolute path to Sentinel-2 images (Sen2Cor format)
        - str
        - False
        - s2_s2c_path


          .. _i2_obia.chain.spatial_resolution:
      * - :ref:`spatial_resolution <desc_i2_obia.chain.spatial_resolution>`
        - []
        - Output spatial resolution
        - list or scalar
        - False
        - :ref:`spatial_resolution <desc_i2_obia.chain.spatial_resolution>`


          .. _i2_obia.chain.user_feat_path:
      * - :ref:`user_feat_path <desc_i2_obia.chain.user_feat_path>`
        - None
        - Absolute path to the user's features path
        - str
        - False
        - :ref:`user_feat_path <desc_i2_obia.chain.user_feat_path>`





Notes
=====

.. _desc_i2_obia.chain.check_inputs:

:ref:`check_inputs <i2_obia.chain.check_inputs>`
------------------------------------------------
Enable the inputs verification. It can take a lot of time for large dataset. Check if region intersect reference data for instance

.. _desc_i2_obia.chain.cloud_threshold:

:ref:`cloud_threshold <i2_obia.chain.cloud_threshold>`
------------------------------------------------------
Indicates the threshold for a polygon to be used for learning. It use the validity count, which is incremented if a cloud, a cloud shadow or a saturated pixel is detected

.. _desc_i2_obia.chain.output_path:

:ref:`output_path <i2_obia.chain.output_path>`
----------------------------------------------
Absolute path to the output directory.It is recommended to have one directory per run of the chain

.. _desc_i2_obia.chain.region_field:

:ref:`region_field <i2_obia.chain.region_field>`
------------------------------------------------
This column in the database must contains string which can be converted into integers. For instance '1_2' does not match this condition

.. _desc_i2_obia.chain.remove_output_path:

:ref:`remove_output_path <i2_obia.chain.remove_output_path>`
------------------------------------------------------------
Before the launch of iota2, remove the content of `output_path`. Only if the `first_step` is `init` and the folder name is valid 

.. _desc_i2_obia.chain.spatial_resolution:

:ref:`spatial_resolution <i2_obia.chain.spatial_resolution>`
------------------------------------------------------------
The spatial resolution expected.It can be provided as integer or float,or as a list containing two values for non squared resolution

.. _desc_i2_obia.chain.user_feat_path:

:ref:`user_feat_path <i2_obia.chain.user_feat_path>`
----------------------------------------------------
Absolute path to the user's features path. They must be stored by tiles

.. _i2_obia.obia:

obia
****

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_obia.obia.buffer_size:
      * - :ref:`buffer_size <desc_i2_obia.obia.buffer_size>`
        - None
        - define the working size batch in number of pixels
        - int
        - False
        - :ref:`buffer_size <desc_i2_obia.obia.buffer_size>`


          .. _i2_obia.obia.full_learn_segment:
      * - :ref:`full_learn_segment <desc_i2_obia.obia.full_learn_segment>`
        - False
        - enable the use of entire segment for learning
        - bool
        - False
        - :ref:`full_learn_segment <desc_i2_obia.obia.full_learn_segment>`


          .. _i2_obia.obia.obia_segmentation_path:
      * - :ref:`obia_segmentation_path <desc_i2_obia.obia.obia_segmentation_path>`
        - None
        - filename for input segmentation
        - str
        - False
        - :ref:`obia_segmentation_path <desc_i2_obia.obia.obia_segmentation_path>`


          .. _i2_obia.obia.region_priority:
      * - :ref:`region_priority <desc_i2_obia.obia.region_priority>`
        - None
        - define an order for region intersection
        - list
        - False
        - :ref:`region_priority <desc_i2_obia.obia.region_priority>`


          .. _i2_obia.obia.stats_used:
      * - :ref:`stats_used <desc_i2_obia.obia.stats_used>`
        - ['mean']
        - list of stats used for train and classification
        - list
        - False
        - :ref:`stats_used <desc_i2_obia.obia.stats_used>`





Notes
=====

.. _desc_i2_obia.obia.buffer_size:

:ref:`buffer_size <i2_obia.obia.buffer_size>`
---------------------------------------------
this parameter is used to avoid memory issue.In case of a large temporal series,i.e one year of Sentinel2 images a recommended size is 2000.For lower number of date, the buffer size can be increased.If buffer_size is larger than the image size, the whole image will be processed in one time.

.. _desc_i2_obia.obia.full_learn_segment:

:ref:`full_learn_segment <i2_obia.obia.full_learn_segment>`
-----------------------------------------------------------
if True: keep each segment which intersect the learning samples. If False, the segments are clipped with learning polygon shape

.. _desc_i2_obia.obia.obia_segmentation_path:

:ref:`obia_segmentation_path <i2_obia.obia.obia_segmentation_path>`
-------------------------------------------------------------------
If parameter is None then a segmentation for each tile is processed using SLIC algorithm

.. _desc_i2_obia.obia.region_priority:

:ref:`region_priority <i2_obia.obia.region_priority>`
-----------------------------------------------------
if a list is provided, the list order is used instead of the numeric order.This option can be used in case of very unbalanced region size.

.. _desc_i2_obia.obia.stats_used:

:ref:`stats_used <i2_obia.obia.stats_used>`
-------------------------------------------
this list accepts only five values: mean, count, min, max, std
The choice of statistics used should be considered in relation to the number of dates used.Because of the constraints on vector formats, one must think about the number of features this creates: nb_stats_choosen * nb_bands * nb_dates. Too many spectral bands can cause an error in the execution of the string.

.. _i2_obia.sensors_data_interpolation:

sensors_data_interpolation
**************************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_obia.sensors_data_interpolation.auto_date:
      * - :ref:`auto_date <desc_i2_obia.sensors_data_interpolation.auto_date>`
        - True
        - Enable the use of `start_date` and `end_date`
        - bool
        - False
        - :ref:`auto_date <desc_i2_obia.sensors_data_interpolation.auto_date>`


          .. _i2_obia.sensors_data_interpolation.use_additional_features:
      * - use_additional_features
        - False
        - enable the use of additional features
        - bool
        - False
        - use_additional_features


          .. _i2_obia.sensors_data_interpolation.use_gapfilling:
      * - use_gapfilling
        - True
        - enable the use of gapfilling (clouds/temporal interpolation)
        - bool
        - False
        - use_gapfilling


          .. _i2_obia.sensors_data_interpolation.write_outputs:
      * - :ref:`write_outputs <desc_i2_obia.sensors_data_interpolation.write_outputs>`
        - False
        - write temporary files
        - bool
        - False
        - :ref:`write_outputs <desc_i2_obia.sensors_data_interpolation.write_outputs>`





Notes
=====

.. _desc_i2_obia.sensors_data_interpolation.auto_date:

:ref:`auto_date <i2_obia.sensors_data_interpolation.auto_date>`
---------------------------------------------------------------
If True, iota2 will automatically guess the first and the last interpolation date. Else, `start_date` and `end_date` of each sensors will be used

.. _desc_i2_obia.sensors_data_interpolation.write_outputs:

:ref:`write_outputs <i2_obia.sensors_data_interpolation.write_outputs>`
-----------------------------------------------------------------------
Write the time series before and after gapfilling, the mask time series, and also the feature time series. This option required a large amount of free disk space.

.. _i2_obia.task_retry_limits:

task_retry_limits
*****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_obia.task_retry_limits.allowed_retry:
      * - allowed_retry
        - 0
        - allow dask to retry a failed job N times
        - int
        - False
        - allowed_retry


          .. _i2_obia.task_retry_limits.maximum_cpu:
      * - :ref:`maximum_cpu <desc_i2_obia.task_retry_limits.maximum_cpu>`
        - 4
        - the maximum number of CPU available
        - int
        - False
        - :ref:`maximum_cpu <desc_i2_obia.task_retry_limits.maximum_cpu>`


          .. _i2_obia.task_retry_limits.maximum_ram:
      * - :ref:`maximum_ram <desc_i2_obia.task_retry_limits.maximum_ram>`
        - 16.0
        - the maximum amount of RAM available. (gB)
        - float
        - False
        - :ref:`maximum_ram <desc_i2_obia.task_retry_limits.maximum_ram>`





Notes
=====

.. _desc_i2_obia.task_retry_limits.maximum_cpu:

:ref:`maximum_cpu <i2_obia.task_retry_limits.maximum_cpu>`
----------------------------------------------------------
the amount of cpu will be doubled if the task is killed due to ram overconsumption until maximum_cpu or allowed_retry are reach

.. _desc_i2_obia.task_retry_limits.maximum_ram:

:ref:`maximum_ram <i2_obia.task_retry_limits.maximum_ram>`
----------------------------------------------------------
the amount of RAM will be doubled if the task is killed due to ram overconsumption until maximum_ram or allowed_retry are reach

.. _i2_obia.userFeat:

userFeat
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_obia.userFeat.arbo:
      * - arbo
        - /*
        - input folder hierarchy
        - str
        - False
        - arbo


          .. _i2_obia.userFeat.patterns:
      * - patterns
        - ALT,ASP,SLP
        - key name for detect the input images
        - str
        - False
        - patterns





