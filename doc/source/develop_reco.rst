Development recommendations
###########################

GIT 
***

iota2 development uses `GIT <https://git-scm.com>`_ as source control tool.

GIT-Flow
========

There are many ways of using GIT. In iota2 ``git-flow`` has been chosen to maintain the project (**without release branches**).
You can find more about it `here <https://jeffkreeftmeijer.com/git-flow/>`_.

To roughly sum-up, for the development of a new feature, the developer **must** :

   1. from ``develop``, create a feature ``branch``
   2. ``commit`` often
   3. ``rebase`` (see below) new features comming from the ``develop`` branch into the feature branch
   4. when the feature is ready, create a ``merge request`` into the develop branch.

Commits must be prefixed with one of the following label :

+-------------+-------------------------------------------+
| Label       | Purpose                                   |
+=============+===========================================+
| BUG FIX     | Fix for runtime crash or incorrect result |
+-------------+-------------------------------------------+
| INSTALL     | Installation                              |
+-------------+-------------------------------------------+
| DOC         | Documentation update                      |
+-------------+-------------------------------------------+
| ENH         | Enhancement of an algorithm               |
+-------------+-------------------------------------------+
| NEW FEATURE | New functionality                         |
+-------------+-------------------------------------------+
| PERF        | Performance improvement                   |
+-------------+-------------------------------------------+
| STYLE       | No logic impact (indentation, comments)   |
+-------------+-------------------------------------------+
| WIP         | Work In Progress not ready for merge      |
+-------------+-------------------------------------------+

Branch naming conventions
=========================

+-----------------------+-------------------+------------------------------------------+
| branch purpose        | naming convention | Example                                  |
+=======================+===================+==========================================+
| develop a new feature | feature-*         | feature-Classsifications_fusion          |
+-----------------------+-------------------+------------------------------------------+
| respond to an issue   | issue-*           | issue-#53_Improve_Classifications        |
+-----------------------+-------------------+------------------------------------------+
| fix a bug             | fix-*             | fix-#27_Bug                              |
+-----------------------+-------------------+------------------------------------------+

the ``#27`` in *fix-#27_Bug* should refer to an issue reported on `iota2 GIT repository <https://framagit.org/inglada/iota2/issues>`_.

Rebase your branch with develop
===============================

To have a clear git tree a good practice is to use `rebase` instead of `merge`. This way all the work on the branch will appear after the last develop commit.

It is important to understand that, for a simple rebase experience, you should avoid using standard merge into the feature branch. Once you have used `merge`, a rebase operation may fail.

The standard procedure is therefore:

1. git checkout develop
2. git branch feature-branch
3. git checkout feature-branch
4. do your work and commit often
5. git rebase develop

Here is a procedure to rebase a branch using emacs and `magit <https://github.com/magit/magit>`_.
Develop and custom branch are supposed to be clean and ready to merge (pulled, all changes commited...).
Emacs is opened on the custom branch to rebase.

.. code-block:: console

	# run magit
    M-x magit
    # enable rebase
	r
	# and choose elsewhere
	e
	# choose the branch to rebase with
	develop
	# then rebase start

If there is no conflict the rebase is done automatically and your recent commits should look like:

.. code-block:: console

    e30ee6eb custom_branch WIP: start to write rebase baseline
	...
    74ef6ea5 develop origin/develop Merge branch 'solve_test_error' into 'develop'

If the custom branch is already pushed to the online repo, you must use `force-push` as you rewrite the history of the branch.

In case of conflict
-------------------

If a file is modified both in develop and in the custom branch, that probably leads to a conflict.
In this case, the conflicting file(s) appear in the `Unstaged files` section in magit.
By pressing `e` on a file, an ediff cession opens.
Using `n` to move forward all conflicting lines, you can choose a variant by typing `a` or `b`. You can also manually solve the conflict by writing in the ediff buffer. Then quit ediff with `q` and save files as asked.

.. warning:: Never commit a file until the end of rebase
			 
Now you can restart the rebase operation with `r` and `r` to continue.

Once ended, your commit history will be clean, and all develop commits will appear aftre the custom branch commits.


.. note::

   In order to avoid conflicts and to lighten the merge operations, it is important to make many small commits. It is then possible, once the rebase with develop is finished, to clean up the history of the branch by merging the commits and submit the merge request for integration in develop.


FRAMAGIT
********

iota2 is hosted on `FramaGit <https://framagit.org/inglada/iota2>`_ .
Anybody can create an account for **free** and submit merge requests. To monitor the project, 
`issues <https://framagit.org/inglada/iota2/issues>`_ are mainly used. Developments are often started by a 
reported issue describing a bug, a new interesting feature or a research idea.

Issues
======

If a developer wants to contribute to iota2, here is the recommended workflow:

   1. If an issue about the new feature does not exist, create a dedicated one.
   2. Assign the issue to herself.
   3. When the contribution is done, close the issue.

Theses simple rules will avoid duplicate work between developers.

.. Note::
    The Board view is very useful to see which features are in development, need to be developed, or are in the backlog.

.. figure:: ./Images/board_view.jpg
    :scale: 50 %
    :align: center
    :alt: Boad view
    
    Board view

CODE 
****

.. Warning::

    Developers must use the pre-commit hook. It can be setup by the script enable_hooks.sh, which just makes a symlink into the ``.git/hooks`` directory. The pre-commit hook will format the code with ``yapf`` and run ``pylint`` and the pre-push hook will launch unit tests.

doc-string
==========

Every usefull functions must constains a doc-string.

It's format must be in NumPy style, but using python `typing <https://docs.python.org/3.6/library/typing.html>`_ in function signature.

To this end, ``Returns`` blocks must not be used. If information about return type is provided, prefer use ``Notes`` blocks.

.. code-block:: python

    def func(arg1: int, arg2: str):
        """Summary line.

        Extended description of function.

        Parameters
        ----------
        arg1 : 
            Description of arg1
        arg2 : 
            Description of arg2

        Notes
        -----
            Description of return value:
			This function always return True, which is a boolean

        """
        return True

Citing code
===========

Source code
-----------

To reference source code in documentation you must use sphinx :ref:`API <api-main-page>` link.

The following rst code: 

.. code-block::
   
   # First declare the current module with
   .. currentmodule:: iota2.Common.customNumpyFeatures

   Then create the link to a class with :class:`data_container`
   or a function :meth:`compute_custom_features`

Produce the following documentation:

.. currentmodule:: iota2.Common.customNumpyFeatures

Then create the link to a class with :class:`data_container`
or a function :meth:`compute_custom_features`

Example code
------------

Creating and citing simple examples could be useful to explain algorithmic choices or function use.
For this kind of citing, put the source file in `iota2/doc/source/examples`.

then you can cite the code:

.. code-block::

   Cite an entire class
   
   .. literalinclude:: examples/example.py
	  :pyobject: example_class

   an entire function
   
   .. literalinclude:: examples/example.py
	  :pyobject: example_method

   or text between two lines
   
   .. literalinclude:: examples/example.py
	  :lines: 30-44

Produce the following documentation

Cite an entire class

.. literalinclude:: examples/example.py
   :pyobject: example_class

an entire function

.. literalinclude:: examples/example.py
   :pyobject: example_method

or text between two lines

.. literalinclude:: examples/example.py
   :lines: 30-44


TESTS
*****

Unittest
========

iota2 is mainly developed in Python, and the `Unittest <https://docs.python.org/2.7/library/unittest.html>`_ library has been chosen
to implement our unit test framework. It is ``highly recommended`` adding tests to each new feature.
Currently, unit tests are placed in the ``/iota2/scripts/Tests/UnitTests`` directory.

Baselines
=========

A set of baseline data is already present in ``/iota2/data/references``. Please use them as references to your tests.
If new baseline must be created, add it in the directory previously quoted after discussion and consensus among the development team.

.. Warning::
    Baselines must be as small as possible.
