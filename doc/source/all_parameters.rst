All configuration parameters
############################

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Name

      * - a_crop_label_replacement
        - ['10', 'annual_crop']
        - Replace a label by a string, ie ['10', 'annual_crop']
        - list
        - a_crop_label_replacement

      * - acor_feat
        - False
        - Apply atmospherically corrected features
        - bool
        - acor_feat

      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - additional_features

      * - allowed_retry
        - 0
        - allow dask to retry a failed job N times
        - int
        - allowed_retry

      * - angle
        - True
        - if True, smoothing corners of pixels (45°)
        - bool
        - angle

      * - annual_classes_extraction_source
        - None
        - 
        - str
        - annual_classes_extraction_source

      * - annual_crop
        - ['11', '12']
        - The list of classes to be replaced by previous data
        - list
        - annual_crop

      * - arbo
        - /*
        - input folder hierarchy
        - str
        - arbo

      * - auto_date
        - True
        - Enable the use of `start_date` and `end_date`
        - bool
        - auto_date

      * - autocontext_iterations
        - 3
        - Number of iterations in auto-context
        - int
        - autocontext_iterations

      * - band_ref
        - 1
        - Date `YYYYMMDD` of the reference image
        - int
        - band_ref

      * - band_src
        - 3
        - Number of the band of the VHR image to use for coregistration
        - int
        - band_src

      * - bingdal
        - None
        - path to GDAL binaries
        - str
        - bingdal

      * - blocksize
        - 2000
        - block size to split raster to prevent Numpy memory error
        - str
        - blocksize

      * - boundary_comparison_mode
        - False
        - Enable classification comparison
        - bool
        - boundary_comparison_mode

      * - boundary_exterior_buffer_size
        - 0
        - Buffer size outside the region
        - int
        - boundary_exterior_buffer_size

      * - boundary_fusion_epsilon
        - 0.0
        - Threshold to avoid weights equals to zero
        - float
        - boundary_fusion_epsilon

      * - boundary_interior_buffer_size
        - 0
        - Buffer size inside the region
        - int
        - boundary_interior_buffer_size

      * - buffer_size
        - None
        - define the working size batch in number of pixels
        - int
        - buffer_size

      * - builders_class_name
        - ['i2_classification']
        - The name of the class defining the builder
        - list
        - builders_class_name

      * - builders_paths
        - /path/to/iota2/sources
        - The path to user builders
        - str
        - builders_paths

      * - check_inputs
        - True
        - Enable the inputs verification
        - bool
        - check_inputs

      * - chunk
        - 10
        - number of chunks for statistics computing
        - int
        - chunk

      * - chunk_size_mode
        - split_number
        - The chunk split mode, currently the choice is 'split_number'
        - str
        - chunk_size_mode

      * - chunk_size_x
        - 50
        - number of cols for one chunk
        - int
        - chunk_size_x

      * - chunk_size_y
        - 50
        - number of rows for one chunk
        - int
        - chunk_size_y

      * - classif_mode
        - separate
        - 'separate' or 'fusion'
        - str
        - classif_mode

      * - classification
        - None
        - Input raster of classification
        - str
        - classification

      * - classifier
        - None
        - otb classification algorithm
        - str
        - classifier

      * - clipfield
        - None
        - field to identify distinct areas
        - str
        - clipfield

      * - clipfile
        - None
        - vector-based file to clip output vector-based classification
        - str
        - clipfile

      * - clipvalue
        - None
        - value of field which identify distinct areas
        - int
        - clipvalue

      * - cloud_threshold
        - 0
        - Threshold to consider that a pixel is valid
        - int
        - cloud_threshold

      * - color_table
        - None
        - Absolute path to the file that links the classes and their colours
        - str
        - color_table

      * - concat_mode
        - True
        - enable the use of all features
        - bool
        - concat_mode

      * - confidence
        - None
        - Input raster of confidence
        - str
        - confidence

      * - copy_input
        - True
        - use spectral bands as features
        - bool
        - copy_input

      * - crop_mix
        - False
        - Enable crop mix workflow
        - bool
        - crop_mix

      * - cross_validation_folds
        - 5
        - the number of k-folds
        - int
        - cross_validation_folds

      * - cross_validation_grouped
        - False
        - 
        - bool
        - cross_validation_grouped

      * - cross_validation_parameters
        - {}
        - 
        - dict
        - cross_validation_parameters

      * - data_field
        - None
        - Field name indicating classes labels in `ground_thruth`
        - str
        - data_field

      * - data_mode_access
        - gapfilled
        - choose which data can be accessed in custom features
        - str
        - data_mode_access

      * - date_src
        - None
        - Date `YYYYMMDD` of the reference image
        - str
        - date_src

      * - date_vhr
        - None
        - Date `YYYYMMDD` of the VHR image
        - str
        - date_vhr

      * - deep_learning_parameters
        - {}
        - deep learning parameter description is available :doc:`here <deep_learning>`
        - dict
        - deep_learning_parameters

      * - dempster_shafer_sar_opt_fusion
        - False
        - Enable the use of both SAR and optical data to train a model.
        - bool
        - dempster_shafer_sar_opt_fusion

      * - dempstershafer_mob
        - precision
        - Choose the dempster shafer mass of belief estimation method
        - str
        - dempstershafer_mob

      * - dim_red
        - False
        - Enable the dimensionality reduction mode
        - bool
        - dim_red

      * - douglas
        - 10
        - Douglas-Peucker tolerance for vector-based generalization
        - int
        - douglas

      * - dozip
        - True
        - Zip output vector-based classification (OSO-like production)
        - bool
        - dozip

      * - enable_autocontext
        - False
        - Enable the auto-context processing
        - bool
        - enable_autocontext

      * - enable_boundary_fusion
        - False
        - Enable the boundary fusion
        - bool
        - enable_boundary_fusion

      * - enable_probability_map
        - False
        - Produce the probability map
        - bool
        - enable_probability_map

      * - end_date
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - end_date

      * - exogeneous_data
        - None
        - Path to a Geotiff file containing additional data to be used in external features
        - str
        - exogeneous_data

      * - external_features_flag
        - False
        - enable the external features mode
        - bool
        - external_features_flag

      * - extract_bands
        - False
        - 
        - bool
        - extract_bands

      * - features
        - ['NDVI', 'NDWI', 'Brightness']
        - List of additional features computed
        - list
        - features

      * - features_from_raw_dates
        - False
        - learn model from raw sensor's date (no interpolations)
        - bool
        - features_from_raw_dates

      * - fill_missing_dates
        - False
        - fill raw data with no data if dates are missing
        - bool
        - fill_missing_dates

      * - first_step
        - None
        - The step group name indicating where the chain start
        - str
        - first_step

      * - force_standard_labels
        - False
        - Standardize labels for feature extraction
        - bool
        - force_standard_labels

      * - full_learn_segment
        - False
        - enable the use of entire segment for learning
        - bool
        - full_learn_segment

      * - functions
        - None
        - function list to be used to compute features
        - str/list
        - functions

      * - fusion_options
        -  -nodatalabel 0 -method majorityvoting
        - OTB FusionOfClassification options for voting method involved if classif_mode is set to 'fusion'
        - str
        - fusion_options

      * - fusionofclassification_all_samples_validation
        - False
        - Enable the use of all reference data to validate the classification merge
        - bool
        - fusionofclassification_all_samples_validation

      * - generate_final_probability_map
        - False
        - Enable the mosaicing of probabilities maps.
        - bool
        - generate_final_probability_map

      * - grasslib
        - None
        - path to grasslib
        - str
        - grasslib

      * - gridsize
        - None
        - number of lines and columns of the serialization process
        - int
        - gridsize

      * - ground_truth
        - None
        - Absolute path to reference data
        - str
        - ground_truth

      * - hermite
        - 10
        - Hermite Interpolation threshold for vector-based smoothing
        - int
        - hermite

      * - higher_stats
        - False
        - If True, compute more complexe statistics (Shanon, majority order and difference, etc.)
        - bool
        - higher_stats

      * - inland
        - None
        - inland water limit shapefile
        - str
        - inland

      * - iterate
        - True
        - Minimal number of SIFT points to find to create the new RPC model
        - bool
        - iterate

      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - keep_bands

      * - keep_duplicates
        - True
        - use 'rel_refl' can generate duplicated feature (ie: NDVI), set to False remove these duplicated features
        - bool
        - keep_duplicates

      * - keep_runs_results
        - True
        - 
        - bool
        - keep_runs_results

      * - l5_path_old
        - None
        - Absolute path to Landsat-5 images coming from old THEIA format (D*H*)
        - str
        - l5_path_old

      * - l8_path
        - None
        - Absolute path to Landsat-8 images comingfrom new tiled THEIA data
        - str
        - l8_path

      * - l8_path_old
        - None
        - Absolute path to Landsat-8 images coming from old THEIA format (D*H*)
        - str
        - l8_path_old

      * - last_step
        - None
        - The step group name indicating where the chain ends
        - str
        - last_step

      * - lcfield
        - Class
        - Name of the field to store landcover class in vector-based classification
        - str
        - lcfield

      * - lib64bit
        - None
        - Path of BandMath and Concatenate OTB executables returning 64-bits float pixel values
        - str
        - lib64bit

      * - list_tile
        - None
        - List of tile to process, separated by space
        - str
        - list_tile

      * - logger_level
        - INFO
        - Set the logger level: NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL
        - str
        - logger_level

      * - max_nn_inference_size
        - None
        - maximum batch inference size
        - int
        - max_nn_inference_size

      * - maximum_cpu
        - 4
        - the maximum number of CPU available
        - int
        - maximum_cpu

      * - maximum_ram
        - 16.0
        - the maximum amount of RAM available. (gB)
        - float
        - maximum_ram

      * - merge_final_classifications
        - False
        - Enable the fusion of classifications mode, merging all run in a unique result.
        - bool
        - merge_final_classifications

      * - merge_final_classifications_method
        - majorityvoting
        - Indicate the fusion of classification method: 'majorityvoting' or 'dempstershafer'
        - str
        - merge_final_classifications_method

      * - merge_final_classifications_ratio
        - 0.1
        - Percentage of samples to use in order to evaluate the fusion raster
        - float
        - merge_final_classifications_ratio

      * - merge_final_classifications_undecidedlabel
        - 255
        - Indicate the label for undecision case during fusion
        - int
        - merge_final_classifications_undecidedlabel

      * - minimum_required_dates
        - 2
        - required minimum number of available dates for each sensor
        - int
        - minimum_required_dates

      * - minsiftpoints
        - 40
        - Minimal number of SIFT points to find to create the new RPC model
        - int
        - minsiftpoints

      * - minstep
        - 16
        - Minimal size of steps between bins in pixels
        - int
        - minstep

      * - mmu
        - 1000
        - MMU of output vector-based classification (projection unit),(Default : 0.1 ha)
        - int
        - mmu

      * - mode
        - 2
        - Coregistration mode of the time series
        - int
        - mode

      * - mode_outside_regionsplit
        - 0.1
        - Set the threshold for split huge model
        - float
        - mode_outside_regionsplit

      * - model_type
        - None
        - machine learning algorthm’s name
        - str
        - model_type

      * - module
        - /path/to/iota2/sources
        - absolute path for user source code
        - str
        - module

      * - no_data_value
        - -10000
        - value considered as no_data in features map mosaic ('i2_features_map' builder name)
        - int
        - no_data_value

      * - no_label_management
        - maxConfidence
        - Method for choosing a label in case of fusion
        - str
        - no_label_management

      * - nomenclature
        - None
        - configuration file which describe nomenclature
        - configuration file which describe nomenclature
        - nomenclature

      * - nomenclature_path
        - None
        - Absolute path to the nomenclature description file
        - str
        - nomenclature_path

      * - number_of_chunks
        - 50
        - the expected number of chunks
        - int
        - number_of_chunks

      * - obia_segmentation_path
        - None
        - filename for input segmentation
        - str
        - obia_segmentation_path

      * - otb_classifier_options
        - None
        - OTB option for classifier.If None, the OTB default values are used
        - dict
        - otb_classifier_options

      * - outprefix
        - dept
        - Prefix to use for naming of vector-based classifications
        - str
        - outprefix

      * - output_name
        - None
        - temporary chunks are written using this name as prefix
        - str
        - output_name

      * - output_path
        - None
        - Absolute path to the output directory
        - str
        - output_path

      * - output_prev_features
        - None
        - Path to previous features for crop mix
        - str
        - output_prev_features

      * - output_statistics
        - True
        - output_statistics
        - bool
        - output_statistics

      * - padding_size_x
        - 0
        - The padding for chunk
        - int
        - padding_size_x

      * - padding_size_y
        - 0
        - The padding for chunk
        - int
        - padding_size_y

      * - pattern
        - None
        - Pattern of the time series files to coregister
        - str
        - pattern

      * - patterns
        - ALT,ASP,SLP
        - key name for detect the input images
        - str
        - patterns

      * - prec
        - 3
        - Estimated shift between source and reference raster in pixel (source raster resolution)
        - int
        - prec

      * - prev_features
        - None
        - Path to a configuration file used to produce previous features
        - str
        - prev_features

      * - prod
        - None
        - OSO-like output vector (aliases) is produced. Other possible value : carhab
        - str
        - prod

      * - proj
        - None
        - The projection wanted. Format EPSG:XXXX is mandatory
        - str
        - proj

      * - random_seed
        - None
        - Fix the random seed for random split of reference data
        - int
        - random_seed

      * - ratio
        - 0.5
        - Should be between 0.0 and 1.0 and represent the proportion of the dataset to include in the train split.
        - float
        - ratio

      * - reduction_mode
        - global
        - The reduction mode
        - str
        - reduction_mode

      * - region_field
        - region
        - The column name for region indicator in`region_path` file
        - str
        - region_field

      * - region_path
        - None
        - Absolute path to a region vector file
        - str
        - region_path

      * - region_priority
        - None
        - define an order for region intersection
        - list
        - region_priority

      * - rel_refl
        - False
        - compute relative reflectances by the red band
        - bool
        - rel_refl

      * - remove_output_path
        - True
        - Before the launch of iota2, remove the content of `output_path`
        - bool
        - remove_output_path

      * - resample
        - True
        - Resample the reference and the source rasterto the same resolution to find SIFT points
        - bool
        - resample

      * - rssize
        - 20
        - Resampling size of input classification raster (projection unit)
        - int
        - rssize

      * - runs
        - 1
        - Number of independant runs processed
        - int
        - runs

      * - s1_path
        - None
        - Absolute path to Sentinel-1 configuration file
        - str
        - s1_path

      * - s2_l3a_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - s2_l3a_output_path

      * - s2_l3a_path
        - None
        - Absolute path to Sentinel-2 L3A images (THEIA format)
        - str
        - s2_l3a_path

      * - s2_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - s2_output_path

      * - s2_path
        - None
        - Absolute path to Sentinel-2 images (THEIA format)
        - str
        - s2_path

      * - s2_s2c_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - s2_s2c_output_path

      * - s2_s2c_path
        - None
        - Absolute path to Sentinel-2 images (Sen2Cor format)
        - str
        - s2_s2c_path

      * - sample_augmentation
        - {'activate': False}
        - OTB parameters for sample augmentation
        - dict
        - sample_augmentation

      * - sample_management
        - None
        - Absolute path to a CSV file containing samples transfert strategies
        - str
        - sample_management

      * - sample_selection
        - {'sampler': 'random', 'strategy': 'all'}
        - OTB parameters for sampling the validation set
        - dict
        - sample_selection

      * - sample_validation
        - {'sampler': 'random', 'strategy': 'all'}
        - OTB parameters for sampling the validation set
        - dict
        - sample_validation

      * - samples_classif_mix
        - False
        - Enable the second step of crop mix
        - bool
        - samples_classif_mix

      * - sampling_validation
        - False
        - Enable sampling validation
        - bool
        - sampling_validation

      * - seed
        - 1
        - seed of input raster classification
        - int
        - seed

      * - spatial_resolution
        - []
        - Output spatial resolution
        - list or scalar
        - spatial_resolution

      * - split_ground_truth
        - True
        - Enable the split of reference data
        - bool
        - split_ground_truth

      * - standardization
        - False
        - 
        - bool
        - standardization

      * - start_date
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - start_date

      * - stats_used
        - ['mean']
        - list of stats used for train and classification
        - list
        - stats_used

      * - statslist
        - {1: 'rate', 2: 'statsmaj', 3: 'statsmaj'}
        - dictionnary of requested landcover statistics
        - dict
        - statslist

      * - step
        - 256
        - Initial size of steps between bins in pixels
        - int
        - step

      * - systemcall
        - False
        - If True, use yours gdal lib (cf. bingdal)
        - bool
        - systemcall

      * - target_dimension
        - 4
        - The number of dimension required, according to `reduction_mode`
        - int
        - target_dimension

      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - temporal_resolution

      * - umc1
        - None
        - MMU for the first regularization
        - int
        - umc1

      * - umc2
        - None
        - MMU for the second regularization
        - int
        - umc2

      * - use_additional_features
        - False
        - enable the use of additional features
        - bool
        - use_additional_features

      * - use_gapfilling
        - True
        - enable the use of gapfilling (clouds/temporal interpolation)
        - bool
        - use_gapfilling

      * - user_feat_path
        - None
        - Absolute path to the user's features path
        - str
        - user_feat_path

      * - validity
        - None
        - Input raster of validity
        - str
        - validity

      * - validity_threshold
        - 1
        - threshold above which a training pixel is considered valid
        - int
        - validity_threshold

      * - vectorize_fusion_of_classifications
        - False
        - flag to inform iota2 to vectorize the fusion of classifications
        - bool
        - vectorize_fusion_of_classifications

      * - vhr_path
        - None
        - Absolute path to the VHR file
        - str
        - vhr_path

      * - write_outputs
        - False
        - write temporary files
        - bool
        - write_outputs

      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - write_reproject_resampled_input_dates_stack

      * - zonal_vector
        - None
        - vector file to compute zonal statistics of classification
        - str
        - zonal_vector


