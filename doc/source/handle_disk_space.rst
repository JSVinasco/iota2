Disk space issue
################

First run
*********

iota2 requires original image data for the first run as mandatory information is extracted from them.

Like:
* original projection
* original resolution
* ...

Next run
********

iota2 can use already preprocessed data.
This requires that interpolation parameters not changes: same spectral resolution, same projection.
Only the spatial resampled data are stored and required, then the temporal resolution can be changed whithout issues.

After run
*********

As described in the :ref:`output-tree-structure`, ``classif`` directory contains per tile and per region maps which are merged in ``final`` directory. If you do not need these intermediate maps, they can be removed to free up disk space.

Input data
**********

If you used the ``write_reproject_resampled_input_dates_stack`` parameter, iota2 will write them in a ``XXX_FRE_STACK.tif`` file. If you are using these file as input and do not need to change the working resulution, the individual bands (``_FRE_B2.tif``, ``_FRE_B2.tif``...) can be removed to free up disk space.