FAQ
===


Facing the "Too many open files" error
--------------------------------------

iota2 may open a lot of files at the same time to complete a run. With some distribution configuration, the error ``Too many open files`` the solution is to increase the number of files that can be opened at the same time thanks to the command :

.. code-block:: bash

    ulimit -u #inform you about the current limit (ie : 4096)
    ulimit -u 5000 # increase user limit to 5000

More information at : https://linuxhint.com/permanently_set_ulimit_value/

.. Note:: If your are using Sentinel-1 data, you have to fill the parameter :ref:`srtm <s1_srtm_desc>` which is the path that contains DEM files. If you are facing the ``Too many open files``, try to reduce the content of this directory as much as possible.

Warning about parameters when the chain start
---------------------------------------------

These warnings implies that the parameter is detected but not known by iota2.
If you are not using a particular branch, ensure that your parameters are correct by looking at the corresponding builder:

- :doc:`i2_classification <i2_classification_builder>`
- :doc:`i2_obia <i2_obia_builder>`
- :doc:`i2_vectorization <i2_vectorization_builder>`
- :doc:`i2_features_map <i2_features_map_builder>`

  
If all parameters seem correct, that probably means that you are not using the latest iota2 version.
Ensure that the ``.condarc`` file is correctly filled, and try to :doc:`re-install iota2 <HowToGetIOTA2>`.
In last case, open a issue with the chain version, the ``.condarc`` contents and the warning message.
