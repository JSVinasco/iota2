i2_features_map
###############

.. _i2_features_map.Landsat5_old:

Landsat5_old
************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_features_map.Landsat5_old.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_features_map.Landsat5_old.end_date:
      * - end_date
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - end_date


          .. _i2_features_map.Landsat5_old.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_features_map.Landsat5_old.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - start_date


          .. _i2_features_map.Landsat5_old.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _i2_features_map.Landsat5_old.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_features_map.Landsat8:

Landsat8
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_features_map.Landsat8.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_features_map.Landsat8.end_date:
      * - end_date
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - end_date


          .. _i2_features_map.Landsat8.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_features_map.Landsat8.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - start_date


          .. _i2_features_map.Landsat8.temporal_resolution:
      * - temporal_resolution
        - 16
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _i2_features_map.Landsat8.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_features_map.Landsat8_old:

Landsat8_old
************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_features_map.Landsat8_old.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_features_map.Landsat8_old.end_date:
      * - end_date
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - end_date


          .. _i2_features_map.Landsat8_old.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_features_map.Landsat8_old.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - start_date


          .. _i2_features_map.Landsat8_old.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _i2_features_map.Landsat8_old.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_features_map.Sentinel_2:

Sentinel_2
**********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_features_map.Sentinel_2.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_features_map.Sentinel_2.end_date:
      * - end_date
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - end_date


          .. _i2_features_map.Sentinel_2.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_features_map.Sentinel_2.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - start_date


          .. _i2_features_map.Sentinel_2.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _i2_features_map.Sentinel_2.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_features_map.Sentinel_2_L3A:

Sentinel_2_L3A
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_features_map.Sentinel_2_L3A.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_features_map.Sentinel_2_L3A.end_date:
      * - end_date
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - end_date


          .. _i2_features_map.Sentinel_2_L3A.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_features_map.Sentinel_2_L3A.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - start_date


          .. _i2_features_map.Sentinel_2_L3A.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _i2_features_map.Sentinel_2_L3A.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_features_map.Sentinel_2_S2C:

Sentinel_2_S2C
**************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_features_map.Sentinel_2_S2C.additional_features:
      * - additional_features
        - 
        - OTB's bandmath expressions, separated by comma
        - str
        - False
        - additional_features


          .. _i2_features_map.Sentinel_2_S2C.end_date:
      * - end_date
        - 
        - The last date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - end_date


          .. _i2_features_map.Sentinel_2_S2C.keep_bands:
      * - keep_bands
        - ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7']
        - The list of spectral bands used for classification
        - list
        - False
        - keep_bands


          .. _i2_features_map.Sentinel_2_S2C.start_date:
      * - start_date
        - 
        - The first date of interpolated image time series : YYYYMMDD format
        - str
        - False
        - start_date


          .. _i2_features_map.Sentinel_2_S2C.temporal_resolution:
      * - temporal_resolution
        - 10
        - The temporal gap between two interpolations
        - int
        - False
        - temporal_resolution


          .. _i2_features_map.Sentinel_2_S2C.write_reproject_resampled_input_dates_stack:
      * - write_reproject_resampled_input_dates_stack
        - True
        - flag to write of resampled stack image for each date
        - bool
        - False
        - write_reproject_resampled_input_dates_stack





.. _i2_features_map.arg_train:

arg_train
*********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_features_map.arg_train.features:
      * - :ref:`features <desc_i2_features_map.arg_train.features>`
        - ['NDVI', 'NDWI', 'Brightness']
        - List of additional features computed
        - list
        - False
        - :ref:`features <desc_i2_features_map.arg_train.features>`





Notes
=====

.. _desc_i2_features_map.arg_train.features:

:ref:`features <i2_features_map.arg_train.features>`
----------------------------------------------------
This parameter enable the computation of the three indices if available for the sensor used.There is no choice for using only one of them

.. _i2_features_map.builders:

builders
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_features_map.builders.builders_class_name:
      * - :ref:`builders_class_name <desc_i2_features_map.builders.builders_class_name>`
        - ['i2_classification']
        - The name of the class defining the builder
        - list
        - False
        - :ref:`builders_class_name <desc_i2_features_map.builders.builders_class_name>`


          .. _i2_features_map.builders.builders_paths:
      * - :ref:`builders_paths <desc_i2_features_map.builders.builders_paths>`
        - /path/to/iota2/sources
        - The path to user builders
        - str
        - False
        - :ref:`builders_paths <desc_i2_features_map.builders.builders_paths>`





Notes
=====

.. _desc_i2_features_map.builders.builders_class_name:

:ref:`builders_class_name <i2_features_map.builders.builders_class_name>`
-------------------------------------------------------------------------
Available builders are : 'i2_classification', 'i2_features_map', 'i2_obia' and 'i2_vectorization'

.. _desc_i2_features_map.builders.builders_paths:

:ref:`builders_paths <i2_features_map.builders.builders_paths>`
---------------------------------------------------------------
If not indicated, the iota2 source directory is used: */iota2/sequence_builders/

.. _i2_features_map.chain:

chain
*****

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_features_map.chain.first_step:
      * - first_step
        - None
        - The step group name indicating where the chain start
        - str
        - False
        - first_step


          .. _i2_features_map.chain.l5_path_old:
      * - l5_path_old
        - None
        - Absolute path to Landsat-5 images coming from old THEIA format (D*H*)
        - str
        - False
        - l5_path_old


          .. _i2_features_map.chain.l8_path:
      * - l8_path
        - None
        - Absolute path to Landsat-8 images comingfrom new tiled THEIA data
        - str
        - False
        - l8_path


          .. _i2_features_map.chain.l8_path_old:
      * - l8_path_old
        - None
        - Absolute path to Landsat-8 images coming from old THEIA format (D*H*)
        - str
        - False
        - l8_path_old


          .. _i2_features_map.chain.last_step:
      * - last_step
        - None
        - The step group name indicating where the chain ends
        - str
        - False
        - last_step


          .. _i2_features_map.chain.list_tile:
      * - list_tile
        - None
        - List of tile to process, separated by space
        - str
        - True
        - list_tile


          .. _i2_features_map.chain.logger_level:
      * - logger_level
        - INFO
        - Set the logger level: NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL
        - str
        - False
        - logger_level


          .. _i2_features_map.chain.minimum_required_dates:
      * - minimum_required_dates
        - 2
        - required minimum number of available dates for each sensor
        - int
        - False
        - minimum_required_dates


          .. _i2_features_map.chain.output_path:
      * - :ref:`output_path <desc_i2_features_map.chain.output_path>`
        - None
        - Absolute path to the output directory
        - str
        - True
        - :ref:`output_path <desc_i2_features_map.chain.output_path>`


          .. _i2_features_map.chain.proj:
      * - proj
        - None
        - The projection wanted. Format EPSG:XXXX is mandatory
        - str
        - True
        - proj


          .. _i2_features_map.chain.remove_output_path:
      * - :ref:`remove_output_path <desc_i2_features_map.chain.remove_output_path>`
        - True
        - Before the launch of iota2, remove the content of `output_path`
        - bool
        - False
        - :ref:`remove_output_path <desc_i2_features_map.chain.remove_output_path>`


          .. _i2_features_map.chain.s1_path:
      * - s1_path
        - None
        - Absolute path to Sentinel-1 configuration file
        - str
        - False
        - s1_path


          .. _i2_features_map.chain.s2_l3a_output_path:
      * - s2_l3a_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_l3a_output_path


          .. _i2_features_map.chain.s2_l3a_path:
      * - s2_l3a_path
        - None
        - Absolute path to Sentinel-2 L3A images (THEIA format)
        - str
        - False
        - s2_l3a_path


          .. _i2_features_map.chain.s2_output_path:
      * - s2_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_output_path


          .. _i2_features_map.chain.s2_path:
      * - s2_path
        - None
        - Absolute path to Sentinel-2 images (THEIA format)
        - str
        - False
        - s2_path


          .. _i2_features_map.chain.s2_s2c_output_path:
      * - s2_s2c_output_path
        - None
        - Absolute path to store preprocessed data in a dedicated directory
        - str
        - False
        - s2_s2c_output_path


          .. _i2_features_map.chain.s2_s2c_path:
      * - s2_s2c_path
        - None
        - Absolute path to Sentinel-2 images (Sen2Cor format)
        - str
        - False
        - s2_s2c_path


          .. _i2_features_map.chain.spatial_resolution:
      * - :ref:`spatial_resolution <desc_i2_features_map.chain.spatial_resolution>`
        - []
        - Output spatial resolution
        - list or scalar
        - False
        - :ref:`spatial_resolution <desc_i2_features_map.chain.spatial_resolution>`


          .. _i2_features_map.chain.user_feat_path:
      * - :ref:`user_feat_path <desc_i2_features_map.chain.user_feat_path>`
        - None
        - Absolute path to the user's features path
        - str
        - False
        - :ref:`user_feat_path <desc_i2_features_map.chain.user_feat_path>`





Notes
=====

.. _desc_i2_features_map.chain.output_path:

:ref:`output_path <i2_features_map.chain.output_path>`
------------------------------------------------------
Absolute path to the output directory.It is recommended to have one directory per run of the chain

.. _desc_i2_features_map.chain.remove_output_path:

:ref:`remove_output_path <i2_features_map.chain.remove_output_path>`
--------------------------------------------------------------------
Before the launch of iota2, remove the content of `output_path`. Only if the `first_step` is `init` and the folder name is valid 

.. _desc_i2_features_map.chain.spatial_resolution:

:ref:`spatial_resolution <i2_features_map.chain.spatial_resolution>`
--------------------------------------------------------------------
The spatial resolution expected.It can be provided as integer or float,or as a list containing two values for non squared resolution

.. _desc_i2_features_map.chain.user_feat_path:

:ref:`user_feat_path <i2_features_map.chain.user_feat_path>`
------------------------------------------------------------
Absolute path to the user's features path. They must be stored by tiles

.. _i2_features_map.external_features:

external_features
*****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_features_map.external_features.concat_mode:
      * - :ref:`concat_mode <desc_i2_features_map.external_features.concat_mode>`
        - True
        - enable the use of all features
        - bool
        - False
        - :ref:`concat_mode <desc_i2_features_map.external_features.concat_mode>`


          .. _i2_features_map.external_features.exogeneous_data:
      * - :ref:`exogeneous_data <desc_i2_features_map.external_features.exogeneous_data>`
        - None
        - Path to a Geotiff file containing additional data to be used in external features
        - str
        - False
        - :ref:`exogeneous_data <desc_i2_features_map.external_features.exogeneous_data>`


          .. _i2_features_map.external_features.external_features_flag:
      * - external_features_flag
        - False
        - enable the external features mode
        - bool
        - False
        - external_features_flag


          .. _i2_features_map.external_features.functions:
      * - :ref:`functions <desc_i2_features_map.external_features.functions>`
        - None
        - function list to be used to compute features
        - str/list
        - False
        - :ref:`functions <desc_i2_features_map.external_features.functions>`


          .. _i2_features_map.external_features.module:
      * - module
        - /path/to/iota2/sources
        - absolute path for user source code
        - str
        - False
        - module


          .. _i2_features_map.external_features.no_data_value:
      * - no_data_value
        - -10000
        - value considered as no_data in features map mosaic ('i2_features_map' builder name)
        - int
        - False
        - no_data_value


          .. _i2_features_map.external_features.output_name:
      * - output_name
        - None
        - temporary chunks are written using this name as prefix
        - str
        - False
        - output_name





Notes
=====

.. _desc_i2_features_map.external_features.concat_mode:

:ref:`concat_mode <i2_features_map.external_features.concat_mode>`
------------------------------------------------------------------
if disabled, only external features are used in the whole processing

.. _desc_i2_features_map.external_features.exogeneous_data:

:ref:`exogeneous_data <i2_features_map.external_features.exogeneous_data>`
--------------------------------------------------------------------------
If the =exogeneous_data= contains '$TILE', it will be replaced by the tile name being processed.If you want to reproject your data on given tiles, you can use the =split_raster_into_tiles.py= command line tool.

Usage: =split_raster_into_tiles.py --help=.

.. _desc_i2_features_map.external_features.functions:

:ref:`functions <i2_features_map.external_features.functions>`
--------------------------------------------------------------
Can be a string of space-separated function namesCan be a list of either strings of function nameor lists of one function name and one argument mapping

.. _i2_features_map.python_data_managing:

python_data_managing
********************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_features_map.python_data_managing.chunk_size_mode:
      * - chunk_size_mode
        - split_number
        - The chunk split mode, currently the choice is 'split_number'
        - str
        - False
        - chunk_size_mode


          .. _i2_features_map.python_data_managing.chunk_size_x:
      * - chunk_size_x
        - 50
        - number of cols for one chunk
        - int
        - False
        - chunk_size_x


          .. _i2_features_map.python_data_managing.chunk_size_y:
      * - chunk_size_y
        - 50
        - number of rows for one chunk
        - int
        - False
        - chunk_size_y


          .. _i2_features_map.python_data_managing.data_mode_access:
      * - :ref:`data_mode_access <desc_i2_features_map.python_data_managing.data_mode_access>`
        - gapfilled
        - choose which data can be accessed in custom features
        - str
        - False
        - :ref:`data_mode_access <desc_i2_features_map.python_data_managing.data_mode_access>`


          .. _i2_features_map.python_data_managing.fill_missing_dates:
      * - :ref:`fill_missing_dates <desc_i2_features_map.python_data_managing.fill_missing_dates>`
        - False
        - fill raw data with no data if dates are missing
        - bool
        - False
        - :ref:`fill_missing_dates <desc_i2_features_map.python_data_managing.fill_missing_dates>`


          .. _i2_features_map.python_data_managing.max_nn_inference_size:
      * - :ref:`max_nn_inference_size <desc_i2_features_map.python_data_managing.max_nn_inference_size>`
        - None
        - maximum batch inference size
        - int
        - False
        - :ref:`max_nn_inference_size <desc_i2_features_map.python_data_managing.max_nn_inference_size>`


          .. _i2_features_map.python_data_managing.number_of_chunks:
      * - number_of_chunks
        - 50
        - the expected number of chunks
        - int
        - False
        - number_of_chunks


          .. _i2_features_map.python_data_managing.padding_size_x:
      * - padding_size_x
        - 0
        - The padding for chunk
        - int
        - False
        - padding_size_x


          .. _i2_features_map.python_data_managing.padding_size_y:
      * - padding_size_y
        - 0
        - The padding for chunk
        - int
        - False
        - padding_size_y





Notes
=====

.. _desc_i2_features_map.python_data_managing.data_mode_access:

:ref:`data_mode_access <i2_features_map.python_data_managing.data_mode_access>`
-------------------------------------------------------------------------------
Three values are allowed:
- gapfilled: give access only the gapfilled data
- raw: gives access only the original raw data
- both: provides access to both data
..Notes:: Data are spatialy resampled, these parameters concern only temporal interpolation

.. _desc_i2_features_map.python_data_managing.fill_missing_dates:

:ref:`fill_missing_dates <i2_features_map.python_data_managing.fill_missing_dates>`
-----------------------------------------------------------------------------------
If raw data access is enabled, this option considers all unique dates for all tiles and identify which dates are missing for each tile. A missing date is filled using a no data constant value.Cloud or saturation are not corrected, but masks are provided Masks contain three value: 0 for valid data, 1 for cloudy or saturated pixels, 2 for a missing date

.. _desc_i2_features_map.python_data_managing.max_nn_inference_size:

:ref:`max_nn_inference_size <i2_features_map.python_data_managing.max_nn_inference_size>`
-----------------------------------------------------------------------------------------
Involved if a neural network inference is performed. If not set (None), the inference size will be the same as the one used during the learning stage

.. _i2_features_map.sensors_data_interpolation:

sensors_data_interpolation
**************************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_features_map.sensors_data_interpolation.auto_date:
      * - :ref:`auto_date <desc_i2_features_map.sensors_data_interpolation.auto_date>`
        - True
        - Enable the use of `start_date` and `end_date`
        - bool
        - False
        - :ref:`auto_date <desc_i2_features_map.sensors_data_interpolation.auto_date>`


          .. _i2_features_map.sensors_data_interpolation.use_additional_features:
      * - use_additional_features
        - False
        - enable the use of additional features
        - bool
        - False
        - use_additional_features


          .. _i2_features_map.sensors_data_interpolation.use_gapfilling:
      * - use_gapfilling
        - True
        - enable the use of gapfilling (clouds/temporal interpolation)
        - bool
        - False
        - use_gapfilling


          .. _i2_features_map.sensors_data_interpolation.write_outputs:
      * - :ref:`write_outputs <desc_i2_features_map.sensors_data_interpolation.write_outputs>`
        - False
        - write temporary files
        - bool
        - False
        - :ref:`write_outputs <desc_i2_features_map.sensors_data_interpolation.write_outputs>`





Notes
=====

.. _desc_i2_features_map.sensors_data_interpolation.auto_date:

:ref:`auto_date <i2_features_map.sensors_data_interpolation.auto_date>`
-----------------------------------------------------------------------
If True, iota2 will automatically guess the first and the last interpolation date. Else, `start_date` and `end_date` of each sensors will be used

.. _desc_i2_features_map.sensors_data_interpolation.write_outputs:

:ref:`write_outputs <i2_features_map.sensors_data_interpolation.write_outputs>`
-------------------------------------------------------------------------------
Write the time series before and after gapfilling, the mask time series, and also the feature time series. This option required a large amount of free disk space.

.. _i2_features_map.task_retry_limits:

task_retry_limits
*****************

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_features_map.task_retry_limits.allowed_retry:
      * - allowed_retry
        - 0
        - allow dask to retry a failed job N times
        - int
        - False
        - allowed_retry


          .. _i2_features_map.task_retry_limits.maximum_cpu:
      * - :ref:`maximum_cpu <desc_i2_features_map.task_retry_limits.maximum_cpu>`
        - 4
        - the maximum number of CPU available
        - int
        - False
        - :ref:`maximum_cpu <desc_i2_features_map.task_retry_limits.maximum_cpu>`


          .. _i2_features_map.task_retry_limits.maximum_ram:
      * - :ref:`maximum_ram <desc_i2_features_map.task_retry_limits.maximum_ram>`
        - 16.0
        - the maximum amount of RAM available. (gB)
        - float
        - False
        - :ref:`maximum_ram <desc_i2_features_map.task_retry_limits.maximum_ram>`





Notes
=====

.. _desc_i2_features_map.task_retry_limits.maximum_cpu:

:ref:`maximum_cpu <i2_features_map.task_retry_limits.maximum_cpu>`
------------------------------------------------------------------
the amount of cpu will be doubled if the task is killed due to ram overconsumption until maximum_cpu or allowed_retry are reach

.. _desc_i2_features_map.task_retry_limits.maximum_ram:

:ref:`maximum_ram <i2_features_map.task_retry_limits.maximum_ram>`
------------------------------------------------------------------
the amount of RAM will be doubled if the task is killed due to ram overconsumption until maximum_ram or allowed_retry are reach

.. _i2_features_map.userFeat:

userFeat
********

.. list-table::
      :widths: auto
      :header-rows: 1

      * - Name
        - Default Value
        - Description
        - Type
        - Mandatory
        - Name

          .. _i2_features_map.userFeat.arbo:
      * - arbo
        - /*
        - input folder hierarchy
        - str
        - False
        - arbo


          .. _i2_features_map.userFeat.patterns:
      * - patterns
        - ALT,ASP,SLP
        - key name for detect the input images
        - str
        - False
        - patterns





