"""Module containing usable function as external features."""

import numpy as np


def get_cumulative_productivity(self):  # pylint: disable=C0116
    print("cumul")
    coef = np.sum(self.get_interpolated_Sentinel2_NDVI() / 1000.0, axis=2)
    labels = ["cumul_prod"]
    return coef, labels


def get_minimum_productivity(self):  # pylint: disable=C0116
    print("min")
    coef = np.min(self.get_interpolated_Sentinel2_NDVI() / 1000.0, axis=2)
    labels = ["min_prod"]
    return coef, labels


def get_seasonal_variation(self):  # pylint: disable=C0116
    print("var")
    coef = np.std(self.get_interpolated_Sentinel2_NDVI() / 1000.0, axis=2) / (
        np.mean(self.get_interpolated_Sentinel2_NDVI() / 1000.0, axis=2) + 1e-6
    )

    labels = ["var_prod"]
    return coef, labels
