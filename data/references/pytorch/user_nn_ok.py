# !/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Module containing all natively usable pytorch neural networks in iota2."""

from typing import Dict, Optional

import torch
import torch.nn.functional as F
from torch import nn

from iota2.learning.pytorch.torch_nn_bank import Iota2NeuralNetwork


class ANN(Iota2NeuralNetwork):
    """Same as ANN but reshape it's inputs."""

    def __init__(
        self,
        nb_features: int,
        nb_class: int,
        layer: int = 1,
        doy_sensors_dic: Optional[Dict] = None,
        **kwargs
    ):
        super().__init__(nb_features, nb_class, doy_sensors_dic)
        self.nb_class = nb_class
        self.layer = layer
        self.sensors_doy = doy_sensors_dic
        self.fc1 = nn.Linear(in_features=nb_features, out_features=16)
        self.fc2 = nn.Linear(in_features=16, out_features=12)
        self.output = nn.Linear(in_features=12, out_features=nb_class)

    def forward(
        self,
        l5,
        l8,
        l8_old,
        s2_theia,
        s2_s2c,
        s2_l3a,
        s1_desvv,
        s1_desvh,
        s1_ascvv,
        s1_ascvh,
        l5_masks,
        l8_masks,
        l8_old_masks,
        s2_theia_masks,
        s2_s2c_masks,
        s2_l3a_masks,
        s1_desvv_masks,
        s1_desvh_masks,
        s1_ascvv_masks,
        s1_ascvh_masks,
    ):
        # flat x due to flat input nn first layer
        data_val = s2_theia.reshape(-1, s2_theia.shape[1] * s2_theia.shape[-1])
        data_val = F.relu(self.fc1(data_val))
        data_val = F.relu(self.fc2(data_val))
        data_val = torch.softmax(self.output(data_val), dim=1)
        return data_val
